<HTML>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HEAD>
<TITLE>The cnet network simulator - Tracing functions</TITLE>
<META NAME="description" CONTENT="cnet enables experimentation
with various data-link layer, network layer, routing and transport
layer networking protocols.  It has been specifically developed for
undergraduate teaching.">

<META NAME="keywords" CONTENT="network, ethernet, WAN, LAN, WLAN,
ad hoc, adhoc, mobile, wireless, protocol, simulator, simulation,
undergraduate, teaching">
</HEAD>

<BODY BGCOLOR="white" TEXT ="black" LINK="blue" VLINK="purple">

<table>
<tr>
<td valign="top" background="images/jigsaw.jpg">
<table cellpadding="4" cellspacing="0" border="0"><tr>

<td nowrap>
    <font size="+1"><b>cnet v3.1.1</b></font>
    <p>
    <a href="index.html">home</a>
    <br>
    <a href="faq.html">FAQ</a>
    <p>

    <a href="options.html">cmdline options</a>
    <br>
    <a href="topology.html">topology files</a>
    <br>
    <a href="attributes.html">attributes</a>
    <p>

    <a href="datatypes.html">datatypes</a>
    <br>
    <a href="api.html">core API</a>
    <br>
    <a href="supportapi.html">support API</a>
    <p>

    <a href="install.html">download/install</a>
</td>

</td></tr></table>
</td>

<td>
<h3>Tracing <i>cnet</i>'s execution</h3>

The development and debugging of network protocols is a difficult task.
Without the support of a development environment,
the development of even a two-node protocol requires access to both nodes
and, possibly, the physical communication medium.
This is, of course, one of the primary reasons that we use network
simulators, such as <i>cnet</i>.
We have the ability to "peek" inside each network node,
and even the communication links,
to see what is happening and to, hopefully, expose errors in our protocols.

<p>
Although <i>cnet</i> permits protocols to be written in C and
executed natively in a single operating system process,
it does not provide traditional debugging support.
Moreover (unfortunately) because of the way <i>cnet</i> manipulates the
single process's data segments to transparently schedule nodes,
standard debuggers, such as <i>gdb</i>,
can't "keep up" with what is happening and begin reporting incorrect
information.

<p>
<i>cnet</i> aids the development and debugging of network protocols with a
minimum of intrusion.
<i>cnet</i> enables the delivery and execution of events and <i>cnet</i>'s
own functions to be <i>traced</i>.
Each event's commencement and termination, and each function's invocation,
parameters, and return value annotated.
The picture below shows a <i>cnet</i> simulation which was commenced with
the <a href="options.html#option-t">-t</a> option to enable tracing.

<p>
<IMG SRC="images/tracing.jpg">
<p>

The displayed first line of the trace, above, indicates that the
handler for the <tt>EV_APPLICATIONREADY</tt> event
for the node Hobart is being invoked at time <tt>40,134,044usecs</tt>.
Four lines later, execution leaves the same handler.
While executing the handler,
three of <i>cnet</i>'s standard functions were invoked.
<p>

The first of these (line 2) was
<a href="application.html#read_application"><tt>CNET_read_application</tt></a>.
On invocation the first two parameters are addresses
(such as requested with C's &amp; operator) to receive the destination
address and actual contents of the next message.
These addresses are printed as
hexadecimal values because no interpretation can be placed on their
initial contents, nor the memory to which these addresses refer.
In reality, the third parameter is also passed by address,
but we know that it represents the maximum possible length of the second
parameter (to receive the new message), so we are able to report that the
maximum length "provided" on entry to the function was <tt>8192</tt>.
<p>

The return value of <tt>CNET_read_application</tt> was <tt>0</tt>,
indicating success,
and the side-effects of the function,
modifying the destination address and length of the new message are also
reported with the new values of <tt>4</tt> and <tt>1603</tt> respectively.
If an error had been detected,
the trace would indicate a return value of <tt>-1</tt> together with the
new value of the <tt>CnetError</tt> type in cnet_errno</tt>.
<p>

Note that due to <i>cnet</i>'s
<a href="eventdriven.html">event driven</a> programming style,
that events do not interrupt or preempt one another.
The trace stream will always show what is being executed with only one
level of indentation.

<p>
With reference to the this screenshot,
we can see that the next event for Melbourne, at <tt>40,136,855usecs</tt>,
was a frame arriving on the Physical Layer's link 1.
In response to this frame arrival,
Melbourne has written 6843 bytes to its own Application Layer
(probably after removing one or more headers from the just arrived frame),
and has then transmitted a short 24 bytes frame, also on link 1.
Such a short frame, and the use of link 1 again,
suggests that this is an acknowledgment frame being sent to the Data Link
Layer of the node on the other end of this link.
<p>

The tracing of function parameters using only their hexadecimal addresses
is error-prone and confusing.
Moreover, different local variables in different event
handlers will have the same hexadecimal addresses (as they'll be on the
function's runtime stack), leading to more confusion.
Additional <i>cnet</i> API calls may be added to the protocol's code,
such as:

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
CNET_trace_name(&amp;destaddr, "dest");
CNET_trace_name(lastmsg,   "newmessage");
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

to request that the strings
<tt>"dest"</tt> and <tt>"newmessage"</tt>
be printed whenever the addresses of the variables
<tt>destaddr</tt> and <tt>lastmsg</tt> would otherwise be printed in
hexadecimal.
Calls to <tt>CNET_trace_name</tt> should be placed near the entry of each
event handler, and obviously before an address is to be traced.
The <tt>CNET_trace_name</tt> function is not, itself, traced.
<p>

By default, a copy of <i>cnet</i>'s <i>trace stream</i> appears in
the trace-window when <i>cnet</i> is invoked with
the <a href="options.html#option-t">-t</a> option.
Alternatively, tracing may be toggled using the windowing interface,
by selecting a checkbox to change the default or specific node's attributes.
When executing without the Tcl/Tk interface,
the trace stream appears via <i>cnet</i>'s <i>standard error</i> stream.
The complete trace stream may also be mirrored to a named file by setting the
<a href="attributes.html#gattrs">tracefile</a> global attribute. 
<p>

From within the C code of a protocol it is possible to trace only certain
events, possibly for only certain nodes of interest.
For example, a call such as
<tt>CNET_set_trace(TE_APPLICATIONREADY)</tt> will trace just
the <tt>EV_APPLICATIONREADY</tt> event for the invoking node.

The parameter to <tt>CNET_set_trace</tt> is really a bitmap of events of
interest, and so we may add or remove particular events
using C's bitwise operators:
<p>

<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
<b>if</b>(nodeinfo.nodenumber == 1) {

    <b>int</b>   oldmask, newmask;

    oldmask  =  CNET_get_trace();
    newmask  =  oldmask | TE_PHYSICALREADY | TE_TIMER2;

    CNET_set_trace(newmask);
    ......
    ......
    CNET_set_trace(oldmask);
}
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

All required <tt>TE_*</tt> constants are defined in <i>cnet</i>'s header
file, along with the useful <tt>TE_ALLEVENTS</tt> and <tt>TE_NOEVENTS</tt>.
During the execution of an event handler that is being traced,
an arbitrary string may also be displayed with <tt>CNET_trace</tt> function,
which accepts a standard C <i>printf</i> format specification.
<p>

</td></tr></table>


<p>

<table width="100%"><tr>
  <td valign=top>
    <i>cnet</i> was written and is maintained by Chris McDonald
    (<A HREF="mailto:chris@csse.uwa.edu.au">chris@csse.uwa.edu.au</a>).
    <p>
    <center>
	<img src="images/UWAsmall.gif">
    </center>
  </td>

  <td valign=top>
    <i>cnet</i> development has been supported by an ACM-SIGCSE
    <A HREF="http://www.sigcse.org/join/SpecialProjects.shtml">
    Special Project Grant</a>,

    and an Australian <a href="http://www.auc.edu.au/index2.html">
    Apple University Consortium</a>
    <!--
    WWDC'2005
    -->
    Scholarship.
    <p>
    <center>
	<A HREF="http://www.sigcse.org/join/SpecialProjects.shtml">
	<img src="images/sigcse.gif"></a>

	&nbsp;

	<A HREF="http://www.auc.edu.au/index2.html">
	<img src="images/auc.gif"></a>
    </center>
  </td>
</tr></table>


</BODY>
</HTML>
