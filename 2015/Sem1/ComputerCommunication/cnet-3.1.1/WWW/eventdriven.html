<HTML>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HEAD>
<TITLE>The cnet network simulator - event driven programming</TITLE>
<META NAME="description" CONTENT="cnet enables experimentation
with various data-link layer, network layer, routing and transport
layer networking protocols.  It has been specifically developed for
undergraduate teaching.">

<META NAME="keywords" CONTENT="network, ethernet, WAN, LAN, WLAN,
ad hoc, adhoc, mobile, wireless, protocol, simulator, simulation,
undergraduate, teaching">
</HEAD>

<BODY BGCOLOR="white" TEXT ="black" LINK="blue" VLINK="purple">

<table>
<tr>
<td valign="top" background="images/jigsaw.jpg">
<table cellpadding="4" cellspacing="0" border="0"><tr>

<td nowrap>
    <font size="+1"><b>cnet v3.1.1</b></font>
    <p>
    <a href="index.html">home</a>
    <br>
    <a href="faq.html">FAQ</a>
    <p>

    <a href="options.html">cmdline options</a>
    <br>
    <a href="topology.html">topology files</a>
    <br>
    <a href="attributes.html">attributes</a>
    <p>

    <a href="datatypes.html">datatypes</a>
    <br>
    <a href="api.html">core API</a>
    <br>
    <a href="supportapi.html">support API</a>
    <p>

    <a href="install.html">download/install</a>
</td>

</td></tr></table>
</td>

<td>
<h3><a name="eventdriven">The event driven programming style</a></h3>

<i>cnet</i> employs an <i>event-driven</i> style of programming similar,
though not identical,
to the data-link layer protocols presented in A.S. Tanenbaum
[Prentice-Hall,1988,...2003].
Execution proceeds when <i>cnet</i>
informs protocols that an <i>event of interest</i> has occurred.
Protocols are expected to respond to these events.

<p>
Events occur when
a node reboots,
the Application Layer has a message for delivery,
the Physical Layer receives a frame on a link,
a timer event expires,
a debugging button (under Tcl/Tk) is selected,
and a node is (politely) shutdown.
No event is delivered if a node pauses, crashes or suffers a hardware failure.
These last few events only occur in simulations designed to
address the higher layers of the OSI model (say, the Session Layer).

<p>
<dl>
<dt><b>IMPORTANT:</b></dt>
    <dd> <i>cnet</i> employs an <i>event-driven</i> style of
    programming, not an <i>interrupt-driven</i> style.
    In particular,
    while an event handler is executing it will <i>not</i> be interrupted by
    the arrival of another event.
    In support of this,
    there are no facilities to specify non-interruptable, high-priority
    handlers nor to protect data structures with semaphores or monitors
    (you won't need them!).
    </dd>
</dl>

<p>
Event-handling functions must first be registered to receive incoming
events with a call to <tt>CNET_set_handler</tt>.
Event-handling functions will be later called by <i>cnet</i>
with three parameters.
The first is the type of event (the reason the handler is being called),
one of the <tt>CnetEvent</tt> enumerated values.
The second parameter is a unique timer (described later)
and the third,
some user-specified data.

<p>
Each node is initially rebooted by calling its <tt>reboot_node</tt> function.
This is the only function that you must provide and is assumed to have
the name <tt>reboot_node()</tt> unless overridden with either the
<a href="options.html#option-R">-R</a> option
or the <tt>rebootfunc</tt> node attribute.
The purpose of calling <tt>reboot_node</tt> is to give protocols
a chance to allocate any necessary dynamic memory,
initialize variables,
inform <i>cnet</i> in which events protocols are interested,
and which handlers that <i>cnet</i> should call when these events occur.

<p>
    <center><hr width="80%"></center>
<p>

<h3>
<a name="timers">An example of <i>cnet</i>'s event handlers</a>
</h3>
Consider the following protocol skeleton.
Like all <i>cnet</i> protocol files,
the standard <i>cnet</i> header file is first included.
This contains all definitions and function prototypes necessary for protocols.
The <tt>reboot_node</tt> function first informs <i>cnet</i>
that when the Application Layer has a message for delivery to another host
(because <tt>EV_APPLICATIONREADY</tt> occurs)
<i>cnet</i> should call the function <tt>new_message</tt> which
reads the new message from the Application Layer to commence the
delivery process.
The function <tt>frame_arrived</tt> will similarly be called when the
<tt>EV_PHYSICALREADY</tt> event occurs.

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
#include &lt;cnet.h&gt;

<b>void</b> new_message(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    ...
    success = CNET_read_application( ... );
    ...
}

<b>void</b> frame_arrived(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    ...
    success = CNET_read_physical( ... );
    ...
}

<b>void</b> reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    ...
    success = CNET_set_handler(EV_APPLICATIONREADY, new_message, 0);
    success = CNET_set_handler(EV_PHYSICALREADY,    frame_arrived, 0);
    ...
}
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

A user-defined data value must be
initially provided as the third parameter to <tt>CNET_set_handler</tt>.
When the handler's event eventually occurs,
the same value is passed as the third parameter to the handler.
Within more complex protocols,
you will typically want to pass an integer or, with care,
a pointer, via this third parameter.
The user-defined data value for timeout handlers
(for <tt>EV_TIMER0..EV_TIMER9</tt>)
must also be provided as the third parameter to <tt>CNET_start_timer</tt>.
<p>

We can also write the above code as:

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
#include &lt;cnet.h&gt;

EVENT_HANDLER(new_message)
{
    ...
    success = CNET_read_application( ... );
    ...
}

EVENT_HANDLER(frame_arrived)
{
    ...
    success = CNET_read_physical( ... );
    ...
}

EVENT_HANDLER(reboot_node)
{
    ...
    success = CNET_set_handler(EV_APPLICATIONREADY, new_message, 0);
    success = CNET_set_handler(EV_PHYSICALREADY,    frame_arrived, 0);
    ...
}
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>
<tt>EVENT_HANDLER</tt> is simply a C macro
defined in the &lt;cnet.h&gt; header file.
While this style improves both readability and consistency,
it may introduce confusion as to where the three formal parameters,
<tt>ev, timer</tt> and <tt>data</tt>,
actually "come from".
<p>

<dl>
<dt><b>IMPORTANT:</b></dt>
    <dd>Event-handling functions must execute to their completion -
    they must perform their actions and then simply return.
    <i>cnet</i> does not employ pre-emptive scheduling - once an event
    handling function is being executed, it <i>will not</i> be interrupted
    by the arrival of another event.
    Event-handling functions are of type <tt>void</tt> -
    that is, they do not return a value.
    If event-handling functions do not return,
    the whole simulation, including all windowing,
    will block and <i>cnet</i> must be interrupted
    via the invoking <i>xterm</i>.
    </dd>
</dl>


<p>
    <center><hr width="80%"></center>
<p>

<h3>
<a name="argv">Providing command-line parameters when a node reboots</a>
</h3>
As a special case, the <tt>data</tt> (third) parameter to each node's
handler for the <tt>EV_REBOOT</tt> event provides "command-line"
arguments when that node reboots.
The value passed in <tt>data</tt> is a pointer to a
<tt>NULL</tt>-terminated vector of character strings,
akin to the value of <tt>argv</tt> in a traditional C program.
Consider the following example code:

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
#include &lt;cnet.h&gt;

<b>void</b> reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    <b>char</b>    **argv  = (<b>char</b> **)data;
    <b>int</b>     argc    = 0;

    <b>if</b>(argv[0] != NULL) {
	printf("command line arguments:");
        <b>for</b>(argc=0 ; argv[argc] != NULL ; ++argc)
            printf(" '%s'", argv[argc]);
        printf("\n");
    }
    <b>else</b>
        printf("no command line arguments\n");
    ...
}
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

The optional command-line arguments for the <tt>EV_REBOOT</tt> handler
may either be passed on <i>cnet</i>'s own command-line
(after the name of the topology file or
the <a href="options.html#option-r">-r</a> option),
or provided (as a single string) via the
<a href="attributes.html#nodeattrs"><tt>rebootargs</tt></a>
node attribute in the topology file.

<p>
    <center><hr width="80%"></center>
<p>

<h3>
<a name="timers">Timers</a>
</h3>
<i>cnet</i> supports 10 timer event <i>queues</i>
providing a call-back mechanism for the protocol code.
For example,
the event <tt>EV_TIMER1</tt> may be requested to be "raised"
in <tt>5000000usec</tt>, and <i>cnet</i> will call
the <tt>EV_TIMER1</tt> event-handler in <tt>5</tt> seconds' time.
Timers are referenced via <i>unique</i> values of type <tt>CnetTimerID</tt>.
For example:

<pre><tt>    CnetTimerID  timer1;

    .....
    timer1 = CNET_start_timer(EV_TIMER1, (CnetTime)5000000, 0);</tt></pre>

<p>
The timer has significance for functions handling timer events;
all other handlers will simply receive the special <tt>NULLTIMER</tt>.
Notice that timers do not reflect the current time,
nor how much time remains until they expire;
they simply specify <i>which</i> timer has expired.
When a timer expires,
the event-handler for the corresponding event is invoked with the event and
the unique timer as parameters.
Timers may be cancelled prematurely
with <tt>CNET_stop_timer</tt>
to prevent them expiring,
for example:

<p>
<pre><tt>    (<b>void</b>)CNET_stop_timer(timer1);</tt></pre>
<p>

though they are automatically cancelled
as a result of their handler being invoked.


<p>
    <center><hr width="80%"></center>
<p>

<h3>
<a name="argv">Determining which physical link has just been severed or reconnected</a>
</h3>
As a special case, the <tt>data</tt> (third) parameter to each node's
handler for the <tt>EV_LINKSTATE</tt> event indicates which
physical link has just been severed or reconnected.
Consider the following example code:

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
#include &lt;cnet.h&gt;

<b>void</b> link_state_changed(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    <b>int</b>     link = (<b>int</b>)data;

    <b>if</b>(linkinfo[link].linkup) {
	......
    <b>else</b>
        ......
}
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

When the <tt>EV_LINKSTATE</tt> hander is invoked,
the node's values of <tt>linkinfo[]</tt> reflect
the new (current) state of each link.

</td></tr></table>


<p>

<table width="100%"><tr>
  <td valign=top>
    <i>cnet</i> was written and is maintained by Chris McDonald
    (<A HREF="mailto:chris@csse.uwa.edu.au">chris@csse.uwa.edu.au</a>).
    <p>
    <center>
	<img src="images/UWAsmall.gif">
    </center>
  </td>

  <td valign=top>
    <i>cnet</i> development has been supported by an ACM-SIGCSE
    <A HREF="http://www.sigcse.org/join/SpecialProjects.shtml">
    Special Project Grant</a>,

    and an Australian <a href="http://www.auc.edu.au/index2.html">
    Apple University Consortium</a>
    <!--
    WWDC'2005
    -->
    Scholarship.
    <p>
    <center>
	<A HREF="http://www.sigcse.org/join/SpecialProjects.shtml">
	<img src="images/sigcse.gif"></a>

	&nbsp;

	<A HREF="http://www.auc.edu.au/index2.html">
	<img src="images/auc.gif"></a>
    </center>
  </td>
</tr></table>


</BODY>
</HTML>
