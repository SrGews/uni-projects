<HTML>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HEAD>
<TITLE>The cnet network simulator - Physical Layer</TITLE>
<META NAME="description" CONTENT="cnet enables experimentation
with various data-link layer, network layer, routing and transport
layer networking protocols.  It has been specifically developed for
undergraduate teaching.">

<META NAME="keywords" CONTENT="network, ethernet, WAN, LAN, WLAN,
ad hoc, adhoc, mobile, wireless, protocol, simulator, simulation,
undergraduate, teaching">
</HEAD>

<BODY BGCOLOR="white" TEXT ="black" LINK="blue" VLINK="purple">

<table>
<tr>
<td valign="top" background="images/jigsaw.jpg">
<table cellpadding="4" cellspacing="0" border="0"><tr>

<td nowrap>
    <font size="+1"><b>cnet v3.1.1</b></font>
    <p>
    <a href="index.html">home</a>
    <br>
    <a href="faq.html">FAQ</a>
    <p>

    <a href="options.html">cmdline options</a>
    <br>
    <a href="topology.html">topology files</a>
    <br>
    <a href="attributes.html">attributes</a>
    <p>

    <a href="datatypes.html">datatypes</a>
    <br>
    <a href="api.html">core API</a>
    <br>
    <a href="supportapi.html">support API</a>
    <p>

    <a href="install.html">download/install</a>
</td>

</td></tr></table>
</td>

<td>
<h3><i>cnet's</i> Physical Layer</a></h3>

The Physical Layer attempts to deliver data frames between nodes.
Frames are delivered along any combination of
<i>wide-area-networking</i> (WAN),
<i>local-area-networking</i> (LAN), or
<i>wireless-local-area-networking</i> (WLAN) links.

Each physical link is numbered within each node from 1 to its total
number of links.
As a special case, link 0 represents a <i>loopback</i> link,
and is provided to simply copy a frame immediately from a node's output
to input.
In general, the Physical Layer will randomly corrupt and drop data frames
on all WAN, LAN, or WLAN links,
but not on the loopback link or Ethernet segments.
<p>
When your protocols wish to transmit a data frame along a link,
they write that frame to the Physical Layer.
On calling the <tt>CNET_write_physical</tt> function,
you indicate the length of the frame to be written and on return
<tt>CNET_write_physical</tt> indicates how many bytes were accepted.
A typical sequence for a network of just 2 nodes,
connected by a single WAN link is:

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
<b>char</b>         myframe[ MAX_MESSAGE_SIZE + MY_OVERHEAD ];
size_t       length;

 ...   <i>/* prepare frame contents for transmission */</i>
length = ... ;
result = CNET_write_physical(1, myframe, &amp;length);
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

When <i>cnet</i> informs the destination node that a frame has arrived,
the handler for <tt>EV_PHYSICALREADY</tt> should read that frame.
On return from a successful call to <tt>CNET_read_physical</tt>,
your protocol is informed on which link the frame arrived and how long it was.

Of course,
in a simple network with just one WAN connection or one Ethernet segment,
all frames will be transmitted and will arrive on link number <tt>1</tt>.
WAN links impose no particular format on the frames written to it;
unless corrupted or lost, whatever is written to a WAN link will
arrive unmodified, and without interpretation, at the other end of the link.
<p>

As an aid to debugging protocols,
the function <tt>CNET_write_physical()</tt> will 'trap' the situation
when a <i>large</i> number of frames have been written to the Physical Layer,
and when the receiving node has not read any of them off.
This trap is currently set at the large value of 1000,
which surely indicates an error in a protocol.
An errant protocol may have some unbounded loop,
or a very short timeout-and-retransmission sequence,
resulting in many calls to <tt>CNET_write_physical()</tt> at the sender,
before any <tt>EV_PHYSICALREADY</tt> events are handled at the receiver.
If the frame limit is exceeded,
<tt>CNET_write_physical()</tt> will return <tt>-1</tt>
and set <tt>cnet_errno</tt> to <tt>ER_TOOBUSY</tt>.

<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
<b>char</b>         myframe[ MAX_MESSAGE_SIZE + MY_OVERHEAD ];
size_t       length;
<b>int</b>          link;

length = <b>sizeof</b>(myframe);
result = CNET_read_physical(&amp;link, myframe, &amp;length);
 ...   <i>/* process frame contents */</i>
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

To provide some sense of realism,
frames (or packets) written to Ethernet links are expected to carry the
address of their destination Network Interface Card (NIC) at the very
beginning of the frame.
<i>cnet</i> provides the data type <tt>CnetNICaddr</tt> to represent the
addresses of its NICs,
as an array of <tt>LEN_NICADDR</tt> (=6) unsigned characters.

<i>cnet</i> interprets the leading <tt>LEN_NICADDR</tt> bytes of each frame
on an Ethernet segment to be an address.
The special address, whose string representation is <tt>ff:ff:ff:ff:ff:ff</tt>,
is interpreted as the Ethernet <i>broadcast address</i>.
Any frame carrying the broadcast address as its destination address will be
delivered to all NICs on the Ethernet segment, except the sender.
<i>cnet</i> does not support multicast or group addressing.
<p>

Consider the following example function,
used to write data to an Ethernet segment:
<p>
<p>
<table cellpadding="1" cellspacing="0" border="0" bgcolor="#000000"><tr><td>
<table cellpadding="8" cellspacing="0" border="0" bgcolor="#dddddd"
width="600"><tr><td><pre><tt>
<b>typedef struct</b> {
    CnetNICaddr    dest;
    CnetNICaddr    src;
    <b>char</b>           type[2];
    <b>char</b>           data[ETH_MAXDATA];
} ETHERPACKET;

#define LEN_ETHERHEADER (sizeof(ETHERPACKET) - ETH_MAXDATA)

<b>static void</b> write_to_ethernet(CnetNICaddr dest, <b>int</b> link, <b>char</b> *buf, size_t len)
{
    ETHERPACKET packet;
    <b>short int</b>   twobytes;

    memcpy(packet.dest, dest,                   <b>sizeof</b>(CnetNICaddr));
    memcpy(packet.src,  linkinfo[link].nicaddr, <b>sizeof</b>(CnetNICaddr));

    twobytes = len;              <i>/* type carries the data's true length */</i>
    memcpy(packet.type, &amp;twobytes, 2);
    memcpy(packet.data, buf, len);

    len  += LEN_ETHERHEADER;
    <b>if</b>(len &lt; ETH_MINPACKET)      <i>/* pad short packets to minimum length */</i>
        len = ETH_MINPACKET;
    CHECK(CNET_write_physical(link, (<b>char</b> *)&amp;packet, &amp;len));
    ......
}
</tt></pre></tr></td></table></td></tr></table>
<p>
<p>

This function assumes that the data's length is not too long for Ethernet
(<tt>&lt;=&nbsp;ETH_MAXDATA</tt> (=1500) bytes).
The required destination's NIC address is first copied to the destination
address field,
and then the address of the local NIC used copied to the source address field.
Notice that because the <tt>CnetNICaddr</tt> type is actually an array of
characters, we do not use the &amp; operator in the calls to <tt>memcpy</tt>.

The data's true length is copied into the packet's two-byte <tt>type</tt>
field, the provided data copied to the packet's data.
After ensuring that the packet to be written is at least
<tt>ETH_MINPACKET</tt> (=64) bytes long,
the packet is written to the link.

Again, <i>cnet</i> does not enforce (nor understand) the use of
<i>our</i> <tt>ETHERPACKET</tt> data type,
but <i>does</i> assume that the first
<tt>LEN_NICADDR</tt> bytes of each packet provides the destination NIC address.
<p>

Two additional Physical Layer functions are provided to assist in the
debugging of multi-layered protocols.
<tt>CNET_write_physical_reliable</tt> is identical to
<tt>CNET_write_physical</tt> except that frames sent using it will not be
subject to frame corruption or loss.
It can be considered as a "perfect" Data Link Layer if you just want
to implement higher-layered protocols.
The function <tt>CNET_write_direct</tt> also bypasses all Physical Layer errors
and instructs a message to be sent directly to the node whose address is
specified as a parameter.
It thus provides perfect a Data Link Layer and Network Layer.

<p>

</td></tr></table>


<p>

<table width="100%"><tr>
  <td valign=top>
    <i>cnet</i> was written and is maintained by Chris McDonald
    (<A HREF="mailto:chris@csse.uwa.edu.au">chris@csse.uwa.edu.au</a>).
    <p>
    <center>
	<img src="images/UWAsmall.gif">
    </center>
  </td>

  <td valign=top>
    <i>cnet</i> development has been supported by an ACM-SIGCSE
    <A HREF="http://www.sigcse.org/join/SpecialProjects.shtml">
    Special Project Grant</a>,

    and an Australian <a href="http://www.auc.edu.au/index2.html">
    Apple University Consortium</a>
    <!--
    WWDC'2005
    -->
    Scholarship.
    <p>
    <center>
	<A HREF="http://www.sigcse.org/join/SpecialProjects.shtml">
	<img src="images/sigcse.gif"></a>

	&nbsp;

	<A HREF="http://www.auc.edu.au/index2.html">
	<img src="images/auc.gif"></a>
    </center>
  </td>
</tr></table>


</BODY>
</HTML>
