#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// -------------------------------------------------------------------

#if	defined(USE_TCLTK)

#if	(TCL_MAJOR_VERSION == 7) && (TCL_MINOR_VERSION < 5)
#define	USE_BEFORE_TCL75
#define	DoOneEvent	Tk_DoOneEvent
#define	DoWhenIdle	Tk_DoWhenIdle
#define	ALL_EVENTS	TK_ALL_EVENTS
#define	DONT_WAIT	TK_DONT_WAIT

#else
#define	DoOneEvent	Tcl_DoOneEvent
#define	DoWhenIdle	Tcl_DoWhenIdle
#define	ALL_EVENTS	TCL_ALL_EVENTS
#define	DONT_WAIT	TCL_DONT_WAIT
#endif

Tcl_Interp	*tcl_interp;

void TCLTK_init(void)
{
    Tk_Window	tcl_mainwindow;

    tcl_interp	= Tcl_CreateInterp();
#if	defined(USE_BEFORE_TCL75)
    tcl_mainwindow = Tk_CreateMainWindow(tcl_interp, NULL, argv0, argv0);
#else
    if(Tcl_Init(tcl_interp) != TCL_OK || Tk_Init(tcl_interp) != TCL_OK) {
	if(*tcl_interp->result)
	    FATAL("%s\n", tcl_interp->result);
	CLEANUP(1);
    }
    tcl_mainwindow = Tk_MainWindow(tcl_interp);
#endif

    if(tcl_mainwindow == NULL) {
        fprintf(stderr, "%s\n", tcl_interp->result);
        exit(EXIT_FAILURE);
    }
    Tk_GeometryRequest(tcl_mainwindow, 200, 200);

#if	defined(USE_BEFORE_TCL75)
    if(Tcl_Init(tcl_interp) != TCL_OK || Tk_Init(tcl_interp) != TCL_OK) {
	if(*tcl_interp->result)
	    FATAL("%s\n", tcl_interp->result);
	CLEANUP(1);
    }
#endif
}

void TCLTK(const char *fmt, ...)
{
    if(Wflag) {
	va_list	ap;

	va_start(ap,fmt);
	vsprintf(chararray,fmt,ap);
	va_end(ap);

	if(vflag > 1)
	    REPORT("%s\n", chararray);
	if(Tcl_Eval(tcl_interp, chararray) != TCL_OK) {
	    if(*tcl_interp->result)
	      FATAL("TCLTK(\"%s\") error - %s\n",chararray,tcl_interp->result);
	    CLEANUP(1);
	}
    }
}

// -------------------------------------------------------------------

static	int	notify_running	= false;

int tcltk_notify_start(void)
{
    if(notify_running)
	return(false);
    notify_running	= true;
    do {
	DoOneEvent(ALL_EVENTS);
    } while(notify_running);
    notify_running	= false;

    return(true);
}

int tcltk_notify_stop(void)
{
    if(!notify_running)
	return(false);
    notify_running	= false;
    return(true);
}

int tcltk_notify_dispatch(void)
{
    if(notify_running)
	return(false);
    while(DoOneEvent(DONT_WAIT) != 0)
	;
    return(true);
}

#endif
