#include <stdarg.h>
#include "cnetprivate.h"
#include <fcntl.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#if	defined(USE_TCLTK)
TCLTK_COMMAND(tk_stdio_input)
{
    NODE	*np;

    TCLTK_ARGCHECK(3);
    n		= atoi(argv[1]);
    if(n < -1 || n >= _NNODES) {
	interp->result	= "invalid node";
	return TCL_ERROR;
    }

    np	= &NODES[n];
    if(np->handler[(int)EV_KEYBOARDREADY] && np->inputline == NULL) {
	np->inputline	= strdup(argv[2]);
	np->inputlen	= strlen(argv[2]);
	newevent(EV_KEYBOARDREADY, n,
		(CnetTime)1, NULLTIMER, np->data[(int)EV_KEYBOARDREADY]);
    }
    return TCL_OK;
}
#endif


// -------------------------------------------------------------------------

static void stdio_flush(char *str)
{
    NODE	*np;
    char	*t	= str;
    int		len	= 0;

#if	defined(USE_TCLTK)
    char	tcltk_buf[BUFSIZ];
#endif

    while(*t++)
	len++;

    np	= &(NODES[THISNODE]);
    if(np->outputfd >= 0)		// duplicate in local file
	write(np->outputfd, str, (unsigned)len);

    if(gattr.stdio_quiet)
	return;

#if	defined(USE_TCLTK)
    while(*str) {
	t	= tcltk_buf;
	while(*str && *str != '\n') {
	    if(*str == '"' || *str == '[' || *str == '\\')
		*t++ = '\\';		// elide significant Tcl chars
	    *t++ = *str++;
	}
	*t	= '\0';
	TCLTK("stdiooutput %d \"%s\" %d", THISNODE, tcltk_buf, (*str=='\n'));
	if(*str == '\n')
	    ++str;
    }
#endif
}

int CNET_printf(const char *fmt, ...)
{
    va_list	ap;
    char	stdio_buf[BUFSIZ];

    if(gattr.stdio_quiet && NODES[THISNODE].outputfd < 0)	// faster!
	return(0);

    va_start(ap,fmt);
    vsprintf(stdio_buf,fmt,ap);
    va_end(ap);
    stdio_flush(stdio_buf);
    return(0);
}

int CNET_puts(const char *str)		// the substitute for puts()
{
    char	stdio_buf[BUFSIZ];
    char	*p = stdio_buf;

    while((*p++ = *str++));
    *(p-1)	= '\n';
    *p		= '\0';
    stdio_flush(stdio_buf);
    return(0);
}

#if	defined(putchar)
#undef	putchar
#endif

int CNET_putchar(int ch)		// the substitute for putchar()
{
    char	stdio_buf[4];

    stdio_buf[0] = ch; stdio_buf[1] = '\0';
    stdio_flush(stdio_buf);
    return(0);
}


int CNET_read_keyboard(char *line, size_t *len)
{
    int		result	= -1;
#if	defined(USE_TCLTK)
    NODE	*np	= &(NODES[THISNODE]);
#endif

    if(!NODE_HAS_KB(THISNODE))
	ERROR(ER_NOTSUPPORTED);

    else if(line == NULL || len == NULL || *len <= 0)
	ERROR(ER_BADARG);

#if	defined(USE_TCLTK)
    else if(np->inputline == NULL)
	ERROR(ER_NOTREADY);

    else if(*len <= np->inputlen)
	ERROR(ER_BADSIZE);

    else {				// only provide inputline once
	strcpy(line, np->inputline);
	*len		= np->inputlen+1;	// strlen() + NULL
	FREE(np->inputline);
	np->inputlen	= 0;
	result		= 0;

	if(gattr.trace_events)
	return(0);
    }
#else
    else
	result	= 0;
#endif

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(%s,%s) = 0\n",
			__func__,
			find_trace_name(line), find_trace_name(len));
	else
	    TRACE(1, "\t%s(%s,%s) = -1 %s\n",
			__func__,
			find_trace_name(line), find_trace_name(len),
			cnet_errname[(int)cnet_errno]);
    }
    return result;
}

void CNET_clear(void)
{
#if	defined(USE_TCLTK)
    if(Wflag)
	TCLTK("stdioclr %d", THISNODE);
#endif
}


// -------------------- NO USER-SERVICEABLE CODE BELOW --------------------

void init_stdio_layer(char *oflag)
{
    NODE	*np;
    int		n;

//  COMMAND-LINE ARGUMENT OVERRIDES TOPOLOGY FILE ATTRIBUTE
    if(oflag)
	DEFAULTNODE.outputfile	= oflag;

//  FOREACH NODE, POSSIBLY OPEN A FILE TO MIRROR ITS stdout
    for(n=0, np=NODES ; n<_NNODES ; n++, np++) {
	np->outputfd	= -1;

	if(np->nattr.outputfile == NULL && DEFAULTNODE.outputfile)
	    np->nattr.outputfile	= DEFAULTNODE.outputfile;

//  IF A FILENM IS EVENTUALLY PROVIDED, FORMAT IT, AND TRY TO OPEN TO IT
	if(np->nattr.outputfile) {
	    char	*filenm;

	    if(strchr(np->nattr.outputfile, '%') == NULL) { // any formatting?
		sprintf(chararray, "%s.%%n", np->nattr.outputfile);
		np->nattr.outputfile	= strdup(chararray);
	    }

	    filenm = format_nodeinfo(np, np->nattr.outputfile);
	    if((np->outputfd =
		    open(filenm, O_WRONLY|O_CREAT|O_TRUNC, 0600)) < 0) {
		fprintf(stderr, "%s: cannot create %s\n", argv0, filenm);
		exit(EXIT_FAILURE);
	    }
	    FREE(filenm);
	}
    }
}

void reboot_stdio_layer(void)
{
#if	defined(USE_TCLTK)
    if(Wflag) {
	NODE *np	= &NODES[THISNODE];

	if(np->inputline)
	    FREE(np->inputline);
	np->inputlen	= 0;
    }
#endif
}
