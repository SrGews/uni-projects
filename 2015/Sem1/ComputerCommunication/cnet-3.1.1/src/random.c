#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

void CNET_srand(unsigned int seed)
{
    NODES[THISNODE].mt = mt19937_init((unsigned long)seed);
}

long CNET_rand(void)
{
    return mt19937_int31(NODES[THISNODE].mt);
}

CnetRandom CNET_newrand(unsigned int seed)
{
    return (CnetRandom)mt19937_init((unsigned long)seed);
}

long CNET_nextrand(CnetRandom mt)
{
    return mt19937_int31((MT *)mt);
}
