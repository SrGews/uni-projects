#include <mach-o/loader.h>
#include <CoreServices/CoreServices.h>
#include <sys/param.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


static int add_compile_args(int ac, char *av[])
{
    av[ac++]	= "-fno-common";
    return(ac);
}

static int add_link_args(int ac, char *av[])
{
    av[ac++]	= "-bundle";
    av[ac++]	= "-flat_namespace";
    av[ac++]	= "-undefined";
    av[ac++]	= "suppress";
    return(ac);
}

static void data_segments(int n, void *handle, char *so_filenm)
{
    struct mach_header	header;
    struct load_command	loadcmd;
    int			fd, ncmds, nsect;
    off_t		offset;
    long		length_data = 0L;

    typedef struct _c {
	char		*so_filenm;
	long		length_data;
	char		*incore_data;
	char		*original_data;
	struct _c	*next;
    } CACHE;

    static CACHE	*chd = NULL;
    CACHE		*cp  = chd;

    NODE		*np	= &NODES[n];

    while(cp != NULL) {
	if(strcmp(cp->so_filenm, so_filenm) == 0)
	    goto found;
	cp	= cp->next;
    }

    if((fd = open(so_filenm, O_RDONLY, 0)) < 0 ||
	read(fd, &header, sizeof(header)) != sizeof(header)) {
	++nerrors;
	return;
    }

    offset	= lseek(fd, (off_t)0, SEEK_CUR);
    for(ncmds=0 ; ncmds<header.ncmds ; ++ncmds) {
	read(fd, &loadcmd, sizeof(loadcmd));
	if(loadcmd.cmd == LC_SEGMENT) {
	    struct segment_command	segment;
	    struct section		section;

	    offset	= lseek(fd, offset, SEEK_SET);
	    read(fd, &segment, sizeof(segment));
	    if(strcmp(segment.segname, SEG_DATA) == 0) {
		length_data	= 0L;
		for(nsect=0 ; nsect<segment.nsects ; ++nsect) {
		    uint32_t	pow2, padded;

		    read(fd, &section, sizeof(section));
		    pow2	= (1<<section.align);
		    padded	= ((section.size+(pow2- 1))/pow2) * pow2;

		    if(dflag)
			REPORT("%s.%s size=%u align=%u pad=%u\n",
					section.segname, section.sectname,
					section.size, section.align, padded);
		    length_data += padded;
		}
		break;
	    }
	}
	offset	+= loadcmd.cmdsize;
	offset	= lseek(fd, offset, SEEK_SET);
    }
    close(fd);

    cp			= malloc(sizeof(CACHE));
    cp->so_filenm	= strdup(so_filenm);
    cp->length_data	= length_data;
    cp->incore_data	= dlsym(handle, CNET_DATA_SYM);
    cp->original_data	= malloc(cp->length_data);
    memcpy(cp->original_data, cp->incore_data, cp->length_data);
    cp->next		= chd;
    chd			= cp;

    if(dflag)
	REPORT("%s dataseg=0x%08lx len(dataseg)=%ld\n",
			    so_filenm, (long)cp->incore_data, cp->length_data);
found:
    np->length_data[0]		= cp->length_data;
    np->incore_data[0]		= cp->incore_data;
    np->original_data[0]	= cp->original_data;

    np->private_data[0]		= malloc(cp->length_data);
    memcpy(np->private_data[0], cp->original_data, cp->length_data);
}
