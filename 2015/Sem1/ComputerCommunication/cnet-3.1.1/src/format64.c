#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

static	bool	output_commas	= OUTPUT_COMMAS;

bool CNET_get_commas(void)
{
    return output_commas;
}

bool CNET_set_commas(bool wanted)
{
    bool	was = output_commas;

    output_commas	= wanted;
    return was;
}

/*  Large (64 bit) integers, such as instances of CnetTime, may be formatted
    with commas to make them more readable.
    cnet only formats large integers with commas on the GUI, only if
    requested, and never in the statistics printed to stdout.
 */

#define	N_COMMABUFS	4

char *CNET_format64(int64_t value)
{
    static	char	result[N_COMMABUFS][32];
    static	int	whichbuf	= 0;

    char	*rp	= &result[whichbuf][0];

    whichbuf	= (whichbuf+1) % N_COMMABUFS;

    sprintf(rp, "%" PRId64, value);
    if(output_commas) {
	char	buf[32], *b=buf, *r=rp;
	int	i, len;

	if(rp[0] == '-') {
	    *b++	= '-';
	    ++r;
	}

	len	= strlen(r);
	for(i=0 ; i<len ; ++i) {
	    *b++	= *r++;
	    if(*r && ((len-i)%3) == 1)
		*b++	= ',';
	}
	*b	= '\0';
	strcpy(rp, buf);
    }
    return rp;
}
