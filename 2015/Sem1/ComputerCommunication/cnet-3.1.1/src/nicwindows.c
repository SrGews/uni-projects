#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

// ------------------ NO CODE IN HERE FOR USE_ASCII ---------------------

#if	defined(USE_TCLTK)

#define	PFC		0
#define	PFL		1
#define	CPB		2
#define	CPF		3

#define	MAX_PFC		11
#define	MAX_PFL		11
#define	MAX_CPB		11
#define	MAX_CPF		11

static struct {
    const char	*label;
    const char	*keyword;
    int		maxvalue;
} sliders[] = {
    {	"probability of frame corruption",	"probframecorrupt", MAX_PFC },
    {	"probability of frame loss",		"probframeloss",    MAX_PFL },
    {	"cost per byte",			"costperbyte",      MAX_CPB },
    {	"cost per frame",			"costperframe",     MAX_CPF }
};

#define	N_NIC_SLIDERS	(sizeof(sliders) / sizeof(sliders[0]))

typedef	struct {
    const char		*nictype;
    NICATTR		*thisnic;
    NICATTR		*defnic;
    const char		*fromstr;
    const char		*tostr;
    int			from;
    int			to;
    int			which;
    int			value;
    char		*title;
} NICARG;

/*  Possibilities:

	WAN	-1	-1	default WAN
	WAN	from	to	WAN link
	LAN	lan-n	-1	LAN lan-n
	LAN	lan-n	nicn	nicn on LAN lan-n
 */

static NICARG *new_nicarg(char *nictype, char *from, char *to)
{
    NICARG	*nicarg	= NEW(NICARG);
    char	title[128];

    nicarg->nictype	= nictype;
    nicarg->fromstr	= from;
    nicarg->tostr	= to;

    nicarg->from	= atoi(from);
    nicarg->to		= atoi(to);

    if(strcmp(nictype, "WAN") == 0) {
	if(nicarg->to == DEFAULT) {
	    nicarg->thisnic	=
	    nicarg->defnic	= &DEFAULTWAN;
	    strcpy(title, "default WAN");
	}
	else {			// a specific WAN
	    NODE	*fromnp;
	    WAN		*wan;
	    int		w, min, max;

	    fromnp		= &NODES[nicarg->from];
	    nicarg->thisnic	= &DEFAULTWAN;

	    if(nicarg->from < nicarg->to)
		min = nicarg->from,	max = nicarg->to;
	    else
		min = nicarg->to,	max = nicarg->from;

	    for(w=0, wan=WANS ; w<gattr.nwans ; w++, wan++)
		if(wan->minnode == min && wan->maxnode == max) {

		    if(nicarg->from == wan->minnode) {
			nicarg->thisnic = &fromnp->nics[wan->minnode_nic];
			nicarg->defnic  = &fromnp->defaultwan;
		    }
		    else {
			nicarg->thisnic = &fromnp->nics[wan->maxnode_nic];
			nicarg->defnic  = &fromnp->defaultwan;
		    }
		    break;
		}
	    sprintf(title, "WAN %s->%s",
		    NODES[nicarg->from].nodename, NODES[nicarg->to].nodename);
	}
    }
    else if(strcmp(nictype, "LAN") == 0) {
	if(nicarg->to == DEFAULT) {
	    if(nicarg->from == DEFAULT) {	// LAN -1 -1
		nicarg->thisnic	=
		nicarg->defnic	= &DEFAULTLAN;
		strcpy(title, "default LAN");
	    }
	    else {				// LAN lan-n -1
		nicarg->thisnic	= &LANS[nicarg->from].segmentnic;
		nicarg->defnic	= &DEFAULTLAN;
		sprintf(title, "LAN %s", LANS[nicarg->from].name);
	    }
	}
	else {					// LAN lan-n nic-n
	    EACHNIC *each	= &LANS[nicarg->from].nics[nicarg->to];

	    nicarg->thisnic	= &NODES[each->node].nics[each->nodeslink];
	    nicarg->defnic	= &DEFAULTLAN;
	    sprintf(title, "%s->LAN %s",
			NODES[each->node].nodename, LANS[nicarg->from].name);
	}
    }
    else if(strcmp(nictype, "WLAN") == 0) {
	if(nicarg->to == DEFAULT) {
	    nicarg->thisnic	=
	    nicarg->defnic	= &DEFAULTWLAN;
	    strcpy(title, "default WLAN");
	}
	else {
	    nicarg->thisnic	= WLANS[nicarg->to].nic;
	    nicarg->defnic	= &DEFAULTWLAN;
	    sprintf(title, "WLAN node %d", nicarg->to);
	}
    }

    nicarg->title	= strdup(title);
    return(nicarg);
}

static void free_nicarg(NICARG *na)
{
    FREE(na->title);
    FREE(na);
}

// ---------------------------------------------------------------------

static void set_nic_scale_label(NICARG *na)
{
    NICATTR	*nicattr;
    char	desc[32];
    int		value=0;

    switch (na->value) {
//  ONLY true NICs CAN SET VALUE TO -1, REQUESTING THEIR DEFAULT VALUES
    case -1 :	value	= -1;
		break;

//  A SCALE VALUE OF ZERO REQUESTS THE VALUE FROM THE TOPOLOGY FILE
    case 0 :	nicattr = (na->to == DEFAULT) ? na->defnic : na->thisnic;
		switch (na->which) {
		case PFC :	value	= nicattr->top_sets_probframecorrupt ?
		    nicattr->top_probframecorrupt : nicattr->probframecorrupt;
				break;
		case PFL :	value	= nicattr->top_sets_probframeloss ?
		    nicattr->top_probframeloss : nicattr->probframeloss;
				break;
		case CPB :	value	= nicattr->top_sets_costperbyte ?
		    nicattr->top_costperbyte : nicattr->costperbyte;
				break;
		case CPF :	value	= nicattr->top_sets_costperframe ?
		    nicattr->top_costperframe : nicattr->costperframe;
				break;
		}
		break;

//  ANY OTHER, POSITIVE, VALUE REQUESTS ONE OF THE DEFAULTS
    default :	value	= na->value-1;
		break;
    }

    if(value == -1)
	strcpy(desc, "(default value)");
    else if(value == 0)
	strcpy(desc, "(zero)");
    else if(na->which == PFL || na->which == PFC)
	sprintf(desc, "(1 chance in %d)", 1<<value);
    else
	sprintf(desc, "(%d)", value);

    TCLTK(".nic(%s,%d,%d).scales.scale%d config -label \"%s %s :\"",
		na->nictype, na->from, na->to,
		na->which, sliders[na->which].label, desc);
}

static TCLTK_COMMAND(set_nic_scale)
{
    NICATTR	*nicattr;
    NICARG	*nicarg;
    int		now=0;

    TCLTK_ARGCHECK(6);
    nicarg		= new_nicarg(argv[1], argv[2], argv[3]);
    nicarg->which	= atoi(argv[4]);
    nicarg->value	= atoi(argv[5]);

    if(nicarg->which < 0 || nicarg->which > N_NIC_SLIDERS) {
	free_nicarg(nicarg);
	interp->result	= "invalid set_nic scale #";
	return TCL_ERROR;
    }

    nicattr	= (nicarg->to == DEFAULT) ? nicarg->defnic : nicarg->thisnic;

    switch (nicarg->which) {
    case PFC:   if(nicarg->value == 0)
		    nicattr->probframecorrupt = nicattr->top_probframecorrupt;
		else
		    nicattr->probframecorrupt = nicarg->value;
		now	= nicattr->probframecorrupt;
		break;

    case PFL:   if(nicarg->value == 0)
		    nicattr->probframeloss = nicattr->top_probframeloss;
		else
		    nicattr->probframeloss = nicarg->value;
		now	= nicattr->probframeloss;
		break;

    case CPB:   if(nicarg->value == 0)
		    nicattr->costperbyte = nicattr->top_costperbyte;
		else
		    nicattr->costperbyte = nicarg->value;
		now	= nicattr->costperbyte;
		break;

    case CPF:   if(nicarg->value == 0)
		    nicattr->costperframe = nicattr->top_costperframe;
		else
		    nicattr->costperframe = nicarg->value;
		now	= nicattr->costperframe;
		break;
    }

    set_nic_scale_label(nicarg);
    if(dflag)
	REPORT("%s.%s = %d\n",nicarg->title,sliders[nicarg->which].keyword,now);

    free_nicarg(nicarg);
    return TCL_OK;
}

TCLTK_COMMAND(nic_created)
{
    static int	first_time	= true;

    NICARG	*nicarg;
    char	tmpbuf[32];

//  CALLED AS:	nic_created $nictype $from $to
    TCLTK_ARGCHECK(4);
    if(first_time) {
	first_time = false;

	TCLTK_createcommand("set_nic_scale", set_nic_scale);

	for(n=0 ; n<N_NIC_SLIDERS; ++n) {
	    sprintf(chararray,"nic_scale_max(%d)", n);
	    sprintf(tmpbuf   ,"%d", sliders[n].maxvalue);
	    Tcl_SetVar(tcl_interp, chararray, tmpbuf, TCL_GLOBAL_ONLY);
	}
    }

    nicarg		= new_nicarg(argv[1], argv[2], argv[3]);

    sprintf(chararray,"nic_scale_value(%d)", PFC);
    sprintf(tmpbuf, "%d", (nicarg->to == DEFAULT) ? 0 :
			  (nicarg->thisnic->top_sets_probframecorrupt ? 0:-1));
    Tcl_SetVar(tcl_interp, chararray, tmpbuf, TCL_GLOBAL_ONLY);

    sprintf(chararray,"nic_scale_value(%d)", PFL);
    sprintf(tmpbuf, "%d", (nicarg->to == DEFAULT) ? 0 :
			  (nicarg->thisnic->top_sets_probframeloss ? 0 : -1));
    Tcl_SetVar(tcl_interp, chararray, tmpbuf, TCL_GLOBAL_ONLY);

    sprintf(chararray,"nic_scale_value(%d)", CPB);
    sprintf(tmpbuf, "%d", (nicarg->to == DEFAULT) ? 0 :
			  (nicarg->thisnic->top_sets_costperbyte ? 0 : -1));
    Tcl_SetVar(tcl_interp, chararray, tmpbuf, TCL_GLOBAL_ONLY);

    sprintf(chararray,"nic_scale_value(%d)", CPF);
    sprintf(tmpbuf, "%d", (nicarg->to == DEFAULT) ? 0 :
			  (nicarg->thisnic->top_sets_costperframe ? 0 : -1));
    Tcl_SetVar(tcl_interp, chararray, tmpbuf, TCL_GLOBAL_ONLY);

    sprintf(chararray, "nic_displayed(%s,%s,%s)", argv[1], argv[2], argv[3]);
    Tcl_LinkVar(tcl_interp, chararray,
    		(char *)&nicarg->thisnic->displayed, TCL_LINK_BOOLEAN);

    free_nicarg(nicarg);
    return TCL_OK;
}
#endif
