#include "cnetprivate.h"
#include <stdarg.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// ------------------------- RUN-TIME ERRORS --------------------------


const char *cnet_errname[N_CNET_ERRORS] = {
    "ER_OK",		"ER_BADALLOC",		"ER_BADARG",
    "ER_BADEVENT",	"ER_BADLINK",		"ER_BADNODE",
    "ER_BADPOSITION",	"ER_BADSENDER",		"ER_BADSESSION",
    "ER_BADSIZE",	"ER_BADTIMERID",	"ER_CORRUPTFRAME",
    "ER_DUPLICATEMSG",	"ER_LINKDOWN",		"ER_MISSINGMSG",
    "ER_NOBATTERY",	"ER_NOTFORME",		"ER_NOTREADY",
    "ER_NOTSUPPORTED",	"ER_TOOBUSY"
};

const char *cnet_errstr[N_CNET_ERRORS] = {
    /* ER_OK */		  "No error",
    /* ER_BADALLOC */	  "Allocation of dynamic memory failed",
    /* ER_BADARG */	  "Invalid argument passed to a function",
    /* ER_BADEVENT */	  "Invalid event passed to a function",
    /* ER_BADLINK */	  "Invalid link number passed to a function",
    /* ER_BADNODE */	  "Invalid node passed to a function",
    /* ER_BADPOSITION */  "Attempt to move mobile node off the map",
    /* ER_BADSENDER */	  "Application Layer given msg from an unknown node",
    /* ER_BADSESSION */	  "Application Layer given msg from incorrect session",
    /* ER_BADSIZE */	  "Indicated length is of incorrect size",
    /* ER_BADTIMERID */	  "Invalid CnetTimerID passed to a function",
    /* ER_CORRUPTFRAME */ "Attempted to transfer a corrupt data frame",
    /* ER_DUPLICATEMSG */ "Application Layer given a duplicate msg",
    /* ER_LINKDOWN */	  "Attempted to transmit on a link that is down",
    /* ER_MISSINGMSG */	 "Application Layer given msg before all previous ones",
    /* ER_NOBATTERY */	  "Battery exhaused on a mobile node",
    /* ER_NOTFORME */	  "Application Layer given msg for another node",
    /* ER_NOTREADY */	  "Function called when service not available",
    /* ER_NOTSUPPORTED */ "Invalid operation for this node or link type",
    /* ER_TOOBUSY */	  "Function is too busy/congested to handle request"
};

void CNET_perror(char *user_msg)
{
    if((int)cnet_errno < 0 || (int)cnet_errno >= N_CNET_ERRORS)
	return;
    if(user_msg == NULL || *user_msg == '\0')
	user_msg	= NODES[THISNODE].nodename;
    fprintf(stderr,"%s: %s (%s)\n",
	user_msg, cnet_errname[(int)cnet_errno], cnet_errstr[(int)cnet_errno]);
}


// --------------- COMPILE AND RUNTIME ERRORS -------------------------

void SYNTAX(const char *fmt, ...)
{
    va_list	ap;
    int		i;

    va_start(ap,fmt);

//  POINT TO THE ERROR
    fputs(input.line,stderr);
    for(i=0 ; i<input.cc ; ++i)
	if(input.line[i] == '\t')
	     fputc('\t',stderr);
	else fputc(' ',stderr);
    fprintf(stderr,"^\n%s, line %d: ", input.name,input.lc);

    vfprintf(stderr,fmt,ap);
    va_end(ap);
    CLEANUP(1);
}

void REPORT(const char *fmt, ...)	// IFF dflag or vflag
{
    va_list	ap;

    va_start(ap,fmt);
    vfprintf(stderr,fmt,ap);
    va_end(ap);
}

void FATAL(const char *fmt, ...)
{
    va_list	ap;
    char	fatal[BUFSIZ];

    va_start(ap,fmt);
    sprintf(fatal, "%s: %s", argv0, fmt);
    vfprintf(stderr,fatal,ap);
    va_end(ap);
    CLEANUP(1);				// does not return
}

void WARNING(const char *fmt, ...)
{
    va_list	ap;
    char	warn[BUFSIZ];

    va_start(ap,fmt);
    sprintf(warn, "*** warning: %s", fmt);
    vfprintf(stderr,warn,ap);
    va_end(ap);
}

void ERROR(CnetError err)
{
    cnet_errno	= err;
    ++NODES[THISNODE].nodestats.nerrors;
    ++gattr.nodestats.nerrors;
}
