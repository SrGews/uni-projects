#include "cnetprivate.h"
#include <netinet/in.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


/*  cnet is indebted to Brian Wellington <bwelling@tis.com> and Matias Duarte
    <matias@hyperimage.com>, the authors of XBill (v1.1 and beyond),
    for their pixmaps of various machines and operating system logos.

    I have modified and converted their pixmaps to GIFs, but the
    hard yards were certainly covered by the XBill authors.  XBill's
    official distribution site is    www.xbill.org
 */

static const char *os_names[] = {
	"bsd", "hurd", "linux", "mac", "macosx", // "mote",
	"next", "os2", "palm", "sgi", "sun", "windows"
};

#define	N_OS_NAMES	(sizeof(os_names) / sizeof(os_names[0]))

#if   defined(USE_TCLTK)
static	void	init_node_images(void);

static	int	image_maxw	= 0;
static	int	image_maxh	= 0;
#endif

void find_mapsize(void)
{
    NODEATTR	*na;
    int		n;
    int		maxx	= gattr.maxposition.x;	// may not be set yet => 0
    int		maxy	= gattr.maxposition.y;

#if   defined(USE_TCLTK)
    if(Wflag) {
	if(gattr.drawnodes)
	    init_node_images();

    //  IF WE HAVE A BACKGROUND IMAGE, DETERMINE THE DIMENSIONS FROM IT
	if(gattr.mapimage || gattr.maptile) {
	    int	pxw, pxh, metres;

	    TCLTK("image create photo im_map -file %s",
		find_cnetfile(gattr.mapimage ? gattr.mapimage : gattr.maptile,
							false, true));
	    TCLTK("image width im_map");
	    pxw		= atoi(tcl_interp->result);
	    metres	= PX2M(pxw);
	    if(maxx < metres)
		maxx	= metres;

	    TCLTK("image height im_map");
	    pxh		= atoi(tcl_interp->result);
	    metres	= PX2M(pxh);
	    if(maxy < metres)
		maxy	= metres;

	    if(gattr.mapimage) {
		gattr.mapwidth		= pxw;
		gattr.mapheight		= pxh;
	    }
	    else {
		gattr.tilewidth		= pxw;
		gattr.tileheight	= pxh;
	    }
	}
    }
#endif

//  NOW, DETERMINE THE MAXIMUM DIMENSIONS OF THE WHOLE MAP
    for(n=0 ; n<_NNODES ; ++n) {
	na      = &NODES[n].nattr;
	if(maxx < na->position.x)
	    maxx = na->position.x;
	if(maxy < na->position.y)
	    maxy = na->position.y;
    }
#if   defined(USE_TCLTK)
    if(Wflag && gattr.drawnodes && gattr.nmobiles == 0)
	maxy	+= DEFAULT_NODE_SPACING;
#endif

//  SET THE MAP'S DIMENSIONS BASED ON THE MAXIMUMS FOUND
    gattr.maxposition.x	= (maxx == 0) ? DEFAULT_MAPWIDTH  : maxx;
    gattr.maxposition.y	= (maxy == 0) ? DEFAULT_MAPHEIGHT : maxy;
    gattr.maxposition.z	= 0;
}


#if   !defined(USE_TCLTK)

const char *random_osname(int randint)
{
    return(os_names[randint % N_OS_NAMES]);
}

#else

// ----------------- only USE_TCLTK code follows -----------------------

const char *random_osname(int randint)
{
    int		n;
    int		try = randint % N_OS_NAMES;
    char	*s;
    char	gif_name[32];

    for(n=0 ; n<N_OS_NAMES ; ++n) {
	sprintf(gif_name, "%s.gif", os_names[try]);
	s	= find_cnetfile(gif_name, false, false);
	if(s) {
	    FREE(s);
	    return(os_names[try]);
	}
	try = (try+1)%N_OS_NAMES;
    }
    FATAL("no operating system icons found via CNETPATH\n");
    return(NULL);
}

static void load_image(const char *filenm)
{
    int		h, w;
    char	*path;
    char	gif_name[32];

    sprintf(gif_name, "%s.gif", filenm);
    path	= find_cnetfile(gif_name, false, true);
    TCLTK("image create photo im_%s -file %s", filenm, path);
    FREE(path);

    TCLTK("image height im_%s", filenm);
    h	= atoi(tcl_interp->result);
    if(image_maxh < h)
	image_maxh	= h;

    TCLTK("image width im_%s", filenm);
    w	= atoi(tcl_interp->result);
    if(image_maxw < w)
	image_maxw	= w;
}

static void init_node_images(void)
{
    NODE	*np;
    const char	**os_wanted;
    int		n_os_wanted;
    int		i,n;

    os_wanted	= calloc((unsigned)_NNODES, sizeof(char *));
    n_os_wanted	= 0;

    for(n=0, np=NODES ; n<_NNODES ; ++n, ++np)
	if(np->nattr.osname) {
	    for(i=0 ; i<n_os_wanted ; ++i)
		if(strcmp(os_wanted[i], np->nattr.osname) == 0)
		    break;
	    if(i == n_os_wanted) {
		load_image(np->nattr.osname);
		os_wanted[n_os_wanted++] = np->nattr.osname;
	    }
	}
    FREE(os_wanted);

    if(gattr.nrouters > 0)
	load_image("router");
    if(gattr.nmobiles > 0)
	load_image("mobile");
    if(gattr.naccesspoints > 0)
	load_image("accesspoint");

    if(gattr.nwans > 0)
	load_image("zap");

    load_image("paused");
    load_image("repair");
}

// ---------------------------------------------------------------------

static void bigX(char *canvas, int x, int y, int size, int t)
{
    typedef struct {
	int		x, y;
    } POINTLIST;

    static POINTLIST BIG[12] = {
	{ -3, -3 },
	{ -1, -3 },
	{ 0, -1 },
	{ 1, -3 },
	{ 3, -3 },
	{ 1, 0 },
	{ 3, 3 },
	{ 1, 3 },
	{ 0, 1 },
	{ -1, 3 },
	{ -3, 3 },
	{ -1, 0 }
    };

    char	buf[256], *b = buf;
    int		p;

    for(p=0 ; p<12 ; ++p) {
	sprintf(b, "%d %d ", x + BIG[p].x * size, y + BIG[p].y * size);
	while(*b) b++;
    }
    TCLTK("%s create polygon %s -fill red -outline black -tag n%d",
		canvas, buf, t);
}

// ---------------------------------------------------------------------

void draw_node_icon(int n, char *canvas, int x, int y)
{
    NODE	*np	= &NODES[n];
    int		l;
    double	theta;

    const char	*name = "";
    char	*title = "";

//  DETERMINE THE ICON/IMAGE FOR THIS NODE
    switch (np->runstate) {
    case STATE_RUNNING :
    case STATE_REBOOTING :
    case STATE_AUTOREBOOT :
    case STATE_CRASHED :

	switch (np->nodetype) {
	case NT_HOST :
	    name	= np->nattr.osname;
	    break;
	case NT_ROUTER :
	    name	= "router";
	    break;
	case NT_MOBILE :
	    name	= np->nattr.osname ? np->nattr.osname : "mobile";
	    break;
	case NT_ACCESSPOINT :
	    name	= "accesspoint";
	    break;
	}
	break;

    case STATE_UNDERREPAIR :
	name	= "repair";
	break;

    case STATE_PAUSED :
	name	= "paused";
	break;

    default :
	FATAL("no node icon for %s\n", np->nodename);
	break;
    }

    TCLTK("global map labelfont");
    if(canvas == NULL) {
//  DELETE ANY EARLIER IMAGE AND LABEL FOR THIS NODE
	TCLTK("$map delete n%d", n);
	TCLTK("$map delete nl%d", n);
	canvas	= "$map";
	x	= np->nattr.position.x;
	y	= np->nattr.position.y;
    }

//  DRAW THE ICON/IMAGE FOR THIS NODE
    TCLTK("set x [%s create image %d %d -image im_%s -anchor c -tag n%d]",
		canvas, M2PX(x), M2PX(y), name, n);
    TCLTK("%s bind $x <Button> \"node_click %d %%b %%x %%y\"", canvas, n);
#if	defined(USE_MACOSX)
    TCLTK("%s bind $x <Control-ButtonPress-1> \"node_click %d 2 %%x %%y\"",
		canvas, n);
#endif
    if(np->runstate == STATE_CRASHED)
	bigX(canvas, M2PX(x), M2PX(y), 6, n);

//  IF WANTED, DRAW THE NODE'S TITLE BELOW THE NODE'S ICON
    title	= format_nodeinfo(np, np->nattr.icontitle);
    if(title[0]) {
	TCLTK("image height im_%s", name);
	np->nattr.iconheight = l = atoi(tcl_interp->result);
	TCLTK(
    "%s create text %d %d -anchor n -font $labelfont -tag nl%d -text \"%s\"",
		canvas, M2PX(x), M2PX(y)+l/2, n, title);
    }
    FREE(title);

//  DRAW THE WAN LINK NUMBERS FOR THIS NODE
    if(strcmp(canvas, "$map") == 0)
      for(l=1 ; l<=np->nnics ; l++) {
	WAN	*wan;
	int	otherend, dx, dy;

	if(np->nics[l].linktype != LT_WAN)
	    continue;

	wan	= &WANS[ np->wans[l] ];
	otherend = (wan->minnode == n) ? wan->maxnode : wan->minnode;

	dx	= NODES[otherend].nattr.position.x - np->nattr.position.x;
	dy	= NODES[otherend].nattr.position.y - np->nattr.position.y;
	theta	= atan2((double)dy, (double)dx) + 0.3;
	x	= np->nattr.position.x + (int)(image_maxw/1.5*cos(theta));
	y	= np->nattr.position.y + (int)(image_maxh/1.5*sin(theta));

	TCLTK(
	"%s create text %d %d -anchor c -font $labelfont -tag nl%d -text %d",
		canvas, M2PX(x), M2PX(y), n, l);
    }

#if 0
{
#define	LEDSIZE	6
    int	led;

    x	= np->nattr.position.x - (int)(CNET_NLEDS * LEDSIZE / 2.5);
    y	= np->nattr.position.y - l/2 - LEDSIZE - 1;
    for(led=0 ; led<CNET_NLEDS ; ++led) {
	TCLTK("$map create rectangle %d %d %d %d -tags {nl%d map%dled%d}",
			x, y, x+LEDSIZE, y+LEDSIZE, n, n, led);
	x += LEDSIZE;
    }
}
#endif
}

#endif		// defined(USE_TCLTK)
