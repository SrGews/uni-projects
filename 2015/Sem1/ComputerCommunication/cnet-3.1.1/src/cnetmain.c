#include "cnetprivate.h"
#include <getopt.h>
#include <sys/resource.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

static	int		mflag	= 0;		// wall-clock minutes
static	char		*Fflag	= NULL;		// Filename of Tcl/Tk code
static	const char	*Rflag	= DEFAULT_REBOOT_FUNCTION; // rebootfunc name
static	bool		sflag	= false;	// cumulative statistics

#define	OPTLIST		"A:BcC:D:dEe:F:f:giI:Jm:NnOo:PpqRr:S:stTu:vWx:z"

static void usage(int status)
{
    fprintf(stderr,
	"Usage: %s [options] {TOPOLOGYFILE | - | -r nnodes} [args-to-%s]\n",
		argv0, Rflag);
    fprintf(stderr, "options are:\n");

    fprintf(stderr,
    "  -A str\tprovide the Application Layer compilation string\n");
    fprintf(stderr,
    "  -B\t\tdisable frame/packet buffering in NICs\n");
    fprintf(stderr,
    "  -c\t\tclocks in all nodes are synchronized\n");
    fprintf(stderr,
    "  -C str\tprovide the Central Layers' compilation string\n");
    fprintf(stderr,
    "  -d\t\tprint some debugging information to stderr\n");
    fprintf(stderr,
    "  -D token\tdefine C-preprocessor tokens for compilation\n");
    fprintf(stderr,
    "  -e period\texecute for the indicated period\n");
    fprintf(stderr,
    "  -E\t\treport corrupt frame arrival with ER_CORRUPTFRAME\n");
    fprintf(stderr,
    "  -f period\tset the period of some periodic activities\n");
    fprintf(stderr,
    "  -F filename\tprovide the filename of the Tcl/Tk code\n");
    fprintf(stderr,
    "  -g\t\tgo - commence execution immediately\n");
    fprintf(stderr,
    "  -i\t\tprint instantaneous non-zero statistics to stdout\n");
    fprintf(stderr,
    "  -I directory\tprovide a C-preprocessor include path\n");
    fprintf(stderr,
    "  -J file[s]\tjust compile one or more C files\n");
    fprintf(stderr,
    "  -m mins\trun for the indicated length of wall-clock time\n");
    fprintf(stderr,
    "  -n\t\tcompile and link the protocols, but do not run\n");
    fprintf(stderr,
    "  -N\t\treveal the number of nodes in the variable NNODES\n");
    fprintf(stderr,
    "  -o filename\tprovide a filename into which each node's printfs are mirrored\n");
    fprintf(stderr,
    "  -O\t\topen each node's window on startup\n");
    fprintf(stderr,
    "  -p\t\tread, check, and print the topology, then exit\n");
    fprintf(stderr,
    "  -q\t\trequest quiet execution, except during EV_DEBUGs\n");
    fprintf(stderr,
    "  -r N\t\trequest a random, connected, topology of N nodes\n");
    fprintf(stderr,
    "  -R func\tname the function to reboot each node (default is %s)\n",
		DEFAULT_REBOOT_FUNCTION);
    fprintf(stderr,
    "  -s\t\tprint cumulative non-zero statistics to stdout\n");
    fprintf(stderr,
    "  -S seed\tprovide a positive starting random seed\n");
    fprintf(stderr,
    "  -t\t\ttrace all cnet events and calls to cnet functions\n");
    fprintf(stderr,
    "  -T\t\tuse (fast) discrete-event simulation\n");
    fprintf(stderr,
    "  -U token\tundefine C-preprocessor tokens for compilation\n");
    fprintf(stderr,
    "  -u period\tspecify the update period for GUI windows\n");
    fprintf(stderr,
    "  -v\t\tprint version (%s) and verbose debugging to stderr\n",
			CNET_VERSION);
    fprintf(stderr,
    "  -W\t\tdo not use the windowing interface\n");
    fprintf(stderr,
    "  -x str\tprovide the compilation string of a code extension\n");
    fprintf(stderr,
    "  -z\t\tprint all statistics to stdout, even if zero\n");

    vflag	= 0;
    fprintf(stderr, "\nUsing the C header file %s\n",
			find_cnetfile("cnet.h", false, true));
    fprintf(stderr, "Protocols will be compiled with %s\n",
			findenv("CNETCC", CNETCC));
    fprintf(stderr, "Protocols will be linked with %s\n",
			findenv("CNETLD", CNETLD));
#if	defined(USE_TCLTK)
    if(Wflag)
	fprintf(stderr, "Using the Tcl/Tk file %s\n",
		find_cnetfile(Fflag ? Fflag : findenv("CNETTCLTK", CNETTCLTK),
			      false, true));
#endif

    fprintf(stderr, "\nPlease report any bugs to %s, %s\n",
			CNET_AUTHOR, CNET_EMAIL);
    exit(status);
}


void CLEANUP(int status)
{
    if(nerrors)
	fprintf(stderr,"\n%d error%s found.\n", nerrors, PLURAL(nerrors));

    if(gattr.tfp)				// close trace-file if open
	fclose(gattr.tfp);

    if(status == 0) {
	extern void	invoke_shutdown(int node);
	extern void	flush_allstats(bool);

	int	n;

	for(n=0 ; n<_NNODES ; ++n)
	    invoke_shutdown(n);
	if(sflag)
	    flush_allstats(true);
    }
    exit(status);
}


// -------------------------- Overtime errors --------------------------


static void signal_catcher(int sig)
{
    if(sig == SIGALRM || sig == SIGXCPU) {
	fprintf(stderr, "%s: %s limit exceeded\n(see %s)\n",
			argv0,
			sig == SIGALRM ? "time" : "simulation execution",
			WWW_FAQ);
	CLEANUP(0);
    }

    fprintf(stderr,"%s: caught signal number %d", argv0, sig);
    if(THISNODE >= 0)			// only if we've been running
	fprintf(stderr," while (last) handling %s.%s\n",
			NODES[THISNODE].nodename, cnet_evname[(int)HANDLING]);
    else
	fputc('\n',stderr);

    if(sig == SIGINT)
	CLEANUP(0);

    if(sig == SIGBUS || sig == SIGSEGV)
	fprintf(stderr, "(see %s)\n", WWW_FAQ);

    if(malloc(1024) == NULL)
	fprintf(stderr,"%s: Out of memory!\n",argv0);
    _exit(EXIT_FAILURE);
}


#if	defined(USE_TCLTK)
static void tcl_game_over(ClientData client_data)
{
    signal_catcher(SIGALRM);
}
#endif


static void init_traps(void)
{
    if(mflag > 0) {
#if	defined(USE_TCLTK)
	if(Wflag)
	    Tcl_CreateTimerHandler(mflag * 60000,	// yes, in millisecs
				(Tcl_TimerProc *)tcl_game_over, (ClientData)0);
	else
#endif
	{
	    signal(SIGALRM,	signal_catcher);
	    alarm((unsigned int)(mflag*60));	// seconds
	}
    }

    signal(SIGBUS,	signal_catcher);
    signal(SIGQUIT,	signal_catcher);

    signal(SIGINT,	signal_catcher);
    signal(SIGSEGV,	signal_catcher);
    signal(SIGILL,	signal_catcher);
    signal(SIGFPE,	signal_catcher);
}

// ----------------------------------------------------------------

int main(int argc, char **argv)
{
    extern void init_globals(bool,const char *,bool,bool,const char *,bool);
    extern void	TCLTK_init(void);
    extern void	init_application_layer(char *, int);
    extern void	init_physical_layer(bool, bool, int);
    extern void	init_stats_layer(bool, bool);
    extern void	init_stdio_layer(char *);
    extern void	init_trace(void);
    extern void	init_scheduler(const char *, const char *, bool,
				int, bool, const char *);

    extern void check_topology(int, int, int, char **);
    extern void compile_topology(char **);
    extern void motd(void);
    extern void parse_topology(char *, char **);
    extern void save_topology(const char *);
    extern void random_topology(const char *, int);
    extern void	schedule(bool single);

    int		opt;
    int		ndefines = 0;
    char	*defines[64];		// hoping that this is enough
    char	*topfile= NULL;		// topology filename

    const char	*Cflag	= DEFAULT_COMPILE_STRING;  // protocol source files

    char	*Aflag	= NULL,		// application layer file string
		*eflag	= NULL,		// period/length of execution
		*fflag	= NULL,		// period of reporting
		*oflag	= NULL,		// prefix of output filenames
		*rflag	= NULL,		// random topology
		*uflag	= UPDATE_PERIOD;// GUI update period

    bool	Bflag	= true,		// NICs are buffered
    		cflag	= false,	// synchronize time_of_day clocks
    		Eflag	= REPORT_PHYSICAL_CORRUPTION,
    		gflag	= false,	// start execution (go) immediately
		iflag	= false,	// display instantaneous statistics
		Jflag	= false,	// just compile *.c -> *.o
		Nflag	= false,	// divulge NNODES
		nflag	= false,	// compile, link, exit
		Oflag	= false,	// open all node windows
		pflag	= false,	// display topology, exit
		qflag	= false,	// quiet execution (except EV_DEBUGs)
		tflag	= false,	// trace event handlers
		zflag	= false;	// display zero statistics

    int		Sflag	= 0;		// random seed


    argv0		= (argv0 = strrchr(argv[0],'/')) ? argv0+1 : argv[0];
    defines[0]		= NULL;
    Sflag		= time(NULL) | getpid();

    opterr	= 0;
    while((opt = getopt(argc, argv, OPTLIST)) != -1) {
	switch (opt) {

// USE MY APPLICATION LAYER
	case 'A' :  Aflag = strdup(optarg);
		    break;

// DISABLE FRAME/PACKET BUFFERING IN NICs
	case 'B' :  Bflag = !Bflag;
		    break;

// KEEP CLOCKS SYNCHRONIZED
	case 'c' :  cflag = !cflag;
		    break;

// COMPILE THIS PROTOCOL SOURCE STRING
	case 'C' :  Cflag = strdup(optarg);
		    break;

// DEBUG PRINTING TO STDERR
	case 'd' :  dflag = !dflag;
		    break;

// ACCEPT SOME CPP SWITCHES
	case 'D' :
	case 'U' :
	case 'I' :  defines[ndefines++] = strdup(optarg);
		    break;

// EXECUTE FOR A SPECIFIC PERIOD
	case 'e' :  if(!isdigit(*optarg)) {
			fprintf(stderr,"%s: invalid period for -e '%s'\n",
					argv0, optarg);
			argc	= 0;
		    }
		    else
			eflag = strdup(optarg);
		    break;

// REPORT OPTION_CHECKSUM ERRORS WITH ER_CORRUPTFRAME
	case 'E' :  Eflag = !Eflag;
		    break;

// PROVIDE TCL/TK FILENAME
	case 'F' :  Fflag = strdup(optarg);
		    break;

// PERIOD OF SCHEDULER REPORTING
	case 'f' :  if(!isdigit(*optarg)) {
			fprintf(stderr,"%s: invalid period for -f '%s'\n",
					argv0, optarg);
			argc	= 0;
		    }
		    else
			fflag = strdup(optarg);
		    break;

// COMMENCE EXECUTION (go) IMMEDIATELY
	case 'g' :  gflag = !gflag;
		    break;

// PRINT INSTANTANEOUS EXECUTION STATISTICS
	case 'i' :  iflag = !iflag;
		    break;

// JUST COMPILE INDIVIDUAL C FILES INTO OBJECT FILES
	case 'J' :  Jflag = !Jflag;
		    break;

// RUN FOR mflag MINUTES OF WALL-CLOCK TIME
	case 'm' :  mflag	= atoi(optarg);
		    if(mflag < 0) {
			fprintf(stderr,"%s : invalid # of minutes\n",argv0);
			argc = 0;
		    }
		    break;

// COMPILE AND LINK but DO NOT RUN
	case 'n' :  nflag = !nflag;
		    break;

// REVEAL THE NUMBER OF NNODES TO STUDENTS' PROTOCOLS
	case 'N' :  Nflag = !Nflag;
		    break;

// OUTPUT FILENAME
	case 'o' :  oflag = strdup(optarg);
		    break;

//  OPEN ALL NODE WINDOWS
	case 'O' :  Oflag = !Oflag;
		    break;

//  PRINT TOPOLOGY
	case 'p' :  pflag = !pflag;
		    break;

//  DEPRECATED
	case 'P' :  fprintf(stderr, "%s: -P flag no longer supported\n", argv0);
		    argc = 0;
		    break;

// QUIET EXECUTION (EXCEPT DURING EV_DEBUGs)
	case 'q' :  qflag = !qflag;
		    break;

// RANDOM TOPOLOGY - ALL FOLLOWING ARGUMENTS ARE PASSED TO reboot_node()
	case 'r' :  rflag = strdup(optarg);
		    goto args_done;
		    break;

// PROVIDE reboot_func() FUNCTION NAME
	case 'R' :  Rflag = strdup(optarg);
		    break;

// PRINT CUMULATIVE EXECUTION STATISTICS
	case 's' :  sflag = !sflag;
		    break;

// PROVIDE RANDOM SEED
	case 'S' :  if(!isdigit(*optarg)) {
			fprintf(stderr,"%s: invalid random seed '%s'\n",
					argv0, optarg);
			argc	= 0;
		    }
		    else
			Sflag	= (unsigned long)atoi(optarg);
		    break;

// TRACE EVENTS
	case 't' :  tflag = !tflag;
		    break;

// TOGGLE USE OF (FAST) DISCRETE-EVENT SIMULATION
	case 'T' :  Tflag = !Tflag;
		    break;

// SPECIFY THE PERIOD OF GUI WINDOW UPDATES 
	case 'u' :  if(!isdigit(*optarg)) {
			fprintf(stderr,"%s: invalid period for -u '%s'\n",
					argv0, optarg);
			argc	= 0;
		    }
		    else
			uflag = strdup(optarg);
		    break;

// VERBOSE DEBUG PRINTING TO STDERR
	case 'v' :  ++vflag;
		    break;

// TOGGLE USE OF THE WINDOWING ENVIRONMENT
	case 'W' :  Wflag = false;
		    break;

// ACCEPT MULTIPLE CODE EXTENSIONS
	case 'x' :  if(gattr.nextensions < MAXEXTENSIONS)
			gattr.extensions[gattr.nextensions++] = strdup(optarg);
		    break;

// DISPLAY STATISTICS EVEN IF ZERO
	case 'z' :  zflag = !zflag;
		    break;

	default :   fprintf(stderr,"%s : illegal option -%c\n", argv0,optopt);
		    argc = 0;
		    break;
	}
    }
    if(argc <= 0 || optind == argc)
	usage(1);

args_done:
    defines[ndefines]	= NULL;

    if(vflag) {
	dflag	= true;
	REPORT("%s\n", CNET_VERSION);
    }
    if(dflag)
	REPORT("seed %d\n", Sflag);

// JUST COMPILE INDIVIDUAL C FILES INTO OBJECT FILES
    if(Jflag) {
	extern void	just_compile(char **defines, int argc, char **argv);
	extern char	*compile_string(char **, const char *, bool);

	while(optind < argc) {
	    compile_string(defines, argv[optind], false);
	    ++optind;
	}
	return(EXIT_SUCCESS);
    }

#if	defined(USE_TCLTK) && !defined(USE_MACOSX)
    if(Wflag && findenv("DISPLAY", NULL) == NULL) {
	WARNING("DISPLAY variable not defined (setting -W option)\n");
	Wflag	= false;
    }
#endif
    if(Wflag == false)		// no windowing => stdio is quiet
	qflag	= true;

    motd();
    init_globals(Bflag, Cflag, Oflag, qflag, Rflag, tflag);

#if	defined(USE_TCLTK)
    if(Wflag)
	TCLTK_init();
#endif

    if(rflag)
	random_topology(rflag, Sflag);
    else {
	char	*dot;

	if((dot=strrchr(argv[optind],'.')) && strcmp(dot,".c") == 0) {
	    fprintf(stderr,
		"%s: hmmm, '%s' looks like a C file, not a topology file.\n",
				    argv0, argv[optind]);
	    CLEANUP(1);
	}

	topfile	= strdup(argv[optind]);
	++optind;

	parse_topology(topfile, defines);
    }

    check_topology(cflag, Sflag, argc-optind, &argv[optind]);
    if(pflag) {
	save_topology(NULL);
	exit(EXIT_SUCCESS);
    }

    compile_topology(defines);
    if(nflag)
	exit(EXIT_SUCCESS);

    if(eflag == NULL) {
	eflag	= SIMULATION_LENGTH;
	if(mflag == 0)
	    mflag	= DEFAULT_mflag_MINUTES;
    }

    init_application_layer(Aflag, Sflag);
    init_physical_layer(Eflag, Nflag, Sflag);
    init_stats_layer(iflag, zflag);
    init_stdio_layer(oflag);
    init_trace();
    init_scheduler(eflag, fflag, Nflag, Sflag, sflag, uflag);

#if	defined(USE_TCLTK)
    if(Wflag) {
	extern void	init_mainwindow(char *, int, int, char *);
	extern void	init_statswindows(void);
	extern void	init_nodewindow(int);

	int	n;
	char	winname[64];

	if(rflag)
	    sprintf(winname, "random(%s)", rflag);
	init_mainwindow(Fflag, gflag, tflag, rflag ? winname : topfile);
	init_statswindows();

	for(n=DEFAULT ; n<_NNODES ; ++n)
	    init_nodewindow(n);

	if(gflag) {
	    init_traps();
	    cnet_state = STATE_RUNNING;
	}
	else
	    cnet_state = STATE_PAUSED;

	while(cnet_state != STATE_UNKNOWN) {
	    switch (cnet_state) {

	    case STATE_PAUSED: {
		static int traps_set = false;

		tcltk_notify_start();
		if(traps_set == false) {
		    init_traps();
		    traps_set	= true;
		}
		break;
	    }
	    case STATE_RUNNING:
		schedule(false);
		break;
	    case STATE_SINGLESTEP: {
		cnet_state = STATE_RUNNING;
		schedule(true);
		cnet_state = STATE_PAUSED;
		break;
	    }
	    case STATE_GAMEOVER:
		signal_catcher(SIGXCPU);	// -e period has elapsed
		break;
	    default :
		cnet_state = STATE_UNKNOWN;
		break;
	    }
	}
    }
    else
#endif
    {
	extern	int	fileno(FILE *fp);

	init_traps();
	if(dflag || isatty(fileno(stdout)))
	    REPORT("running\n");
	cnet_state = STATE_RUNNING;
	schedule(false);
    }
    CLEANUP(0);
    return(EXIT_SUCCESS);
}
