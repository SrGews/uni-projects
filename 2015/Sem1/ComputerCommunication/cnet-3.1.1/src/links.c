#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

typedef struct _n {
    CnetLinkType	linktype;
    char		*name;
    NICATTR		nic;
    struct _n		*next;
} NICLIST;

static NICLIST	*nic_head	= NULL;

NICATTR *add_linktype(CnetLinkType linktype, char *name)
{
    NICLIST		*new	= nic_head;

    while(new) {
	if(strcasecmp(name, new->name) == 0) {
	    WARNING("link type '%s' redefined\n", name);
	    break;
	}
	new	= new->next;
    }
    new			= NEW(NICLIST);
    new->linktype	= linktype;
    new->name		= strdup(name);

    switch (linktype) {
	case LT_LOOPBACK:					break;
	case LT_WAN:	init_nicattrs(&new->nic, NULL, NULL);	break;
	case LT_LAN:	init_nicattrs(NULL, &new->nic, NULL);	break;
	case LT_WLAN:	init_nicattrs(NULL, NULL, &new->nic);	break;
    }

    new->next		= nic_head;
    nic_head		= new;

    return(&nic_head->nic);
}

NICATTR *find_linktype(char *name, CnetLinkType *type)
{
    NICLIST	*nlp = nic_head;

    while(nlp) {
	if(strcasecmp(name, nlp->name) == 0) {
	    *type	= nlp->linktype;
	    return(&nlp->nic);
	}
	nlp	= nlp->next;
    }
    return(NULL);
}

void print_linktypes(FILE *topfp)
{
    extern void print_nic1(FILE *, const char *, int,
			   NICATTR *, NICATTR *, const char *);

    NICLIST	*ltp	= nic_head;

    while(ltp) {
	switch (ltp->linktype) {
	case LT_LOOPBACK:
	    break;

	case LT_WAN:
	    fprintf(topfp,"wantype %s {\n", ltp->name);
	    print_nic1(topfp, "wan", false, &ltp->nic, &DEFAULTWAN, "    ");
	    break;

	case LT_LAN:
	    fprintf(topfp,"lantype %s {\n", ltp->name);
	    print_nic1(topfp, "lan", false, &ltp->nic, &DEFAULTLAN, "    ");
	    break;

	case LT_WLAN:
	    fprintf(topfp,"wlantype %s {\n", ltp->name);
	    print_nic1(topfp, "wlan", false, &ltp->nic, &DEFAULTWLAN, "    ");
	    break;
	}
	fprintf(topfp,"}\n\n");
	ltp	= ltp->next;
    }
}
