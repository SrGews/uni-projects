#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


//  ------------ Nothing in here for USE_ASCII compilation ------------

#if	defined(USE_TCLTK)

static TCLTK_COMMAND(exit_button)
{
    cnet_state  = STATE_UNKNOWN;
    CLEANUP(0);
    return TCL_OK;
}

static TCLTK_COMMAND(run_pause_step)
{
    if(strcmp(argv[1], "run") == 0) {
        if(cnet_state == STATE_PAUSED) {
	    TCLTK(".cnet.mb.cnet entryconfigure 0 -label pause");
            cnet_state  = STATE_RUNNING;
            tcltk_notify_stop();
        }
        else if(cnet_state == STATE_RUNNING) {
	    TCLTK(".cnet.mb.cnet entryconfigure 0 -label run");
            cnet_state  = STATE_PAUSED;
        }
    }
    else /* (argv[1] == "step") */ {
	TCLTK(".cnet.mb.cnet entryconfigure 0 -label run");
        cnet_state      = STATE_SINGLESTEP;
        tcltk_notify_stop();
    }
    return TCL_OK;
}

static TCLTK_COMMAND(save_button)
{
    extern void	save_topology(const char *);

    save_topology(argc > 1 ? argv[1] : NULL);
    return TCL_OK;
}

// ------------------ EVENT_HANDLE/REFRESH/REDRAW canvas-------------------

TCLTK_COMMAND(mouse_position)
{
    int	x, y;

    TCLTK_ARGCHECK(3);
    x	= atoi(argv[1]);
    y	= atoi(argv[2]);
    if(x > 0 && y > 0) {
	sprintf(chararray," %d,%d", PX2M(x), PX2M(y) );
	Tcl_SetVar(tcl_interp, "CN_POSITION", chararray, TCL_GLOBAL_ONLY);
    }
    return TCL_OK;
}

TCLTK_COMMAND(node_click)
{
    extern void	display_nodemenu(int);
    int		button, evx, evy;
    int		ac;

    TCLTK_ARGCHECK(5);
    ac		= 1;
    n		= atoi(argv[ac++]);
    button	= atoi(argv[ac++]);
    evx		= atoi(argv[ac++]);
    evy		= atoi(argv[ac++]);

    if(button == LEFT_BUTTON)
	TCLTK("toggleNode %d", n);
    else if(cnet_state == STATE_RUNNING)
	display_nodemenu(n);

    return TCL_OK;
}

static void draw_map(void)
{
    extern void	draw_wans(void);
    extern void	draw_lans(void);

    int		n;

//  DRAW THE BACKGROUP IMAGE IF WE HAVE DEFINED ONE
    if(gattr.mapimage) {
	TCLTK("$map configure -background white");
	TCLTK("$map create image %d %d -image im_map -anchor c",
		    gattr.mapwidth/2, gattr.mapheight/2);	// in pixels
    }
//  OR THE BACKGROUND TILE
    else if(gattr.maptile) {
	int	x, y;

	TCLTK("$map configure -background white");
	for(x=0 ; x<gattr.maxposition.x ; x += gattr.mapwidth)
	    for(y=0 ; y<gattr.maxposition.y ; y += gattr.mapheight)
		TCLTK("$map create image %d %d -image im_map -anchor nw", x, y);
    }
//  OR THE SOLID BACKGROUND COLOUR IS ALWAYS DRAWN
    else if(gattr.mapcolour[0] != '\0')
	TCLTK("$map configure -background %s", gattr.mapcolour);

//  DRAW THE BACKGROUD HEX PATTERN
    if(gattr.maphex) {
	int	x  = 0;
	int	y  = 0;
	int	dx = (int)(1.5*gattr.maphex);
	int	dy = (int)(sin(60.0/M_PI_2)*gattr.maphex);

	while(y < gattr.maxposition.y) {
	    x	= 0;
	    while(x < gattr.maxposition.x) {
		TCLTK("$map create line %d %d %d %d -fill \"%s\"",
				    x, y, x+gattr.maphex-1, y, COLOUR_GRID);
		TCLTK("$map create line %d %d %d %d -fill \"%s\"",
				    x+gattr.maphex, y,
				    x+dx, y-dy, COLOUR_GRID);
		TCLTK("$map create line %d %d %d %d -fill \"%s\"",
				    x+gattr.maphex+1, y,
				    x+dx, y+dy-1, COLOUR_GRID);
		x	+= 2*dx;
	    }
	    y	+= dy;

	    x	= dx;
	    while(x < gattr.maxposition.x) {
		TCLTK("$map create line %d %d %d %d -fill \"%s\"",
				    x, y, x+gattr.maphex-1, y, COLOUR_GRID);
		TCLTK("$map create line %d %d %d %d -fill \"%s\"",
				    x+gattr.maphex, y,
				    x+dx, y-dy, COLOUR_GRID);
		TCLTK("$map create line %d %d %d %d -fill \"%s\"",
				    x+gattr.maphex+1, y,
				    x+dx, y+dy-1, COLOUR_GRID);
		x	+= 2*dx;
	    }
	    y	+= dy;
	}
    }
//  OR THE BACKGROUD GRID PATTERN
    else if(gattr.mapgrid) {
	int	x = gattr.mapgrid;
	int	y = gattr.mapgrid;

	while(x < gattr.maxposition.x) {
	    TCLTK("$map create line %d %d %d %d -fill \"%s\"",
			M2PX(x), 0,
			M2PX(x), M2PX(gattr.maxposition.y),
			COLOUR_GRID);
	    x += gattr.mapgrid;
	}
	while(y < gattr.maxposition.y) {
	    TCLTK("$map create line %d %d %d %d -fill \"%s\"",
			0, M2PX(y),
			M2PX(gattr.maxposition.x), M2PX(y),
			COLOUR_GRID);
	    y += gattr.mapgrid;
	}
    }

//  DRAW ALL OF THE LANS
    if(gattr.drawlinks && gattr.nlans > 0)
	draw_lans();

//  DRAW ALL OF THE WANS
    if(gattr.drawlinks && gattr.nwans > 0)
	draw_wans();

//  DRAW THE NIC NUMBERS AROUND EACH NODE AND THE NODE ICONS
    if(gattr.drawnodes)
	for(n=0 ; n<_NNODES ; ++n)
	    draw_node_icon(n, NULL, 0, 0);

    TCLTK("$map create text 4 2 -anchor nw -font \"%s\" -text \"%s\"",
		EMAIL_FONT, CNET_VERSION);
}


// ------------------------------------------------------------------------

char *NICdesc(NICATTR *nic)
{
    char	desc[128], *p=desc;

    sprintf(p, "bandwidth %sbps, ",	CNET_format64(nic->bandwidth));
    while(*p)
	++p;
    sprintf(p, "propagation %susecs",	CNET_format64(nic->propagation));
    return(strdup(desc));
}

void init_mainwindow(const char *Fflag, int gflag, int tflag, char *topology)
{
    extern int	nic_created(ClientData, Tcl_Interp *, int, char **);
    extern int	tk_stdio_input(ClientData, Tcl_Interp *, int, char **);
    extern int	toggle_drawwlans(ClientData, Tcl_Interp *, int, char **);

    char	*tcltk_source;

    if(Fflag == NULL)
	Fflag	= findenv("CNETTCLTK", CNETTCLTK);
    tcltk_source = find_cnetfile(Fflag, false, true);
    if(dflag)
	REPORT("reading \"%s\"\n\n", tcltk_source);
    if(Tcl_EvalFile(tcl_interp, tcltk_source) != TCL_OK) {
	if(*tcl_interp->result)
	    FATAL("%s\n", tcl_interp->result);
	CLEANUP(1);
    }
    FREE(tcltk_source);

    sprintf(chararray, "%d", N_CNET_EVENTS);
    Tcl_SetVar(tcl_interp, "N_CNET_EVENTS", strdup(chararray), 0);

    TCLTK_createcommand("exit_button",		exit_button);
    TCLTK_createcommand("mouse_position",	mouse_position);
    TCLTK_createcommand("nic_created",		nic_created);
    TCLTK_createcommand("node_click",		node_click);
    TCLTK_createcommand("run_pause_step",	run_pause_step);
    TCLTK_createcommand("save_topology",	save_button);
    TCLTK_createcommand("stdio_input",		tk_stdio_input);
    TCLTK_createcommand("toggle_drawwlans",	toggle_drawwlans);

    TCLTK(
"initworld %s \"%s\" %d %d %s %d {%s %s %s %s} {%d %d %d} {\"%s\" \"%s\" \"%s\"}",
	topology,
	gattr.mapcolour,
	M2PX(gattr.maxposition.x)+1, M2PX(gattr.maxposition.y)+1,
	gflag ? "pause" : "run",
	Tflag,
	"exit_button", "run_pause_step", "save_topology", "toggle_drawwlans",
	gattr.nwans, gattr.nlans, gattr.nwlans,
	gattr.nwans  > 0 ? NICdesc(&DEFAULTWAN)  : "",
	gattr.nlans  > 0 ? NICdesc(&DEFAULTLAN)  : "",
	gattr.nwlans > 0 ? NICdesc(&DEFAULTWLAN) : "" );

    draw_map();
    if(tflag)
	TCLTK("toggleTrace \"%s\"", topology);
}
#endif		// defined(USE_TCLTK)
