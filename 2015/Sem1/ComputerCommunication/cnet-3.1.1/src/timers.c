#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// --------------------------- Timer Layer -------------------------------


//  WE DO *NOT* START TIMERS AT 0, IN AN ATTEMPT TO TRAP ANY INCORRECT TIMERS
void reboot_timer_layer(void)
{
    NODES[THISNODE].nexttimer	= 1000;
}


CnetTimerID CNET_start_timer(CnetEvent ev, CnetTime usec, CnetData data)
{
    CnetTimerID	timer	= NULLTIMER;

    if(usec <= 0)
	ERROR(ER_BADARG);
    else if(!IS_TIMER(ev))
	ERROR(ER_BADEVENT);
    else {
	timer	= (CnetTimerID)NODES[THISNODE].nexttimer++ ;
	newevent(ev, THISNODE, usec, timer, data);
    }
    
    if(gattr.trace_events) {
	if(timer != NULLTIMER)
	    TRACE(0, "\t%s(%s,usec=%s,data=%ld) = %ld\n",
			__func__,
			cnet_evname[(int)ev], CNET_format64(usec),
			data, timer);
	else
	    TRACE(1, "\t%s(ev=%d,usec=%s,data=%ld) = %ld\n",
			__func__,
			(int)ev, CNET_format64(usec),
			data, cnet_errname[(int)cnet_errno]);
    }
    return timer;
}


int CNET_stop_timer(CnetTimerID timer)
{
    extern bool	unschedule_timer(CnetTimerID, CnetData *, bool);

    int	result	= -1;

    if(unschedule_timer(timer, NULL, true) == true)
	result	= 0;
    else
	ERROR(ER_BADTIMERID);

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(t=%u) = 0\n", __func__, (unsigned)timer);
	else
	    TRACE(1, "\t%s(t=%u) = -1 %s\n",
			__func__,
			(unsigned)timer, cnet_errname[(int)cnet_errno]);
    }
    return result;
}


int CNET_timer_data(CnetTimerID timer, CnetData *data)
{
    extern bool	unschedule_timer(CnetTimerID, CnetData *, bool);

    int	result	= -1;

    if(data == NULL)
	ERROR(ER_BADARG);
    else if(unschedule_timer(timer, data, false) == true)
	result = 0;
    else
	ERROR(ER_BADTIMERID);

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(t=%u,%s) = 0\n",
			__func__, (unsigned)timer, find_trace_name(data));
	else
	    TRACE(1, "\t%s(t=%u,%s) = -1 %s\n",
			__func__, (unsigned)timer,
			find_trace_name(data), cnet_errname[(int)cnet_errno]);
    }
    return result;
}
