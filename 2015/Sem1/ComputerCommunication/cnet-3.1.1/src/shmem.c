#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

void *CNET_shmem(size_t wanted)
{
    static void		*addr	= NULL;
    static size_t	length	= 0;

    void		*result	= NULL;

    if(wanted == 0)
	ERROR(ER_BADARG);
    else if(length != 0 && wanted > length)
	ERROR(ER_BADSIZE);
    else if(length == 0) {
	addr = calloc((unsigned)1, (size_t)wanted);
	if(addr == NULL)
	    ERROR(ER_BADALLOC);
	else {
	    result	= addr;
	    length	= wanted;
	}
    }
    else
	result	= addr;

    if(gattr.trace_events) {
	if(result != NULL)
	    TRACE(0, "\t%s(len=%u) = 0%0xs\n", __func__, wanted, result);
	else
	    TRACE(1, "\t%s(len=%u) = -1 %s\n",
			__func__, wanted, cnet_errname[(int)cnet_errno]);
    }
    return result;
}
