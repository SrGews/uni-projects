#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

const char *cnet_evname[N_CNET_EVENTS] = {
    "EV_NULL",			"EV_REBOOT",		"EV_SHUTDOWN",
    "EV_APPLICATIONREADY",	"EV_PHYSICALREADY",	"EV_FRAMECOLLISION",
    "EV_KEYBOARDREADY",		"EV_LINKSTATE",		"EV_DRAWFRAME",
    "EV_PERIODIC",		"EV_UPDATEGUI",

    "EV_DEBUG0", "EV_DEBUG1", "EV_DEBUG2", "EV_DEBUG3", "EV_DEBUG4",

    "EV_TIMER0", "EV_TIMER1", "EV_TIMER2", "EV_TIMER3", "EV_TIMER4",
    "EV_TIMER5", "EV_TIMER6", "EV_TIMER7", "EV_TIMER8", "EV_TIMER9"
};


static int check_handler(CnetEvent ev, long handler, CnetData data,
			 const char *name)
{
    int		result = -1;

    if((int)ev < (int)EV_NULL || (int)ev >= N_CNET_EVENTS)
	ERROR(ER_BADEVENT);
    else if(ev == EV_APPLICATIONREADY && !NODE_HAS_AL(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else if(ev == EV_KEYBOARDREADY && !NODE_HAS_KB(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else
	result = 0;

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(%s,%s,data=%ld) = 0\n",
			name, cnet_evname[(int)ev],
			find_trace_name((void *)handler), data);
	else
	    TRACE(1, "\t%s(ev=%d,%s,data=%ld) = -1 %s\n",
			name, (int)ev,
			find_trace_name((void *)handler), data,
			cnet_errname[(int)cnet_errno]);
    }
    return result;
}

int CNET_set_handler(CnetEvent ev,
		     void (*handler)(CnetEvent, CnetTimerID, CnetData),
		     CnetData data)
{
    if(check_handler(ev, (long)handler, (long)data, __func__) == 0) {
	NODES[THISNODE].handler[(int)ev - (int)EV_NULL] = handler;
	NODES[THISNODE].data   [(int)ev - (int)EV_NULL] = data;
	return 0;
    }
    return(-1);
}

int CNET_get_handler(CnetEvent ev,
		     void (*handler)(CnetEvent, CnetTimerID, CnetData),
		     CnetData *data)
{
    if(check_handler(ev, (long)handler, (long)data, __func__) == 0) {
	if(handler)
	    handler	= NODES[THISNODE].handler[(int)ev - (int)EV_NULL];
	if(data)
	    *data	= NODES[THISNODE].data   [(int)ev - (int)EV_NULL];
	return 0;
    }
    return(-1);
}
