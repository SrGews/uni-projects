#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

void CNET_check_version(const char *using)
{
    if(strcmp(using, CNET_VERSION) == 0)
	return;

    fprintf(stderr,"This is '%s'.\n", CNET_VERSION);
    fprintf(stderr,"Your protocol includes the header file from '%s'.\n",using);
    fprintf(stderr,
	    "Ensure that your protocols include the correct header file,\n");
    fprintf(stderr,
	    "remove all *.o and *%s files, and re-run the simulation.\n",
				    findenv("CNETFILEEXT",CNETFILEEXT));
    exit(EXIT_FAILURE);
}

void CNET_exit(const char *filenm, const char *function, int lineno)
{
    NODE	*np = &NODES[THISNODE];

    sprintf(chararray,
"Error @%" PRIdCnetTime "usec while executing %s, file %s, line %d, function %s() -\n%s: %s",
	MICROSECONDS, np->nodename, filenm, lineno, function,
	cnet_errname[cnet_errno], cnet_errstr[cnet_errno]);

    fprintf(stderr,"%s\n", chararray);

#if defined(USE_TCLTK)
    if(Wflag) {
	char *str	= strdup(chararray);

	TCLTK("show_error %s \"%s\" \"%s\" %d", argv0, str, filenm, lineno);
	FREE(str);
    }
#endif
    exit(EXIT_FAILURE);
}
