#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

// ---------------------------------------------------------------------

static void init_nodeattrs(NODEATTR *new, NODEATTR *from)
{
    if(from) {
	memcpy(new, from, sizeof(NODEATTR));
	if(from->messagerate == 0)
	    new->messagerate	= DEFAULT;
	if(from->minmessagesize == 0)
	    new->minmessagesize	= DEFAULT;
	if(from->maxmessagesize == 0)
	    new->maxmessagesize	= DEFAULT;
    }
    else {
	memcpy(new, &DEFAULTNODE, sizeof(NODEATTR));
	new->messagerate	= DEFAULT;
	new->minmessagesize	= DEFAULT;
	new->maxmessagesize	= DEFAULT;
    }
}

int add_node(CnetNodeType nodetype, char *name, bool defn, NODEATTR *defaults)
{
    NODE	*np;
    int		 n;

    for(n=0, np=NODES ; n<_NNODES ; n++, np++)
	if(strcasecmp(name, np->nodename) == 0) {
	    if(defn) {
		if(np->nodetype != (CnetNodeType)UNKNOWN)
		    FATAL("attempt to redefine node '%s'\n", name);
		goto defining;
	    }
	    return(n);				// node seen before
	}

    if(strlen(name) >= MAX_NODENAME_LEN) {
	WARNING("node name '%s' is too long (max is %d)\n",
		    name, MAX_NODENAME_LEN-1);
	++nerrors;				// continue anyway
    }

    NODES		= realloc(NODES, (unsigned)(_NNODES+1)*sizeof(NODE));
    n			= _NNODES++;
    np			= &NODES[n];

    memset(np, 0, sizeof(NODE));
    np->nodename	= strdup(name);
    np->nodetype	= (CnetNodeType)UNKNOWN;

    np->nics		= NEW(NICATTR);
    np->nics[LT_LOOPBACK].linktype	= LT_LOOPBACK;
    np->nics[LT_LOOPBACK].up		= true;
    np->nics[LT_LOOPBACK].mtu		= MAX_MESSAGE_SIZE + 1024;

    np->wans		= NEW(int);
    np->lans		= NEW(int);

    np->nnics		= 0;
    np->nwans		= 0;
    np->nlans		= 0;
    np->nwlans		= 0;
    np->runstate	= STATE_REBOOTING;

    init_nodeattrs(&np->nattr, defaults);
    np->nattr.battery_volts	= (nodetype==NT_MOBILE) ?
				    DEFAULTNODE.battery_volts : 0.0;
    np->nattr.battery_mAH	= (nodetype==NT_MOBILE) ?
				    DEFAULTNODE.battery_mAH : 0.0;
    init_nicattrs(&np->defaultwan, &np->defaultlan, &np->defaultwlan);

defining:
    if(defn == true)
	np->nodetype	= nodetype;

    np->nattr.address	= (CnetAddr)n;
    return(n);
}

// ------------------------------------------------------------------------

typedef struct _n {
    char		*name;
    CnetNodeType	nodetype;
    NODEATTR		nattr;
    struct _n		*next;
} NTLIST;

static NTLIST	*nt_head	= NULL;

NODEATTR *add_nodetype(CnetNodeType ntype, char *name)
{
    NTLIST	*new = nt_head;

    while(new) {
	if(strcasecmp(name, new->name) == 0) {
	    WARNING("nodetype '%s' redefined\n", name);
	    break;
	}
	new	= new->next;
    }
    new			= NEW(NTLIST);
    new->name		= strdup(name);
    new->nodetype	= ntype;
    init_nodeattrs(&new->nattr, NULL);

    new->next		= nt_head;
    nt_head		= new;
    return(&nt_head->nattr);
}

NODEATTR *find_nodetype(char *name, CnetNodeType *type)
{
    NTLIST	*ntp = nt_head;

    while(ntp) {
	if(strcasecmp(name, ntp->name) == 0) {
	    *type	= ntp->nodetype;
	    return(&ntp->nattr);
	}
	ntp	= ntp->next;
    }
    return(NULL);
}

void print_nodetypes(FILE *topfp)
{
    extern void print_node_attr(int always, NODEATTR *me, const char *indent);

    NTLIST	*ntp = nt_head;
    char	*word;

    while(ntp) {
	switch (ntp->nodetype) {
	case NT_ACCESSPOINT :	word = "accesspointtype";	break;
	case NT_HOST :		word = "hosttype";		break;
	case NT_MOBILE :	word = "mobiletype";		break;
	case NT_ROUTER :	word = "routertype";		break;
	}
	fprintf(topfp, "accesspointtype %s {\n", ntp->name);
	print_node_attr(false, &ntp->nattr, "    ");
	fprintf(topfp, "}\n\n");
	ntp	= ntp->next;
    }
}


// ------------------------------------------------------------------------

int CNET_get_nodestats(CnetNodeStats *stats)
{
    int	result = 0;

    if(stats == NULL) {
	ERROR(ER_BADARG);
	result	= -1;
    }
    else
	memcpy(stats, &NODES[THISNODE].nodestats, sizeof(CnetNodeStats));

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(%s) = 0\n", __func__, find_trace_name(stats));
	else
	    TRACE(1, "\t%s(NULL) = -1 %s\n",
			__func__, cnet_errname[(int)cnet_errno]);
    }
    return result;
}
