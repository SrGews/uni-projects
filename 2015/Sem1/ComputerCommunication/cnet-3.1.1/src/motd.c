#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */



/*  The motd() function will be called early from main() to give you
    a chance to provide any message-of-the-day to students.  Examples may
    include any recently discovered problems (?) or modifications to
    project requirements.
 */


#define	MOTD_FILE	"/cslinux/examples/it312/cnet.motd"

void motd(void)			// called from cnetmain.c:main()
{
#if	MOTD_WANTED
    char	line[BUFSIZ];
    FILE	*fp;

    if((fp = fopen(MOTD_FILE, "r")) != NULL) {
	while(fgets(line, sizeof(line), fp) != NULL)
	    fputs(line, stdout);
	fclose(fp);
    }
#endif
}
