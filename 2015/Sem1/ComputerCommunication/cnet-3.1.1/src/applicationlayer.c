#include "cnetprivate.h"
#include <dlfcn.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// ---------------- Application Layer Interface -------------------------

extern	int	std_init_application_layer(int);
extern	int	std_application_bounds(int *minmsg, int *maxmsg);
extern	int	std_reboot_application_layer(CnetTime *);
extern	int	std_poll_application(CnetTime *);
extern	int	std_CNET_read_application(CnetAddr *,char *, size_t *);
extern	int	std_CNET_write_application(char *, size_t *);
extern	int	std_CNET_enable_application(CnetAddr);
extern	int	std_CNET_disable_application(CnetAddr);

static	int	(*INIT_APPLICATION_LAYER)(int)				=
		std_init_application_layer;
static	int	(*APPLICATION_BOUNDS)(int *, int *)			=
		std_application_bounds;
static	int	(*REBOOT_APPLICATION_LAYER)(CnetTime *)			=
		 std_reboot_application_layer;
static	int	(*POLL_APPLICATION)(CnetTime *)				=
		std_poll_application;
static	int	(*READ_APPLICATION)(CnetAddr *, char *, size_t *)	=
		std_CNET_read_application;
static	int	(*WRITE_APPLICATION)(char *, size_t *)			=
		std_CNET_write_application;
static	int	(*ENABLE_APPLICATION)(CnetAddr)				=
		std_CNET_enable_application;
static	int	(*DISABLE_APPLICATION)(CnetAddr)			=
		std_CNET_disable_application;

static	char *PROTOTYPES[] = {
    "std_init_application_layer(int)",
    "std_application_bounds(int *minmsg, int *maxmsg)",
    "std_reboot_application_layer(CnetTime *)",
    "std_poll_application(CnetTime *)",
    "std_CNET_read_application(CnetAddr *,char *, size_t *)",
    "std_CNET_write_application(char *, size_t *)",
    "std_CNET_enable_application(CnetAddr)",
    "std_CNET_disable_application(CnetAddr)"
};

#define	LOAD(fn)				\
	name	= strdup(PROTOTYPES[f]);	\
	b	= strchr(name, '(');		\
	*(int **)(&fn) = dlsym(handle, name);	\
	if(fn == NULL)				\
	 FATAL("cannot find the function '%s' in %s\n",PROTOTYPES[f],so_file); \
	FREE(name);				\
	++f

void init_application_layer(char *Aflag, int Sflag)
{
    if(Aflag) {
	extern char	*compile_string(char **, const char *, bool);
	char		*so_file;
	void		*handle;
	char		*name, *b;
	int		f	= 0;

	if((so_file = compile_string(NULL, Aflag, true)) == NULL)
	    CLEANUP(1);

	if((handle = dlopen(so_file, RTLD_LAZY)) == NULL)
	    FATAL("cannot load application layer from %s\n", so_file);

	LOAD(INIT_APPLICATION_LAYER);
	LOAD(APPLICATION_BOUNDS);
	LOAD(REBOOT_APPLICATION_LAYER);
	LOAD(POLL_APPLICATION);
	LOAD(READ_APPLICATION);
	LOAD(WRITE_APPLICATION);
	LOAD(ENABLE_APPLICATION);
	LOAD(DISABLE_APPLICATION);

	// we do *not* dlclose(handle)
    }
    (*INIT_APPLICATION_LAYER)(Sflag);
}


// ----------------------------------------------------------------------


int application_bounds(int *minmsg, int *maxmsg)
{
    return (*APPLICATION_BOUNDS)(minmsg, maxmsg);
}

int reboot_application_layer(CnetTime *ask_next)
{
    return (*REBOOT_APPLICATION_LAYER)(ask_next);
}

int poll_application(CnetTime *ask_next)
{
    return (*POLL_APPLICATION)(ask_next);
}


int CNET_read_application(CnetAddr *destaddr, char *msg, size_t *len)
{
    int	result		= -1;
    int	lengiven	= 0;

    if(destaddr == NULL || msg == NULL || len == NULL || *len == 0)
	ERROR(ER_BADARG);
    else if(!NODE_HAS_AL(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else {
	lengiven	= *len;
	result		= (*READ_APPLICATION)(destaddr, msg, len);
    }

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(%s,%s,*len=%ld) = 0 (*dest=%lu,*len=%d)\n",
		    __func__,
		    find_trace_name(destaddr), find_trace_name(msg), lengiven,
		    *destaddr, *len);
	else {
	    char	buf[64];

	    if(len == NULL)
		strcpy(buf, "NULL");
	    else
		sprintf(buf, "*len=%zd", *len);

	    TRACE(1, "\t%s(%s,%s,%s) = -1 %s\n",
		    __func__,
		    find_trace_name(destaddr), find_trace_name(msg),
		    buf,
		    cnet_errname[(int)cnet_errno]);
	}
    }
    return result;
}


int CNET_write_application(char *msg, size_t *len)
{
    int result 		= -1;
    int lengiven	= 0;

    if(msg == NULL || len == NULL || *len == 0)
	ERROR(ER_BADARG);
    else if(!NODE_HAS_AL(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else {
	lengiven	= *len;
	result		= (*WRITE_APPLICATION)(msg, len);
    }

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(%s,*len=%ld) = 0 (*len=%d)\n",
			__func__,
			find_trace_name(msg), lengiven, *len);
	else {
	    char	buf[64];

	    if(len == NULL)
		strcpy(buf, "NULL");
	    else
		sprintf(buf, "*len=%zd", *len);

	    TRACE(1, "\t%s(%s,%s) = -1 %s\n",
			__func__,
			find_trace_name(msg), buf,
			cnet_errname[(int)cnet_errno]);
	}
    }
    return result;
}


// --------------    Enable/Disable Application Layer -------------------


int CNET_enable_application(CnetAddr destaddr)
{
    int	result	= -1;

    if(destaddr == NODES[THISNODE].nattr.address)
	ERROR(ER_BADARG);
    else if(!NODE_HAS_AL(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else
	result	= (*ENABLE_APPLICATION)(destaddr);

    if(gattr.trace_events) {
	char	buf[32];

	if(destaddr == ALLNODES)
	    strcpy(buf, "ALLNODES");
	else
	    sprintf(buf, "dest=%lu", (unsigned long)destaddr);

	if(result == 0)
	    TRACE(0, "\t%s(%s) = 0\n", __func__, buf);
	else
	    TRACE(1, "\t%s(%s) = -1 %s\n",
			__func__, buf, cnet_errname[(int)cnet_errno]);
    }
    return result;
}

int CNET_disable_application(CnetAddr destaddr)
{
    int	result	= -1;

    if(destaddr == NODES[THISNODE].nattr.address)
	ERROR(ER_BADARG);
    else if(!NODE_HAS_AL(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else
	result	= (*DISABLE_APPLICATION)(destaddr);

    if(gattr.trace_events) {
	char	buf[32];

	if(destaddr == ALLNODES)
	    strcpy(buf, "ALLNODES");
	else
	    sprintf(buf, "dest=%lu", (unsigned long)destaddr);

	if(result == 0)
	    TRACE(0, "\t%s(%s) = 0\n", __func__, buf);
	else
	    TRACE(1, "\t%s(%s) = -1 %s\n",
			__func__, buf, cnet_errname[(int)cnet_errno]);
    }
    return result;
}
