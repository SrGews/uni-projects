#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

NODEATTR	DEFAULTNODE = {
	.address		= (CnetAddr)UNKNOWN,
	.icontitle		= DEFAULT_ICONTITLE,
	.iconheight		= 0,

	.top_minmessagesize	= MIN_MESSAGE_SIZE,
	.top_maxmessagesize	= MAX_MESSAGE_SIZE,
	.top_messagerate	= 1000000,

	.minmessagesize		= MIN_MESSAGE_SIZE,
	.maxmessagesize		= MAX_MESSAGE_SIZE,
	.messagerate		= 1000000,

	.nodemtbf		= (CnetTime)0,
	.nodemttr		= (CnetTime)0,

	.battery_volts		= DEFAULT_BATTERY_volts,
	.battery_mAH		= DEFAULT_BATTERY_mAH,

	.outputfile		= NULL,
	.reboot_argv		= NULL,
	.rebootfunc		= NULL,
	.compile		= NULL,
	.osname			= NULL,
	.stdio_quiet		= false,
	.trace_all		= false,
	.trace_mask		= 0,

	.position.x		= UNKNOWN,
	.position.y		= UNKNOWN,
	.position.z		= 0,
	.lastpx			= UNKNOWN,
	.lastpy			= UNKNOWN,

	.winx			= UNKNOWN,
	.winy			= UNKNOWN,
	.winopen		= false
};

GLOBALATTR	gattr = {
	.nhosts			= 0,
	.nrouters		= 0,
	.nmobiles		= 0,
	.naccesspoints		= 0,
	.nwans			= 0,
	.nlans			= 0,
	.nwlans			= 0,

	.maxposition		= {0, 0, 0},
	.positionerror		= 0,
	.mapwidth		= 0,
	.mapheight		= 0,
	.tilewidth		= 0,
	.tileheight		= 0,
	.mapgrid		= 0,
	.maphex			= 0,
	.mapscale		= 1.0,

	.mapcolour		= "",
	.mapimage		= NULL,
	.maptile		= NULL,

	.drawnodes		= true,
	.drawlinks		= true,
	.drawwlans		= true,

	.showcostperbyte	= false,
	.showcostperframe	= false,

	.stdio_quiet		= false,
	.trace_events		= false,
	.trace_filenm		= NULL,
	.tfp			= NULL,
	.nextensions		= 0
	// extensions
	// hiddenstats
	// nodestats
	// linkstats
};

char	*argv0;				// name of program from command-line

bool	dflag		= false;	// debugging/diagnostics
bool	Tflag		= false;	// use fast-as-you-can scheduling
int	vflag		= 0;		// be verbose about cnet's actions

#if	defined(USE_TCLTK)
bool	Wflag		= true;		// run under the windowing env.
#else
bool	Wflag		= false;	// run without the windowing env.
#endif

// ------------------- NETWORKING/TOPOLOGY VARIABLES ---------------------

int             THISNODE	= 0;
CnetEvent	HANDLING	= EV_NULL;

int             _NNODES		= 0;
int             NNODES		= 0;	// divulged with  cnet -N

NODE            *NODES;
CnetNodeInfo	nodeinfo;
CnetLinkInfo	*linkinfo;

CnetError	cnet_errno	= ER_OK;
RUNSTATE	cnet_state	= STATE_PAUSED;

CnetTime	MICROSECONDS	= 0;

CnetNICaddr	NICADDR_ZERO	= { 0, 0, 0, 0, 0, 0 };
CnetNICaddr	NICADDR_BCAST	= { 255, 255, 255, 255, 255, 255 };

// ------------------------ PARSING VARIABLES ----------------------------

INPUT	input;
int	nerrors		= 0;

char	chararray[4096];

// -----------------------------------------------------------------------

void init_globals(bool Bflag, const char *Cflag, bool Oflag, bool qflag,
		  const char *Rflag, bool tflag)
{
    extern void	init_defaultwan(bool);
    extern void	init_defaultlan(bool);
    extern void	init_defaultwlan(bool);

//  INITIALIZE SPECIFIC DEFAULT ATTRIBUTES
    DEFAULTNODE.compile		= Cflag;
    DEFAULTNODE.stdio_quiet	= qflag;
    DEFAULTNODE.winopen		= Oflag;
    DEFAULTNODE.rebootfunc	= Rflag;
    DEFAULTNODE.trace_all	= tflag;

    init_defaultwan(Bflag);
    init_defaultlan(Bflag);
    init_defaultwlan(Bflag);
}

// ---------------------------------------------------------------------

const char *findenv(const char *name, const char *fallback)
{
    const char	*value;

    value	= getenv(name);
    if(value == NULL || *value == '\0')
	value = fallback;
    if(vflag && value)
	REPORT("%s=\"%s\"\n", name,value);
    return(value);
}
