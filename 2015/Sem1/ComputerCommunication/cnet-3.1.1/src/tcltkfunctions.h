
/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef	_TCLTK_FUNCTIONS_H

#define	_TCLTK_FUNCTIONS_H

#include <tcl.h>
#include <tk.h>

#define	LEFT_BUTTON	1

#define	TCLTK_COMMAND(func)	\
	int func(ClientData data, Tcl_Interp *interp, int argc, char *argv[])

#define TCLTK_createcommand(str, func) \
	    Tcl_CreateCommand(tcl_interp, str, (Tcl_CmdProc *)func, \
			    (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL)

#define	TCLTK_ARGCHECK(want)	\
    	int	n;	\
    	if(vflag > 1) {	\
	    for(n=0 ; n<argc-1 ; ++n)	\
	    	REPORT("%s ", argv[n]);	\
	    REPORT("%s\n", argv[argc-1]);	\
    	}	\
    	if(argc != want) { interp->result = "wrong # args"; return TCL_ERROR; }

extern	Tcl_Interp	*tcl_interp;

extern	void		TCLTK(const char *fmt, ...);
extern	int		tcltk_notify_start(void);
extern	int		tcltk_notify_stop(void);
extern	int		tcltk_notify_dispatch(void);

#endif
