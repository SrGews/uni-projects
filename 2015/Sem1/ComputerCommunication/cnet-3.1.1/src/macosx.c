#include "cnetprivate.h"

#if	defined(USE_MACOSX)
//  Code in this file is only required for Apple's Mac-OSX environment

#include <sys/param.h>

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


void findOSXbundle(const char *argv0path)
{
    char	binary[MAXPATHLEN];
    char	*s;

//  FIRSTLY, WE MAY NEED TO FIND OUR BINARY VIA OUR SHELL'S $PATH
    if(argv0path[0] != '/') {
	char	*path;

	path	= getenv("PATH");
	if(path == NULL)
	    path	= ".";

        while(*path) {
            while(*path == ':')				// skip multiple :s
                path++;

            s   = binary;
            while(*path && *path != ':')
                *s++ = *path++;
            sprintf(s, "/%s", argv0path);
	    if(access(binary, X_OK) == 0) {		// executable?
		argv0path	= binary;
		break;
	    }
        }
    }   

//  THE APPLICATION'S NAME MAY BE A SYMBOLIC LINK - RESOLVE IT FIRST
    if(readlink(argv0path, binary, (sizeof binary)-1) == -1) {
	if(errno == EINVAL)
	    strcpy(binary, argv0path);			// not a symlink
	else
	    FATAL("cannot readlink(%s)\n", argv0path);
    }

//  FINALLY, FIND THE APPLICATION'S BUNDLE AND SET SOME ENVIRONMENT VARIABLES
    s	= strstr(binary, "/Contents/MacOS/");
    if(s) {
	setenv("CFProcessPath", strdup(binary), 1);	// may help someone
	*s	= '\0';
	strcpy(s, "/Contents/Resources");
	setenv("CNETPATH", strdup(binary), 1);
    }
    else
	FATAL("cannot set CNETPATH\n");
}
#endif
