#include "cnetprivate.h"
#include "physicallayer.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


NICATTR		DEFAULTLAN;
LAN		*LANS		= NULL;

// ---------------------------------------------------------------------

NICATTR *extend_lan(int whichlan, int whichnode) // called from parser.c
{
    NODE	*np		= &NODES[whichnode];
    LAN		*lan		= &LANS[whichlan];
    NICATTR	*nic;
    EACHNIC	*each;
    char	buf[16];

//  ADD A NEW NIC TO whichnode
    np->nics	= realloc(np->nics,(np->nnics+2)*sizeof(NICATTR));
    nic		= &np->nics[np->nnics+1];

    memcpy(nic, &lan->segmentnic, sizeof(NICATTR)); // default NIC attributes
    memcpy(nic->nicaddr, NICADDR_ZERO,  LEN_NICADDR);
    sprintf(buf, "lan%d", np->nlans);
    nic->name	= strdup(buf);

    np->lans		= realloc(np->lans,(np->nnics+2)*sizeof(int));
    np->lans[np->nnics+1] = whichlan;

//  ADD THIS NEW NIC TO whichlan
    lan->nics		= realloc(lan->nics, (lan->nnics+1)*sizeof(EACHNIC));
    each		= &lan->nics[lan->nnics];
    memset(each, 0, sizeof(EACHNIC));
    each->node		= whichnode;
    each->nodeslink	= np->nnics+1;

    ++np->nlans;
    ++np->nnics;
    ++lan->nnics;

    return nic;
}

int add_lan(char *name)			// called from parser.c
{
    LAN		*lan;

    if(gattr.nlans == 0)
	LANS	= calloc(1, sizeof(LAN));
    else {
	int	l;

	for(l=0, lan=LANS ; l<gattr.nlans ; ++l, ++lan)
	    if(strcasecmp(lan->name, name) == 0)
		return(l);
	LANS	= realloc(LANS, (gattr.nlans+1)*sizeof(LAN));
    }

    lan			= &LANS[gattr.nlans];
    memset(lan, 0, sizeof(LAN));
    lan->name		= strdup(name);
    lan->x		= UNKNOWN;
    lan->y		= UNKNOWN;

    lan->segmentnic	= DEFAULTLAN;
    lan->nics		= calloc(4, sizeof(EACHNIC));
    return(gattr.nlans++);
}

// ---------------------------------------------------------------------

static void generate_lan_collision(NODE *np, int thislan, NICATTR *segment)
{
    extern	void	unschedule_lan_collision(int);

    LAN			*lan	 = &LANS[thislan];
    NODE		*destnp;
    EACHNIC		*each;
    NICATTR		*destnic;

    FRAMEARRIVAL	*newf;
    int			n;

    if(vflag)
	REPORT("node %s introduces collision on LAN %s\n",
		np->nodename, LANS[thislan].name);

//  REMOVE ALL FRAMES AND COLLISIONS SCHEDULED ON THIS LAN
    unschedule_lan_collision(thislan);

//  RECORD THAT THIS SEGMENT IS NOW BUSY DUE TO JAMMING
    segment->tx_until	= MICROSECONDS + segment->slottime +
	  ((segment->jitter == 0) ? 0 : poisson_usecs(segment->jitter,np->mt));

//  DELIVER A COLLISION FRAME TO ALL NODES ON THIS LAN (INC. OURSELVES)
    for(n=0, each=lan->nics ; n<lan->nnics ; ++n, ++each) {
	destnp	= &NODES[each->node];
	if(destnp->runstate != STATE_RUNNING)
	    continue;

	destnic	= &destnp->nics[each->nodeslink];

//  EACH DESTINATION NIC CANNOT TRANSMIT WHILE COLLISION IS BEING RECEIVED
	destnic->tx_until	= segment->tx_until;

//  PROPAGATION DELAY COMES FROM DISTANCE OF (SRC -> DEST) AT 200m/usec     
	destnic->rx_until	= segment->tx_until +
		      abs(np->nattr.position.x-destnp->nattr.position.x)/200;

//  PREPARE A FRAMEARRIVAL STRUCTURE FOR THE DESTINATION
	newf		= NEW(FRAMEARRIVAL);
	newf->destnode	= each->node;
	newf->destlink	= each->nodeslink;
	newf->linktype	= LT_LAN;
	newf->linkindex	= thislan;
	newf->arrives	= destnic->rx_until + LAN_DECODE_TIME;

	newevent(EV_FRAMECOLLISION, newf->destnode,
		    newf->arrives - MICROSECONDS, NULLTIMER, (CnetData)newf);
    }
}

int write_lan(int thislink, char *packet, size_t len, bool unreliable)
{
    NODE		*np	= &NODES[THISNODE];
    NODE		*destnp;
    int			thislan	= np->lans[thislink];

    LAN			*lan	= &LANS[thislan];
    NICATTR		*segment= &lan->segmentnic;
    NICATTR		*nic	= &np->nics[thislink];

    EACHNIC		*each;
    NICATTR		*destnic;

    FRAMEARRIVAL	*newf;
    CnetTime		Twrite;
    int			n, tcost;

//  ENSURE THAT PACKET IS NEITHER TOO SHORT, NOR TOO BIG 
    if(len < LAN_MINDATA || len > LAN_MAXDATA) {
	ERROR(ER_BADSIZE);
	return(-1);
    }

//  IF WE TRANSMIT WHILE THE SEGMENT OR NIC IS IN USE, WE HAVE A COLLISION
    if(MICROSECONDS < segment->tx_until || MICROSECONDS < nic->rx_until) {
	generate_lan_collision(np, thislan, segment);
	segment->linkstats.rx_frames_collisions++;
	goto write_done;
    }

//  POSSIBLY LOSE THE FRAME (not really transmitted)
    if(unreliable && lose_frame(nic, len))
	goto write_done;

//  NIC BUSY UNTIL = NOW + (LENGTH_IN_BITS / BANDWIDTH_BPS) 
    Twrite	= LAN_ENCODE_TIME +
		  ((int64_t)len*(int64_t)8000000) / segment->bandwidth;

//  WE CANNOT COMMENCE WRITING IF OUR NIC IS CURRENTLY TRANSMITTING
    if(MICROSECONDS < nic->tx_until)
	nic->tx_until	= nic->tx_until + Twrite;
    else
	nic->tx_until	= MICROSECONDS + Twrite;
    segment->tx_until	= nic->tx_until +
	  ((segment->jitter == 0) ? 0 : poisson_usecs(segment->jitter,np->mt));
    nic->rx_until	= nic->tx_until;

//  SCHEDULE A NEW FRAME TO ARRIVE AT EACH OTHER NIC ON THIS LAN SEGMENT
    for(n=0, each=lan->nics ; n<lan->nnics ; ++n, ++each) {
	if(each->node == THISNODE)
	    continue;
	destnp	= &NODES[each->node];
	if(destnp->runstate != STATE_RUNNING)
	    continue;

	destnic	= &destnp->nics[each->nodeslink];

//  PROPAGATION DELAY COMES FROM DISTANCE OF (SRC -> DEST) AT 200m/usec     
	destnic->rx_until	= nic->tx_until +
		    abs(np->nattr.position.x - destnp->nattr.position.x)/200;

//  PREPARE A FRAMEARRIVAL STRUCTURE FOR THE DESTINATION
	newf		= NEW(FRAMEARRIVAL);
	newf->destnode	= each->node;
	newf->destlink	= each->nodeslink;
	newf->linktype	= LT_LAN;
	newf->linkindex	= thislan;
	newf->len	= len;
	newf->frame	= calloc(1, (unsigned)len);
	memcpy(newf->frame, packet, (unsigned)len);

//  POSSIBLY CORRUPT THE FRAME (still transmitted)
	newf->corrupted	=
		    (unreliable && corrupt_frame(nic, newf->frame, newf->len));
	destnp->nframes++;

	newf->arrives	= destnic->rx_until + LAN_DECODE_TIME;
	newevent(EV_PHYSICALREADY, each->node,
		    newf->arrives - MICROSECONDS, NULLTIMER, (CnetData)newf);
    }

write_done:
    tcost = WHICH(nic->costperframe, DEFAULTLAN.costperframe) +
		  (len * WHICH(nic->costperbyte, DEFAULTLAN.costperbyte));

//  UPDATE GLOBAL LINK TRANSMISSION STATISTICS
    gattr.linkstats.tx_frames++;
    gattr.linkstats.tx_bytes	+= len;
    gattr.linkstats.tx_cost	+= tcost;

//  UPDATE THIS TRANSMITTING SEGMENT's STATISTICS
    segment->linkstats.tx_frames++;
    segment->linkstats.tx_bytes	+= len;
    segment->linkstats.tx_cost	+= tcost;

//  UPDATE THIS TRANSMITTING NIC's STATISTICS
    nic->linkstats.tx_frames++;
    nic->linkstats.tx_bytes	+= len;
    nic->linkstats.tx_cost	+= tcost;
#if	defined(USE_TCLTK)
    segment->stats_changed	= true;
    nic->stats_changed		= true;
#endif

    return 0;
}

// ---------------------------------------------------------------------

void check_lans(bool **adj)
{
    LAN		*lan;
    EACHNIC	*each1, *each2;
    NICATTR	*nic1, *nic2;
    int		l, n1, n2;
    int		maxlen=0, x;

//  ENSURE THAT EACH LAN NIC HAS AN ADDRESS
    for(l=0, lan=LANS ; l<gattr.nlans ; ++l, ++lan)
	for(n1=0, each1=lan->nics ; n1<lan->nnics ; ++n1, ++each1)
	    assign_nicaddr(each1->node, each1->nodeslink);

//  WARN IF ANY LAN ADDRESSES ON A SINGLE SEGMENT ARE NOT UNIQUE
    for(l=0, lan=LANS ; l<gattr.nlans ; ++l, ++lan)
	for(n1=0, each1=lan->nics ; n1<(lan->nnics-1) ; ++n1, ++each1) {
	    nic1	= &NODES[each1->node].nics[each1->nodeslink];
	    for(n2=n1+1, each2=&lan->nics[n2] ; n2<lan->nnics ; ++n2, ++each2) {
		nic2	= &NODES[each2->node].nics[each2->nodeslink];
		if(memcmp(nic1->nicaddr, nic2->nicaddr, LEN_NICADDR) == 0) {
		    char	buf[32];

		    CNET_format_nicaddr(buf, nic1->nicaddr);
		    WARNING(
	  "LAN segment %s has duplicate NIC addresses\n\t%s(%s) and %s(%s)\n",
				    lan->name,
				    NODES[each1->node].nodename, buf,
				    NODES[each2->node].nodename, buf);
		}
//  ESTABLISH ENTRIES IN THE WHOLE NETWORK'S ADJACENCY MATRIX
		adj[each1->node][each2->node] =
		adj[each2->node][each1->node] = true;
	    }
	}

//  GIVE EACH SEGMENT MEANINGFUL COORDINATES IF NOT ALREADY ASSIGNED
    for(l=0, lan=LANS ; l<gattr.nlans ; ++l, ++lan) {

	if(lan->x == UNKNOWN || lan->y == UNKNOWN) {

//  IF ANY NIC ALREADY HAS COORDINATES, POSITION THIS SEGMENT WRT THAT NIC
	    for(n1=0, each1=lan->nics ; n1<lan->nnics ; ++n1, ++each1)
		if(NODES[each1->node].nattr.position.x != UNKNOWN	&&
		   NODES[each1->node].nattr.position.y != UNKNOWN) {

		    lan->x	= NODES[each1->node].nattr.position.x -
			(n1+1)*DEFAULT_NODE_SPACING + DEFAULT_NODE_SPACING/2;
		    lan->y	= NODES[each1->node].nattr.position.y +
			(((n1%2) == 0) ? 1 : -1) * DEFAULT_NODE_SPACING;
		    break;
		}

//  IF NO COORDINATES YET, POSITION SEGMENT RELATIVE TO OTHER SEGMENTS
	    if(lan->x == UNKNOWN || lan->y == UNKNOWN) {
		lan->x	= (l/2+1)*DEFAULT_NODE_SPACING + (l/2)*maxlen;
		lan->y	= 2*DEFAULT_NODE_SPACING + (l%2)*4*DEFAULT_NODE_SPACING;
	    }
	}
	lan->x1 = lan->x + lan->nnics*DEFAULT_NODE_SPACING;
	if(maxlen < (lan->x1-lan->x))
	    maxlen	= (lan->x1-lan->x);
    }

//  FINALLY, POSITION ALL OF THIS SEGMENT'S NICs THAT DO NOT HAVE COORDINATES
    for(l=0, lan=LANS ; l<gattr.nlans ; ++l, ++lan) {
	x	= lan->x + DEFAULT_NODE_SPACING/2;

	for(n1=0, each1=lan->nics ; n1<lan->nnics ; ++n1, ++each1) {
	    if(NODES[each1->node].nattr.position.x == UNKNOWN)
		NODES[each1->node].nattr.position.x	= x;
	    x	+= DEFAULT_NODE_SPACING;
	    if(NODES[each1->node].nattr.position.y == UNKNOWN)
		NODES[each1->node].nattr.position.y =
		(lan->y-DEFAULT_NODE_SPACING) + (n1%2)*2*DEFAULT_NODE_SPACING;
	}
    }
}


// ---------------------------------------------------------------------

void print_lans(FILE *fp)
{
    extern void print_nic1(FILE *, const char *, int,
			   NICATTR *, NICATTR *, const char *);

    LAN		*lan;
    int		n;

    if(gattr.nlans > 0) {
	fprintf(fp, "/* LAN definitions */\n");
	for(n=0, lan=LANS ; n<gattr.nlans ; ++n, ++lan) {
	    fprintf(fp, "lan %s {\t\t/* has %d NICS */\n",lan->name,lan->nnics);
	    fprintf(fp, "    x=%d, y=%d\n", lan->x, lan->y);
	    print_nic1(fp, "lan", false, &lan->segmentnic, &DEFAULTLAN, "    ");
	    fprintf(fp, "}\n");
	}
	fprintf(fp, "\n");
    }
}


// ---------------------------------------------------------------------

#if	defined(USE_TCLTK)

static TCLTK_COMMAND(lan_click)
{
    LAN		*lan;
    char	desc[BUFSIZ];
    bool	was;
    int		lann, nicn, button, evx, evy;

    TCLTK_ARGCHECK(6);
    n		= 1;
    lann	= atoi(argv[n++]);	// segment number
    nicn	= atoi(argv[n++]);	// nth NIC on this segment
    button	= atoi(argv[n++]);
    evx		= atoi(argv[n++]);
    evy		= atoi(argv[n++]);

    lan		= &LANS[lann];

    was	= CNET_set_commas(true);
    sprintf(desc, "bandwidth %sbps, slottime %susecs",
		    CNET_format64(lan->segmentnic.bandwidth),
		    CNET_format64(lan->segmentnic.slottime));
    CNET_set_commas(was);

    if(nicn == UNKNOWN) {
	if(button == LEFT_BUTTON)
	    TCLTK("toggleNIC LAN %d %d \"LAN %s\" %d %d \"%s\"",
			lann, UNKNOWN, lan->name, evx, evy, desc);
    }
    else
	TCLTK("toggleNIC LAN %d %d \"%s->LAN %s\" %d %d \"%s\"",
		lann, nicn, NODES[lan->nics[nicn].node].nodename, lan->name,
		evx, evy, desc);

    return TCL_OK;
}

static void draw_1_lan(int n)
{
    LAN		*lan	= &LANS[n];
    EACHNIC	*nic;
    int		i, x, y;

//  DRAW THE MAIN LAN SEGMENT
    TCLTK("set x [$map create line %d %d %d %d -fill %s -width %d]",
		    M2PX(lan->x),  M2PX(lan->y),
		    M2PX(lan->x1), M2PX(lan->y),
		    COLOUR_LAN, CANVAS_FATLINK/2 );
    TCLTK("$map bind $x <Button> \"lan_click %d -1 %%b %%x %%y\"", n);
#if	defined(USE_MACOSX)
    TCLTK("$map bind $x <Control-ButtonPress-1> \"lan_click %d -1 2 %%x %%y\"", n);
#endif
//  DRAW THE TWO TERMINATORS ON THE SEGMENT
    TCLTK("$map create line %d %d %d %d -fill %s -width %d",
		    M2PX(lan->x), M2PX(lan->y)-CANVAS_FATLINK/2,
		    M2PX(lan->x), M2PX(lan->y)+CANVAS_FATLINK/2,
		    COLOUR_LAN, CANVAS_FATLINK/2 );
    TCLTK("$map create line %d %d %d %d -fill %s -width %d",
		    M2PX(lan->x1), M2PX(lan->y)-CANVAS_FATLINK/2,
		    M2PX(lan->x1), M2PX(lan->y)+CANVAS_FATLINK/2,
		    COLOUR_LAN, CANVAS_FATLINK/2 );
//  AND THE NAME OF THE SEGMENT
    TCLTK("$map create text %d %d -anchor w -font $labelfont -text %s",
		    M2PX(lan->x)+4, M2PX(lan->y)+8, lan->name);

//  DRAW EACH NODE'S CONNECTION TO THE SEGMENT
    x	= lan->x + DEFAULT_NODE_SPACING/2;
    for(i=0, nic=lan->nics ; i<lan->nnics ; ++i, ++nic) {
	y	= NODES[nic->node].nattr.position.y;

	if(x == NODES[nic->node].nattr.position.x) {
	    TCLTK("set x [$map create line %d %d %d %d -fill %s -width %d]",
		    M2PX(NODES[nic->node].nattr.position.x), M2PX(y),
		    M2PX(x), M2PX(lan->y),
		    COLOUR_LAN, CANVAS_FATLINK/2 );
	    TCLTK("$map bind $x <Button> \"lan_click %d %d %%b %%x %%y\"",n,i);
#if	defined(USE_MACOSX)
	    TCLTK("$map bind $x <Control-ButtonPress-1> \"lan_click %d %d 2 %%x %%y\"",n,i);
#endif
	}
	else {
	    int	midy = lan->y + (lan->y < y ? 1 : -1)*DEFAULT_NODE_SPACING/4;

		TCLTK("set x [$map create line %d %d %d %d -fill %s -width %d]",
			M2PX(x), M2PX(lan->y),
			M2PX(x), M2PX(midy),
			COLOUR_LAN, CANVAS_FATLINK/2 );
	    TCLTK("$map bind $x <Button> \"lan_click %d %d %%b %%x %%y\"",n,i);
#if	defined(USE_MACOSX)
	    TCLTK("$map bind $x <Control-ButtonPress-1> \"lan_click %d %d 2 %%x %%y\"",n,i);
#endif

	    TCLTK("set x [$map create line %d %d %d %d -fill %s -width %d]",
			M2PX(x), M2PX(midy),
			M2PX(NODES[nic->node].nattr.position.x), M2PX(y),
			COLOUR_LAN, CANVAS_FATLINK/2 );
	    TCLTK("$map bind $x <Button> \"lan_click %d %d %%b %%x %%y\"",n,i);
#if	defined(USE_MACOSX)
	    TCLTK("$map bind $x <Control-ButtonPress-1> \"lan_click %d %d 2 %%x %%y\"",n,i);
#endif
	}

//  AND THE LINK-NUMBER OF THIS CONNECTION TO THE SEGMENT
	TCLTK("$map create text %d %d -anchor c -font $labelfont -text %d",
		    M2PX(x)+8, M2PX(lan->y)-8 + (y<lan->y ? 0 : 16),
		    nic->nodeslink);
	x	+= DEFAULT_NODE_SPACING;
    }

    if(gattr.showcostperframe || gattr.showcostperbyte) {
#define	H2	6
#define	W2	3

	int	midx	= lan->x + (lan->nnics * DEFAULT_NODE_SPACING)/2;
	int	width	= W2 + 2;
	TCLTK("$map create rectangle %d %d %d %d -fill %s -outline %s",
			M2PX(midx) - width, M2PX(lan->y) - H2,
			M2PX(midx) + width, M2PX(lan->y) + H2,
			"yellow", "black" );
	TCLTK("$map create text %d %d -anchor c -font 5x7 -text \"%s\"",
			M2PX(midx), M2PX(lan->y), "0");
#undef	W2
#undef	H2
    }
}

void draw_lans(void)
{
    int	n;

    for(n=0 ; n<gattr.nlans ; ++n)
	draw_1_lan(n);
}
#endif

// ---------------------------------------------------------------------

void init_defaultlan(bool Bflag)
{
//  INITIALIZE DEFAULT LAN ATTRIBUTES
    memset(&DEFAULTLAN,  0, sizeof(DEFAULTLAN));
    DEFAULTLAN.linktype			= LT_LAN;
    DEFAULTLAN.up			= true;
    DEFAULTLAN.bandwidth		= LAN_BANDWIDTH;
    DEFAULTLAN.buffered			= Bflag;
    DEFAULTLAN.mtu			= LAN_MTU;
    DEFAULTLAN.propagation		= (CnetTime)LAN_PROPAGATION;

    DEFAULTLAN.costperbyte		= 0;
    DEFAULTLAN.costperframe		= 0;
    DEFAULTLAN.probframecorrupt		= 0;
    DEFAULTLAN.probframeloss		= 0;

    DEFAULTLAN.slottime			= LAN_SLOTTIME;

    DEFAULTLAN.linkmtbf			= (CnetTime)0;
    DEFAULTLAN.linkmttr			= (CnetTime)0;
    DEFAULTLAN.tx_until			= (CnetTime)0;
    DEFAULTLAN.rx_until			= (CnetTime)0;
}

void init_lans(void)
{
#if	defined(USE_TCLTK)
    if(Wflag)
	TCLTK_createcommand("lan_click", lan_click);
#endif
}
