#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

typedef struct {
    int			destnode;	// index into NODES[]
    int			destlink;	// link at NODES[destnode]
    CnetTime		arrives;	// time in usec the frame arrives
    bool		corrupted;
    size_t		len;		// len of data iff not corrupted
    char		*frame;

    CnetLinkType	linktype;	// one of LT_*
    int			linkindex;	// into WANS[], LANS[], or WLANS[]
    double		rx_signal;	// WLAN - signal strength of arrival
    double		rx_angle;	// WLAN - angle of arrival
} FRAMEARRIVAL;

extern	void	assign_nicaddr(int thisnode, int thisnic);

extern	int	corrupt_frame(NICATTR *nic, char *frame, size_t len);
extern	int	lose_frame   (NICATTR *nic, size_t len);
