#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


// ---------- CALLED VIA cnet -p OR THE "Save Topology" BUTTON ----------

#define	OFFSET	25

static	FILE	*topfp;

void print_nic1(FILE *fp, const char *type,
		    int always, NICATTR *nic, NICATTR *wrt, const char *indent)
{
    char	fmt[64], name[64];

//  FIRSTLY, PRINT THE NIC'S ADDRESS
    if(memcmp(nic->nicaddr, NICADDR_ZERO, LEN_NICADDR) != 0) {
	char	buf[32];

	sprintf(fmt, "%s%%-%ds", indent, OFFSET-(int)strlen(indent));
	CNET_format_nicaddr(buf, nic->nicaddr);
	sprintf(name, fmt, "nicaddr");
	fprintf(fp,"%s= %s\n", name, buf);
    }

//  NEXT, THE ATTRIBUTES THAT CAN'T BE CHANGED BY THE GUI
    sprintf(fmt, "%s%s-%%-%ds",
		indent, type, OFFSET-(int)strlen(indent)-(int)strlen(type)-1);

    if(always ||
	(nic->bandwidth != DEFAULT && nic->bandwidth != wrt->bandwidth)) {
	sprintf(name, fmt, "bandwidth");
	fprintf(fp,"%s= %dbps\n", name, nic->bandwidth);
    }
    if(always || (nic->mtu != DEFAULT && nic->mtu != wrt->mtu)) {
	sprintf(name, fmt, "mtu");
	fprintf(fp,"%s= %dbytes\n", name, nic->mtu);
    }
    if(always ||
	(nic->jitter != DEFAULT && nic->jitter != wrt->jitter)) {
	sprintf(name, fmt, "jitter");
	fprintf(fp,"%s= %susec\n", name, CNET_format64(nic->jitter));
    }
    if(always ||
	(nic->propagation != DEFAULT && nic->propagation != wrt->propagation)) {
	sprintf(name, fmt, "propagationdelay");
	fprintf(fp,"%s= %susec\n", name, CNET_format64(nic->propagation));
    }
    if(always ||
	(nic->linkmtbf != DEFAULT && nic->linkmtbf != wrt->linkmtbf)) {
	sprintf(name, fmt, "linkmtbf");
	fprintf(fp,"%s= %susec", name, CNET_format64(nic->linkmtbf));
	if(nic->linkmtbf == 0)
	    fprintf(fp,"\t\t/* will not fail */\n");
	else
	    fprintf(fp,"\n");
    }
    if(always ||
	(nic->linkmttr != DEFAULT && nic->linkmttr != wrt->linkmttr)) {
	sprintf(name, fmt, "linkmttr");
	fprintf(fp,"%s= %susec", name, CNET_format64(nic->linkmttr));
	if(nic->linkmttr == 0)
	    fprintf(fp,"\t\t/* instant repair */\n");
	else
	    fprintf(fp,"\n");
    }

/*
    if(nic->linktype == LT_WLAN) {
      if(always ||
  (nic->rxsensitivity != DEFAULT && nic->rxsensitivity != wrt->rxsensitivity)) {
	sprintf(name, fmt, "rxsensitivity");
	fprintf(fp,"%s= %.3fdBm\n", name, nic->rxsensitivity);
      }

      if(always ||
	(nic->txpower != DEFAULT && nic->txpower != wrt->txpower)) {
	sprintf(name, fmt, "txpower");
	fprintf(fp,"%s= %.3fdBm\n", name, nic->txpower);
      }
    }
 */

//  NOW, THE ATTRIBUTES THAT CAN BE CHANGED BY THE GUI
    if(always ||
	(nic->costperbyte != DEFAULT && nic->costperbyte != wrt->costperbyte)) {
	sprintf(name, fmt, "costperbyte");
	fprintf(fp,"%s= %d\n", name, nic->costperbyte);
    }
    if(always ||
	(nic->costperframe != DEFAULT && nic->costperframe != wrt->costperframe)) {
	sprintf(name, fmt, "costperframe");
	fprintf(fp,"%s= %d\n", name, nic->costperframe);
    }
    if(always ||
	(nic->probframecorrupt != DEFAULT && nic->probframecorrupt != wrt->probframecorrupt)) {
	sprintf(name, fmt, "probframecorrupt");
	fprintf(fp,"%s= %d\n", name, nic->probframecorrupt);
    }
    if(always ||
	(nic->probframeloss != DEFAULT && nic->probframeloss != wrt->probframeloss)) {
	sprintf(name, fmt, "probframeloss");
	fprintf(fp,"%s= %d\n", name, nic->probframeloss);
    }
}

static void print_nic0(int always, NICATTR *nic, NICATTR *wrt, int curly)
{
    const char *type="???";
    const char *indent;

    if(always)		indent = "";
    else if(curly)	indent = "         ";
    else		indent = "    ";

    if(curly)
	fprintf(topfp, "{\t/* %s */\n", nic->name);

    switch((int)nic->linktype) {
	case LT_WAN :	type	= "wan";	break;
	case LT_LAN :	type	= "lan";	break;
	case LT_WLAN :	type	= "wlan";	break;
    }

    print_nic1(topfp, type,  always, nic, wrt, indent);

    if(curly)
	fputs("    }",topfp);
}

static void print_nodes_nics(int thisnode)
{
    NODE	*np	= &NODES[thisnode];
    NICATTR	*nic;
    int		n;

    for(n=1, nic=&np->nics[1] ; n<=np->nnics ; n++, nic++) {
	fputs("    ",topfp);

	switch((int)nic->linktype) {
	case LT_WAN : {
	    WAN		*wan	= &WANS[ np->wans[n] ];

	    if(wan->minnode == thisnode)
		fprintf(topfp,"wan to %s ",NODES[wan->maxnode].nodename);
	    else
		fprintf(topfp,"wan to %s ",NODES[wan->minnode].nodename);
	    print_nic0(false, nic, &np->defaultwan, true);
	    fputs("\n",topfp);
	    break;
	}
	case LT_LAN : {
	    LAN		*lan	= &LANS[ np->lans[n] ];

	    fprintf(topfp,"lan to %s {\n", lan->name);
	    print_nic0(false, nic, &np->defaultlan, true);
	    fputs("\n",topfp);
	    break;
	}
	case LT_WLAN : {
	    fprintf(topfp,"wlan ");
	    print_nic0(false, nic, &np->defaultwlan, true);
	    fputs("\n",topfp);
	    break;
	  }
	}
    }
}

void print_node_attr(int always, NODEATTR *me, const char *indent)
{
    char	fmt[64], name[64];

    sprintf(fmt, "%s%%-%ds", indent, OFFSET-(int)strlen(indent));
//  FIRSTLY, THE ATTRIBUTES THAT CAN'T BE CHANGED BY THE GUI
    if(always || strcmp(me->compile, DEFAULTNODE.compile) != 0) {
	sprintf(name, fmt, "compile");
	fprintf(topfp,"%s= \"%s\"\n", name, me->compile);
    }
    if(always || strcmp(me->rebootfunc, DEFAULTNODE.rebootfunc) != 0) {
	sprintf(name, fmt, "rebootfunc");
	fprintf(topfp,"%s= \"%s\"\n", name, me->rebootfunc);
    }

    if(always ||
	(me->nodemtbf != DEFAULT && me->nodemtbf != DEFAULTNODE.nodemtbf)) {
	sprintf(name, fmt, "nodemtbf");
	fprintf(topfp,"%s= %susec", name, CNET_format64(me->nodemtbf));
	if(me->nodemtbf == 0)
	    fprintf(topfp,"\t\t/* will not fail */\n");
	else
	    fprintf(topfp,"\n");
    }
    if(always ||
	(me->nodemttr != DEFAULT && me->nodemttr != DEFAULTNODE.nodemttr)) {
	sprintf(name, fmt, "nodemttr");
	fprintf(topfp,"%s= %susec", name, CNET_format64(me->nodemttr));
	if(me->nodemttr == 0)
	    fprintf(topfp,"\t\t/* instant repair */\n");
	else
	    fprintf(topfp,"\n");
    }

//  NOW, THE ATTRIBUTES THAT CAN BE CHANGED BY THE GUI
    if(always ||
	(me->messagerate != DEFAULT && me->messagerate != DEFAULTNODE.messagerate)) {
	sprintf(name, fmt, "messagerate");
	fprintf(topfp,"%s= %susec\n", name, CNET_format64(me->messagerate));
    }
    if(always ||
	(me->minmessagesize != DEFAULT && me->minmessagesize != DEFAULTNODE.minmessagesize)) {
	sprintf(name, fmt, "minmessagesize");
	fprintf(topfp,"%s= %dbytes\n", name, me->minmessagesize);
    }
    if(always ||
	(me->maxmessagesize != DEFAULT && me->maxmessagesize != DEFAULTNODE.maxmessagesize)) {
	sprintf(name, fmt, "maxmessagesize");
	fprintf(topfp,"%s= %dbytes\n", name, me->maxmessagesize);
    }
}

void save_topology(const char *filenm)
{
    extern void print_nodetypes(FILE *);
    extern void print_linktypes(FILE *);
    extern void	print_lans(FILE *fp);

    bool	commas;
    NODE	*np;
    int		n;

    if(filenm == NULL || *filenm == '\0')
	topfp	= stdout;
    else
	if((topfp = fopen(filenm, "w")) == NULL) {
	    WARNING("%s: cannot create %s\n", filenm);
	    return;
	}

    commas	= CNET_set_commas(false);

//  PRINT ANY GLOBAL ATTRIBUTES THAT HAVE BEEN DEFINED
    fprintf(topfp,"/* global attributes */\n");
    if(gattr.mapimage)
	fprintf(topfp,"mapimage = \"%s\"\n", gattr.mapimage);
    else if(gattr.maptile)
	fprintf(topfp,"maptile = \"%s\"\n", gattr.maptile);
    else if(gattr.mapgrid != 0)
	fprintf(topfp,"mapgrid = %d\n", gattr.mapgrid);
    if(gattr.mapscale != 1.0)
	fprintf(topfp,"mapscale = %.1f\n", gattr.mapscale);

    if(gattr.trace_filenm)
	fprintf(topfp,"tracefile = \"%s\"\n", gattr.trace_filenm);
    if(gattr.showcostperframe)
	fprintf(topfp,"showcostperframe = true\n");
    else if(gattr.showcostperbyte)
	fprintf(topfp,"showcostperbyte = true\n");
    fprintf(topfp,"\n");

//  FIRSTLY, PRINT THE DEFAULT NODE AND NIC ATTRIBUTES
    fprintf(topfp,"/* default node attributes */\n");
	print_node_attr(true, &DEFAULTNODE, "");
    fprintf(topfp,"\n");
    if(gattr.nwans > 0) {
	fprintf(topfp,"/* default WAN attributes */\n");
	    print_nic0(true, &DEFAULTWAN,  NULL, false);
	fprintf(topfp,"\n");
    }
    if(gattr.nlans > 0) {
	fprintf(topfp,"/* default LAN attributes */\n");
	    print_nic0(true, &DEFAULTLAN,  NULL, false);
	fprintf(topfp,"\n");
    }
    if(gattr.nwlans > 0) {
	fprintf(topfp,"/* default WLAN attributes */\n");
	    print_nic0(true, &DEFAULTWLAN, NULL, false);
	fprintf(topfp,"\n");
    }

    print_nodetypes(topfp);
    print_linktypes(topfp);

//  NEXT, PRINT ANY LAN SEGMENTS THAT WE HAVE
    print_lans(topfp);

//  FINALLY, FOR EACH NODE, PRINT ITS LOCAL NODE AND NIC INFORMATION
    for(n=0, np=NODES ; n<_NNODES ; n++, np++) {
	switch (np->nodetype) {
	    case NT_HOST	: fprintf(topfp, "host ");	break;
	    case NT_ROUTER	: fprintf(topfp, "router ");	break;
	    case NT_MOBILE	: fprintf(topfp, "mobile ");	break;
	    case NT_ACCESSPOINT	: fprintf(topfp, "accesspoint "); break;
	}
	fprintf(topfp,"%s {\n", np->nodename);
	fprintf(topfp,"    x=%d, y=%d\n",
			np->nattr.position.x, np->nattr.position.y);
	if(np->nattr.osname != NULL)
	    fprintf(topfp,"    osname = \"%s\"\n", np->nattr.osname);

	print_node_attr(false, &np->nattr, "    ");
	print_nic0(false, &np->defaultwan,  &DEFAULTWAN, false);
	print_nic0(false, &np->defaultlan,  &DEFAULTLAN, false);
	print_nic0(false, &np->defaultwlan, &DEFAULTWLAN, false);
	fprintf(topfp,"\n");
	print_nodes_nics(n);
	fprintf(topfp,"}\n");
	if(n<(_NNODES-1))
	    fprintf(topfp,"\n");
    }
    if(topfp != stdout)
	fclose(topfp);

    CNET_set_commas(commas);
}
