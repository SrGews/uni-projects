/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#if	!defined(_CNETPRIVATE_H_)

#define	_CNETPRIVATE_H_		1

#define	CNET_AUTHOR		"Chris McDonald"
#define	CNET_EMAIL		"chris@csse.uwa.edu.au"

#include <stdbool.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include "preferences.h"	// installation dependent preferences
#include "hidenames.h"		// renames important global symbols
#include "cnet.h"		// this is the students' header file
#include "mt19937.h"		// Mersenne Twister random number generator

#if	defined(USE_LINUX)
#define	NDATASEGS		2
#define	PREPEND_DOT_TO_LDPATH

#elif   defined(USE_MACOSX)
#define NDATASEGS               1
#ifndef OS_INLINE
#define OS_INLINE		static __inline__
#endif
#undef	PRId64
#define	PRId64			"lld"

#elif	defined(USE_SOLARIS)
#define	NDATASEGS		1
#define	PREPEND_DOT_TO_LDPATH

#elif	defined(USE_FREEBSD)
#define	NDATASEGS		1
#define	PREPEND_DOT_TO_LDPATH
 
#elif	defined(USE_NETBSD)
#define	NDATASEGS		2
#define	PREPEND_DOT_TO_LDPATH
 
#elif	defined(USE_IRIX5)
#define	NDATASEGS		2
#define	PREPEND_DOT_TO_LDPATH
#define	UNLINK_SO_LOCATIONS

#elif	defined(USE_OSF1)
#define	NDATASEGS		4
#define	UNLINK_SO_LOCATIONS

#elif	defined(USE_SUNOS)
#define	NDATASEGS		1

#endif

#if   !defined(USE_MACOSX)
extern	char	*strdup(const char *);
extern	int	strcasecmp(const char *, const char *);
#endif


#define	NODE_HAS_AL(n)		(NODES[n].nodetype == NT_HOST || \
			 	 NODES[n].nodetype == NT_MOBILE)
#define	NODE_HAS_KB(n)		(NODES[n].nodetype == NT_HOST || \
			 	 NODES[n].nodetype == NT_MOBILE)
#define	NODE_HAS_WANS(n)	(NODES[n].nodetype != NT_MOBILE)
#define	NODE_HAS_LANS(n)	(NODES[n].nodetype != NT_MOBILE)
#define	NODE_HAS_WLANS(n)	(NODES[n].nodetype == NT_HOST || \
				 NODES[n].nodetype == NT_MOBILE || \
				 NODES[n].nodetype == NT_ACCESSPOINT)
#define	NODE_MAY_MOVE(n)	(NODES[n].nodetype == NT_MOBILE)


#if	defined(USE_TCLTK)

#include "tcltkfunctions.h"

extern	void	draw_node_icon(int n, char *canvas, int x, int y);

#elif	defined(USE_ASCII)

#else
#error	You have not specified a valid windowing system in preferences.h
#endif


// ---------------------------------------------------------------------

#define	PLURAL(n)		((n)==1 ? "" : "s")

#define	NOBODY			(-1)
#define	UNKNOWN			(-1)
#define	DEFAULT			(-1)

#define	WHICH(current,def)	((current == DEFAULT) ? def : current)

typedef	enum {
    STATE_RUNNING = 0,	STATE_REBOOTING,	STATE_SHUTDOWN,
    STATE_CRASHED,	STATE_AUTOREBOOT,	STATE_PAUSED,
    STATE_UNDERREPAIR,	STATE_SINGLESTEP,	STATE_GAMEOVER,
    STATE_UNKNOWN
} RUNSTATE;

extern void newevent(CnetEvent ne, int node,
			CnetTime usecs, CnetTimerID timer, CnetData data);


// ------------------------ NIC INFORMATION -----------------------

typedef struct {
    CnetLinkType	linktype;
    CnetNICaddr		nicaddr;
    char		*name;

    CnetTime		tx_until;
    CnetTime		rx_until;
    bool		buffered;
    bool	 	up;

    int			bandwidth;
    CnetTime		jitter;
    CnetTime		propagation;
    int			mtu;

    bool		top_sets_probframecorrupt;	// set in topology
    int			top_probframecorrupt;	// value from topology
    int			probframecorrupt;	// actual run-time value

    bool		top_sets_probframeloss;
    int			top_probframeloss;
    int			probframeloss;

    bool		top_sets_costperbyte;
    int			top_costperbyte;
    int			costperbyte;

    bool		top_sets_costperframe;
    int			top_costperframe;
    int			costperframe;

    CnetTime		linkmtbf;
    CnetTime		linkmttr;

    CnetTime		slottime;		// usecs, only for LT_LAN
    WLANINFO		*wlaninfo;		// only for LT_WLAN

    CnetLinkStats	linkstats;
#if	defined(USE_TCLTK)
    int			displayed;
    bool		stats_changed;
#endif
} NICATTR;

extern void init_nicattrs(NICATTR *defw, NICATTR *defl, NICATTR *defwl);


// ------------------------ WAN INFORMATION -----------------------

#if	defined(USE_TCLTK)
typedef struct {
    int			wan;
    int			src;
    bool		corrupt;
    bool		lost;
    CnetTime		leaves;
    CnetTime		inflight;
    CnetDrawFrame	cdf;
} DRAWFRAME;
#endif

typedef struct {
    bool	 	up;
    int64_t	 	ntxed;

    int		 	minnode;
    int		 	minnode_nic;

    int		 	maxnode;
    int		 	maxnode_nic;
#if	defined(USE_TCLTK)
    bool		drawframes_init;
    int			drawframes_displayed;
    DRAWFRAME		*dfs[MAX_DRAWFRAMES];
    int			ndrawframes;
    int			lhsnode;
    int			rhsnode;
#endif
} WAN;

extern	WAN		*WANS;
extern	NICATTR		DEFAULTWAN;


// ------------------------ LAN INFORMATION -----------------------

typedef struct {
    int			node;		// index into NODES[]
    int			nodeslink;	// link of this nic on node
    int			ncollisions;
    int			nsuccesses;
} EACHNIC;

typedef struct {
    char		*name;
    int			x, y, x1;	// all LANs drawn horizontally

    NICATTR		segmentnic;	// default attrs of whole segment
    int			nnics;		// number of NICs on segment
    EACHNIC		*nics;		// vector of NICs on segment
} LAN;

extern	LAN		*LANS;
extern	NICATTR		DEFAULTLAN;


// ------------------------ WLAN INFORMATION -----------------------

typedef struct {
    int			node;
    int			node_link;
    NICATTR		*nic;
    WLANSTATE		state;
    WLANINFO		info;
    double		rx_signal;
    double		rx_angle;

#if	defined(USE_TCLTK)
    CnetColour		signalcolour;
    int			radius;
    int			maxradius;
#endif
} WLAN;

extern	WLAN		*WLANS;
extern	NICATTR		DEFAULTWLAN;


// ------------------------ NODE INFORMATION -----------------------

typedef struct {
    int64_t		tx_frames_lost;
    int64_t		msgs_deliverytime;
    int64_t		msgs_write_errors;
    int64_t		event_count[N_CNET_EVENTS];
} CnetHiddenStats;

typedef struct {
    CnetAddr		address;

//  APPLICATION LAYER INFO
    int64_t		top_messagerate;	// value from topology
    int64_t		messagerate;		// actual run-time value

    int			top_minmessagesize;
    int			minmessagesize;

    int			top_maxmessagesize;
    int			maxmessagesize;

    CnetTime		nodemtbf;
    CnetTime		nodemttr;

    double		battery_volts;		// only for NT_MOBILE
    double		battery_mAH;		// only for NT_MOBILE

    const char		*osname;
    const char		*icontitle;
    int			iconheight;

    const char		*compile;
    const char		**reboot_argv;
    const char		*rebootfunc;

    bool		stdio_quiet;
    const char		*outputfile;

    bool		trace_all;
    int			trace_mask;

    CnetPosition	position;		// coords in metres
    int			lastpx, lastpy;		// coords in pixels

    int			winx, winy;
    bool		winopen;
} NODEATTR;


typedef struct {
    RUNSTATE		runstate;
    CnetNodeType	nodetype;
    char		*nodename;

    NICATTR		*nics;		// indexed by nic we're writing to
    int			nnics;
    int			nframes;

    int			*wans;		// index into WANS[]
    int			*lans;		// index into LANS[]
    int			nwans;		// # of WAN NICs on this node
    int			nlans;		// # of LAN NICs on this node
    int			nwlans;		// # of WLAN NICs on this node

    int			os_errno;
    CnetError		cnet_errno;

    unsigned long	length_data[NDATASEGS];
    char		*incore_data[NDATASEGS];
    char		*private_data[NDATASEGS];
    char		*original_data[NDATASEGS];

    CnetTime		reboot_time;
    CnetTime		resume_time;
    CnetTime		clock_skew;

    NODEATTR		nattr;		//  actual attributes of this node
    NICATTR		defaultwan;	// default WAN attributes
    NICATTR		defaultlan;	// default LAN attributes
    NICATTR		defaultwlan;	// default WLAN attributes
    int			nexttimer;

    void	(*handler[N_CNET_EVENTS])(CnetEvent, CnetTimerID, CnetData);
    CnetData		data[N_CNET_EVENTS];
    int			outputfd;	// to file given by -o

    MT			*mt;		// random sequence used by protocol

    CnetHiddenStats	hiddenstats;
    CnetNodeStats	nodestats;

#if	defined(USE_TCLTK)
    int			displayed;
    bool		stats_changed;
    char		*debug_str[N_CNET_DEBUGS];
    char		*inputline;	// from the keyboard
    size_t		inputlen;

    double       	wlan_rx_history[WLAN_RX_HISTORY];
    int			wlan_index_history;
    int			wlan_prev_nbars;
#endif
} NODE;

extern	NODE		*NODES;

extern	NODEATTR	DEFAULTNODE;

// ------------------------------------------------------------------

typedef struct {
    int			nhosts;
    int			nrouters;
    int			nmobiles;
    int			naccesspoints;
    int			nwans;
    int			nlans;
    int			nwlans;

    CnetPosition	maxposition;		// coords in metres
    int			positionerror;		// coords in metres
    int			mapwidth, mapheight;	// coords in pixels
    int			tilewidth, tileheight;	// coords in pixels
    int			mapgrid;
    int			maphex;
    double		mapscale;

    const char		*mapcolour;
    const char		*mapimage;
    const char		*maptile;

    bool		drawnodes;
    bool		drawlinks;
    bool		drawwlans;
    bool		showcostperbyte;
    bool		showcostperframe;
    bool		stdio_quiet;
    bool		trace_events;
    const char		*trace_filenm;
    FILE		*tfp;

    int			nextensions;
    const char		*extensions[MAXEXTENSIONS];

    CnetHiddenStats	hiddenstats;
    CnetNodeStats	nodestats;
    CnetLinkStats	linkstats;
} GLOBALATTR;

extern	GLOBALATTR	gattr;

extern	int		_NNODES;	// used internally by cnet
extern	int		THISNODE;
extern	CnetEvent	HANDLING;

// ------------- parsing information and global things --------------

typedef struct {
    FILE		*fp;
    const char		*name;
    char		line[BUFSIZ];
    int			cc, ll, lc;

    int			ivalue;
    double		dvalue;
    CnetNICaddr		nicaddr;
} INPUT ;

extern	INPUT		input;

extern	char		chararray[];
extern	int		nerrors;

extern	RUNSTATE	cnet_state;

extern	CnetTime	MICROSECONDS;

extern	char		*argv0;

extern	bool		dflag;		// turn on debugging/diagnostics
extern	bool		Tflag;		// use fast-as-you-can scheduling
extern	bool		Wflag;		// run under the windowing env.
extern	int		vflag;		// be verbose about cnet's actions

extern	const char	*findenv(const char *, const char *);
extern	char		*find_cnetfile(const char *fm, int wantdir, bool fatal);
extern	char		*find_trace_name(void *addr);
extern	char		*format_nodeinfo(NODE *np, const char *str);

extern	CnetTime	poisson_usecs(CnetTime, MT *);

extern	void		CLEANUP(int);			// calls exit(int)
extern	void		ERROR(CnetError err);		// sets cnet_errno
extern	void		FATAL(const char *fmt, ...);	// calls CLEANUP(1)
extern	void		REPORT(const char *fmt, ...);	// IFF dflag or vflag
extern	void		SYNTAX(const char *fmt, ...);
extern	void		TRACE(int result, const char *fmt, ...);
extern	void		WARNING(const char *fmt, ...);


// ----------------------------------------------------------------------

#define NEW(type)	calloc((unsigned)1, sizeof(type))
#define FREE(ptr)	do { free(ptr); ptr = NULL; } while(false)

#ifndef	M_PI
#define	M_PI		3.14159265358979323846  // pi                        
#endif
#ifndef	M_PI_2
#define M_PI_2		1.57079632679489661923	// pi/2
#endif

#define M2PX(metres)	((int)((metres)/gattr.mapscale))
#define PX2M(pixel)	((int)((pixel)*gattr.mapscale))

#define	MAX(a, b)	((a) > (b) ? (a) : (b))

#endif
