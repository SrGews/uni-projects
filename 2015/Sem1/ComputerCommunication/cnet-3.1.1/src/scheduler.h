#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef	_SCHEDULER_H_
#define	_SCHEDULER_H_	1

/*  We can employ one of two types of queueing here - either a basic
    Linear Queuing, which is reasonable for queuing up to a hundred pending
    events, or the more complex and fast Calendar Queuing, which remains
    fast for several thousand pending events.
    Define either:
			USE_CALENDARQ
	or		USE_LINEARQ
 */

#define	USE_CALENDARQ


#define	EACH_SELECT		20000		// in usecs
#define	MIN_SELECT		1000		// in usecs

#if	defined(USE_TCLTK)
#define	PING_TCLTK_FREQ		40000		// in usecs
#define	N_SPEEDS		10
#define	NORMAL_SPEED		((N_SPEEDS+1) / 2)

extern	bool	debug_pressed;
#endif

typedef enum {
    SCHED_NULL = 0,
    SCHED_POLLAPPL,
    SCHED_FRAMEARRIVAL,
    SCHED_FRAMECOLLISION,
    SCHED_TIMER,
    SCHED_NODESTATE,
    SCHED_LINKSTATE,
#if	defined(USE_TCLTK)
    SCHED_DEBUGBUTTON,
    SCHED_KEYBOARDREADY,
    SCHED_DRAWFRAME,
    SCHED_DRAWSIGNAL,
    SCHED_UPDATEGUI,
    SCHED_MOVEFRAME,
#endif
    SCHED_PERIODIC,
    SCHED_GAMEOVER
} SCHEDEVENT;

typedef struct _e {
    int64_t	usec;
    SCHEDEVENT	se;
    int		node;
    CnetEvent	ne;
    CnetTimerID	timer;
    CnetData	data;
    struct _e	*next;
} EVENT;


#if	defined(USE_CALENDARQ)

/*  THIS IMPLEMENTATION OF A CALENDAR QUEUE CLOSELY FOLLOWS THE IDEAS
    FIRST PUBLISHED IN:

	R. Brown. "Calendar queues: A fast O(1) priority
	queue implementation for the simulation event set problem".
	Comm. ACM, 31(10):1220--1227, Oct.  1988.
 */

#define	INIT_NBUCKETS		2
#define	INIT_WIDTH		3
#define	PREDAWN			(0)

#define FOREACH_BUCKET  for(b=0, bp=CQ->buckets ; b<CQ->nbuckets ; ++b, ++bp)
#define	FOREACH_EVENT	for(e = bp->head ; e ; e = e->next)

typedef struct {
    int		nevents;
    int64_t	earliest;
    EVENT	*head;
    EVENT	*tail;
} BUCKET;

typedef struct {
    int		nevents;	// # of events in the calendar
    int		nbuckets;	// number of buckets in queue
    BUCKET	*buckets;	// point to Q->nbuckets buckets

    int64_t	lastusec;
    int		lastbucket;

    int64_t	bucketwidth;	// width of each day in the calendar
    int64_t	buckettop;

    int		minthreshold;
    int		maxthreshold;

    bool	resizeable;
} CALENDARQ;


#elif	defined(USE_LINEARQ)

#define	FOREACH_EVENT	for(e = LQ->head ; e ; e = e->next)

typedef struct {
    EVENT	*head;
    int		nevents;
} LINEARQ;
#endif


extern	void	init_queuing(void);
extern	EVENT	*get_next_event(void);

extern	void	internal_event(SCHEDEVENT, CnetEvent, int, CnetTime, CnetData);
extern	void	schedule_draw_wlansignal(int thiswan);
extern	void	unschedule_node(int node);
extern	void	unschedule_link(int node, int link);

extern	int	nqueued;

#endif
