
#if	!defined(_CNET2TO3_H)
#define	_CNET2TO3_H

//  DATATYPES THAT HAVE CHANGED
#define	CnetNodeinfo		CnetNodeInfo
#define	CnetLinkinfo		CnetLinkInfo
#define	CnetInt64		CnetTime
#define	CnetTimer		CnetTimerID

//  ENUMERATED CONSTANTS THAT HAVE CHANGED
#define	LT_POINT2POINT		LT_WAN
#define	EV_DEBUG5		EV_DEBUG0
#define	EV_TIMER10		EV_TIMER0
#define	TE_TIMER10		TE_TIMER0

#define	ER_BADTIMER		ER_BADTIMERID

//  STRUCTURE FIELDS THAT HAVE CHANGED
#define	transmitbufsize		mtu

//  FUNCTION NAMES THAT HAVE CHANGED
#define	checksum_crc16		CNET_crc16
#define	checksum_crc32		CNET_crc32
#define	checksum_ccitt		CNET_ccitt
#define	checksum_internet	CNET_IP_checksum
#define	CNET_shared		CNET_shmem

//  COLOURS REPRESENTATIONS THAT HAVE CHANGED
#define CN_GREEN                "green"
#define CN_PURPLE               "purple"
#define CN_CYAN                 "cyan"
#define CN_RED                  "red"
#define CN_YELLOW               "yellow"
#define CN_BLUE                 "blue"

#endif
