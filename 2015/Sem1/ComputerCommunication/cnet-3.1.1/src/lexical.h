
/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

typedef enum {

T_EOF		= -1,
T_BAD		=  0,

T_NICATTR_FIRST,
T_WANATTR_FIRST,
    T_WANATTR_BANDWIDTH,
    T_WANATTR_BUFFERED,
    T_WANATTR_JITTER,
    T_WANATTR_MTU,
    T_WANATTR_PROPAGATION,
    T_WANATTR_COSTPERBYTE,
    T_WANATTR_COSTPERFRAME,
    T_WANATTR_MTBF,
    T_WANATTR_MTTR,
    T_WANATTR_PROBFRAMECORRUPT,
    T_WANATTR_PROBFRAMELOSS,
T_WANATTR_LAST,

T_LANATTR_FIRST,
    T_LANATTR_BANDWIDTH,
    T_LANATTR_BUFFERED,
    T_LANATTR_JITTER,
    T_LANATTR_MTU,
    T_LANATTR_PROPAGATION,
    T_LANATTR_COSTPERBYTE,
    T_LANATTR_COSTPERFRAME,
    T_LANATTR_MTBF,
    T_LANATTR_MTTR,
    T_LANATTR_PROBFRAMECORRUPT,
    T_LANATTR_PROBFRAMELOSS,
    T_LANATTR_SLOTTIME,
T_LANATTR_LAST,

T_WLANATTR_FIRST,
    T_WLANATTR_BANDWIDTH,
    T_WLANATTR_BUFFERED,
    T_WLANATTR_COSTPERBYTE,
    T_WLANATTR_COSTPERFRAME,
    T_WLANATTR_FREQUENCY,
    T_WLANATTR_IDLECURRENT,
    T_WLANATTR_JITTER,
    T_WLANATTR_MTBF,
    T_WLANATTR_MTTR,
    T_WLANATTR_MTU,
    T_WLANATTR_PROBFRAMECORRUPT,
    T_WLANATTR_PROBFRAMELOSS,
    T_WLANATTR_RXANTENNAGAIN,
    T_WLANATTR_RXCABLELOSS,
    T_WLANATTR_RXCURRENT,
    T_WLANATTR_RXSENSITIVITY,
    T_WLANATTR_RXSIGNALTONOISE,
    T_WLANATTR_SLEEPCURRENT,
    T_WLANATTR_TXANTENNAGAIN,
    T_WLANATTR_TXCABLELOSS,
    T_WLANATTR_TXCURRENT,
    T_WLANATTR_TXPOWER,
T_WLANATTR_LAST,
T_NICATTR_LAST,

T_NODEATTR_FIRST,
    T_NODEATTR_ADDRESS,
    T_NODEATTR_COMPILE,
    T_NODEATTR_ICONTITLE,
    T_NODEATTR_MAXMESSAGESIZE,
    T_NODEATTR_MESSAGERATE,
    T_NODEATTR_MINMESSAGESIZE,
    T_NODEATTR_MTBF,
    T_NODEATTR_MTTR,
    T_NODEATTR_OSNAME,
    T_NODEATTR_OUTPUTFILE,
    T_NODEATTR_REBOOTARGV,
    T_NODEATTR_REBOOTFUNC,
    T_NODEATTR_TRACE,
    T_NODEATTR_WINOPEN,
    T_NODEATTR_WINX,
    T_NODEATTR_WINY,
    T_NODEATTR_X,
    T_NODEATTR_Y,
    T_NODEATTR_Z,
T_NODEATTR_LAST,

T_ACCESSPOINT,
T_ACCESSPOINTTYPE,
T_BITSPS,
T_BYTES,
T_BYTESPS,
T_C,
T_COMMA,
T_dBm,
T_DRAWLINKS,
T_DRAWNODES,
T_EQ,
T_ETHERADDR,
T_EXTENSION,
T_FALSE,
T_FROM,
T_GHz,
T_HOST,
T_HOSTTYPE,
T_INTCONST,
T_JOIN,
T_KBITSPS,
T_KBYTES,
T_KBYTESPS,
T_LAN,
T_LANSEGMENT,
T_LANTYPE,
T_LCURLY,
T_mA,
T_MAPCOLOUR,
T_MAPGRID,
T_MAPHEIGHT,
T_MAPHEX,
T_MAPIMAGE,
T_MAPSCALE,
T_MAPTILE,
T_MAPWIDTH,
T_MBITSPS,
T_MBYTES,
T_MBYTESPS,
T_METRES,
T_MHz,
T_MOBILE,
T_MOBILETYPE,
T_mW,
T_MSEC,
T_NAME,
T_NICADDR,
T_NICADDRVALUE,
T_OF,
T_POSITIONERROR,
T_RCURLY,
T_REALCONST,
T_ROUTER,
T_ROUTERTYPE,
T_SEC,
T_SHOWCOSTPERBYTE,
T_SHOWCOSTPERFRAME,
T_STRCONST,
T_TO,
T_TOGGLE,
T_TRACEFILE,
T_TRUE,
T_USEC,
T_WAN,
T_WANTYPE,
T_WLAN,
T_WLANTYPE,

T_NORTH,
T_NORTHEAST,
T_EAST,
T_SOUTHEAST,
T_SOUTH,
T_SOUTHWEST,
T_WEST,
T_NORTHWEST

} TOKEN;

extern	TOKEN		token;

extern	void		init_lexical(void);
extern	void		gettoken(void);

#define	is_node_type(t)		(t==T_HOST	|| t==T_ROUTER || \
				 t==T_MOBILE	|| t==T_ACCESSPOINT)
#define	is_node_attr(t)		((int)t>(int)T_NODEATTR_FIRST && \
				 (int)t<(int)T_NODEATTR_LAST)
#define	is_AL_attr(t)		(t == T_NODEATTR_MESSAGERATE	|| \
			 	 t == T_NODEATTR_MINMESSAGESIZE	|| \
			 	 t == T_NODEATTR_MAXMESSAGESIZE)
#define	is_nic_attr(t)		((int)t>(int)T_NICATTR_FIRST && \
				 (int)t<(int)T_NICATTR_LAST)
#define	is_wan_attr(t)		((int)t>(int)T_WANATTR_FIRST && \
				 (int)t<(int)T_WANATTR_LAST)
#define	is_lan_attr(t)		((int)t>(int)T_LANATTR_FIRST && \
				 (int)t<(int)T_LANATTR_LAST)
#define	is_wlan_attr(t)		((int)t>(int)T_WLANATTR_FIRST && \
				 (int)t<(int)T_WLANATTR_LAST)
#define	is_compass_direction(t)	((int)t>=(int)T_NORTH && \
				 (int)t<=(int)T_NORTHWEST)
