#include "cnetprivate.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

int CNET_set_battery(double new_volts, double new_mAH)
{
    int	 result	= -1;
    NODE *np	= &NODES[THISNODE];

    if(np->nodetype != NT_MOBILE)
	ERROR(ER_NOTSUPPORTED);
    else if(new_volts < 0.0 || new_mAH < 0.0)
	ERROR(ER_BADARG);
    else {
	np->nattr.battery_volts	= new_volts;
	np->nattr.battery_mAH	= new_mAH;
	result			= 0;
    }

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(new_mAH=%.2f, new_mAH=%.2f) = 0\n",
			__func__, new_volts, new_mAH);
	else
	    TRACE(1, "\t%s(new_mAH=%.2f, new_mAH=%.2f) = -1\n",
			__func__, new_volts, new_mAH,
			cnet_errname[(int)cnet_errno]);
    }
    return(result);
}

int CNET_get_battery(double *volts, double *mAH)
{
    int	 result	= -1;
    NODE *np	= &NODES[THISNODE];

    if(np->nodetype != NT_MOBILE)
	ERROR(ER_NOTSUPPORTED);
    else {
	if(volts)
	    *volts	= np->nattr.battery_volts;
	if(mAH)
	    *mAH	= np->nattr.battery_mAH;
	result			= 0;
    }

    if(gattr.trace_events) {
	char	res1[32], res2[32];

	if(volts)
	    sprintf(res1, "%.2f", np->nattr.battery_volts);
	else
	    strcmp(res1, "NULL");
	if(mAH)
	    sprintf(res2, "%.2f", np->nattr.battery_mAH);
	else
	    strcmp(res2, "NULL");

	if(result == 0)
	    TRACE(0, "\t%s(*volts, *mAH) = 0 (%s,%s)\n",
			__func__, res1, res2);
	else
	    TRACE(1, "\t%s(*volts, *mAH) = -1\n",
			__func__,
			cnet_errname[(int)cnet_errno]);
    }
    return(result);
}
