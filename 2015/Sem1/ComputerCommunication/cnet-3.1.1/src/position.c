#include "cnetprivate.h"
#include "physicallayer.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


//  COORDINATES ARE IN METRES
int CNET_get_position(CnetPosition *now, CnetPosition *max)
{
    NODE	*np = &NODES[THISNODE];
    NODEATTR	*na = &np->nattr;

    if(now) {
	if(gattr.positionerror == 0) {
//  REPORT CURRENT POSITION ACCURATELY
	    now->x	= na->position.x;
	    now->y	= na->position.y;
	    now->z	= na->position.z;
	}
	else {
//  INTRODUCE SOME ERROR INTO THE REPORTED POSITION
	    double angle  = (CNET_nextrand(np->mt) % 360) * M_PI / 180.0;
	    double radius = (CNET_nextrand(np->mt) % (gattr.positionerror+1));
	    double dx	  = radius * cos(angle);
	    double dy	  = radius * sin(angle);

	    now->x	= na->position.x + (int)dx;
	    now->y	= na->position.y + (int)dy;
	    now->z	= na->position.z;
	}
    }

//  THE SIMULATION BOUNDS ARE ALWAYS REPORTED ACCURATELY
    if(max) {
	max->x	= gattr.maxposition.x;
	max->y	= gattr.maxposition.y;
	max->z	= gattr.maxposition.z;
    }

    if(gattr.trace_events) {
	char	fmta[64], fmtb[64];

	if(now)
	    sprintf(fmta,"{x=%d,y=%d,z=%d}", now->x, now->y, now->z);
	else
	    strcpy(fmta,"NULL");

	if(max)
	    sprintf(fmtb,"{x=%d,y=%d,z=%d}", max->x, max->y, max->z);
	else
	    strcpy(fmtb,"NULL");

	TRACE(0, "\t%s(&now,&max) = 0 (%s,%s)\n", __func__, fmta, fmtb);
    }
    return(0);
}

//  COORDINATES ARE IN METRES
int CNET_set_position(CnetPosition new)
{
    int		result	= -1;
    NODEATTR	*na	= &NODES[THISNODE].nattr;

    if(!NODE_MAY_MOVE(THISNODE))
	ERROR(ER_NOTSUPPORTED);
    else if(new.x < 0 || new.x >= gattr.maxposition.x ||
	    new.y < 0 || new.y >= gattr.maxposition.y ||
	    new.z < 0 )
	ERROR(ER_BADPOSITION);
    else {
#if	defined(USE_TCLTK)
	if(Wflag) {
	    int savev	= vflag;

	    vflag	= false;
	    TCLTK("$map coords n%d %d %d",
		    THISNODE, M2PX(new.x), M2PX(new.y));
	    TCLTK("$map coords nl%d %d %d",
		    THISNODE, M2PX(new.x), M2PX(new.y)+na->iconheight/2);
	    vflag	= savev;
	}
#endif
	na->position.x	= new.x;
	na->position.y	= new.y;
	na->position.z	= new.z;
	result		= 0;
    }

    if(gattr.trace_events) {
	char	fmt[64];

	sprintf(fmt, "{x=%d,y=%d,z=%d}", new.x, new.y, new.z);
	if(result == 0)
	    TRACE(0, "\t%s(%s) = 0\n", __func__, fmt);
	else
	    TRACE(1, "\t%s(%s) = -1 %s\n",
			__func__, fmt, cnet_errname[(int)cnet_errno]);
    }
    return result;
}

double CNET_get_mapscale(void)
{
    if(gattr.trace_events)
	TRACE(0, "\t%s() = %.2f\n", __func__, gattr.mapscale);

    return gattr.mapscale;
}
