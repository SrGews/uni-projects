#include "cnetprivate.h"
#include "scheduler.h"
#include "physicallayer.h"

/*  The cnet network simulator (v3.1.1)
    Copyright (C) 1992-onwards, Chris McDonald

    Chris McDonald, chris@csse.uwa.edu.au
    School of Computer Science & Software Engineering
    The University of Western Australia,
    Crawley, Western Australia, 6009
    PH: +61 8 6488 2533, FAX: +61 8 6488 1089.

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, version 2 of the License.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#define	SCHEDULER_SEED		6432085

#if	defined(USE_TCLTK)
static	TCLTK_COMMAND(scheduler_speed);
static	int64_t		gui_every_nevents	= 0;
static	CnetTime	gui_every_nusecs	= 0;
#endif

static	NODE		*np;			// always &NODES[THISNODE]
static	MT		*mt;
static	CnetTime	BIGBANG;

static	int64_t		finish_after_nevents	= 0;
static	CnetTime	finish_after_nusecs	= 0;
static	int64_t		stats_every_nevents	= 0;
static	CnetTime	stats_every_nusecs	= 0;

static	int		cntx			= 0;
static	int		events_handled		= 0;
static	int		reveal_nnodes 		= 0;
static	int		min_nqueued		= 0;

static	bool		sflag			= false;


// ---------- CALCULATE TIMES OF NODE AND LINK FAILURE AND REPAIR ---------

static CnetTime node_mtbf(void)
{
    CnetTime extent = WHICH(np->nattr.nodemtbf, DEFAULTNODE.nodemtbf);

    if(extent == 0)
	return((CnetTime)0);
    else
	return(poisson_usecs(extent, mt));
}

static CnetTime node_mttr(void)
{
    CnetTime extent = WHICH(np->nattr.nodemttr, DEFAULTNODE.nodemttr);

    if(extent == 0)
	return((CnetTime)0);
    else
	return(poisson_usecs(extent, mt));
}


static CnetTime link_mtbf(WAN *wanp)
{
    CnetTime	t1, t2, extent;

    t1	= WHICH(NODES[wanp->minnode].nics[wanp->minnode_nic].linkmtbf,
		DEFAULTWAN.linkmtbf);

    t2	= WHICH(NODES[wanp->maxnode].nics[wanp->maxnode_nic].linkmtbf,
		DEFAULTWAN.linkmtbf);

         if(t1 == 0)
	extent = t2;
    else if(t2 == 0 || t1 < t2)
	extent = t1;
    else
	extent = t2;

    if(extent == 0)
	return((CnetTime)0);
    else
	return(poisson_usecs(extent, mt));
}

static CnetTime link_mttr(WAN *wanp)
{
    CnetTime	t1, t2;

    t1	= WHICH(NODES[wanp->minnode].nics[wanp->minnode_nic].linkmttr,
		DEFAULTWAN.linkmttr);

    t2	= WHICH(NODES[wanp->maxnode].nics[wanp->maxnode_nic].linkmttr,
		DEFAULTWAN.linkmttr);

    if(t1 > t2)
	return(poisson_usecs(t1, mt));
    else
	return(poisson_usecs(t2, mt));
}


// ------------------------------------------------------------------------

//  THE COMPILER SHOULD UNROLL THESE LOOPS IF NDATASEGS > 1

static void swapout_data(int n)
{
    NODE *p	= &NODES[n];
    int	 i;

    for(i=0 ; i<NDATASEGS ; ++i)
	if(p->length_data[i])
	    memcpy(p->private_data[i], p->incore_data[i], p->length_data[i]);
}

static void swapin_data(int n)
{
    NODE *p	= &NODES[n];
    int	 i;

    for(i=0 ; i<NDATASEGS ; ++i)
	if(p->length_data[i])
	    memcpy(p->incore_data[i], p->private_data[i], p->length_data[i]);
}

static void reboot_data(void)
{
    int  i;

    for(i=0 ; i<NDATASEGS ; ++i)
	if(np->length_data[i])
	    memcpy(np->incore_data[i],np->original_data[i],np->length_data[i]);
}

// ------------------------------------------------------------------------

#define	CONTEXT_SWITCH()	if(THISNODE != SWAPPED_IN) context_switch()

static	int		SWAPPED_IN;

static void context_switch(void)
{
    CnetLinkInfo	*li;
    NICATTR		*nic;
    int			l;

// SWAP OUT THE SWAPPED_IN PROCESS, SWAP IN THE THISNODE PROCESS
    NODES[SWAPPED_IN].os_errno		= errno;
    NODES[SWAPPED_IN].cnet_errno	= cnet_errno;

    swapout_data(SWAPPED_IN);
    swapin_data(THISNODE);

    nodeinfo.nodenumber	= THISNODE;
    strcpy(nodeinfo.nodename, np->nodename);
    nodeinfo.nodetype	= np->nodetype;
    nodeinfo.address	= np->nattr.address;

    nodeinfo.minmessagesize	=
	    WHICH(np->nattr.minmessagesize, DEFAULTNODE.minmessagesize);
    nodeinfo.maxmessagesize	=
	    WHICH(np->nattr.maxmessagesize, DEFAULTNODE.maxmessagesize);
    nodeinfo.messagerate	=
	    WHICH(np->nattr.messagerate, DEFAULTNODE.messagerate);

    nodeinfo.nlinks		= np->nnics;

//  INITIALIZE THE NODE'S LOOPBACK LINK
    memset(&linkinfo[LOOPBACK_LINK], 0, sizeof(CnetLinkInfo));
    strcpy(linkinfo[LOOPBACK_LINK].linkname, "lo");
    linkinfo[LOOPBACK_LINK].linktype	= LT_LOOPBACK;
    linkinfo[LOOPBACK_LINK].linkup	= true;
    linkinfo[LOOPBACK_LINK].mtu		= MAX_MESSAGE_SIZE + 1024;

//  INITIALIZE THE NODE'S PHYSICAL LINKS
    for(l=1, li=&linkinfo[1] ; l<=np->nnics ; ++l, ++li) {
	nic			= &np->nics[l];
	strcpy(li->linkname, nic->name);

	li->linktype		= nic->linktype;
	li->linkup		= nic->up;
	memcpy(li->nicaddr, nic->nicaddr, LEN_NICADDR);

//  THESE ATTRIBUTES CANNOT BE CHANGED VIA THE GUI
	li->bandwidth		= nic->bandwidth;
	li->mtu			= nic->mtu;
	li->propagationdelay	= nic->propagation;

//  THESE ATTRIBUTES MAY BE CHANGED VIA THE GUI
    switch(nic->linktype) {
      case LT_LOOPBACK :
	break;
      case LT_WAN :
	li->costperbyte  = WHICH(nic->costperbyte, DEFAULTWAN.costperbyte);
	li->costperframe = WHICH(nic->costperframe,DEFAULTWAN.costperframe);
	break;
      case LT_LAN :
	li->costperbyte  = WHICH(nic->costperbyte, DEFAULTLAN.costperbyte);
	li->costperframe = WHICH(nic->costperframe,DEFAULTLAN.costperframe);
	break;
      case LT_WLAN :
	li->costperbyte  = WHICH(nic->costperbyte, DEFAULTWLAN.costperbyte);
	li->costperframe = WHICH(nic->costperframe,DEFAULTWLAN.costperframe);
	break;
      }
    }

//  AND FINALLY, THE NODE'S FINAL GLOBAL ATTRIBUTES
    errno		= np->os_errno;
    cnet_errno		= np->cnet_errno;

    NNODES		= reveal_nnodes;	// iff invoked with -N
    SWAPPED_IN		= THISNODE;
    cntx++;
}

// ------------------------------------------------------------------------

static void HANDLER(CnetEvent ev, CnetTimerID timer, CnetData data)
{
    CnetTime	usec;

    if(np->handler[(int)ev] == NULL)
	return;

    CONTEXT_SWITCH();

//  SET THE REAL-WORLD TIME ON THE NODE, ALLOWING FOR ITS CLOCK SKEW
    usec			= BIGBANG + MICROSECONDS + np->clock_skew;
    nodeinfo.time_of_day.sec	= (int)(usec / (CnetTime)1000000);
    nodeinfo.time_of_day.usec	= (int)(usec % (CnetTime)1000000);
    nodeinfo.time_in_usec	= MICROSECONDS - np->reboot_time;

    np->nodestats.nevents++;
    gattr.hiddenstats.event_count[(int)ev]++;
    gattr.nodestats.nevents++;

    gattr.stdio_quiet	= DEFAULTNODE.stdio_quiet	|
			  np->nattr.stdio_quiet;

    gattr.trace_events	= DEFAULTNODE.trace_all		|
			  np->nattr.trace_all		|
			  (np->nattr.trace_mask & (1<<(int)ev));

    HANDLING = ev;
    if(gattr.trace_events) {
	extern	void	trace_handler(NODE *, CnetEvent, CnetTimerID, CnetData);

	trace_handler(np, ev, timer, data);
    }
    else
	np->handler[(int)ev](ev, timer, data);

    ++events_handled;
}

static void invoke_reboot(int which)
{
    np	= &(NODES[THISNODE = which]);
    if(np->handler[(int)EV_REBOOT]) {

	extern int	reboot_application_layer(void);
	extern int	reboot_stdio_layer(void);
	extern void	reboot_timer_layer(void);

	CnetTime	evtime;

	context_switch();		// ALWAYS PERFORMED
	reboot_data();
	unschedule_node(THISNODE);

	if(NODE_HAS_AL(THISNODE))
	    reboot_application_layer();

	reboot_stdio_layer();
	reboot_timer_layer();

	memset(&np->nodestats, 0, sizeof(CnetNodeStats));
	np->reboot_time	= MICROSECONDS;
	np->nframes	= 0;
	np->os_errno	= 0;
	np->cnet_errno	= ER_OK;
	cnet_errno	= ER_OK;

	np->runstate	= STATE_REBOOTING;
	HANDLER(EV_REBOOT, NULLTIMER, (CnetData)np->nattr.reboot_argv);
	np->runstate	= STATE_RUNNING;

//  NOW INSERT AN EVENT IF THIS NODE IS DOOMED TO FAIL
	evtime	= node_mtbf();
	if(evtime != 0)
	    internal_event(SCHED_NODESTATE, EV_REBOOT, THISNODE, evtime, 0);
    }
    else
	FATAL("%s rebooted without an EV_REBOOT handler!\n", np->nodename);
}

void invoke_shutdown(int whichnode)
{
    np	= &(NODES[THISNODE = whichnode]);

    HANDLER(EV_SHUTDOWN, NULLTIMER, np->data[(int)EV_SHUTDOWN]);
    np->runstate	= STATE_SHUTDOWN;
}


// ------------------------------------------------------------------------

static void parse_period(const char *str, int64_t *nevents, CnetTime *nusecs,
			 char *buf)
{
    const char	*s	= str;
    int64_t	intval	= 0;

    char	*u1	= "Updated every";
    char	*u2	= "of simulated time";

    *nevents	= 0;
    *nusecs	= 0;

    while(isdigit(*s)) {
	intval = intval*(int64_t)10 + (int64_t)(*s - '0');
	++s;
    }
    while(*s == ' ' || *s == '\t')
	++s;
    switch (*s) {
    case 's' :	if(buf)
		    sprintf(buf, "%s %" PRId64 " second%s %s",
					    u1, intval, PLURAL(intval), u2);
		*nusecs = intval * (int64_t)1000000;
		break;
    case 'm' :	if(*(s+1) == 's') {
		    if(buf)
			sprintf(buf, "%s %" PRId64 "msec %s", u1, intval, u2);
		    *nusecs = intval * (int64_t)1000;
		}
		else {
		    if(buf)
			sprintf(buf, "%s %" PRId64 "min%s %s",
					    u1, intval, PLURAL(intval), u2);
		    *nusecs = intval * (int64_t)60 * (int64_t)1000000;
		}
		break;
    case 'h' :	if(buf)
		    sprintf(buf, "%s %" PRId64 " hour%s %s",
					    u1, intval, PLURAL(intval), u2);
		*nusecs = intval * (int64_t)3600 * (int64_t)1000000;
		break;
    case 'u' :	if(buf)
		    sprintf(buf, "%s %" PRId64 "usec%s %s",
					    u1, intval, PLURAL(intval), u2);
		*nusecs = intval;
		break;
    case 'e' :	if(buf)
		    sprintf(buf, "%s %" PRId64 " event%s %s",
					    u1, intval, PLURAL(intval), u2);
		*nevents = intval;
		break;
    default :	FATAL("unrecognized period '%s'\n", str);
		break;
    }
}

// ------------------------------------------------------------------------

#if	defined(USE_TCLTK)
static	char	*update_title;

//  CHANGE THE FREQUENCY WITH WHICH WE FLUSH THE GUI
static TCLTK_COMMAND(update_speed)
{
    TCLTK_ARGCHECK(2);
    parse_period(argv[1],&gui_every_nevents,&gui_every_nusecs,update_title);
    Tcl_UpdateLinkedVar(tcl_interp, "UPDATE_TITLE");
    return TCL_OK;
}
#endif

// ------------------------------------------------------------------------

void init_scheduler(const char *eflag, const char *fflag, bool Nflag,
			int Sflag, bool _sflag, const char *uflag)
{
    struct timeval	NOW;
    CnetTime		evtime;
    int			n;

    gettimeofday(&NOW, NULL);
    BIGBANG		= (CnetTime)NOW.tv_sec*(CnetTime)1000000+NOW.tv_usec;
    MICROSECONDS	= 0;

    reveal_nnodes	= Nflag ? _NNODES : 0;
    mt			= mt19937_init((unsigned long)(SCHEDULER_SEED+Sflag));
    sflag		= _sflag;

    THISNODE		= -1;
    SWAPPED_IN		= _NNODES-1;

    init_queuing();

//  IT IS ESSENTIAL THAT NODE 0 REBOOTS FIRST (cf. context_switch() )
    for(n=0 ; n<_NNODES ; ++n) {
	NODES[n].runstate = STATE_REBOOTING;
	internal_event(SCHED_NODESTATE, EV_REBOOT, n, (CnetTime)0, 0);
    }

//  NEXT, INSERT ALL POSSIBLE WAN FAILURES
    for(n=0 ; n<gattr.nwans ; ++n) {
	evtime	= link_mtbf(&WANS[n]);
	if(evtime != 0)
	    newevent(EV_LINKSTATE, WANS[n].minnode,
				evtime, NULLTIMER, (CnetData)n);
    }

//  DETERMINE THE FREQUENCY WITH WHICH WE FLUSH THE GUI
#if	defined(USE_TCLTK)
    if(Wflag) {

	update_title	= Tcl_Alloc(128);
	Tcl_LinkVar(tcl_interp, "UPDATE_TITLE", (char *)&update_title,
			TCL_LINK_STRING|TCL_LINK_READ_ONLY); 
	parse_period(uflag,&gui_every_nevents,&gui_every_nusecs,update_title);
	Tcl_UpdateLinkedVar(tcl_interp, "UPDATE_TITLE");
	if(gui_every_nusecs != 0) {
	    internal_event(SCHED_UPDATEGUI,EV_NULL,UNKNOWN,gui_every_nusecs,0);
	    ++min_nqueued;
	}
	TCLTK_createcommand("update_speed", update_speed);

	TCLTK_createcommand("scheduler_speed", scheduler_speed);
    }
#endif

//  INSERT AN EVENT TO PERIODICALLY DO THINGS
    if(fflag) {
	parse_period(fflag,&stats_every_nevents,&stats_every_nusecs,NULL);
	if(stats_every_nusecs != 0) {
	    internal_event(SCHED_PERIODIC,EV_NULL,UNKNOWN,stats_every_nusecs,0);
	    ++min_nqueued;
	}
    }

//  AND FINALLY, AN EVENT TO FINISH THE SIMULATION
    parse_period(eflag, &finish_after_nevents, &finish_after_nusecs, NULL);
    if(finish_after_nusecs != 0) {
	internal_event(SCHED_GAMEOVER, EV_NULL, UNKNOWN,
			finish_after_nusecs+(CnetTime)1, 0);
	++min_nqueued;
    }
}


// ------------------------------------------------------------------------

static void do_periodically(void)
{
    extern void	flush_allstats(bool);
    int		n;

//  PERIODICALLY REPORT SCHEDULER CONTEXT TO STDERR
    if(dflag) {
	REPORT("\tusec=%s contexts=%5d events=%6d nqueued=%3d\n",
		  CNET_format64(MICROSECONDS), cntx, events_handled, nqueued);
	events_handled	= 0;
	cntx		= 0;
    }
//  PERIODICALLY REPORT STATISTICS TO STDOUT
    if(sflag)
	flush_allstats(false);

//  INVOKE THE EV_PERIODIC HANDLER FOR ANY INTERESTED NODE
    for(n=0 ; n<_NNODES ; ++n) {
	np	= &(NODES[THISNODE = n]);
	HANDLER(EV_PERIODIC, NULLTIMER, np->data[(int)EV_PERIODIC]);
    }
}

#if	defined(USE_TCLTK)
static void do_GUIupdates(void)		// only called if -W set
{
    extern void	flush_GUIstats(void);
    int		n;

    flush_GUIstats();			// report statistics to TCL/TK GUI

//  INVOKE THE EV_UPDATEGUI HANDLER FOR ANY INTERESTED NODE
    for(n=0 ; n<_NNODES ; ++n) {
	np	= &NODES[THISNODE = n];
	HANDLER(EV_UPDATEGUI, NULLTIMER, np->data[(int)EV_UPDATEGUI]);
    }
}

// ------------------------------------------------------------------------

bool	debug_pressed		= false;
static	float	delaymult	= 1.0;

static TCLTK_COMMAND(scheduler_speed)
{
    TCLTK_ARGCHECK(2);
    n	= atoi(argv[1]);
    if(n < 1 || n > N_SPEEDS) {
	interp->result	= "invalid speed #";
	return TCL_ERROR;
    }

    Tflag	= (n == N_SPEEDS);
    if(n == NORMAL_SPEED)
	delaymult = 1.0;
    else if(n < NORMAL_SPEED)
	delaymult = (1<<(NORMAL_SPEED - n));
    else	// n > NORMAL_SPEED
	delaymult = 1.0/(1<<(n - NORMAL_SPEED));
    return TCL_OK;
}

void schedule_draw_wlansignal(int thiswan)
{
    internal_event(SCHED_DRAWSIGNAL, EV_NULL, THISNODE,
		  (CnetTime)DELAY_DRAW_WLANSIGNAL, (CnetData)thiswan);
}

void schedule_moveframe(CnetTime usec, CnetData data)
{
    internal_event(SCHED_MOVEFRAME, EV_NULL, THISNODE, usec, data);
}

static void PING_TCLTK(void)
{
    static CnetTime	time_next_ping	= 0;

    struct timeval	NOW;
    CnetTime		now_usecs;

    gettimeofday(&NOW, NULL);
    now_usecs	= (CnetTime)NOW.tv_sec*(CnetTime)1000000 + NOW.tv_usec;

    if(now_usecs > time_next_ping) {
	tcltk_notify_dispatch();

	time_next_ping	= now_usecs + PING_TCLTK_FREQ;
    }
}
#endif

static void advance_time(CnetTime newMICROSECONDS)
{
/*  WE ADVANCE TIME TO THE TIME OF THE NEXT PENDING EVENT.
    WE EITHER DO THIS IMMEDIATELY OR MORE SLOWLY VIA A SERIES OF SHORT SLEEPs.
    WE USE THIS OPPORTUNITY TO SEE IF ANY TCL/TK EVENTS NEED PROCESSING. */

    if(Tflag) {
#if	defined(USE_TCLTK)
	if(Wflag)
	    PING_TCLTK();
#endif
	;
    }
    else {
	struct timeval	timeout;
	CnetTime	usecs = newMICROSECONDS - MICROSECONDS;

#if	defined(USE_TCLTK)
	usecs		*= delaymult;
	debug_pressed	 = false;
#endif

	while(usecs > MIN_SELECT) {
#if	defined(USE_TCLTK)
	    if(Wflag) {
		PING_TCLTK();
		if(debug_pressed) {	// break from sleeping
		    newMICROSECONDS = MICROSECONDS + 1;
		    break;
		}
	    }
#endif
	    if(usecs > EACH_SELECT) {
		timeout.tv_sec	= 0;
		timeout.tv_usec	= EACH_SELECT;
		usecs		-= EACH_SELECT;
	    }
	    else {
		timeout.tv_sec	= usecs / (int64_t)1000000;
		timeout.tv_usec	= usecs % (int64_t)1000000;
		usecs		= 0;
	    }
	    select(0, NULL, NULL, NULL, &timeout);
	}
    }
    MICROSECONDS	= newMICROSECONDS;
}

// ------------------------------------------------------------------------

void schedule(bool singlestep)
{
    EVENT	*ev;
    SCHEDEVENT	se;
    CnetTime	evtime;
    CnetEvent	ne;
    CnetTimerID	timer;
    CnetData	data;

//  KEEP PULLING NEW EVENTS UNTIL NO MORE OR UNTIL SIMULATION FINISHES

    while(cnet_state == STATE_RUNNING && (ev = get_next_event())) {

//  ADVANCE THE VALUE OF MICROSECONDS
	advance_time(ev->usec);

//  DELIVER NEXT EVENT FROM THE EVENT QUEUE
	THISNODE	= ev->node;
	np		= &(NODES[THISNODE]);
	se		= ev->se;
	ne		= ev->ne;
	timer		= ev->timer;
	data		= ev->data;
	FREE(ev);

	switch (se) {
	case SCHED_POLLAPPL : {
	    if(np->runstate == STATE_PAUSED)
		internal_event(SCHED_POLLAPPL, EV_APPLICATIONREADY, THISNODE,
				np->resume_time-MICROSECONDS+(CnetTime)1, 0);
	    else {
		extern int	poll_application(CnetTime *);

		if(poll_application(&evtime))
		    HANDLER(EV_APPLICATIONREADY, NULLTIMER, data);
		internal_event(SCHED_POLLAPPL, EV_APPLICATIONREADY, THISNODE,
				evtime, 0);
	    }
	    break;
	}				// end of SCHED_POLLAPPL

	case SCHED_FRAMEARRIVAL : {
	    extern bool prepare4framearrival(FRAMEARRIVAL *arrival);

	    if(prepare4framearrival((FRAMEARRIVAL *)data))
		HANDLER(EV_PHYSICALREADY, NULLTIMER,
				np->data[(int)EV_PHYSICALREADY]);
	    break;
	}				// end of SCHED_FRAMEARRIVAL

	case SCHED_FRAMECOLLISION : {
	    extern bool prepare4framecollision(FRAMEARRIVAL *arrival);

	    FRAMEARRIVAL	*arr	= (FRAMEARRIVAL *)data;
	    CnetData		link	= arr->destlink;

	    if(prepare4framecollision(arr))
		HANDLER(EV_FRAMECOLLISION, NULLTIMER, link);
	    break;
	}				// end of SCHED_FRAMECOLLISION

#if	defined(USE_TCLTK)
	case SCHED_DEBUGBUTTON :
	case SCHED_KEYBOARDREADY :
#endif
	case SCHED_TIMER :
	    if(np->runstate == STATE_PAUSED)
		internal_event(se,ne,THISNODE,np->resume_time-MICROSECONDS+1,0);
	    else {
#if	defined(USE_TCLTK)
		if(se == SCHED_DEBUGBUTTON) {
		    int	savequiet1		= np->nattr.stdio_quiet;
		    int	savequiet2		= DEFAULTNODE.stdio_quiet;

		    np->nattr.stdio_quiet	= false;
		    DEFAULTNODE.stdio_quiet	= false;
		    HANDLER(ne, timer, data);
		    DEFAULTNODE.stdio_quiet	= savequiet2;
		    np->nattr.stdio_quiet	= savequiet1;
		}
		else
		    HANDLER(ne, timer, data);
#else
		HANDLER(ne, timer, data);
#endif
	    }
	    break;

	case SCHED_NODESTATE : {
	    switch (np->runstate) {

	    case STATE_AUTOREBOOT :
		invoke_shutdown(THISNODE);
		invoke_reboot(THISNODE);
		break;

	    case STATE_UNDERREPAIR :
		if(dflag)
		    REPORT("%s.repaired\n",np->nodename);

	    case STATE_CRASHED :
	    case STATE_REBOOTING :
		invoke_reboot(THISNODE);
		break;

	    case STATE_PAUSED :
		np->runstate	= STATE_RUNNING;
		if(dflag)
		    REPORT("%s.resumed\n",np->nodename);
		break;

	    case STATE_RUNNING :
		np->runstate	= STATE_UNDERREPAIR;
		if(dflag)
		    REPORT("%s.underrepair\n",np->nodename);

		unschedule_node(THISNODE);
		evtime = node_mttr();
		if(evtime != 0)
		    internal_event(SCHED_NODESTATE,EV_REBOOT,THISNODE,evtime,0);
		break;
	     default:
		break;
	    }				// end of switch (np->runstate)
#if	defined(USE_TCLTK)
	    if(Wflag)
		draw_node_icon(THISNODE, NULL, 0, 0);
#endif
	    break;
	}				// end of case SCHED_NODESTATE

	case SCHED_LINKSTATE : {
	    extern void report_linkstate(int);

	    int		w	= data;	// WAN # that just changed
	    WAN		*wanp	= &WANS[w];

	    if(wanp->up) {
		wanp->up = false;
		unschedule_link(wanp->minnode, wanp->minnode_nic);
		unschedule_link(wanp->maxnode, wanp->maxnode_nic);
		evtime	= link_mttr(wanp);
		if(evtime != 0)
		    newevent(EV_LINKSTATE, wanp->minnode,
				    evtime, NULLTIMER, (CnetData)w);
#if	defined(USE_TCLTK)
		if(wanp->ndrawframes != 0) {
		    extern void	forget_drawframes(int w);

		    forget_drawframes(w);
		}
#endif
	    }
	    else {
		wanp->up = true;
		evtime	= link_mtbf(wanp);
		if(evtime != 0)
		    newevent(EV_LINKSTATE, wanp->minnode,
				    evtime, NULLTIMER, (CnetData)w);
	    }
	    if(dflag)
		REPORT("%s->%s.%s\n",
			NODES[wanp->minnode].nodename,
			NODES[wanp->maxnode].nodename,
			wanp->up ? "up" : "down" );
	    report_linkstate(w);
	    break;
	}				// end of SCHED_LINKSTATE

#if	defined(USE_TCLTK)
	case SCHED_DRAWFRAME : {
	    extern void		add_drawframe(DRAWFRAME *df);
	    DRAWFRAME		*df	= (DRAWFRAME *)data;

//  DOES THIS NODE HAVE AN EV_DRAWFRAME HANDLER?
	    if(np->handler[(int)EV_DRAWFRAME])
		HANDLER(EV_DRAWFRAME, NULLTIMER, (CnetData)&df->cdf);
	    else {
//  PROVIDE A SIMPLE, DEFAULT EV_DRAWFRAME HANDLER
		sprintf(df->cdf.text, "%zd", df->cdf.len);
		df->cdf.colours[0]	= DRAWFRAME_COLOUR;
		df->cdf.pixels[0]	= 30;
		df->cdf.pixels[1]	= 0;
	    }
	    add_drawframe(df);
	    break;
	}				// end of SCHED_DRAWFRAME

	case SCHED_DRAWSIGNAL : {
	    extern bool	 draw_wlansignal(int wlan);

	    if(draw_wlansignal((int)data))
		schedule_draw_wlansignal((int)data);
	    break;
	}				// end of SCHED_DRAWSIGNAL

	case SCHED_UPDATEGUI : {
	    do_GUIupdates();
	    internal_event(SCHED_UPDATEGUI,EV_NULL,UNKNOWN,gui_every_nusecs,0);
	    break;
	}				// end of SCHED_UPDATEGUI

	case SCHED_MOVEFRAME : {
	    extern void	 move_drawframe(CnetData);

	    move_drawframe(data);
	    break;
	}				// end of SCHED_MOVEFRAME
#endif
	case SCHED_PERIODIC : {
	    do_periodically();
	    internal_event(SCHED_PERIODIC,EV_NULL,UNKNOWN,stats_every_nusecs,0);
	    break;
	}				// end of SCHED_PERIODIC

	case SCHED_GAMEOVER :
	    --MICROSECONDS;
	    cnet_state	= STATE_GAMEOVER;
	    goto done;
	    break;

	default:
	    FATAL("unknown value of SE in global event queue");
	    break;
	}				// end of switch( SCHED_? )

//  CHECK FOR ACTIONS PERFORMED AFTER A CERTAIN NUMBER OF EVENTS 
	if(stats_every_nevents != 0 &&
	  (gattr.nodestats.nevents % stats_every_nevents) == 0)
	    do_periodically();

	if(singlestep)
	    break;

	if(gattr.nodestats.nevents == finish_after_nevents) {
	    cnet_state	= STATE_GAMEOVER;
	    break;
	}

#if	defined(USE_TCLTK)
	if(gui_every_nevents != 0 &&
	  (gattr.nodestats.nevents % gui_every_nevents) == 0)
	    do_GUIupdates();
#endif
	if(nqueued == min_nqueued) {
	    WARNING("no more events are scheduled\n");
	    min_nqueued	= (1<<30);
	}
    }

done:
#if	defined(USE_TCLTK)
    if(Wflag)
	do_GUIupdates();
#endif
    fflush(stdout);
}


void single_event(int n, CnetEvent ev, CnetTimerID timer, CnetData data)
{
    if(NODES[n].runstate == STATE_RUNNING && NODES[n].handler[(int)ev])  {
	np	= &(NODES[THISNODE = n]);
	HANDLER(ev, timer, data);
    }
}

void report_linkstate(int w)
{
    WAN	*wanp	= &WANS[w];

#if	defined(USE_TCLTK)
    if(Wflag) {
	extern	void draw_wan(int, bool, int);

	draw_wan(w, true, CANVAS_FATLINK/2);
	draw_node_icon(wanp->minnode, NULL, 0, 0);
	draw_node_icon(wanp->maxnode, NULL, 0, 0);
    }
#endif

    NODES[wanp->minnode].nics[wanp->minnode_nic].up	= wanp->up;
    if(wanp->minnode == SWAPPED_IN)
	linkinfo[wanp->minnode_nic].linkup      = wanp->up;
    single_event(wanp->minnode,
		 EV_LINKSTATE, NULLTIMER, (CnetData)wanp->minnode_nic);

    NODES[wanp->maxnode].nics[wanp->maxnode_nic].up	= wanp->up;
    if(wanp->maxnode == SWAPPED_IN)
	linkinfo[wanp->maxnode_nic].linkup      = wanp->up;
    single_event(wanp->maxnode,
		 EV_LINKSTATE, NULLTIMER, (CnetData)wanp->maxnode_nic);
}


// ------------------------------------------------------------------------


// DOESN'T REALLY BELONG IN THIS FILE, BUT NEEDS BIGBANG!
int CNET_set_time_of_day(long newsec, long newusec)
{
    int		result	= -1;
    CnetTime	delta;

    if(newsec < 0 || newusec < 0 || newusec >= 1000000)
	ERROR(ER_BADARG);
    else {
	delta = (CnetTime)newsec*(CnetTime)1000000 + newusec;
	NODES[THISNODE].clock_skew	= delta - (BIGBANG + MICROSECONDS);

	nodeinfo.time_of_day.sec	= newsec;
	nodeinfo.time_of_day.usec	= newusec;
    }

    if(gattr.trace_events) {
	if(result == 0)
	    TRACE(0, "\t%s(newsec=%ld, newusec=%ld) = 0\n",
		    __func__, newsec, newusec);
	else
	    TRACE(1, "\t%s(newsec=%ld, newusec=%ld) = -1 %s\n",
		    __func__, newsec, newusec, cnet_errname[(int)cnet_errno]);
    }
    return result;
}
