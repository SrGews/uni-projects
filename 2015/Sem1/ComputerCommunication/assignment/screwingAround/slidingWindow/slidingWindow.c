#include "slidingWindow.h"


static char*       	    msg             = "This is the actual message I want to send.";/*43 characters including /0*/
static int              lenghtMsg       = 42;
static char             msgReceive[43]  ;
static int              lastFrameReceive;
static int              nextFrame       ;
static int              endWindow       ;
static int              startWindow     ;
static int              lastWindow      ;
static CnetTimerID      lastTimer       ;
static int linkList[6][6] = {{0,1,2,1,1,1},
                            {1,0,2,3,3,3},
                            {1,2,0,2,2,2},
                            {1,1,1,0,2,3},
                            {1,1,1,1,0,2},
                            {1,1,1,1,2,0}};


void sendMsg()
{
    FRAME f;
    int i,  length, msgLength, sendStuff, sentStuff;
    CnetTime timeout;
    
    msgLength = MAX_MSG_SIZE ;
    f.destNode = 1;
    f.frameNumber = nextFrame;
    f.kind = DL_DATA;
    f.len = MAX_MSG_SIZE;
    f.sendrNode = nodeinfo.nodenumber;
    sentStuff = 1;

    nextFrame = startWindow;

    while(nextFrame<endWindow)
    {
        sendStuff = 1;
        printf("________________________\n");
        printf("SendFrame with nextFrame = %i;\n", nextFrame);

        i = 0;
        while ((i < msgLength)&&((i+nextFrame*msgLength) < lenghtMsg ))
        {
            sendStuff = 0;
            f.msg[i] = msg[i+nextFrame*(msgLength)];
            
            i++;
        }
        if( sendStuff == 0 )
        {

            f.len = i;
            f.checkSum = 0;
            printf("Sending a message\n");

            /*This line will print garbage after the message if use '%s, f.msg)' because the message doesn't have a '\0'*/
            printf("message = %c%c%c%c%c%c\n", f.msg[0], f.msg[1], f.msg[2], f.msg[3], f.msg[4], f.msg[5]);

            length = (int)FRAME_SIZE(f);

            f.checkSum  = CNET_ccitt((unsigned char *)&f, length);
        
            CHECK(CNET_write_physical( linkList[nodeinfo.nodenumber][f.destNode], (char *)&f, (size_t*)&length));

            sentStuff = 0;
        }

        f.frameNumber ++;
        nextFrame ++;

    }

    if (sentStuff == 0)
    {
        timeout = FRAME_SIZE(f)*((CnetTime)8000000 / linkinfo[linkList[nodeinfo.nodenumber][f.destNode]].bandwidth) +
                linkinfo[1].propagationdelay;

        lastTimer = CNET_start_timer(EV_TIMER1, 3 * timeout, 0);
    }
    else
    {
        printf("Finished sending message!\n");
    }

    perror("send Done; read done\t\t\t\t");

}



static EVENT_HANDLER(application_ready)
{
    sendMsg();
    CNET_disable_application(ALLNODES);
}

void sendAck(FRAME f)
{
    FRAME ackf;
    size_t length;

    ackf.kind = DL_ACK;
    ackf.sendrNode = nodeinfo.nodenumber;
    ackf.len = (size_t)0;
    ackf.destNode = f.sendrNode;
    
    printf("Sending ack\n");

    length = FRAME_SIZE(ackf);    
    ackf.frameNumber = lastFrameReceive;

    ackf.checkSum = 0;
    ackf.checkSum = CNET_ccitt((unsigned char *)&ackf, length);

    CHECK(CNET_write_physical(linkList[nodeinfo.nodenumber][ackf.destNode], (char *)&ackf, (size_t*)&length));
}

void processFrame(FRAME f)
{
    int i=0;
    if(f.kind == DL_DATA)
    {
        printf("frameNumber = %i;    lastReceive = %i\n", f.frameNumber, lastFrameReceive);
        if (f.frameNumber == (lastFrameReceive+1))
            {

            /*This line will print garbage after the message because the message doesn't have a '\0' to save space*/
            printf("%s\n", f.msg);

            while (  i < f.len )
            {
                msgReceive[i+f.frameNumber*MAX_MSG_SIZE] = f.msg[i];
                i++;
            }
            msgReceive[i+f.frameNumber*MAX_MSG_SIZE] = '\0';

            printf("msgReceive = %s\n", msgReceive );

            lastFrameReceive ++;
            sendAck(f);
        }

    }
    else
    {
        printf("ACK received: Ack for frame: %i\n", f.frameNumber);
        if(f.frameNumber >= startWindow)
        {
            startWindow = f.frameNumber;
            
            CNET_stop_timer(lastTimer);
            
            endWindow = WINDOW_SIZE+startWindow;
            if (endWindow > lastWindow)
            {
                endWindow = lastWindow;
            }
    
            sendMsg();
        }
    }    
}

static EVENT_HANDLER(physical_ready)
{
    FRAME    f;
    size_t   len;
    int      link, checksum, length;

    perror("Physical ready; about to read\t\t\t");


    len         = sizeof(FRAME);
    CHECK(CNET_read_physical(&link, (char *)&f, &len));
    perror("Physical ready; read done\t\t\t\t");

    checksum = f.checkSum;
    f.checkSum = 0;
    length = (int)FRAME_SIZE(f);
    f.checkSum  = CNET_ccitt((unsigned char *)&f, length);


    if (checksum == f.checkSum)
    {
        perror("Physical ready; checksum correct\t\t\t");
        processFrame(f);
        perror("Physical ready; frame has been processed\t\t");
    }
    else
    {
        printf("\nCorrupt frame.\n");
    }
    
    perror("Physical ready; finished\t\t\t\t");
}

static EVENT_HANDLER(timeouts)
{
    nextFrame = startWindow;

    fprintf(stderr, "endWindow = %i\n", endWindow);

    sendMsg();
}

EVENT_HANDLER(reboot_node)
{
    nextFrame       = 0;
    endWindow       = WINDOW_SIZE;
    startWindow     = endWindow - WINDOW_SIZE;
    lastTimer       = NULLTIMER;
    lastWindow      = lenghtMsg/MAX_MSG_SIZE;
    lastFrameReceive= -1;
    nextFrame       = startWindow;

    if(nodeinfo.nodenumber > 1) {
	fprintf(stderr,"This is not a 2-node network!\n");
	exit(1);
    }

    CHECK(CNET_set_handler( EV_APPLICATIONREADY, application_ready, 0));
    CHECK(CNET_set_handler( EV_PHYSICALREADY,    physical_ready, 0));
    CHECK(CNET_set_handler( EV_TIMER1,           timeouts, 0));

    if(nodeinfo.nodenumber == 0)
	CNET_enable_application(ALLNODES);
}