#include <cnet.h>
#include <stdlib.h>
#include <string.h>

#define WINDOW_SIZE 4
#define MAX_MSG_SIZE (int)6

typedef enum    { DL_DATA, DL_ACK }   FRAMEKIND;

typedef struct 
{
	int checkSum;
	int frameNumber;
	FRAMEKIND kind;
	int destNode;
	int sendrNode;
	size_t len;
	char msg[MAX_MSG_SIZE];
}FRAME;

#define FRAME_HEADER_SIZE  (sizeof(FRAME) - sizeof(char[MAX_MSG_SIZE]))
#define FRAME_SIZE(f)      (FRAME_HEADER_SIZE + MAX_MSG_SIZE)
