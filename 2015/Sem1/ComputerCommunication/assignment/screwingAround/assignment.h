#include <cnet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_MSG_SIZE 80
#define DATA_FRAME_SIZE(f) sizeof(FRAME)
#define EN_DATA 0
#define EN_ACK 1
#define WINDOW_SIZE 6



typedef struct
{
	char msg[10];
}Data;

typedef struct 
{
	int len;
	int type;
	int destNode;
	Data data;
}FRAME;

void forward_message( FRAME f,size_t len);
int getLink(FRAME* f);






