#include "assignment.h"

static int destNode = 0;
static int linkList[6][6] = {{0,1,2,1,1,1},
							{1,0,2,3,3,3},
							{1,2,0,2,2,2},
							{1,1,1,0,2,3},
							{1,1,1,1,0,2},
							{1,1,1,1,2,0}};


static void sendFrame(size_t length)
{
	FRAME f;
	int nextNode;

	f.type = EN_DATA;
	f.len = length;
		
	f.data.msg[0] = 's';
	f.data.msg[1] = 'a';
	f.data.msg[2] = 'd';
	f.data.msg[3] = '\0';
	f.len =sizeof(f.data.msg);
	f.destNode = destNode;
	length = DATA_FRAME_SIZE(f);
	printf("%s\n",f.data.msg );
	printf("%d is the frame size\n", DATA_FRAME_SIZE(f));
	printf("Sending Frame to node %d\n", destNode);
	
	nextNode = getLink(&f);

	CHECK(CNET_write_physical(nextNode, (char *)&f, &length));
}

static EVENT_HANDLER(plusNode)
{
	destNode = destNode + 1;

	if (destNode > 5)
	{
		destNode = 5;
		printf("The maximum value for destNode is %d.\n", 5);
	}

	printf("%d is the new destination node.\n", destNode);
}

static EVENT_HANDLER(minusNode)
{
	destNode = destNode - 1;
	if (destNode < 0)
	{
		destNode = 0;
		printf("The minimum value for destNode is 0.\n");
	}
	printf("%i is the new destination node.\n", destNode);
}

static EVENT_HANDLER(send_message)
{
	sendFrame((size_t)MAX_MSG_SIZE);
}

static EVENT_HANDLER(physical_ready)
{
	FRAME f;
	int msgDestNode;
	size_t len;

	len = DATA_FRAME_SIZE(f)
	CHECK(CNET_read_physical(&msgDestNode, (char *)&f, &len));


	if (f.destNode == nodeinfo.nodenumber)
	{
		switch (f.type)
		{
			case EN_ACK:

			break;

			case EN_DATA:
				printf("%s\n", f.data.msg);
			break;
		}
	}
	else 
	{
		printf("Forwarding frame\n");
		CHECK(CNET_write_physical(getLink(&f), (char *)&f, &len));
	}
}

int getLink(FRAME* f)
{
	return linkList[nodeinfo.nodenumber][f->destNode];
}



void forward_message(FRAME f, size_t len)
{
	int link;
	
	link = getLink(&f);

	CHECK(CNET_write_physical(link, (char *)&f, &len));	
	printf("Forwarding to link %s\n", link);
}


void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
{


	printf("%d\n", destNode);
	printf("i am node %d\n",nodeinfo.nodenumber);
	if (nodeinfo.nlinks > 0)
		printf("there are %d link(s) connecting me to the outside world\n",nodeinfo.nlinks);
	else
		printf("is there anybody out there?\n");

	CHECK(CNET_set_handler(EV_DEBUG0,	plusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG0, "Node + 1"));
	CHECK(CNET_set_handler(EV_DEBUG1,	minusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG1, "Node - 1"));
	CHECK(CNET_set_handler( EV_DEBUG2, send_message, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG2, "Send message"));

    CHECK(CNET_set_handler( EV_PHYSICALREADY,    physical_ready, 0));


}