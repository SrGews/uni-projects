#include <cnet.h>
#include <stdlib.h>
#include <string.h>

#define WINDOW_SIZE 4
#define MAX_MESSAGE__SIZE 8

typedef enum    { DL_DATA, DL_ACK }   FRAMEKIND;

typedef struct 
{
	int 	checkSum;
	int 	frameNumber;
	FRAMEKIND kind;
	int 	destNode;
	int 	sendrNode;
	size_t 	len;
	size_t	fullLen;
	char 	msg[MAX_MESSAGE__SIZE];
}FRAME;

typedef struct 
{
	int 		endWindow;
	int 		lastWindow;
	int 		nextFrame;
	int		 	destNode;
	CnetTimerID timer;
	size_t 		msgSize;
	char* 		message;
}MESSAGES;

#define SIZE_FRAME(f) (sizeof(FRAME))


void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data);
static void create_message();
static void send_message(int msgNumber);
static void sendFrame(FRAME f);
static void sendMessage(FRAME f);
static void plusNode();
static void minusNode();
static EVENT_HANDLER(timeouts);
static EVENT_HANDLER(physical_ready);
static int checkFrame(FRAME f);
static void processFrame();
static int getMessage(FRAME f);
static void deleteReceive(int idx);
static void createReceive(FRAME f);
static void makeAck(FRAME* f);
static void updateMessages(FRAME f);
static void deleteMessage(int idx);