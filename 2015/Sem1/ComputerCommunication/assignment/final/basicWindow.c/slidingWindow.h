#include <cnet.h>
#include <stdlib.h>
#include <string.h>

#define WINDOW_SIZE 4
#define MAX_MESSAGE__SIZE 8
#define BUFFER_SIZE 40

typedef enum    { DL_DATA, DL_ACK }   FRAMEKIND;

typedef struct 
{
	int 	checkSum;
	int 	frameNumber;
	FRAMEKIND kind;
	int 	destNode;
	int 	sendrNode;
	int startNode;
	size_t 	len;
	size_t	fullLen;
	char 	msg[MAX_MESSAGE__SIZE];
}FRAME;

typedef struct 
{
	int startWindow;
	int lastWindow;
	int nextFrame;
	int lastFrameReceived;
	CnetTimerID timer;
	FRAME frameList[BUFFER_SIZE];	
}BUFFER;

typedef struct
{
	int 		lastWindow;
	int		 	destNode;
	size_t 		msgSize;
	char* 		message;
}MESSAGE;

/*
typedef struct 
{
	int 		endWindow;
	int 		lastWindow;
	int 		nextFrame;
	int		 	destNode;
	CnetTimerID timer;
	size_t 		msgSize;
	char* 		message;
}MESSAGES;
*/

#define SIZE_FRAME(f) (sizeof(FRAME))


/*static void printFrame(FRAME f);*/

static void forwardMessage(FRAME f);
void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data);
static void create_message();
static void addToBuffer(MESSAGE message);
static void addFrameToBuffer(FRAME f);
static void send_message(int buffID);
static void startTimer( CnetTime timeout, int buffID);
static void sendNextFrame(int buffID);
/*static void removeAck(int buffID, int fidx);*/
static void sendFrame(FRAME f);
static void sendMessage(FRAME f);
static void plusNode();
static void minusNode();
static EVENT_HANDLER(timeouts);
static EVENT_HANDLER(physical_ready);
static int checkFrame(FRAME f);
static void processFrame(FRAME f, int link);
static int getMessage(FRAME f);
static void makeAck(FRAME oldF,FRAME* f);
static void updateMessages(FRAME f);
static EVENT_HANDLER(timeout2);
static EVENT_HANDLER(timeout3);