#include "slidingWindow.h"


/*destNode is used to change the destination node for the message to be sent
	linkList is a list of the paths to take to reach a node from the current
	node. eg if you're in node 1 and you want to go to node 3 go through
	link linkList[1][3].
	linkBuffers is a set of buffers, one for each link
	destinationMessage is used instead of application layer because I didn't
	have enough time.
*/
static int destNode 		= 0;
static int linkList[6][6] 	= {{0,1,2,1,1,1},
							{1,0,2,3,3,3},
							{1,2,0,2,2,2},
							{1,1,1,0,2,3},
							{1,1,1,1,0,2},
							{1,1,1,1,2,0}};


static BUFFER *linkBuffers;

static char destinationMessage[61];

/*	reboot node declares the necessary events and creates buffers which hold the outgoing frames for each link.
*/
void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
{
	int i;

	linkBuffers = (BUFFER*)calloc(( nodeinfo.nlinks), sizeof(BUFFER));

	for ( i = 0; i < nodeinfo.nlinks; ++i)
	{
		linkBuffers[i].nextFrame = 0;
		linkBuffers[i].startWindow = linkBuffers[i].nextFrame;
		linkBuffers[i].lastWindow = 0;
		linkBuffers[i].lastFrameReceived = 0;
	}


	printf("%d\n", destNode);
	printf("I am node %d\n",nodeinfo.nodenumber);
	if (nodeinfo.nlinks > 0)
		printf("There are %d link(s) connecting me to the outside world\n",nodeinfo.nlinks);
	else
		printf("Is there anybody out there?\n");

	CHECK(CNET_set_handler(EV_DEBUG0,	plusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG0, "Node + 1"));
	CHECK(CNET_set_handler(EV_DEBUG1,	minusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG1, "Node - 1"));
	CHECK(CNET_set_handler( EV_DEBUG2, create_message, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG2, "Send message"));

    CHECK(CNET_set_handler( EV_PHYSICALREADY,    physical_ready, 0));

    CHECK(CNET_set_handler( EV_TIMER1,           timeouts, 0));
    CHECK(CNET_set_handler( EV_TIMER2,           timeout2, 0));
    CHECK(CNET_set_handler( EV_TIMER3,           timeout3, 0));

}



/* I didn't get the application layer working in time. Instead use the debug keys 
	to choose a destination node for a message, and send it.
*/
/*static EVENT_HANDLER(application_ready)
{
    CnetAddr destaddr;
    size_t lastlength;
    FRAME f;
    char* msg;
    int i;

    msg = (char*)calloc(MAX_MESSAGE__SIZE, sizeof(char));


    lastlength  = MAX_MESSAGE__SIZE*sizeof(char);
    printf("%i\n", lastlength);

    CHECK(CNET_read_application(&destaddr, (char *)msg, &lastlength));
    CNET_disable_application(ALLNODES);

    f.len = lastlength;

	f.kind 			= DL_DATA;
	f.destNode 		= destNode;
	f.sendrNode 	= nodeinfo.nodenumber;
	f.fullLen 		= f.len;
	f.startNode		= nodeinfo.nodenumber;

	for ( i = 0; i < lastlength; ++i)
	{
		f.msg[i] = msg[i];
	}

    printf("down from application, seq=%d\n", linkBuffers[linkList[nodeinfo.nodenumber][f.destNode]].nextFrame);
    addFrameToBuffer(f);
	send_message((linkList[nodeinfo.nodenumber][f.destNode] - 1));
}*/


/* This creates a message to be sent.
*/
static void create_message()
{

	MESSAGE message;

	/*destination is to stop destNode changing after a printf statement causing undefined results*/
	message.destNode = destNode;

	message.msgSize = 43;
	message.message = (char*)calloc(message.msgSize, sizeof(char));
	message.message = "This is the actual message I want to send.\0";

	printf("The message is:'%s'\n", message.message);

	/*printf("lastWindow = %i;", message.lastWindow);*/
	/*fprintf(stderr, "done reading in message\n");*/
	addToBuffer(message);

}


/*addToBuffer breaks a message up and places the contents onto frames to be sent.
	The frames are then added to the appropriate buffer, and that buffer is then
	called to start sending messages.
*/
static void addToBuffer(MESSAGE message)
{
	FRAME f;
	int  i, j;


	f.kind 			= DL_DATA;
	f.destNode 		= message.destNode;
	f.sendrNode 	= nodeinfo.nodenumber;
	f.len			= (size_t)(MAX_MESSAGE__SIZE);
	f.fullLen 		= message.msgSize;
	f.startNode		= nodeinfo.nodenumber;

	i = 0;
	j = 0;
	while(i < f.fullLen)
	{
		
		f.msg[j] = message.message[i];

		if( ((j ==(f.len-2) && (i > 0))))
		{
			j = 0;
			/*f.msg[] = '\0';*/
			addFrameToBuffer(f);

		}
		else
		{
			j++;
		}


		i++;
	}

	if (i != (f.len-2))
	{
		addFrameToBuffer(f);
	}
	
	send_message((linkList[nodeinfo.nodenumber][f.destNode] - 1));
}

/*addFrameToBuffer adds a frame to the buffer by copying the contents of
	an input frame into an appropriate buffer location
*/
static void addFrameToBuffer(FRAME f)
{
	int buffID, lastWindow, j;

	buffID 	= linkList[nodeinfo.nodenumber][f.destNode] - 1;

	lastWindow = linkBuffers[buffID].lastWindow;
	
	if(linkBuffers[buffID].lastWindow != (linkBuffers[buffID].startWindow - 1))
	{

		linkBuffers[buffID].frameList[lastWindow].kind 			= f.kind;
		linkBuffers[buffID].frameList[lastWindow].destNode 		= f.destNode;
		linkBuffers[buffID].frameList[lastWindow].sendrNode 	= nodeinfo.nodenumber;
		linkBuffers[buffID].frameList[lastWindow].len			= f.len;
		linkBuffers[buffID].frameList[lastWindow].fullLen 		= f.fullLen;
		linkBuffers[buffID].frameList[lastWindow].startNode		= nodeinfo.nodenumber;

		
		j = 0;
		do
		{
			linkBuffers[buffID].frameList[lastWindow].msg[j] = f.msg[j];
			j++;
		}while((j < (linkBuffers[buffID].frameList[lastWindow].len-1))&&(f.msg[j] != '\0'));

		linkBuffers[buffID].frameList[lastWindow].msg[MAX_MESSAGE__SIZE-1] = '\0';

		lastWindow ++;
		lastWindow = lastWindow % BUFFER_SIZE;
		linkBuffers[buffID].lastWindow = lastWindow;
	}
}


/*send_message gets buffer's messages to begin sending.
	It checks if there are messages in the buffer, and if the maximum window
	size has been exceded. If not the next frame will be sent

	If the window is full the buffer's timer will be started, otherwise it 
	will state the buffer is empty.
*/
static void send_message(int buffID)
{
	CnetTime timeout;

	if (linkBuffers[buffID].nextFrame > linkBuffers[buffID].lastWindow)
	{
		linkBuffers[buffID].lastWindow += BUFFER_SIZE;
	}

	while( (linkBuffers[buffID].frameList[linkBuffers[buffID].nextFrame].kind == DL_ACK ) ||((linkBuffers[buffID].nextFrame < linkBuffers[buffID].lastWindow) && ( linkBuffers[buffID].nextFrame <  (linkBuffers[buffID].startWindow + WINDOW_SIZE))))
	{

		sendNextFrame(buffID);

		linkBuffers[buffID].nextFrame ++;

		if (linkBuffers[buffID].nextFrame == BUFFER_SIZE)
		{
			linkBuffers[buffID].nextFrame -=  BUFFER_SIZE ;
			linkBuffers[buffID].lastWindow -= BUFFER_SIZE ;
			linkBuffers[buffID].startWindow -= BUFFER_SIZE ;
		}
	}

	while (linkBuffers[buffID].lastWindow >= BUFFER_SIZE)
	{
		linkBuffers[buffID].lastWindow -= BUFFER_SIZE;
	}

	while (linkBuffers[buffID].startWindow >= BUFFER_SIZE)
	{
		linkBuffers[buffID].startWindow -= BUFFER_SIZE;
	}

	timeout = (sizeof(FRAME)*((CnetTime)8000000) / linkinfo[buffID+1].bandwidth) + linkinfo[buffID+1].propagationdelay;

	if ( linkBuffers[buffID].startWindow == linkBuffers[buffID].lastWindow )
	{
		printf("\t\t\t\t\t\tBuffer %i is empty\n", buffID);
	}
	else
	{	
		startTimer(timeout, buffID);
	}

}

/*startTimer starts the approriate timer for the buffer which has been input
*/
static void startTimer( CnetTime timeout, int buffID)
{
	switch (buffID)
	{
		case 0:
			linkBuffers[buffID].timer = CNET_start_timer(EV_TIMER1, 5 * timeout, 0);
		break;
		case 1:
			linkBuffers[buffID].timer = CNET_start_timer(EV_TIMER2, 5 * timeout, 0);
		break;
		case 2:
			linkBuffers[buffID].timer = CNET_start_timer(EV_TIMER3, 5 * timeout, 0);
		break;
	}
}


/*sendNextFrame selects and sends the next frame in the buffer's queue
*/
static void sendNextFrame(int buffID)
{
	int nextFrame;

	nextFrame = linkBuffers[buffID].nextFrame;

	linkBuffers[buffID].frameList[nextFrame].frameNumber = nextFrame;

	sendFrame(linkBuffers[buffID].frameList[nextFrame%BUFFER_SIZE]);

}

/*sendFrame creates the checksum for a frame, and then sends it
*/
static void sendFrame(FRAME f)
{
	size_t fLength;
	f.checkSum = 0;

	fLength = SIZE_FRAME(f);

    f.checkSum  = CNET_ccitt((unsigned char *)&f, fLength);

	sendMessage(f);
}

/*sendMessage writes a complete frame to the physical layer
	and tells the user a frame has been sent.
*/
static void sendMessage(FRAME f)
{
	size_t length;
	int nextNode;

	nextNode = linkList[nodeinfo.nodenumber][f.destNode];

	length = SIZE_FRAME(f);
	if (f.kind == DL_DATA)
	{
		printf("\tFrame %i sent through link %i\n", f.frameNumber, nextNode);
	}
	else
	{
		printf("\tACK %i sent through link %i\n", f.frameNumber ,nextNode);
	}
	

	CHECK(CNET_write_physical(nextNode, (char *)&f, (size_t*)&length));

}

/*plusNode and minusNode ore called when the debug buttons are pressed.
	plusNode increments the destination node by one, and minuNode decrements
	the destination by one.
	They both ensure a valid range for the destination is kept.
*/
static void plusNode()
{
	destNode = destNode + 1;

	if (destNode > 5)
	{
		destNode = 5;
		printf("The maximum value for destNode is %d.\n", 5);
	}

	printf("%d is the new destination node.\n", destNode);
}

static void minusNode()
{
	destNode = destNode - 1;
	if (destNode < 0)
	{
		destNode = 0;
		printf("The minimum value for destNode is 0.\n");
	}
	printf("%i is the new destination node.\n", destNode);
}


/*The timeout event handlers create timers for the Buffers, in case a
	frame is sent and receives no ack. The timers will call send_message
	which sends messages from the start of the window until the end.
*/
static EVENT_HANDLER(timeouts)
{
	if ( linkBuffers[0].startWindow != linkBuffers[0].lastWindow )
	{	
		linkBuffers[0].nextFrame = linkBuffers[0].startWindow;
		printf("timeout occurred, resending frame = %i through link = %i\n", linkBuffers[0].startWindow, 0);
		send_message(0);
	}
}

static EVENT_HANDLER(timeout2)
{
	if ( linkBuffers[1].startWindow != linkBuffers[1].lastWindow )
	{
		linkBuffers[1].nextFrame = linkBuffers[1].startWindow;
		printf("timeout occurred, resending frame = %i through link = %i\n", linkBuffers[1].startWindow, 2);
		send_message(1);
	}
}

static EVENT_HANDLER(timeout3)
{
	if ( linkBuffers[2].startWindow != linkBuffers[2].lastWindow )
	{
		linkBuffers[2].nextFrame = linkBuffers[2].startWindow;
		printf("timeout occurred, resending frame = %i through link = %i\n", linkBuffers[2].startWindow, 3);
		send_message(2);
	}
}


/*physical_ready reads in a frame from the physical layer, and checks
	whether the checksum is correct, if it is it will process the frame
	and if it's data it will send an ack.
	It then updates the number of the last frame received for the link
	it was received from.
*/
static EVENT_HANDLER(physical_ready)
{
	FRAME f, newF;
	size_t len;
	int link;

	f.len = MAX_MESSAGE__SIZE;
	len = SIZE_FRAME(f);

	CHECK(CNET_read_physical(&link, (char*)&f, &len));

	if (checkFrame(f) == 0)
	{
		if(f.frameNumber == linkBuffers[(link)].lastFrameReceived)
			{
				printf("Source: %i; Destination: %i; kind: %i; Sequence number: %i;\n", f.startNode,f.destNode,f.kind,f.frameNumber);
			if ( f.destNode == nodeinfo.nodenumber )
			{
				processFrame(f, link);
			}
			else
			{
				forwardMessage(f);	
			}
			if(f.kind == DL_DATA)
			{
				makeAck(f, &newF);
				sendFrame(newF);
			}
			linkBuffers[link].lastFrameReceived = (f.frameNumber + 1)%BUFFER_SIZE;
		}
		else if (((linkBuffers[link].lastFrameReceived + BUFFER_SIZE) % BUFFER_SIZE) >= ((f.frameNumber + BUFFER_SIZE)% BUFFER_SIZE))
		{
			if(f.kind == DL_DATA)
			{
				makeAck(f, &newF);
				sendFrame(newF);
			}
			linkBuffers[link].lastFrameReceived = (f.frameNumber + 1)%BUFFER_SIZE;
		}
	}
}


/*forwardMessage adds a frame to the appropriate buffer and gets the buffer 
	to attempt to send the message
*/
static void forwardMessage(FRAME f)
{
	addFrameToBuffer(f);
	send_message((linkList[nodeinfo.nodenumber][f.destNode] - 1));
}

/*checkFrame checks that the input frame's checkSum value is correct.
	if it is, it will return 0, otherwise -1
*/
static int checkFrame(FRAME f)
{
	int checkSum, length;
	int retVal;

	checkSum 	= f.checkSum;

	f.checkSum 	= 0;

	length 		= SIZE_FRAME(f);
	f.checkSum	= CNET_ccitt((unsigned char *)&f, length);


	if (f.checkSum == checkSum)
	{
		retVal = 0;
	}
	else
	{
		retVal = -1;
		printf("\t\tChecksum failed\n");
	}

	return retVal;
}


/*processFrame starts the processing of the frame.
	if the frame is data, it retreives the message otherwise it updates
	the appropriate buffer
*/
static void processFrame(FRAME f, int link)
{
	if (f.kind == DL_DATA)
	{
		 getMessage(f);
	}
	else
	{
		updateMessages(f);
	}
}

/*getMessage is used to retreive msg from the frame
	If the application layer was used, CNET_write_application would go here

	getMessage retreives the data from the frame and adds it to the retreived
	message. At the moment it will only do this for the first full message
	only.

	It will print what has been received so far
*/
static int getMessage(FRAME f)
{
	int i,retVal, thisLength;

	retVal = 0;

	thisLength = f.fullLen-f.frameNumber*(MAX_MESSAGE__SIZE-1);

	if (thisLength > MAX_MESSAGE__SIZE)
	{
		thisLength = MAX_MESSAGE__SIZE;
		
	}
	
	for ( i = 0; i < thisLength; ++i)
	{
		destinationMessage[i+f.frameNumber*(MAX_MESSAGE__SIZE-1)] = f.msg[i];
	}

	printf("\tThe message so far is: '%s'\n", destinationMessage);

	return retVal;
}


/*makeAck makes and acknowledgement frame for the input frame fold
*/
static void makeAck(FRAME fold, FRAME* f)
{
	f->destNode		= fold.sendrNode;
	f->sendrNode	= nodeinfo.nodenumber;
	f->frameNumber 	= fold.frameNumber;
	f->startNode	= nodeinfo.nodenumber;
	f->len 			= 0;
	f->kind			= DL_ACK;

}


/*updateMessages updates the start of the window when an ack has been received.
	it checks that the start is less than or equal to the frame received, if
	so it will update the start approriately, and begin sending the next
	message in the buffer.
*/
static void updateMessages(FRAME f)
{
	int buffID;

	buffID = (linkList[nodeinfo.nodenumber][f.sendrNode] - 1);

	if (linkBuffers[buffID].startWindow <= f.frameNumber)
	{
		linkBuffers[buffID].startWindow = (f.frameNumber+1) % BUFFER_SIZE;

		CNET_stop_timer(linkBuffers[buffID].timer);
		send_message(buffID);
	}
}