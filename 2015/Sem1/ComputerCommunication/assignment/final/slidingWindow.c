#include "slidingWindow.h"

static int destNode 		= 0;
static int linkList[6][6] 	= {{0,1,2,1,1,1},
							{1,0,2,3,3,3},
							{1,2,0,2,2,2},
							{1,1,1,0,2,3},
							{1,1,1,1,0,2},
							{1,1,1,1,2,0}};
static MESSAGES messageList[6];
static MESSAGES receivedList[6];



void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
{
	int i;

	for ( i = 0; i < 6; ++i)
	{
		receivedList[i].msgSize = 0;
	}

	printf("%d\n", destNode);
	printf("I am node %d\n",nodeinfo.nodenumber);
	if (nodeinfo.nlinks > 0)
		printf("There are %d link(s) connecting me to the outside world\n",nodeinfo.nlinks);
	else
		printf("Is there anybody out there?\n");

	CHECK(CNET_set_handler(EV_DEBUG0,	plusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG0, "Node + 1"));
	CHECK(CNET_set_handler(EV_DEBUG1,	minusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG1, "Node - 1"));
	CHECK(CNET_set_handler( EV_DEBUG2, create_message, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG2, "Send message"));

    CHECK(CNET_set_handler( EV_PHYSICALREADY,    physical_ready, 0));

    CHECK(CNET_set_handler( EV_TIMER1,           timeouts, 0));

}

/*
static void printFrame(FRAME f)
{
	printf("checkSum = %i\t", f.checkSum);
	printf("frameNumber = %i\t", f.frameNumber);
	printf("kind = %i\t", f.kind);
	printf("destNode = %i\t", f.destNode);
	printf("sendrNode = %i\t", f.sendrNode);
	printf("len = %i\n\t\t", f.len);
	printf("fullLen = %i\t", f.fullLen);
	printf("msg = %s\n", f.msg);
}
*/

static void create_message()
{
	int destination;
	/*destination is to stop destNode changing after a printf statement causing undefined results*/
	destination = destNode;

	messageList[destNode].destNode = destination;

	/*printf("How long is the message?");
	scanf(" %i\n", &messageList[destination].msgSize);

	messageList[destination].msgSize ++;

	messageList[destination].message = (char*)calloc(messageList[destination].msgSize, sizeof(char));
	printf("What is the message?\n");
	scanf("%s\n",messageList[destination].message);
	*/

	messageList[destination].msgSize = 43;
	messageList[destination].message = (char*)calloc(messageList[destination].msgSize, sizeof(char));
	messageList[destination].message = "This is the actual message I want to send.\0";
	messageList[destination].lastWindow = ((int)messageList[destination].msgSize/((int)MAX_MESSAGE__SIZE-1));

	printf("The message is:'%s'\n", messageList[destination].message);


	/*Ensuring the number of windows isn't mistaken due to integer division*/
	if ((messageList[destination].lastWindow*(int)(MAX_MESSAGE__SIZE-1)) != (int)messageList[destination].msgSize)
	{
		messageList[destination].lastWindow ++;
	}


	messageList[destination].nextFrame = 0;
	messageList[destination].endWindow = messageList[destination].nextFrame + WINDOW_SIZE;

	printf("lastWindow = %i;\tendWindow = %i\n", messageList[destination].lastWindow, messageList[destination].endWindow);

	send_message(destination);

}

static void send_message(int msgNumber)
{
	FRAME f;
	int j, charIdx, nextNode;
	CnetTime timeout;

	messageList[msgNumber].lastWindow = ((int)messageList[msgNumber].msgSize/((int)MAX_MESSAGE__SIZE-1));
	if ((messageList[msgNumber].lastWindow*(int)(MAX_MESSAGE__SIZE-1)) != (int)messageList[msgNumber].msgSize)
	{
		messageList[msgNumber].lastWindow ++;
	}


	messageList[msgNumber].nextFrame = messageList[msgNumber].endWindow - WINDOW_SIZE;

	if (messageList[msgNumber].nextFrame <0)
	{
		messageList[msgNumber].nextFrame = 0;
	}

	f.kind 			= DL_DATA;
	f.destNode 		= messageList[msgNumber].destNode;
	f.sendrNode 	= nodeinfo.nodenumber;
	f.len			= (size_t)(MAX_MESSAGE__SIZE);
	f.fullLen 		= messageList[msgNumber].msgSize;

/*	f.msg 			= (char*)calloc(f.len, sizeof(char));
*/
	nextNode 		= linkList[nodeinfo.nodenumber][f.destNode];

printf("Frame almost prepped\n");
printf("\t\t\tnextFrame = %i;\tlastWindow = %i;\tendWindow = %i;\n", messageList[msgNumber].nextFrame,messageList[msgNumber].lastWindow,messageList[msgNumber].endWindow);

	while((messageList[msgNumber].nextFrame < messageList[msgNumber].lastWindow) && ( messageList[msgNumber].nextFrame <= messageList[msgNumber].endWindow ))
	{
		printf("\tnextFrame = %i;\tlastWindow = %i;\tendWindow = %i;\n", messageList[msgNumber].nextFrame,messageList[msgNumber].lastWindow,messageList[msgNumber].endWindow);

		f.frameNumber 	= messageList[msgNumber].nextFrame;

		/*for ( j = 0; j < (f.len-1); ++j)
		{
			charIdx = j+f.frameNumber*(f.len-1);
			f.msg[j] = messageList[msgNumber].message[charIdx];
		}*/

		j = 0;
		do
		{
			charIdx = j+f.frameNumber*(f.len-1);
			f.msg[j] = messageList[msgNumber].message[charIdx];
			j++;
		}while((j < (f.len-1))&&(messageList[msgNumber].message[charIdx] != '\0'));

		f.msg[MAX_MESSAGE__SIZE-1] = '\0';
printf("\tFrame prepped and sending\n");
		sendFrame(f);

		messageList[msgNumber].nextFrame ++;
	}

	timeout = SIZE_FRAME(f)*((CnetTime)8000000 / linkinfo[nextNode].bandwidth) +
                linkinfo[nextNode].propagationdelay;
	messageList[msgNumber].timer = CNET_start_timer(EV_TIMER1, 3 * timeout, f.destNode + 6*nodeinfo.nodenumber);
	
}

static void sendFrame(FRAME f)
{
	size_t fLength;
	f.checkSum = 0;

	fLength = SIZE_FRAME(f);

    f.checkSum  = CNET_ccitt((unsigned char *)&f, fLength);
printf("\t\tChecksum generated\n");

	sendMessage(f);

}

static void sendMessage(FRAME f)
{
	size_t length;
	int nextNode;

	nextNode = linkList[nodeinfo.nodenumber][f.destNode];

	length = SIZE_FRAME(f);
printf("\t\t\tFrame sent\n");

	CHECK(CNET_write_physical(nextNode, (char *)&f, (size_t*)&length));
	printf("sending; f.msg = %s\n", f.msg);

}

static void plusNode()
{
	destNode = destNode + 1;

	if (destNode > 5)
	{
		destNode = 5;
		printf("The maximum value for destNode is %d.\n", 5);
	}

	printf("%d is the new destination node.\n", destNode);
}

static void minusNode()
{
	destNode = destNode - 1;
	if (destNode < 0)
	{
		destNode = 0;
		printf("The minimum value for destNode is 0.\n");
	}
	printf("%i is the new destination node.\n", destNode);
}

static EVENT_HANDLER(timeouts)
{
	fprintf(stderr, "data = %i\n",(int)data);
	printf("timeout occurred, resending frame = %i to node = %i\n", messageList[(int)data].nextFrame, messageList[(int)data].destNode);
	send_message((int)data- nodeinfo.nodenumber*6);
}

static EVENT_HANDLER(physical_ready)
{
	FRAME f;
	size_t len;
	int link;

	f.len = MAX_MESSAGE__SIZE;
	len = SIZE_FRAME(f);

	CHECK(CNET_read_physical(&link, (char*)&f, &len));

	printf("Reading physical:\n");

	if (checkFrame(f) == 0)
	{
		
		if ( f.destNode == nodeinfo.nodenumber )
		{
			printf("Processing Frame = %i; msg = %s\n", f.frameNumber, f.msg);
			processFrame(f);
		}
		else
		{
			printf("Forwarding message\n");
			sendMessage(f);
		}
		
	}
	else
	{
		printf("Checksum failed\n");
	}

}


static int checkFrame(FRAME f)
{
	int checkSum, length;
	int retVal;

	checkSum 	= f.checkSum;
		/*printFrame(f);*/


	f.checkSum 	= 0;

	length 		= SIZE_FRAME(f);
	f.checkSum	= CNET_ccitt((unsigned char *)&f, length);


	if (f.checkSum == checkSum)
	{
		retVal = 0;
	}
	else
	{
		retVal = -1;
		printf("Checksum failed\n");
	}

	return retVal;
}

static void processFrame(FRAME f)
{
	if (f.kind == DL_DATA)
	{

		if( getMessage(f) == 0)
		{	
			makeAck(&f);
			sendFrame(f);
		}
		else if (receivedList[f.sendrNode].nextFrame == f.frameNumber)
		{
			makeAck(&f);
			sendFrame(f);
		}
	}
	else
	{
		printf("ACK received\n");
		updateMessages(f);
	}
}

static int getMessage(FRAME f)
{
	int idx, i,retVal, thisLength;

	retVal = -1;

	idx = f.sendrNode;

	if (receivedList[idx].msgSize == 0)
	{
		createReceive(f);
	}

	if (receivedList[idx].nextFrame == f.frameNumber)
	{	
		retVal = 0;

		thisLength = f.fullLen-f.frameNumber*(MAX_MESSAGE__SIZE-1);

		if (thisLength > MAX_MESSAGE__SIZE)
		{
			thisLength = MAX_MESSAGE__SIZE;
			
		}
		else
		{
			perror("Last message reading");
		}

		for ( i = 0; i < thisLength; ++i)
		{
			receivedList[idx].message[i+f.frameNumber*(MAX_MESSAGE__SIZE-1)] = f.msg[i];
		}

		printf("The message so far is: '%s'\n", receivedList[idx].message);

		if ((receivedList[idx].nextFrame+1) == receivedList[idx].lastWindow)
		{
			printf("Full message received is: '%s'\n", receivedList[idx].message);
			
			deleteReceive(idx);

		}
		receivedList[idx].nextFrame ++;
	}
	return retVal;
}

static void deleteReceive(int idx)
{
	free(receivedList[idx].message);
	receivedList[idx].msgSize = 0;
}

static void createReceive(FRAME f)
{
	int i;

	i = f.sendrNode;

	receivedList[i].msgSize = f.fullLen;
	receivedList[i].message = (char*)calloc(receivedList[i].msgSize, sizeof(char));

	receivedList[i].lastWindow = (int)(receivedList[i].msgSize/((size_t)(MAX_MESSAGE__SIZE-1)));

	/*Ensuring the number of windows isn't mistaken due to integer division*/
	if ((receivedList[i].lastWindow*(int)(MAX_MESSAGE__SIZE-1)) != (int)receivedList[i].msgSize)
	{
		receivedList[i].lastWindow ++;
	}
	
	receivedList[i].nextFrame = 0;

}

static void makeAck(FRAME* f)
{
	int temp;

	temp 			= f->destNode;
	f->destNode 	= f->sendrNode;
	f->sendrNode 	= temp;
	
	f->len 			= 0;
	f->kind 		= DL_ACK;

}

static void updateMessages(FRAME f)
{
	int idx;

	idx = f.sendrNode;

printf("\t\t\tUpdating messageList[%i] with frameNumber = %i\n", idx, f.frameNumber);
	if (messageList[idx].endWindow <= (f.frameNumber+WINDOW_SIZE))
	{
		messageList[idx].endWindow = f.frameNumber+1 + WINDOW_SIZE;

		if ((f.frameNumber+1) == messageList[idx].lastWindow )
		{
			printf("Message:'%s' to node %i has finished sending\n", messageList[idx].message, messageList[idx].destNode);
			CNET_stop_timer(messageList[idx].timer);
			deleteMessage(idx);
		}	
	}
}

static void deleteMessage(int idx)
{
	if(messageList[idx].msgSize == 0)
	{
		free(messageList[idx].message);
		messageList[idx].msgSize = 0;
	}
}