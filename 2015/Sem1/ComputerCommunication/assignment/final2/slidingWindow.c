

void reboot_node(CnetEvent ev, CnetTimerID timer, CnetData data)
{
	int i;

	for ( i = 0; i < 6; ++i)
	{
		receivedList[i].msgSize = 0;
	}

	printf("%d\n", destNode);
	printf("I am node %d\n",nodeinfo.nodenumber);
	if (nodeinfo.nlinks > 0)
		printf("There are %d link(s) connecting me to the outside world\n",nodeinfo.nlinks);
	else
		printf("Is there anybody out there?\n");

	CHECK(CNET_set_handler(EV_DEBUG0,	plusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG0, "Node + 1"));
	CHECK(CNET_set_handler(EV_DEBUG1,	minusNode, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG1, "Node - 1"));
	CHECK(CNET_set_handler( EV_DEBUG2, create_message, 0));
	CHECK(CNET_set_debug_string( EV_DEBUG2, "Send message"));

    CHECK(CNET_set_handler( EV_PHYSICALREADY,    physical_ready, 0));

    CHECK(CNET_set_handler( EV_TIMER1,           timeouts, 0));

}

static void plusNode()
{
	destNode = destNode + 1;

	if (destNode > 5)
	{
		destNode = 5;
		printf("The maximum value for destNode is %d.\n", 5);
	}

	printf("%d is the new destination node.\n", destNode);
}

static void minusNode()
{
	destNode = destNode - 1;
	if (destNode < 0)
	{
		destNode = 0;
		printf("The minimum value for destNode is 0.\n");
	}
	printf("%i is the new destination node.\n", destNode);
}

static EVENT_HANDLER(timeouts)
{
	send_message((int)data);
}

static EVENT_HANDLER(physical_ready)
{
	FRAME f;
	size_t len;
	int link;

	f.len = MAX_MESSAGE__SIZE;
	len = SIZE_FRAME(f);

	CHECK(CNET_read_physical(&link, (char*)&f, &len));


	if (checkFrame(f) == 0)
	{
		/*if ( f.destNode == nodeinfo.nodenumber )
		{
			processFrame(f);
		}
		else
		{
			addToQueue(f);
		}
		
	}
	else
	{
		printf("Checksum failed\n");
	}

}