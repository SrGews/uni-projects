/***********************************************************************************/
/*                                                                                 */
/*FILE:         Adder.c                                                            */
/*AUTHOR:       Angus Campbell                                                     */
/*STUDENT ID:   17163042                                                           */
/*UNIT:         Operating Systems                                                  */
/*LAST MOD:     09/05/2015                                                         */
/*                                                                                 */
/***********************************************************************************/

#include "Adder.h"

/*Takes a filename and an int as inputs which are the file to read in 
**and the number of processes to create
**The program will read in integers from the file, each seperated by
**a comma only, and find the sum of the numbers by creating k sub
**processes to each add a range of the numbers which the parent process
**will then add together.
**
**an example file would be:
1,2,1,2,2,45,3215,45,231,879,5,9,6,


**The program has issues with very large files
*/




/*main initialises the shared memory of Buffer1 which has each number
**from the input file, Sub_total which sub processes will use to give
**the parent process their sub total, and three semaphores used to
**stop processes from accessing Sub_total out of turn
**
**main then reads in the file provided by the command line and dynamically
**populates Buffer1
**
**main then initialises the semaphores and Sub_total and creates the
**child processes which then begin finding the total of a range of 
**Buffer1 which has been assigned to them during creation
**
**the parent process will then add the Sub_totals one by one printing
**the results to screen
*/
int main( int argc, char **argv )
{
    int k, numSize = 100, isChild;
    int *Buffer1, *Sub_total;
    sem_t *fullS, *emptyS, *mutex;
    pid_t *pid;


    if( argc!=3 )
    {
        perror( "ERROR: Wrong number of inputs" );
    }
    else if(atoi(argv[2])<1)
    {
        perror( "ERROR: k is an invalid value");
    }
    else
    {
        Buffer1 = (int*) readFile(argv, &numSize);

        fullS = (sem_t*)getShM( sizeof(sem_t) );
        emptyS = (sem_t*)getShM( sizeof(sem_t) );
        mutex = (sem_t*)getShM( sizeof(sem_t) );
        pid = (pid_t*)getShM( sizeof(pid_t));

        Sub_total = (int*)getShM( sizeof(int) );  

        init_sem(emptyS,1,1);
        init_sem(mutex,1,1);
        init_sem(fullS,1,0);

        *Sub_total = (int)0;

        k = atoi(argv[2]);
        
        isChild = makeProcesses( k, numSize, Buffer1, Sub_total, emptyS, fullS, mutex, pid);


        if(isChild == 0)
        {
            getTotal( k, fullS, emptyS, mutex, Sub_total, pid);
        }
    }    
    return 0;
}

/*readFile reads in the file and populates numbers with them.
**numbers is dynamically changed in size to only be the size of the elements
**read in from the file
**
**Buffer1 is then declared as shared memory of the size of numbers, and
**the values in numbers is copied into Buffer1's memory
*/
void* readFile(char** argv, int* numSize)
{
FILE *file;
int* numbers, *Buffer1;
int i, ch, error;

    file = fopen( argv[1], "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv[1] );
        perror( "ERROR: Input file not valid" );
        error = -1;
    }

    if ( error == -1 )
    {
        perror( "ERROR: File didn't open successfully\n" );
    }
    else
    {            
        numbers = ( int* )malloc( sizeof( int ) * (*numSize) );
        i=0;
        
        do
        {
            error = fscanf( file, "%i,", &ch );

            if ( error == 1 )
            {
                numbers[i]=ch;
            }

            i ++;
            if (i>*numSize)
            {   
                numbers = (int*)realloc( numbers ,((*numSize+100) * sizeof(int) ));
                *numSize += 100;
            }

        }while( ( error == 1 ) && ( feof( file) == 0 ) );
        
        *numSize += (i-*numSize);

        Buffer1 = (int*)getShM( *numSize*sizeof(int) );
        
        for ( i=0; i < *numSize; ++i)
        {
            Buffer1[i] = numbers[i];
        }
        free( numbers);
    }

    fclose( file );

    return Buffer1;
}



/*initialises semaphores to an appropriate value
*/
void init_sem(sem_t* semaphore, int type, int value)
{
    if( sem_init(semaphore, type, value) < 0)
    {
      perror("semaphore initilization");
    }
}


/*allocates shared memory of size sizeShM and returns a void pointer 
**to the memory location
*/
void* getShM( int sizeShM)
{
    void* shM;
    int memAddr;

    if ((memAddr = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0666)) < 0) {
    perror("shmget");
    }

    if ((shM = shmat(memAddr, NULL, 0)) == (void*) -1) {
        perror("shmat");
    }

    return shM;
}


/*getTotal is called by the parent process to sum up the sub totals 
**generated by each child process.
**each child process will lock mutex and fill Sub_total. when that is
**done they will unlock mutex and unlock fullS, signalling getTotal
**to run
**getTotal then locks fullS and mutex and reads the child process' id
**and it's sub total, adding Sub_total to total and printing to screen
**the result.
**mutex and emptyS are both unlocked signalling the next child process
**to place it's sub total into Sub_total and replace pid with it's
**process id
*/
void getTotal(int k, sem_t* fullS, sem_t* emptyS, sem_t* mutex, int* Sub_total, pid_t* pid)
{
    int total = 0, i, subT = 0;
    pid_t thePid;
    
    for ( i=0; i<k; i++)
    {
        wait(fullS);
        wait(mutex);
        subT = *Sub_total;
        thePid = *pid;
        signal(mutex);
        signal(emptyS);

        total += subT;
        printf("Sub_total produced by Processor %6i: %6i\n", thePid, subT);
    }
    printf("Final total = %32i\n", total);
}


/*makeProcess is called by the parent process to create the child processes.
**makeProcess creates k child process and assignes them a range of
**values from Buffer1 to add
**the created child process will then call calcSubTotal
*/
int makeProcesses(int k, int numSize, int* Buffer1, int* Sub_total, sem_t* emptyS, sem_t* fullS, sem_t* mutex, pid_t* pid)
{
    int i, leftVal, subT, extra, rangeToAdd, isChild, child;

    isChild = 0;
    leftVal = 0;
    subT = 0;

    extra = numSize % k;
    rangeToAdd = (numSize / k) + 1;

    for(i = 0; i < k; i++)
    {
        child = fork();

        if ( i == extra )
        {
            rangeToAdd -- ;
        }
        
        if ( child == 0 )
        {
            isChild = 1;

            calcSubTotal(Buffer1, Sub_total, leftVal, rangeToAdd, emptyS, fullS, mutex, pid);
            
            i = k;
        }
        else
        {
            leftVal += rangeToAdd;
        }
    }
    return isChild;
}


/*calcSubTotal is called by the child processes
**the child will first find the sum of all numbers in Buffer1 from leftVal
**to leftVal + rangeToAdd
**the child will then wait until Sub_total in empty (emptyS) and will then
**lock the mutex to stop other processes from accessing their critical
**sectiion
**
**the child then sets Sub_total to it's sub total, and pid to the child's
**process id
**
**the child will then return to main and exit.
*/
void calcSubTotal(int* Buffer1, int* Sub_total, int leftVal, int rangeToAdd, sem_t* emptyS, sem_t* fullS, sem_t* mutex, pid_t* pid)
{
    int j, subT;
    pid_t thisPid;

    j = 0;
    subT = 0;

    for ( j = leftVal; j < (leftVal+rangeToAdd); ++j)
    {

        subT += Buffer1[j];

    }
    thisPid = getpid();

    wait(emptyS);
    wait(mutex);
    *Sub_total = subT;
    *pid = thisPid;
    signal(mutex);
    signal(fullS);
}


/*these two functions unlock and wait-lock the semaphores.
**fairly useless at the moment but good if the semaphores are changed
**plus I like using the signal() and wait() functions
*/
void signal(sem_t* semaphore)
{
    sem_post(semaphore);
}
void wait(sem_t* semaphore)
{
    sem_wait(semaphore);
}