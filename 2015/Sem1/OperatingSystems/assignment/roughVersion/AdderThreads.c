/*Works to a file length of 81, and up to 20 processes*/


#include "AdderThreads.h"

int k, numSize = 100;
int *Buffer1, Sub_total[1];
pthread_mutex_t  *mutex;
pthread_cond_t conditionFull, conditionEmpty;
int lastThread;


IDList* idList;

enum
{
    STATE_EMPTY,
    STATE_FULL
} state = STATE_EMPTY;

int main( int argc, char **argv )
{
    Buffer1 = ( int* )calloc( numSize , sizeof( int ) );
    Sub_total[0] = 0;

    if( argc!=3 )
    {
        perror( "ERROR: Wrong number of inputs\n" );
    }
    else
    {
        readFile(argv[1]);

        mutex = (pthread_mutex_t*)calloc(1,sizeof(pthread_mutex_t));

        k = atoi(argv[2]);
        Sub_total[0] = 0;

        initMutex(mutex);
        initCondition(&conditionFull);
        initCondition(&conditionEmpty);

        makeThreads();

        getTotal();
        
        free(idList);
        free(Buffer1);

        dedstroyMutex();
        destroyCond(&conditionFull);
        destroyCond(&conditionEmpty);

        free (mutex);        
    }
    return 0;
}

void readFile(char *fName)
{

    FILE* file;
    int error, i, ch;

    file = fopen( fName, "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", fName );
        perror( "ERROR: Input file not valid" );
        error = -1;
    }

    if ( error == -1 )
    {
        perror( "ERROR: File didn't open successfully\n" );
    }
    else
    {
        i=0;
        do
        {
            error = fscanf( file, "%i,", &ch );

            if ( error == 1 )
            {
                Buffer1[i]=ch;
            }

            i ++;
            if (i==numSize)
            {   
                 Buffer1 = (int*)realloc( Buffer1, ((numSize + 100)* sizeof(int)));
                 numSize += 100;
            }

        }while( ( error == 1 ) && ( feof( file) == 0 ) );

        numSize += (i-numSize);
        Buffer1 = (int*)realloc( Buffer1, ((numSize + (i - numSize))* sizeof(int)));
    }
    fclose( file );
}

void destroyCond(pthread_cond_t* condition)
{
    if( pthread_cond_destroy(condition) != 0)
    {
        perror("wait condition initilization");
    }
}

void dedstroyMutex()
{
    if( pthread_mutex_destroy(mutex) != 0)
    {
      perror("semaphore destruction 1");
    }
}

void initCondition(pthread_cond_t* condition)
{
    if( pthread_cond_init(condition, NULL) != 0)
    {
        perror("wait condition initilization");
    }

}

void initMutex(pthread_mutex_t* mutex)
{        
    if( pthread_mutex_init(mutex, NULL) != 0)
    {
    perror("semaphore initilization 1");
    }
}


void signal()
{
    if (state == STATE_EMPTY)
    {
        state = STATE_FULL;
        pthread_cond_signal(&conditionFull);
    } else 
    {
        state = STATE_EMPTY;
        pthread_cond_signal(&conditionEmpty);
    }
    pthread_mutex_unlock(mutex);
}

void waitEmpty(pthread_mutex_t* semaphore, pthread_cond_t *conditionEmpty)
{
    pthread_mutex_lock(semaphore);
    while (state != STATE_EMPTY)
    {
        /*printf("waitEmpty, state = %i;\n", state);*/
        pthread_cond_wait(conditionEmpty, semaphore);
    }
}

void waitFull(pthread_mutex_t* semaphore, pthread_cond_t *conditionFull)
{
    pthread_mutex_lock(semaphore);
    while (state != STATE_FULL)
    {
      /*  printf("waitFull\n");*/
        pthread_cond_wait(conditionFull, semaphore);
    }
    /*printf("done waitingFull\n");*/
}

void* doThreadStuff()
{
    int j, location, subT = 0;
    pthread_t thisThread;

    thisThread = pthread_self();

    for ( j = 0; j < k; ++j)
    {
        if (idList[j].thid == thisThread)
        {
            location = j;
            j = k;
        }
    }

    for ( j = idList[location].leftVal; j < (idList[location].leftVal+idList[location].rangeToAdd); ++j)
    {
        subT += Buffer1[j];
        /*printf("subT = %i;     s = %i;\n", Sub_total, s[j]);*/
    }

    waitEmpty(mutex, &conditionEmpty);
    Sub_total[0] = subT;
    lastThread = location;
    signal();

    return NULL;
}

void getTotal()
{
    int total = 0, i;

    for ( i=0; i<k; i++)
    {
        waitFull(mutex, &conditionFull);
        total += Sub_total[0];
        printf("Sub_total from thread %3i = %8i;\n", lastThread , Sub_total[0]);
        signal();
    }
    printf("Final total = %i\n", total);
}

void makeThreads()
{
    int i, extra;
    int bufferRange[2] = {0,0};

    idList = (IDList*)calloc(k, sizeof(IDList));

    extra = numSize % k;
    bufferRange[1] = (numSize / k) + 1;

    for(i = 0; i < k; i++)
    {
        /*printf("Making child number %i;.\n", (i+1));*/

        if (( i == extra ))
        {
            bufferRange[1] -- ;
        }

        pthread_create(&(idList[i].thid), NULL, &doThreadStuff, NULL);

        idList[i].leftVal = bufferRange[0];
        idList[i].rangeToAdd = bufferRange[1];

        /*printf("leftVal = %i;      rangeToAdd = %i;\n", idList[i].leftVal, idList[i].rangeToAdd);
        */
        bufferRange[0] += bufferRange[1];
    }
}

void switchArrays(int arr1[], int arr2[], int sizeArr)
{
    int k;
    for ( k = 0; k < sizeArr; ++k)
    {
        arr1[k]=arr2[k];
    }
}

void readExpressions( int *Buffer1, FILE *file )
{
    int i = 0, ch, error;

    do
    {
       
        error = fscanf( file, "%i\n", &ch );

        if ( error == 1 )
        {
            Buffer1[i]=ch;
        }

        i ++;
    }while( ( error == 1 ) && ( feof( file) == 0 ) );
}