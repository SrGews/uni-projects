/*Works to a file length of 81, and up to 897 processes*/


#include "Adder.h"

int main( int argc, char **argv )
{
    int error = 0, ch, i, k, numSize = 100, buffrAddr, dutAddr, mutAddr, futAddr, subAddr, isChild;
    int *numbers, *shm, *Buffer1, *Sub_total, *temp;
    sem_t *dutex, *mutex, *futex;

    FILE *file;

    if( argc!=3 )
    {
        perror( "ERROR: Wrong number of inputs\n" );
    }
    else
    {

        file = fopen( argv[1], "r" );
        if( file == NULL )
        {
            printf( "ERROR: could not open '%s'\n", argv[1] );
            perror( "ERROR: Input file not valid" );
            error = -1;
        }

        if ( error == -1 )
        {
            perror( "ERROR: File didn't open successfully\n" );
        }
        else
        {            
            numbers = ( int* )malloc( sizeof( int ) * numSize );

            i=0;
            do
            {
               
                error = fscanf( file, "%i,", &ch );

                if ( error == 1 )
                {
                    numbers[i]=ch;
                }

                i ++;
                if (i>numSize)
                {   

                    numbers = (int*)realloc( numbers ,((numSize+100) * sizeof(int) ));
                    numSize += 100;


                }
            }while( ( error == 1 ) && ( feof( file) == 0 ) );
            
            temp = (int*)malloc( numSize * sizeof(int) );

            switchArrays(temp, numbers, numSize);
            free(numbers);
            
            numbers = (int*)malloc(( numSize + (i-numSize)) * sizeof(int) );

            numSize += (i-numSize);

            switchArrays(numbers,temp,numSize);

            free(temp);


            if ((buffrAddr = shmget(IPC_PRIVATE, (numSize * sizeof(int)), IPC_CREAT | 0666)) < 0) {
                perror("shmget");
            }

            /*
             * Now we attach the segment to our data space.
             */
            if ((shm = (int*)shmat(buffrAddr, NULL, 0)) == (int *) -1) {
                perror("shmat");
            }

            Buffer1 = shm;
            
            for ( i=0; i < numSize; ++i)
            {
                
                *Buffer1 = numbers[i];
                Buffer1++;
            }

            
        }

        fclose( file );
        free( numbers);
        



        dutex = (sem_t*)getShM( sizeof(sem_t) );

/*        if ((dutAddr = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0666)) < 0) {
            perror("shmget");
        }


         * Now we attach the segment to our data space.

        if ((dutex = (sem_t*)shmat(dutAddr, NULL, 0)) == (sem_t *) -1) {
            perror("shmat");
        }
*/
        if ((mutAddr = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0666)) < 0) {
            perror("shmget");
        }

        /*
         * Now we attach the segment to our data space.
         */
        if ((mutex = (sem_t*)shmat(mutAddr, NULL, 0)) == (sem_t *) -1) {
            perror("shmat");
        }

        if ((subAddr = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) < 0) {
            perror("shmget");
        }

        /*
         * Now we attach the segment to our data space.
         */
        if ((Sub_total = (int*)shmat(subAddr, NULL, 0)) == (int *) -1) {
            perror("shmat");
        }

        if ((futAddr = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0666)) < 0) {
            perror("shmget");
        }

        /*
         * Now we attach the segment to our data space.
         */
        if ((futex = (sem_t*)shmat(futAddr, NULL, 0)) == (sem_t *) -1) {
            perror("shmat");
        }        


        if( sem_init(mutex,1,1) < 0)
        {
          perror("semaphore initilization");
        }
        if( sem_init(futex,1,1) < 0)
        {
          perror("semaphore initilization");
        }
        if( sem_init(dutex,1,0) < 0)
        {
          perror("semaphore initilization");
        }


        *Sub_total = (int)0;

        k = atoi(argv[2]);

        Buffer1 = shm;
        
        isChild = makeProcesses( k, numSize, Buffer1, Sub_total, mutex, dutex, futex);


        if(isChild == 0)
        {
            getTotal( k, dutex, mutex, futex, Sub_total);
        }
        

    }
    return 0;
}

void* getShM( int sizeShM)
{
    void* shM;
    int memAddr;

    if ((memAddr = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0666)) < 0) {
    perror("shmget");
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((shM = shmat(memAddr, NULL, 0)) == (void*) -1) {
        perror("shmat");
    }

    return shM;
}

void getTotal(int k, sem_t* dutex, sem_t* mutex, sem_t* futex, int* Sub_total)
{
    int total = 0, i, subT = 0;

    for ( i=0; i<k; i++)
    {
        wait(dutex);
        wait(futex);
        total += *Sub_total;
        subT = *Sub_total;
        signal(futex);
        signal(mutex);
        printf("Sub_total produced by Processor %4i: %i\n", (i+1), subT);

    }
    printf("Final total = %i\n", total);

}

int makeProcesses(int k, int numSize, int* Buffer1, int* Sub_total, sem_t* mutex, sem_t* dutex, sem_t* futex)
{
    int i, j, leftVal = 0, subT = 0, extra, rangeToAdd, isChild=0, child;

    extra = numSize % k;
    rangeToAdd = (numSize / k) + 1;

    for(i = 0; i < k; i++)
    {
        /*printf("Making child number %i.\n", (i+1));*/
        child = fork();

        if ( i == extra )
        {
            rangeToAdd -- ;
        }
        
        if ( child == 0 )
        {
            isChild = 1;
            for ( j = leftVal; j < (leftVal+rangeToAdd); ++j)
            {

                subT += Buffer1[j];
                /*printf("subT = %i;     s = %i;\n", *Sub_total, s[j]);*/

            }
            /*printf("k = %i;    leftVal = %i;    rangeToAdd = %i;     subT = %i;\n",k, leftVal, rangeToAdd, subT);
            */


            wait(mutex);
            wait(futex);
            *Sub_total = subT;
            /*printf("Sub_total = %i\n", (int)*Sub_total);*/
            signal(futex);
            signal(dutex);
            i = k;
        }
        else
        {
            leftVal += rangeToAdd;
        }
    }
    return isChild;
}


pid_t getPid()
{
    pid_t pid;
    pid = getpid();
    return pid;
}

void signal(sem_t* semaphore)
{
    sem_post(semaphore);
}
void wait(sem_t* semaphore)
{
    sem_wait(semaphore);
}




void switchArrays(int arr1[], int arr2[], int sizeArr)
{
    int k;
    for ( k = 0; k < sizeArr; ++k)
    {
        arr1[k]=arr2[k];
    }
}

void readExpressions( int *numbers, FILE *file )
{
    int i = 0, ch, error;

    do
    {
       
        error = fscanf( file, "%i\n", &ch );

        if ( error == 1 )
        {
            numbers[i]=ch;
        }

        i ++;
    }while( ( error == 1 ) && ( feof( file) == 0 ) );

}




/*For whatever reason, the malloc'd space from this function is not persistant. Do not use this function.*/
int resizeArray(int* arr1, int sizeArr, int sizeIncrease)
{
    int* temp;

    temp = (int*)malloc( sizeArr * sizeof(int) );

    switchArrays(temp, arr1, sizeArr);
    free(arr1);
    
    arr1 = (int*)malloc(( sizeArr + sizeIncrease) * sizeof(int) );

    sizeArr += sizeIncrease;

    switchArrays(arr1,temp,sizeArr);

    free(temp);

    return sizeArr;
}