/*Works to a file length of 81, and up to 897 processes*/


#include "Adder.h"

int main( int argc, char **argv )
{
    int k, numSize = 100, isChild;
    int *Buffer1, *Sub_total;
    sem_t *fullS, *emptyS, *mutex;
    pid_t *pid;


    if( argc!=3 )
    {
        perror( "ERROR: Wrong number of inputs\n" );
    }
    else
    {
        Buffer1 = (int*) readFile(argv, &numSize);

        fullS = (sem_t*)getShM( sizeof(sem_t) );
        emptyS = (sem_t*)getShM( sizeof(sem_t) );
        mutex = (sem_t*)getShM( sizeof(sem_t) );
        pid = (pid_t*)getShM( sizeof(pid_t));

        Sub_total = (int*)getShM( sizeof(int) );  

        init_sem(emptyS,1,1);
        init_sem(mutex,1,1);
        init_sem(fullS,1,0);

        *Sub_total = (int)0;

        k = atoi(argv[2]);
        
        isChild = makeProcesses( k, numSize, Buffer1, Sub_total, emptyS, fullS, mutex, pid);


        if(isChild == 0)
        {
            getTotal( k, fullS, emptyS, mutex, Sub_total, pid);
        }
    }    
    return 0;
}

void* readFile(char** argv, int* numSize)
{
FILE *file;
int* numbers, *Buffer1;
int i, ch, error;

    file = fopen( argv[1], "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv[1] );
        perror( "ERROR: Input file not valid" );
        error = -1;
    }

    if ( error == -1 )
    {
        perror( "ERROR: File didn't open successfully\n" );
    }
    else
    {            
        numbers = ( int* )malloc( sizeof( int ) * *numSize );
        i=0;
        
        do
        {
            error = fscanf( file, "%i,", &ch );

            if ( error == 1 )
            {
                numbers[i]=ch;
            }

            i ++;
            if (i>*numSize)
            {   
                numbers = (int*)realloc( numbers ,((*numSize+100) * sizeof(int) ));
                *numSize += 100;
            }

        }while( ( error == 1 ) && ( feof( file) == 0 ) );
        
        *numSize += (i-*numSize);

        Buffer1 = (int*)getShM( *numSize*sizeof(int) );
        
        for ( i=0; i < *numSize; ++i)
        {
            Buffer1[i] = numbers[i];
        }
        free( numbers);
    }

    fclose( file );

    return Buffer1;
}

void init_sem(sem_t* semaphore, int type, int value)
{
    if( sem_init(semaphore, type, value) < 0)
    {
      perror("semaphore initilization");
    }
}

void* getShM( int sizeShM)
{
    void* shM;
    int memAddr;

    if ((memAddr = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0666)) < 0) {
    perror("shmget");
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((shM = shmat(memAddr, NULL, 0)) == (void*) -1) {
        perror("shmat");
    }

    return shM;
}

void getTotal(int k, sem_t* fullS, sem_t* emptyS, sem_t* mutex, int* Sub_total, pid_t* pid)
{
    int total = 0, i, subT = 0;
    pid_t thePid;
    
    for ( i=0; i<k; i++)
    {
        wait(fullS);
        wait(mutex);
        subT = *Sub_total;
        thePid = *pid;
        signal(mutex);
        signal(emptyS);

        total += subT;
        printf("Sub_total produced by Processor %6i: %6i\n", thePid, subT);
    }
    printf("Final total = %32i\n", total);
}

int makeProcesses(int k, int numSize, int* Buffer1, int* Sub_total, sem_t* emptyS, sem_t* fullS, sem_t* mutex, pid_t* pid)
{
    int i, leftVal, subT, extra, rangeToAdd, isChild, child;

    isChild = 0;
    leftVal = 0;
    subT = 0;

    extra = numSize % k;
    rangeToAdd = (numSize / k) + 1;

    for(i = 0; i < k; i++)
    {
        /*printf("Making child number %i.\n", (i+1));*/
        child = fork();

        if ( i == extra )
        {
            rangeToAdd -- ;
        }
        
        if ( child == 0 )
        {
            isChild = 1;

            calcSubTotal(Buffer1, Sub_total, leftVal, rangeToAdd, emptyS, fullS, mutex, pid);
            
            i = k;
        }
        else
        {
            leftVal += rangeToAdd;
        }
    }
    return isChild;
}

void calcSubTotal(int* Buffer1, int* Sub_total, int leftVal, int rangeToAdd, sem_t* emptyS, sem_t* fullS, sem_t* mutex, pid_t* pid)
{
    int j, subT;
    pid_t thisPid;

    j = 0;
    subT = 0;

    for ( j = leftVal; j < (leftVal+rangeToAdd); ++j)
    {

        subT += Buffer1[j];
        /*printf("subT = %i;     s = %i;\n", *Sub_total, s[j]);*/

    }
    /*printf("k = %i;    leftVal = %i;    rangeToAdd = %i;     subT = %i;\n",k, leftVal, rangeToAdd, subT);
    */
    thisPid = getpid();

    wait(emptyS);
    wait(mutex);
    *Sub_total = subT;
    *pid = thisPid;
    signal(mutex);
    signal(fullS);
}

void signal(sem_t* semaphore)
{
    sem_post(semaphore);
}
void wait(sem_t* semaphore)
{
    sem_wait(semaphore);
}




void switchArrays(int arr1[], int arr2[], int sizeArr)
{
    int k;
    for ( k = 0; k < sizeArr; ++k)
    {
        arr1[k]=arr2[k];
    }
}

void readExpressions( int *numbers, FILE *file )
{
    int i = 0, ch, error;

    do
    {
       
        error = fscanf( file, "%i\n", &ch );

        if ( error == 1 )
        {
            numbers[i]=ch;
        }

        i ++;
    }while( ( error == 1 ) && ( feof( file) == 0 ) );

}