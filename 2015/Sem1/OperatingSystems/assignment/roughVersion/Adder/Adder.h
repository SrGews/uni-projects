#include <semaphore.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>




void* readFile(char** argv, int* numSize);
void calcSubTotal(int* Buffer1, int* Sub_total, int leftVal, int rangeToAdd, sem_t* mutex, sem_t* dutex, sem_t* futex, pid_t* pid);
void init_sem(sem_t* semaphore, int type, int value);
void getTotal(int k, sem_t* dutex, sem_t* mutex, sem_t* futex, int* Sub_total, pid_t* pid);
int makeProcesses(int k, int numSize, int* s, int* Sub_total, sem_t* mutex, sem_t* dutex, sem_t* futex, pid_t* pid);
void signal(sem_t* semaphore);
void wait(sem_t* semaphore);
pid_t getPid();
int resizeArray(int arr1[], int sizeArr, int sizeIncrease);
void switchArrays(int arr1[], int arr2[], int sizeArr);
void readExpressions( int *numbers, FILE *file );
void* getShM( int sizeShM);
