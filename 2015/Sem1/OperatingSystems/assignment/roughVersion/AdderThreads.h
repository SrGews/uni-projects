#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct 
{
    int leftVal;
    int rangeToAdd;
    pthread_t thid;
} IDList;


void readFile(char *fName);
void destroyCond(pthread_cond_t* condition);
void dedstroyMutex();
void initCondition(pthread_cond_t* condition);
void initMutex(pthread_mutex_t* mutex);
void signal();
void waitEmpty(pthread_mutex_t* semaphore, pthread_cond_t *conditionEmpty);
void waitFull(pthread_mutex_t* semaphore, pthread_cond_t *conditionFull);
void* doThreadStuff();
void getTotal();
void makeThreads();
void switchArrays(int arr1[], int arr2[], int sizeArr);
int resizeArray(int arr1[], int sizeArr, int sizeIncrease);
void readExpressions( int *Buffer1, FILE *file );