/****************************************************************************/
/*                                                                          */
/*FILE:         branchb-search.c                                              */
/*AUTHOR:       Angus Campbell                                              */
/*STUDENT ID:   17163042                                                    */
/*UNIT:         AMI                                                         */
/*LAST MOD:     15/05/2015                                                  */
/*                                                                          */
/****************************************************************************/

#include "branchb-search.h"

/*This program is supposed to perform a branch and bound search for paths from
**an input node to an input destination node.
**
**The program in it's current state will exit prematurely without finding any
**paths.
*/

/*Main calls a function to read in the file, and passes the values to 
**branchSearch
*/

int main( int argc, char **argv )
{
	NodeData nodeData;

	readFile(argv, &nodeData);

    branchbSearch(argv, &nodeData);

	return 0;
}

/*This starts the branch and bound search by initialising stuff
**
*/
void branchbSearch(char** argv, NodeData *nodeData)
{
    int startNode, destNode;
    ListPaths listPaths;
    listPaths.numPathways = 1;

    startNode = addNode(argv[3], nodeData);
    destNode = addNode(argv[4], nodeData);

    listPaths.pathWays[0].path[0] = startNode;
    listPaths.pathWays[0].nodesVisited = 1;
    listPaths.pathWays[0].totalCost = 0;
    nodeData->nodeList[startNode].shortestCost = 0;

    branchb(&listPaths, nodeData, destNode);
    
}

void branchb(ListPaths *listPaths, NodeData *nodeData, int destNode)
{
    int finished =0;
    while ((listPaths->numPathways>0)&&(finished==0))
    {

        expand(listPaths, nodeData);
        sort(listPaths);
        if(listPaths->pathWays[0].path[
            listPaths->pathWays[0].nodesVisited]==destNode)
        {
            printf("A solution has been found\n");
            showPaths(listPaths, nodeData);
            printf("Continue searching? '1' for no, '0' for yes\n" );
            scanf(" %i",&finished);
        }
    }

    if(finished==0)
    {
        printf("No more solutions\n");
    }

}

void showPaths(ListPaths *listPaths, NodeData *nodeData)
{
    int i, j;

    printf("All pathways:\n");
    for ( i = 0; i < listPaths->numPathways; ++i)
    {
        printf("Cost = %i\t",listPaths->pathWays[i].totalCost);
        for ( j = 0; j < listPaths->pathWays[i].nodesVisited; ++j)
        {
            printf(" -> %s", 
                nodeData->nodeList[listPaths->pathWays[i].path[j]].name);
        }
        printf("\n");
    }
}

void expand(ListPaths *listPaths, NodeData *nodeData)
{
    int i,j, pathEndNode, pathLength, pathCost, newCost;


    pathEndNode = listPaths->pathWays[0].path[
        listPaths->pathWays[0].nodesVisited];
    pathLength = listPaths->pathWays[0].nodesVisited;
    pathCost = listPaths->pathWays[0].totalCost;



    for ( i = 0; i <= nodeData->nodeList[pathEndNode].numEdges; ++i)
    {

        newCost = pathCost + 
        nodeData->nodeList[pathEndNode].edgeNodes[i].cost;

        if ((nodeData->nodeList[pathEndNode].shortestCost > newCost)||
            (nodeData->nodeList[pathEndNode].shortestCost == 0))
        {

            for ( j = 0; j < pathLength; ++j)
            {
                listPaths->pathWays[listPaths->numPathways].path[j] = 
                    listPaths->pathWays[0].path[j];
                listPaths->numPathways ++;
            }
        }
        
    }



}

void sort(ListPaths *listPaths)
{
    int i, shortest=1;

    for ( i = 2; i < listPaths->numPathways; ++i)
    {
        if ( listPaths->pathWays[shortest].totalCost > 
            listPaths->pathWays[i].totalCost)
        {
         shortest = i;
        }
    }

    for ( i = 0; i < listPaths->pathWays[shortest].nodesVisited; ++i)
    {
        listPaths->pathWays[0].path[i]=listPaths->pathWays[shortest].path[i];
    }
    listPaths->pathWays[0].totalCost = listPaths->pathWays[shortest].
    totalCost;
    listPaths->pathWays[0].nodesVisited = listPaths->pathWays[
        shortest].nodesVisited;

    for ( i = 0; i < listPaths->pathWays[
        listPaths->numPathways].nodesVisited; ++i)
    {
        listPaths->pathWays[shortest].path[i]=listPaths->pathWays[
            listPaths->numPathways].path[i];
    }
    listPaths->pathWays[shortest].totalCost = listPaths->pathWays[
        listPaths->numPathways].totalCost;
    listPaths->pathWays[shortest].nodesVisited = listPaths->pathWays[
        listPaths->numPathways].nodesVisited;

    listPaths->numPathways --;
}


void readFile(char** argv, NodeData* nodeData)
{

	readNodes(argv[1], nodeData);

}


void readNodes(char* argv, NodeData* nodeData)
{
    FILE *file;
    int i = 0, cost, idN1, idN2;
    char name1[15], name2[15];

    file = fopen( argv, "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv );
        perror( "ERROR: Input file not valid" );
    }
    else
    {   
        nodeData->numNodes = -1;

        /*fscanf(file, "%s %s %i", name1, name2, &cost);*/
        readNode(&file,name1);
        readNode(&file,name2);
        readCost(&file, &cost);

        do
        {

            idN1 = addNode(name1, nodeData);
            idN2 = addNode(name2, nodeData);

            nodeData->nodeList[idN1].numEdges ++;
            nodeData->nodeList[idN1].edgeNodes[nodeData->nodeList[idN1].
                numEdges].nodeid = idN2;
            nodeData->nodeList[idN1].edgeNodes[nodeData->nodeList[idN1].
                numEdges].cost = cost;

            nodeData->nodeList[idN2].numEdges ++;
            nodeData->nodeList[idN2].edgeNodes[nodeData->nodeList[idN2].
                numEdges].nodeid = idN1;
            nodeData->nodeList[idN2].edgeNodes[nodeData->nodeList[idN2].
                numEdges].cost = cost;

        	i++;

            readNode(&file,name1);
            readNode(&file,name2);
            readCost(&file, &cost);

            

        }while( ( feof( file) == 0 ) );

    }

    fclose( file );
}

int compareNames(char name1[15], char name2[15])
{
    int i, j,retVal = 0;

    for (i = 0; i < 15; ++i)
    {
        if(name1[i] != name2[i])
        {
            retVal ++;
            i = 15;
        }
        else if((name1[i]=='\0')||(name2[i]=='\0'))
            {
                if(name1[i]!=name2[i])
                {
                    retVal ++;
                    for (j = 0; j< i; ++j)
                    {
                        printf("%i|%i\t", (int)name1[j], (int)name2[j]);
                    }
                    printf("\n");
                }
                i = 15;
            }

    }
    return retVal;
}

int addNode(char name[15], NodeData* nodeData)
{
    /*printf("Adding: '%s'\n", name);*/
    int nodeid = -1, i;

    for ( i = 0; i <= nodeData->numNodes; ++i)
    {
        
        if (nodeData->nodeList[i].name[0] == name[0])
        {
            nodeid = i;
            i += nodeData->numNodes;
        }

    }


    if ( nodeid == -1)
    {
        nodeData->numNodes ++;
        strcpy(nodeData->nodeList[nodeData->numNodes].name, name);
        nodeData->nodeList[nodeData->numNodes].numEdges = -1;
        nodeData->nodeList[nodeData->numNodes].shortestCost = -1;
        nodeid = nodeData->numNodes;
    }

    return nodeid;
}

void readNode(FILE** file, char nodeName[15])
{
    int ch, i=0, error=1;
    char nextChar;

    do
    {
        ch =  fgetc(*file);
        if((ch!=-1)&&( i<10 ))
        {
            nextChar = (char)ch;
            nodeName[i] = nextChar;
            i++;
        }
        else
        {
            perror("error in file i/o:");
        }
    }while(( error==1 )&&( feof(*file)==0 )&&( nextChar!=' ' ));
    nodeName[i] = '\0';
}

void readCost(FILE** file,int* cost)
{
    int error;

    error = fscanf( *file, "%i\n", cost );
    if ( error==-1 )
    {
        perror("error: cost couldn't be read in");
    }
}