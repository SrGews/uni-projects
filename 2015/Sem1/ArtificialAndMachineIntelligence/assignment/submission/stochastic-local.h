#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

typedef struct 
{
	int x;
	int y;
}Coords;

typedef struct GridValue
{
	int xVal;
	int yVal;
	int zVal;
}GridValue;

typedef struct 
{
	int xRange;
	int yRange;
}GraphRange;

typedef struct Graph
{
	GridValue gridValues[100][100];
	GraphRange graphRange;
}Graph;

Coords stochHillClimb(Graph graph);
void readFile(char** argv, Graph* graph);
void readGridVal(FILE** file, GridValue* gridVal);
