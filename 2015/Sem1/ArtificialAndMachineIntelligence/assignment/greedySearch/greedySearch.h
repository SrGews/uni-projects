#include <stdlib.h>
#include <string.h>
#include <stdio.h>


typedef struct 
{
	int nodeidList[200];
	int length;
}PathTaken;

typedef struct
{
	int nodeid;
	int cost;
}EdgeNode;

typedef struct 
{
	EdgeNode edgeNodes[20];
	int numEdges;
	int heur;
	char name[15];
}Node;

typedef struct 
{
	Node nodeList[200];
	int numNodes;
}NodeData;

void greedySearch(char** argv, NodeData *nodeData);
int greedy(int currNode, int destNode, NodeData *nodeData, PathTaken *PathTaken);
void sort(NodeData* nodeData, int node);
void readOut(PathTaken *pathTaken, NodeData *nodeData);
void readHeur(char* argv, NodeData* hhnodeData);
int compareNames(char name1[15], char name2[15]);
int addNode(char name[15], NodeData* nodeData);
void readNodes(char* argv, NodeData* nodeData);
void readFile(char** argv, NodeData* nodeData);
void readCost(FILE** file,int* cost);
void readNode(FILE** file, char nodeName[15]);
