#include "nodes.h"

Node* makeNode(char* nodeName,Node* nodeList,int sizeList);
int getNode(char* nodeName, Node* nodeList, int sizeList);
void order(Node** edges, int* costs, int numEdges);
void greedySearch(int startNode,int destNode,Node* nodeList);
int greedy(int currNode,int destNode,Node* nodeList);
void* readGreedyFile(char** argv,int numNodes, Node* nodeList);
void readHeuristic(FILE** file, Node* nodeList, int numNodes);
void freeNodes(int numNodes, Node* nodeList);
void* upDateNodeList(char *nodeA, char* nodeB, int cost, Node* nodeList, int* sizeList, int* numNodes);void* readFile(char** argv, int* numNodes, Node* nodeList);
void readNode(FILE** file, char* nodeName);
void readCost(FILE** file,int* cost);
