#include <stdlib.h>
#include <string.h>
#include <stdio.h>




typedef struct Node
{
	char name[11];
	int numEdges;
	struct Node* edgeNode[10];
	int edgeCost[10];
	int heuristic;
}Node;