/***********************************************************************************/
/*                                                                                 */
/*FILE:         Adder.c                                                            */
/*AUTHOR:       Angus Campbell                                                     */
/*STUDENT ID:   17163042                                                           */
/*UNIT:         Operating Systems                                                  */
/*LAST MOD:     09/05/2015                                                         */
/*                                                                                 */
/***********************************************************************************/

#include "greedySearchMain.h"



int main( int argc, char **argv )
{
    Node nodeList[100];
    int numNodes = 100,i,j, startNode, destNode;
    char start[11], dest[11];

    /*nodeList = (Node*)calloc(2,sizeof(Node));
*/
    if( (argc!=5) && (argc!=3) )
    {
        perror( "ERROR: Wrong number of inputs" );
    }
    else
    {
        readFile(argv, &numNodes, nodeList);
        readGreedyFile(argv, numNodes, nodeList);
    }    

    if(argc == 5)
    {
        strcpy(start,argv[3]);
        strcpy(dest,argv[4]);
    }
    /*printf("startNode = %s;\n", startNode);*/

    startNode = getNode(start, nodeList, numNodes);
    destNode = getNode(dest, nodeList, numNodes);

    for ( i = 0; i < numNodes; ++i)
    {
        printf("node %s;\t\theuristic = %i\n",nodeList[i].name, nodeList[i].heuristic );
        for (j=0; j<nodeList[i].numEdges; j++)
        {
            printf("\t\tconnected to: %s ; cost = %i\n", nodeList[i].edgeNode[j]->name, nodeList[i].edgeCost[j]);
        }
    }

    greedySearch(startNode, destNode, nodeList);

    
/*
    freeNodes(numNodes, nodeList);
*/
    return 0;
}

void greedySearch(int startNode,int destNode,Node* nodeList)
{
    greedy(startNode, destNode, nodeList);

    printf("Greedy search is complete; all paths done.\n");
}

int greedy(int currNode,int destNode,Node* nodeList)
{
    int i, complete = 0;
    printf("sadfsadasgdsfgd\t\t%i\n", currNode);
    printf("%s\n",nodeList[currNode].name);
    if (strcmp(nodeList[currNode].name, nodeList[destNode].name)==0)
    {
        printf("The path from goal to start:\n");
        /*base case --> path found and at dest*/
    }
    else
    {
        order(nodeList[currNode].edgeNode, nodeList[currNode].edgeCost, nodeList[currNode].numEdges);
        for ( i = 0; i < nodeList[currNode].numEdges; ++i)
        {
            complete = greedy(i, destNode, nodeList);
        }
        
    }

    printf("%s - ", nodeList[currNode].name);

return complete;
}

void order(Node** edges, int* costs, int numEdges)
{
    int i, k, tempC;
    Node* tempN;
    for(i=0; i<numEdges; i++)
    {
        for(k=i; k<numEdges; k++)
        {
            if (costs[k]<costs[i])
            {
                tempC = costs[k];
                costs[k] = costs[i];
                costs[i] = tempC;

                tempN = edges[k];
                edges[k] = edges[i];
                edges[i] = tempN;
            }
        }
    }

}



int getNode(char* nodeName, Node* nodeList, int sizeList)
{
    int retNode;
    int i=0;

    while((i<sizeList))
    {
        if(strcmp(nodeList[i].name,nodeName)==0)
        {
            retNode = i;
            i = sizeList;
        }
        else
        {
            i++;
        }
    }
printf("%i\n", retNode);
    return retNode;
}

void freeNodes(int numNodes, Node* nodeList)
{
    int i;

    for ( i = 0; i < numNodes; ++i)
    {

        free(nodeList[i].edgeNode);
        free(nodeList[i].edgeCost);
        
    }
    free(nodeList);
}

void* readGreedyFile(char** argv,int numNodes, Node* nodeList)
{
    FILE *file;
 
    file = fopen( argv[2], "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv[1] );
        perror( "ERROR: Input file not valid" );
    }
    else
    {            
        do
        {
            readHeuristic(&file, nodeList, numNodes);
            
        }while( ( feof( file) == 0 ) );        
    }

    fclose( file );

    return nodeList;
}

void readHeuristic(FILE** file, Node* nodeList, int numNodes)
{
    int ch, heuristic, i = 0;
    char node[11], nextChar;

    do
    {
        ch =  fgetc(*file);
        if((ch!=-1)&&( i<10 ))
        {
            nextChar = (char)ch;
            node[i] = nextChar;
            i++;
        }
        else
        {
            perror("error in file i/o:");
        }
    }while(( feof(*file)==0 )&&( nextChar!=' ' ));
    node[i] = '\0';

    fscanf(*file, "%i\n",&heuristic);

    i = 0;

    while((strcmp(node,nodeList[i].name)!=0) && ( i < numNodes ))
    {
        /*printf("Node: %s\t\t%i\t\tinNode: %s \n",nodeList[i].name, i, node);*/
        i++;
    }
    nodeList[i].heuristic = heuristic;

    /*printf("Node: %s;\t\theuristic: %i\n",nodeList[i].name, nodeList[i].heuristic );*/
}

void* readFile(char** argv, int* numNodes, Node* nodeList)
{
FILE *file;
char nodeA[11], nodeB[11];
int i, cost, sizeList;

    sizeList = 0;
    *numNodes = 100;
    file = fopen( argv[1], "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv[1] );
        perror( "ERROR: Input file not valid" );
    }
    else
    {   
    /*         
        nodeList = (Node*)calloc(2,sizeof(Node));
    */    i=0;
        
        do
        {

            readNode(&file, nodeA);
            readNode(&file, nodeB);
            readCost(&file, &cost);
            
            /*printf("%s\t\t%s\t\t%i\n",nodeA, nodeB, cost );*/
            


            nodeList = (Node*)upDateNodeList(nodeA, nodeB, cost, nodeList, &sizeList, numNodes);
            /*nodeList = (Node*)upDateNodeList(nodeA, nodeB, cost, nodeList, &sizeList, numNodes);
*/

        }while( ( feof( file) == 0 ) );

        /*nodeList = (Node*) realloc(nodeList, ((*numNodes)* sizeof(Node)));*/
        
    }

    *numNodes = sizeList;

    fclose( file );

    return nodeList;
}

void* upDateNodeList(char *nodeA, char* nodeB, int cost, Node* nodeList, int* sizeList, int* numNodes)
{
    int numFound=0, i=0, nA, nB;

    nA = -1;
    nB = -1;

    while(((numFound)<2) && ( i < *sizeList ))
    {

        if ((strcmp(nodeList[i].name, nodeA)==0) && (nA == -1))
        {
            nA = i;
            numFound ++;
            /*printf("found %s ==  %s\n", nodeList[i].name, nodeA);*/

        }
        else if((strcmp(nodeList[i].name, nodeB)==0)&& (nB == -1))
        {
            nB = i;
            numFound ++;
            /*printf("found %s ==  %s\n", nodeList[i].name, nodeB);*/
        }
        i++;
    }

    /*printf("numFound = %i;\n", numFound);*/


    if(numFound<2)
    {
        if ((*sizeList+2-numFound) > (*numNodes))
        {   
            /*
            nodeList = (Node*)realloc( nodeList ,((*sizeList+100) * sizeof(Node) ));
            *numNodes += 100;
            printf("Resizing nodeList\n");*/
        }

        if (nA==-1)
        {
            nA = *sizeList-numFound;
            strcpy(nodeList[nA].name, nodeA);
            /*nodeList[*sizeList-numFound].edgeNode = (Node**)calloc(1,sizeof(Node*));*/
            nodeList[nA].numEdges = 0;
            nodeList[nA].heuristic = 0;
            *sizeList +=1 ;
        }
        if (nB==-1)
        {
            
            nB = *sizeList-numFound;
            strcpy(nodeList[nB].name, nodeB);
            /*nodeList[*sizeList-numFound].edgeNode = (Node**)calloc(1,sizeof(Node*));*/
            nodeList[nB].numEdges = 0;
            nodeList[nB].heuristic = 0;
            *sizeList +=1 ;
        }
    }
    /* printf("sizeList = %i;\n", *sizeList);*/

    nodeList[nA].numEdges ++;
    nodeList[nB].numEdges ++;
/*
    nodeList[nA].edgeNode = (Node**)realloc(nodeList[nA].edgeNode, (nodeList[nA].numEdges*sizeof(Node*)));
    nodeList[nB].edgeNode = (Node**)realloc(nodeList[nB].edgeNode, (nodeList[nB].numEdges*sizeof(Node*)));
*/
    nodeList[nA].edgeNode[nodeList[nA].numEdges-1] = &nodeList[nB];
    nodeList[nB].edgeNode[nodeList[nB].numEdges-1] = &nodeList[nA];
/*
    printf("nA: %s\t\tedge: %s;\t\tedge number = %i;\n", nodeList[nA].name, nodeList[nA].edgeNode[nodeList[nA].numEdges-1]->name, nodeList[nA].numEdges );
    printf("nB: %s\t\tedge: %s;\t\tedge number = %i;\n", nodeList[nB].name, nodeList[nB].edgeNode[nodeList[nB].numEdges-1]->name, nodeList[nB].numEdges );*/
/*
    nodeList[nA].edgeCost = (int*)realloc(nodeList[nA].edgeCost, ((nodeList[nA].numEdges)*(sizeof(int))));
    nodeList[nB].edgeCost = (int*)realloc(nodeList[nB].edgeCost, ((nodeList[nB].numEdges)*(sizeof(int))));
*/
    nodeList[nA].edgeCost[nodeList[nA].numEdges-1] = cost;
    nodeList[nB].edgeCost[nodeList[nB].numEdges-1] = cost;

/*
    nodeList = (Node*)realloc(nodeList,((*sizeList)*sizeof(Node)));
    printf("nC: %s\t\tedge: %s;\t\tedge number = %i;\n", nodeList[nA].name, nodeList[nA].edgeNode[nodeList[nA].numEdges-1]->name, nodeList[nA].numEdges );
*/
    /*printf("The things:\nNode A = %s: edge1 = %s\t\tNode B = %s: edge1 = %s\n", nodeList[nA].name, nodeList[nA].edgeNode[0]->name, nodeList[nB].name, nodeList[nB].edgeNode[0]->name);
*/

    return nodeList;
}


Node* makeNode(char* nodeName,Node* nodeList,int sizeList)
{
    Node* retNode;

    retNode = &nodeList[sizeList];
    strcpy(retNode->name, nodeName);
    retNode->numEdges = 0;
    retNode->heuristic = 0;

    return retNode;
}

void readNode(FILE** file, char* nodeName)
{
    int ch, i=0, error=1;
    char nextChar;

    do
    {
        ch =  fgetc(*file);
        if((ch!=-1)&&( i<10 ))
        {
            nextChar = (char)ch;
            nodeName[i] = nextChar;
            i++;
        }
        else
        {
            perror("error in file i/o:");
        }
    }while(( error==1 )&&( feof(*file)==0 )&&( nextChar!=' ' ));
    nodeName[i] = '\0';

/*    if ( i>1 )
    {
        addedNode = 1;
    }
    else
    {
        addedNode = 0;
    }

    return addedNode;
*/
}

void readCost(FILE** file,int* cost)
{
    int error;

    error = fscanf( *file, "%i\n", cost );
    if ( error==-1 )
    {
        perror("error: cost couldn't be read in");
    }
}