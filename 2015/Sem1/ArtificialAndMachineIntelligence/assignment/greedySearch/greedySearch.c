/****************************************************************************/
/*                                                                          */
/*FILE:         greedySearch.c                                              */
/*AUTHOR:       Angus Campbell                                              */
/*STUDENT ID:   17163042                                                    */
/*UNIT:         AMI                                                         */
/*LAST MOD:     15/05/2015                                                  */
/*                                                                          */
/****************************************************************************/



#include "greedySearch.h"

/*This is an implementation of the heuristic based greedy search.
**The program will read in 2 filenames from the user, a start node, and an
**end node. The program will read in a graph of nodes and their heuristic
**values.
**
**This is a recursive greedy which doesn't keep track which node it came from
**so it is prone to looping between two nodes if they are eachother's best
**local options.
*/


/*main reads in the file and calls greedySearch for the data read in
*/

int main( int argc, char **argv )
{
	NodeData nodeData;

	readFile(argv, &nodeData);

    greedySearch(argv,&nodeData);

	return 0;
}


/*greedySearch starts the search function. It finds the nodeID for the input
**start and destination nodes, and initialises pathTaken which keeps track of
**the path the greedy search has taken.
*/
void greedySearch(char** argv, NodeData *nodeData)
{
    PathTaken pathTaken;
    int startNode, destNode;

    startNode = addNode(argv[3], nodeData);
    destNode = addNode(argv[4], nodeData);

    pathTaken.length = 0;

    greedy(startNode, destNode, nodeData, &pathTaken);

    printf("Greedy has finished\n");
}



/*greedy is the main part of the greedy search.
**
**greedy first checks if the current node is the goal node. If so it prompts
**the user for input as to whether it should contine searching for paths.
**
**If it is not a goal node greedy will sort the edges in order of increasing
**heuristic value and will call greedy for each node. Due to the nature of
**greedy, this includes the previous visited node.
*/

int greedy(int currNode, int destNode, NodeData *nodeData, 
    PathTaken *pathTaken)
{
    int finishState = 0, i=0;
    pathTaken->nodeidList[pathTaken->length] = currNode;
    pathTaken->length ++;
    if (currNode==destNode)
    {
        printf("Solution found:");
        readOut(pathTaken, nodeData);
        printf("Continue searching? '1' for no, '0' for yes\n" );
        scanf(" %i",&finishState);
        pathTaken->length --;
    }
    else
    {

        sort(nodeData, currNode);
        while ((i<=nodeData->nodeList[currNode].numEdges) && (finishState==0))
        {
            finishState = greedy(
                nodeData->nodeList[currNode].edgeNodes[i].nodeid, 
                destNode, nodeData, pathTaken);
            i++;
        }
    }

    return finishState;
}

/*readOut is called when a solution has been found.
**readOut prints the path taken to reach the solution to the screen
*/
void readOut(PathTaken *pathTaken, NodeData *nodeData)
{
    int i;

    printf("%s", nodeData->nodeList[pathTaken->nodeidList[0]].name);
    for(i=1; i<pathTaken->length; i++)
    {
        printf(" -> %s", nodeData->nodeList[pathTaken->nodeidList[i]].name);
    }
    printf("\n");
}


/*sort sorts the edges of a node in order if increasing heuristic value.
**
*/
void sort(NodeData* nodeData, int node)
{
    int i, j, temp;

    for ( i = 0; i <= nodeData->nodeList[node].numEdges; ++i)
    {
        for ( j = 0; j < i; ++j)
        {
            if ((nodeData->nodeList[nodeData->nodeList[node].
                edgeNodes[i].nodeid].heur) < 
                (nodeData->nodeList[nodeData->nodeList[node].
                    edgeNodes[j].nodeid].heur))
            {
             temp = nodeData->nodeList[node].edgeNodes[i].nodeid;

             nodeData->nodeList[node].
             edgeNodes[i].nodeid = nodeData->nodeList[node].
             edgeNodes[j].nodeid;

             nodeData->nodeList[node].edgeNodes[j].nodeid = temp;   
            }
        }
    }
}


/*readFile calls the reading methods responsible for reading in the nodes and
**their edges, and the heuristic values for each node.
*/
void readFile(char** argv, NodeData* nodeData)
{
	readNodes(argv[1], nodeData);
    readHeur(argv[2], nodeData);
}


/*readHeur reads in the heuristic values for each node
*/
void readHeur(char* argv, NodeData* nodeData)
{
    FILE *file;
    int cost, idN1;
    char name1[15];

    file = fopen( argv, "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv );
        perror( "ERROR: Input file not valid" );
    }
    else
    {   
        do
        {
            readNode(&file,name1);
            readCost(&file, &cost);

            idN1 = addNode(name1, nodeData);

            nodeData->nodeList[idN1].heur = cost;

        }while( ( feof( file) == 0 ) );
    }

    fclose( file );
}



/*readNodes reads in the nodes, their neighbours, and the cost to travel to
**a neighbour
*/
void readNodes(char* argv, NodeData* nodeData)
{
    FILE *file;
    int i = 0, cost, idN1, idN2;
    char name1[15], name2[15];

    file = fopen( argv, "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv );
        perror( "ERROR: Input file not valid" );
    }
    else
    {   
        nodeData->numNodes = -1;

        do
        {
            readNode(&file,name1);
            readNode(&file,name2);
            readCost(&file, &cost);

            idN1 = addNode(name1, nodeData);
            idN2 = addNode(name2, nodeData);

            nodeData->nodeList[idN1].numEdges ++;
            nodeData->nodeList[idN1].edgeNodes[nodeData->nodeList[idN1].
                numEdges].nodeid = idN2;
            nodeData->nodeList[idN1].edgeNodes[nodeData->nodeList[idN1].
                numEdges].cost = cost;

            nodeData->nodeList[idN2].numEdges ++;
            nodeData->nodeList[idN2].edgeNodes[nodeData->nodeList[idN2].
                numEdges].nodeid = idN1;
            nodeData->nodeList[idN2].edgeNodes[nodeData->nodeList[idN2].
                numEdges].cost = cost;

        	i++;           

        }while( ( feof( file) == 0 ) );
    }
    fclose( file );
}


/*addNode either adds a node to the list of nodes, or it finds the 
**corresponding node location of the existing node. It does this by comparing
**the node name to the input name.
**
**addNode will return the index location of the node in the array nodeData
*/
int addNode(char name[15], NodeData* nodeData)
{
    int nodeid = -1, i;

    for ( i = 0; i <= nodeData->numNodes; ++i)
    {
        if (strcmp(nodeData->nodeList[i].name, name)==0)
        {
            nodeid = i;
            i += nodeData->numNodes;
        }
    }


    if ( nodeid == -1)
    {
        nodeData->numNodes ++;

        strcpy(nodeData->nodeList[nodeData->numNodes].name, name);

        nodeData->nodeList[nodeData->numNodes].numEdges = -1;
        nodeData->nodeList[nodeData->numNodes].heur = 0;

        nodeid = nodeData->numNodes;
    }
    return nodeid;
}


/*ReadNode reads in the next word from the file and returns it
*/
void readNode(FILE** file, char nodeName[15])
{
    int ch, i=0, error=1;
    char nextChar;

    do
    {
        ch =  fgetc(*file);
        if((ch!=-1)&&( i<10 ))
        {
            nextChar = (char)ch;
            nodeName[i] = nextChar;
            i++;
        }
        else
        {
            perror("error in file i/o:");
        }
    }while(( error==1 )&&( feof(*file)==0 )&&( nextChar!=' ' ));
    nodeName[i-1] = '\0';
}

/*readCost reads in the next int from the file and returns it
*/
void readCost(FILE** file,int* cost)
{
    int error;

    error = fscanf( *file, "%i\n", cost );
    if ( error==-1 )
    {
        perror("error: cost couldn't be read in");
    }
}
