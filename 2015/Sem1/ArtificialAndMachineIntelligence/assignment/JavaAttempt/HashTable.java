import io.*;
import java.io.*;
import java.util.*;

public class HashTable
{
	private GreedyHashEntry[] table;
	private int numEntries = 0;
	private int tableSize = 193;

	public void HashTable()
	{
			table = new GreedyHashEntry[tableSize];
	}

	public void addEntry(String inKey1, String inKey2, int dist)
	{

		addTheEntry(inKey1, inKey2, dist);
		addTheEntry(inKey2, inKey1, dist);
	}

	public void addTheEntry(String inKey1, String inKey2, int inDist)
	{
		int hashIdx;

		hashIdx = getHash(inKey1);
		
		table[hashIdx].setNodeKey(inKey1);
		table[hashIdx].addEdge(inKey2, inDist);

	}

	public int getHash(String inKey)
	{
		int hashIdx;

		hashIdx = hash(inKey);
		if ((table != null))
		{
			if (table[hashIdx]!=null)
				{
				while(table[hashIdx].getState()==1)
				{
					hashIdx = reHash(hashIdx);
				}
			}
			else
			{
				System.out.println("Error: HashTable: getHash()");
			}
		}
		return hashIdx;
	}

	public int hash(String inKey)
	{
		int hashIdx=0,a = 63689,b = 378551, jj;
		
		for (int ii = 1; ii <= tableSize; ii++)
		{
		jj = 0;
		hashIdx = (hashIdx * a) + inKey.hashCode();
		a*=b;
		}
	return hashIdx % tableSize;
	}

	public int reHash(int idx)
	{
		int hashIdx;

		hashIdx = (idx + 1)%tableSize;

		return hashIdx;
	}

	public GreedyHashEntry getTableEntry(int hashIdx)
	{
		GreedyHashEntry hashEntry;

		hashEntry = table[hashIdx];

		return hashEntry;
	}
}
