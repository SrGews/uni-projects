import io.*;
import java.io.*;
import java.util.*;

public class NextNode
{
	private NextNode nNode;
	private String name;
	private int dist;
	private int heuristic;

	public void NextNode()
	{
		name = "";
		nNode = null;
		dist = 0;
		heuristic = 0;
	}

	public void NextNode(NextNode inNode)
	{
		name = inNode.getName();
		nNode = null;
		dist = inNode.getDist();
		heuristic = inNode.getHeuristic();
	}

	public void setVals(String inName, int inDist, int inHeuristic)
	{
		name = inName;
		dist = inDist;
		heuristic = inHeuristic;
	}

	public void setNextNode(NextNode inNextNode)
	{
		nNode = new NextNode();
		nNode.setVals(inNextNode.getName(),inNextNode.getDist(),inNextNode.getHeuristic());
	}

	public String getName()
	{
		return name;
	}
	public int getDist()
	{
		return dist;
	}
	public int getHeuristic()
	{
		return heuristic;
	}
	public NextNode getNNode()
	{
		return nNode;
	}
}