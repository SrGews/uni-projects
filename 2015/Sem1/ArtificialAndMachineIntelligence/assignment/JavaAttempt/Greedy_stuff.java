import io.*;
import java.io.*;
import java.util.*;

public class Greedy_stuff
{

	private int entriesHash;
	private HashTable hashTable;
	private String startNode, endNode;
	private String inputFile, outputFile;

	public void Greedy_stuff()
	{
		entriesHash = 0;
		hashTable = new HashTable();
		startNode = ConsoleInput.readLine("Enter a start node");
		endNode = ConsoleInput.readLine("Enter an end node");
		inputFile = "a";
		outputFile = "b";
	}

	public void loadNodeDist()
	{
		int error = 0;

		FileInputStream fileStrm = null;
		InputStreamReader rdr;
		BufferedReader bufRdr;
		int lineNum = 0;
		String line;


		System.out.println(" "+ inputFile);

			inputFile = ConsoleInput.readLine("Enter an input filename for the distances");
			outputFile = ConsoleInput.readLine("Enter an output filename");

		try
		{
			fileStrm = new FileInputStream( inputFile );
			rdr = new InputStreamReader( fileStrm );
			bufRdr = new BufferedReader( rdr );

			line = bufRdr.readLine();

			while((line!=null) && (error == 0))
			{
				lineNum ++;
				error = processLine( line );

				line = bufRdr.readLine();
			}




		}catch(IOException ex2)
		{
			if ( fileStrm != null )
			{
				try
				{
					fileStrm.close();
				}
				catch ( IOException ex3 )
				{

				}

				System.out.println("Error in file processing: " + 
					ex2.getMessage());
			}
		}
	}

	public void loadHeuristics()
	{

	}

	public void savePaths()
	{

	}

	public int processLine( String csvRow )
	{
		int error = 0;
		String node1, node2;
		int dist;
		StringTokenizer strTok;

		strTok = new StringTokenizer( csvRow, " ");
		try
		{
			node1 = strTok.nextToken(  );
			node2 = strTok.nextToken(  );
			dist = Integer.parseInt( strTok.nextToken(  ) );
		}
		catch(Exception e)
		{
			error = 1;
			throw new IllegalStateException("CSV row had invalid format" + e);
		}
		System.out.println("here we go  "+node1+"   "+node2+"    "+dist);

		hashTable.addTheEntry(node1, node2, dist);
		System.out.println("here we go"+node1+"   "+node2+"    "+dist);

		return error;
	}

}