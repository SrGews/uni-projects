import io.*;
import java.io.*;
import java.util.*;


public class GreedyHashEntry
{
	private String nodeKey;
	private int heuristic;
	private int state;
	private int numEdges;
	private Edges edges;

	public void GreedyHashEntry()
	{
		nodeKey = "";
		heuristic = 0;
		state = 0;
		numEdges = 0;
		edges = null;
	}

	public String getNodeKey()
	{
		return nodeKey;
	}
	public int getHeuristic()
	{
		return heuristic;
	}
	public int getState()
	{
		return state;
	}
	public int getNumEdges()
	{
		return numEdges;
	}

	public void setEntry(String inKey, int inHeuristic)
	{
		if((state == 0) || (state == -1) )
		{
			nodeKey = inKey;
			heuristic = inHeuristic;
			state = 1;
		}
		else if (inHeuristic != heuristic)
		{
			System.out.println("There are multiple heuristic values for " + nodeKey);
		}
	}

	public void setNodeKey(String inKey)
	{
		nodeKey = inKey;
		state = 1;
	}

	public void addEdge(String inEdge, int inDist)
	{
		if (edges == null)
		{
			edges = new Edges();
			edges.setKey(inEdge);
			edges.setDist(inDist);
		}
		else if(inEdge.equals(edges.getKey())==false)
		{
			numEdges += edges.addEdge(inEdge, inDist);
		}
	}

	public Edges getEdge(int edgeNum)
	{
		Edges edge;

		if (numEdges>0)
		{
			edgeNum --;
			edge = edges.getEdge(edgeNum);
		}
		else
		{
			edge = null;
		}

		return edge;
	}

	public boolean isEmpty()
	{
		boolean retBool = false;
		if (state < 1)
		{
			retBool = true;
		}
		return retBool;
	}
}