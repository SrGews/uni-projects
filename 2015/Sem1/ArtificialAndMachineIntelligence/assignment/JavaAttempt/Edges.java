import io.*;
import java.io.*;
import java.util.*;


public class Edges
{
	private String edgeKey;
	private int dist;
	private Edges nextEdge;

	public void Edges()
	{
		edgeKey = "";
		nextEdge = null;
		dist = 0;
	}

	public void Edges(String inKey)
	{
		edgeKey = inKey;
		nextEdge = null;
		dist = 0;
	}

	public void setNextEdge(Edges inEdge)
	{
		String inKey;

		inKey = inEdge.getKey();

		nextEdge = new Edges();
		nextEdge.setKey(inEdge.getKey());
		nextEdge.setDist(inEdge.getDist());
	}


	public void setBoth(String inKey, int inDist)
	{
		setKey(inKey);
		setDist(inDist);
	}
	public String getKey()
	{
		return edgeKey;
	}
	public void setKey(String inKey)
	{
		edgeKey = inKey;
	}
	public int getDist()
	{
		return dist;
	}
	public void setDist(int inDist)
	{
		dist = inDist;
	}

	public int addEdge(String inEdge, int inDist)
	{
		int retVal = 0;
		if (nextEdge != null)
		{
			nextEdge.addEdge(inEdge, inDist);
		}
		else 
		{
			nextEdge = new Edges();
			nextEdge.setKey(inEdge);
			nextEdge.setDist(inDist);
			retVal ++;
		}
		return retVal;
	}


	public Edges getEdge(int edgeNum)
	{
		Edges foundEdge;

		if (edgeNum>1)
		{
			edgeNum --;
			foundEdge = nextEdge.getEdge(edgeNum);
		}
		else
		{
			foundEdge = nextEdge;
		}

		return foundEdge;
	}
}