import io.*;
import java.io.*;
import java.util.*;


public class ListHead
{
	private NextNode head;
	private NextNode tail;
	private String startNode;
	private int heuristic;

	public void ListHead()
	{
		head = null;
		tail = null;
		startNode = "";
		heuristic = 0;
	}

	public void ListHead(String inStartNode, int inHeuristic)
	{
		head = null;
		tail = null;
		startNode = inStartNode;
		heuristic = inHeuristic;
	}

	public String getStartNode()
	{
		return startNode;
	}
	public int getHeuristic()
	{
		return heuristic;
	}

	public void addNode(NextNode inNode)
	{
		if(head == null)
		{
			head = new NextNode();
			head.setNextNode(inNode);
			tail = head;
		}
		else if ((tail == null)&&(tail.getNNode()!=null))
		{
			tail.setNextNode(inNode);
			tail = new NextNode();
			tail.setNextNode(inNode);
		}
	}
}