#include <stdlib.h>
#include <string.h>
#include <stdio.h>


typedef struct 
{
	int totalCost;
	int path[30];
	int nodesVisited;
}PathWay;

typedef struct 
{
	PathWay pathWays[20];
	int numPathways;
}ListPaths;

typedef struct
{
	int nodeid;
	int cost;
}EdgeNode;

typedef struct 
{
	EdgeNode edgeNodes[20];
	int numEdges;
	int shortestCost;
	char name[15];
}Node;

typedef struct 
{
	Node nodeList[200];
	int numNodes;
}NodeData;


void showPaths(ListPaths *listPaths, NodeData *nodeData);
void branchbSearch(char** argv, NodeData *nodeData);
void branchb(ListPaths *listPaths, NodeData *nodeData, int destNode);
void expand(ListPaths *listPaths, NodeData *nodeData);
void sort(ListPaths *listPaths);
int compareNames(char name1[15], char name2[15]);
int addNode(char name[15], NodeData* nodeData);
void readNodes(char* argv, NodeData* nodeData);
void readFile(char** argv, NodeData* nodeData);
void readCost(FILE** file,int* cost);
void readNode(FILE** file, char nodeName[15]);
