#include "greedySearch.h"

int main( int argc, char **argv )
{
	NodeData nodeData;
    int i,j;


	readFile(argv, &nodeData);



    for ( i = 0; i <= nodeData.numNodes; ++i)
    {
        printf("The Node :%s\t\thas the edges:\n",nodeData.nodeList[i].name );
        for ( j = 0; j <= nodeData.nodeList[i].numEdges; ++j)
        {
            printf("\t\t\tEdge number %i;\t\tname; %s;\t\tcost;%i\n", j, nodeData.nodeList[nodeData.nodeList[i].edgeNodes[j].nodeid].name,nodeData.nodeList[i].edgeNodes[j].cost);
        }

    }



	return 0;
}


void readFile(char** argv, NodeData* nodeData)
{

	readNodes(argv[1], nodeData);

}


void readNodes(char* argv, NodeData* nodeData)
{
    FILE *file;
    int i = 0, cost, idN1, idN2;
    char name1[15], name2[15];

    file = fopen( argv, "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv );
        perror( "ERROR: Input file not valid" );
    }
    else
    {   
        nodeData->numNodes = -1;

        /*fscanf(file, "%s %s %i", name1, name2, &cost);*/
        readNode(&file,name1);
        readNode(&file,name2);
        readCost(&file, &cost);

        do
        {

            idN1 = addNode(name1, nodeData);
            idN2 = addNode(name2, nodeData);

            nodeData->nodeList[idN1].numEdges ++;
            nodeData->nodeList[idN1].edgeNodes[nodeData->nodeList[idN1].numEdges].nodeid = idN2;
            nodeData->nodeList[idN1].edgeNodes[nodeData->nodeList[idN1].numEdges].cost = cost;

            nodeData->nodeList[idN2].numEdges ++;
            nodeData->nodeList[idN2].edgeNodes[nodeData->nodeList[idN2].numEdges].nodeid = idN1;
            nodeData->nodeList[idN2].edgeNodes[nodeData->nodeList[idN2].numEdges].cost = cost;

        	i++;
            /*printf("%i %i %s %s %i\n", idN1, idN2, name1, name2, cost);
*/
            readNode(&file,name1);
            readNode(&file,name2);
            readCost(&file, &cost);

            

        }while( ( feof( file) == 0 ) );

    }

    fclose( file );
}

int compareNames(char name1[15], char name2[15])
{
    int i, j,retVal = 0;

    for (i = 0; i < 15; ++i)
    {
        if(name1[i] != name2[i])
        {
            retVal ++;
            i = 15;
        }
        else if((name1[i]=='\0')||(name2[i]=='\0'))
            {
                if(name1[i]!=name2[i])
                {
                    retVal ++;
                    for (j = 0; j< i; ++j)
                    {
                        printf("%i|%i\t", (int)name1[j], (int)name2[j]);
                    }
                    printf("\n");
                }
                i = 15;
            }

    }
    return retVal;
}

int addNode(char name[15], NodeData* nodeData)
{
    /*printf("Adding: '%s'\n", name);*/
    int nodeid = -1, i;

    for ( i = 0; i <= nodeData->numNodes; ++i)
    {
        /*printf("Comparing: %s and %s;\tresult = %i\n", nodeData->nodeList[i].name, name, strcmp(nodeData->nodeList[i].name, name));*/
        
        if (nodeData->nodeList[i].name[0] == name[0])
        {
            nodeid = i;
            /*printf("Comparing: %s and %s;\tresult = %i\n", nodeData->nodeList[i].name, name, nodeid);*/
            i += nodeData->numNodes;
        }
        /*else
        {
            printf("Comparing: %s and %s;\tresult = %i\n", nodeData->nodeList[i].name, name, nodeid);
        }*/
    }


    if ( nodeid == -1)
    {
        /*printf("'%s'\n", name);*/
        nodeData->numNodes ++;
        /*printf("Making node: %s\t\tat id: %i\n", name,nodeData->numNodes);*/
        strcpy(nodeData->nodeList[nodeData->numNodes].name, name);
        nodeData->nodeList[nodeData->numNodes].numEdges = -1;
        nodeid = nodeData->numNodes;
    }

    return nodeid;
}

void readNode(FILE** file, char nodeName[15])
{
    int ch, i=0, error=1;
    char nextChar;

    do
    {
        ch =  fgetc(*file);
        if((ch!=-1)&&( i<10 ))
        {
            nextChar = (char)ch;
            nodeName[i] = nextChar;
            i++;
        }
        else
        {
            perror("error in file i/o:");
        }
    }while(( error==1 )&&( feof(*file)==0 )&&( nextChar!=' ' ));
    nodeName[i] = '\0';
}

void readCost(FILE** file,int* cost)
{
    int error;

    error = fscanf( *file, "%i\n", cost );
    if ( error==-1 )
    {
        perror("error: cost couldn't be read in");
    }
}