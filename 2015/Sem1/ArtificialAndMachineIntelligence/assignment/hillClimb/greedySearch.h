#include <stdlib.h>
#include <string.h>
#include <stdio.h>


typedef struct
{
	int nodeid;
	int cost;
}EdgeNode;

typedef struct 
{
	EdgeNode edgeNodes[20];
	int numEdges;
	char name[15];
}Node;

typedef struct 
{
	Node nodeList[200];
	int numNodes;
}NodeData;

int addNode(char name[15], NodeData* nodeData);
void readNodes(char* argv, NodeData* nodeData);
void readFile(char** argv, NodeData* nodeData);