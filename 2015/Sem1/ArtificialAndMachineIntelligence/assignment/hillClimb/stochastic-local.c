/****************************************************************************/
/*                                                                          */
/*FILE:         Adder.c                                                     */
/*AUTHOR:       Angus Campbell                                              */
/*STUDENT ID:   17163042                                                    */
/*UNIT:         Operating Systems                                           */
/*LAST MOD:     09/05/2015                                                  */
/*                                                                          */
/****************************************************************************/

#include "stochastic-local.h"

/*This program reads in a 3D graph from a file input by the user, and finds 
**the maximum z value using a stochastic local search
*/


/*Main checks the number of inputs, and calls the readFile function whic
**reads in the data from the file.
**
**stochHillClimb then performs the local search. Main then prints out the 
**result.
*/

int main( int argc, char **argv )
{
    Graph graph;
    Coords maxPoint;

    if( argc!=2 )
    {
        perror( "ERROR: Wrong number of inputs" );
    }
    else
    {
        readFile(argv, &graph);
    }    

        maxPoint = stochHillClimb(graph);

        printf("The max is z=%i at x=%i, y=%i\n",
        graph.gridValues[maxPoint.x][maxPoint.y].zVal,
        graph.gridValues[maxPoint.x][maxPoint.y].xVal, 
        graph.gridValues[maxPoint.x][maxPoint.y].yVal);

    return 0;
}


/*stochHillClimb generates a random starting location then performs a local
**search from that location. It can only make maxComps comparisons. This will
**stop it from looping forever on a plane or ridge.
**
**It will perform maxComps/10 restarts. The number of restarts is done 
**dynamically to account for varying sizes of data.
**
**Once the local search is complete, it will compare the maximum found against
**the previous maximum. If it is greater, it will keep the new max and
**discard the old.
*/
Coords stochHillClimb( Graph graph)
{
    Coords retVal;
    int maxComps, i, j, k, l, changed, x, y;
    time_t t;

    time(&t);
    srand((unsigned)t);

    l = 0;
    x = 0;
    y = 0;
    maxComps = graph.graphRange.xRange*graph.graphRange.yRange;

    for(i=1; i<(maxComps/10); i++)
    {
        x = rand()%graph.graphRange.xRange;
        y = rand()%graph.graphRange.yRange;
        do
        {
            changed = 0;
            for (j = -1; j <=1 ; j+=2)
            {
                for ( k = -1; k <=1 ; k+=2)
                {
                    if (( (x+k)>0) && ((y+j)>0) &&
                    ((x+k)<graph.graphRange.xRange) && 
                    ((y+j)<graph.graphRange.yRange))
                    {

                        if (graph.gridValues[x+k][y+j].zVal>=
                            graph.gridValues[x][y].zVal)
                        {
                            x+=k;
                            y+=j;
                            changed = 1;
                        }
                    }
                }
            }
            l++;
        }while((l<maxComps)&&(changed == 1));

        if(graph.gridValues[x][y].zVal>=
        graph.gridValues[retVal.x][retVal.y].zVal)
        {
            retVal.x = x;
            retVal.y = y;
        }
    }
    return retVal;
}



/*A standard file reading.
**The File must be aranged:
xVal yVal zVal
xVal yVal zVal

**or not all data will be read in.
**
*/
void readFile(char** argv, Graph* graph)
{
    FILE *file;
    int i=0, j=0, thisX, thisY, thisZ, lastX;

    file = fopen( argv[1], "r" );
    if( file == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv[1] );
        perror( "ERROR: Input file not valid" );
    }
    else
    {   
        fscanf(file, "%i %i %i\n", &thisX, &thisY, &thisZ);
        lastX = thisX;
        do
        {
            if (thisX != lastX)
            {
                j++;
                i = 0;
                /*printf("\n%i  %i\t", i,j);*/
            }

            graph->gridValues[i][j].xVal = thisX;
            graph->gridValues[i][j].yVal = thisY;
            graph->gridValues[i][j].zVal = thisZ;

            lastX = thisX;
            fscanf(file, "%i %i %i\n", &thisX, &thisY, &thisZ);

            i++;

        }while( ( feof( file) == 0 ) );
        graph->graphRange.xRange = i;
        graph->graphRange.yRange = j;
    }

    fclose( file );
}

