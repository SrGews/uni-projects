import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 17163042 on 20/10/15.
 */
public class FaultySortsTest {

    @Test
    public void testBubbleSort() throws Exception {
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        FaultySorts.bubbleSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("1." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("B1.lengths", startVal.length, expected.length);


        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        FaultySorts.bubbleSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("B2." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("B2.lengths", startVal.length, expected.length);


        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        FaultySorts.bubbleSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("B3." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("B3.lengths", startVal.length, expected.length);

    }

    @Test
    public void testSelectionSort() throws Exception {
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        FaultySorts.selectionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("S1." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("S1.lengths", startVal.length, expected.length);


        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        FaultySorts.selectionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("S2." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("S2.lengths", startVal.length, expected.length);


        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        FaultySorts.selectionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("S3." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("S3.lengths", startVal.length, expected.length);

    }

    @Test
    public void testInsertionSort() throws Exception {
        int[] startVal = {5,4,3,2,1};
        int[] expected = {1,2,3,4,5};

        FaultySorts.insertionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("I1." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("I1.lengths", startVal.length, expected.length);

        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        Sorts.insertionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("I2." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("I2.lengths", startVal.length, expected.length);

        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        FaultySorts.insertionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("I3." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("I3.lengths", startVal.length, expected.length);

    }

    @Test
    public void testMergeSort() throws Exception {
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        FaultySorts.mergeSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("M1." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("M1.lengths", startVal.length, expected.length);


        startVal[4] = 1;
        startVal[3] = 1;
        startVal[2] = 1;
        startVal[1] = 8;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 1;
        expected[2] = 1;
        expected[1] = 1;
        expected[0] = 1;
        FaultySorts.mergeSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("M2." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("M2.lengths", startVal.length, expected.length);


        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        FaultySorts.mergeSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("M3." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("M3.lengths", startVal.length, expected.length);


    }

    @Test
    public void testQuickSort() throws Exception {
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        FaultySorts.quickSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("1." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("Q1.lengths", startVal.length, expected.length);


        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        FaultySorts.quickSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("Q2." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("Q2.lengths", startVal.length, expected.length);


        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        FaultySorts.quickSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            Assert.assertEquals("Q3." + i, expected[i], startVal[i]);
        }
        Assert.assertEquals("Q3.lengths", startVal.length, expected.length);


    }
}