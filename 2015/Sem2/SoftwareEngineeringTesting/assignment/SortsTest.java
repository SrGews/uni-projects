import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;




@RunWith(JUnit4.class)
public class SortsTest
{
	@Test
	public void testBubbleSort()
	{
		int[] startVal = {1,2,3,4,5};
		int[] expected = {1,2,3,4,5};
		
		Sorts.bubbleSort(startVal);
		for (int i=0; i<startVal.length ; i++) {
			assertEquals("1."+i,expected[i],startVal[i]);	
		}
		assertEquals("B1.lengths" ,startVal.length,expected.length);

		startVal[4] = 3;
		startVal[3] = 8;
		startVal[2] = 1;
		startVal[1] = 2;
		startVal[0] = 1;
		expected[4] = 8;
		expected[3] = 3;
		expected[2] = 2;
		expected[1] = 1;
		expected[0] = 1;
		Sorts.bubbleSort(startVal);
		for (int i=0; i<startVal.length ; i++) {
			assertEquals("B2."+i,expected[i],startVal[i]);
		}
		assertEquals("B2.lengths" ,startVal.length,expected.length);

		startVal = new int[1];
		expected = new int[1];
		startVal[0] = 50;
		expected[0] = 50;
		Sorts.bubbleSort(startVal);
		for (int i=0; i<startVal.length ; i++) {
			assertEquals("B3."+i,expected[i],startVal[i]);
		}
		assertEquals("B3.lengths" ,startVal.length,expected.length);
	}

	@Test
	public void testSelectionSort()
	{
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        Sorts.selectionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("S1."+i,expected[i],startVal[i]);
        }
        assertEquals("S1.lengths" ,startVal.length,expected.length);

        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        Sorts.selectionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("S2."+i,expected[i],startVal[i]);
        }
        assertEquals("S2.lengths" ,startVal.length,expected.length);

        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        Sorts.selectionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("S3."+i,expected[i],startVal[i]);
        }
        assertEquals("S3.lengths" ,startVal.length,expected.length);
	}

	@Test
	public void testInsertionSort()
	{
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        Sorts.insertionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("I1."+i,expected[i],startVal[i]);
        }
        assertEquals("I1.lengths" ,startVal.length,expected.length);

        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        Sorts.insertionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("I2."+i,expected[i],startVal[i]);
        }
        assertEquals("I2.lengths" ,startVal.length,expected.length);

        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        Sorts.insertionSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("I3."+i,expected[i],startVal[i]);
        }
        assertEquals("I3.lengths" ,startVal.length,expected.length);
	}

    @Test
    public void testMergeSort()
    {
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        Sorts.mergeSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("M1."+i,expected[i],startVal[i]);
        }
        assertEquals("M1.lengths" ,startVal.length,expected.length);

        startVal[4] = 1;
        startVal[3] = 1;
        startVal[2] = 1;
        startVal[1] = 8;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 1;
        expected[2] = 1;
        expected[1] = 1;
        expected[0] = 1;
        Sorts.mergeSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("M2."+i,expected[i],startVal[i]);
        }
        assertEquals("M2.lengths" ,startVal.length,expected.length);

        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        Sorts.mergeSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("M3."+i,expected[i],startVal[i]);
        }
        assertEquals("M3.lengths" ,startVal.length,expected.length);
    }

    @Test
    public void testQuickSort()
    {
        int[] startVal = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        Sorts.quickSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("1."+i,expected[i],startVal[i]);
        }
        assertEquals("Q1.lengths" ,startVal.length,expected.length);

        startVal[4] = 3;
        startVal[3] = 8;
        startVal[2] = 1;
        startVal[1] = 2;
        startVal[0] = 1;
        expected[4] = 8;
        expected[3] = 3;
        expected[2] = 2;
        expected[1] = 1;
        expected[0] = 1;
        Sorts.quickSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("Q2."+i,expected[i],startVal[i]);
        }
        assertEquals("Q2.lengths" ,startVal.length,expected.length);

        startVal = new int[1];
        expected = new int[1];
        startVal[0] = 50;
        expected[0] = 50;
        Sorts.quickSort(startVal);
        for (int i=0; i<startVal.length ; i++) {
            assertEquals("Q3."+i,expected[i],startVal[i]);
        }
        assertEquals("Q3.lengths" ,startVal.length,expected.length);
    }
}
