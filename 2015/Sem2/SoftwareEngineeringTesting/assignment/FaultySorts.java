


class FaultySorts {
    // broken bubble sort
    public static void bubbleSort(int[] A)
    {
        int temp;
        for (int i=2; i<(A.length) ; i++){          //should set i to 1
            for (int j=1; j<(A.length) ; j++){
                if (A[i-1] > A[i]){
                    temp = A[i-1];
                    A[i-1] = A[i];
                    A[i] = temp;
                }
            }
         }
    }//bubbleSort()

    // broken selection sort
    public static void selectionSort(int[] A)
    {
        int minIdx, temp;
        for(int i=1; i<(A.length); i++){           //should set i to 0
            minIdx = i;
            for(int j=i+1; j<A.length; j++){
                if(A[j] < A[minIdx]){
                    minIdx = j;
                }
            }
            temp = A[minIdx];
            A[minIdx] = A[i];
            A[i] = temp;
        }
    }// selectionSort()

    // broken insertion sort
    public static void insertionSort(int[] A){
        int j, temp;
        for(int i = 1; i<(A.length); i++){      //should set i to 0
            j = i;
            while ( (j>0) && (A[j-1] > A[j]) ){
                temp = A[j];
                A[j] = A[j-1];
                A[j-1] = temp;
                j-- ;
            }
        }
    }// insertionSort()

    // faulty mergeSort - front-end for kick-starting the recursive algorithm
    public static void mergeSort(int[] A){
        int leftIdx, rightIdx;

        rightIdx = A.length - 1;
	    leftIdx = 1;                //should set leftIdx to 0

        mergeSortRecurse(A,leftIdx, rightIdx);
    }//mergeSort()
    private static void mergeSortRecurse(int[] A, int leftIdx, int rightIdx){
        int midIdx;
        if(leftIdx < rightIdx){
            midIdx = (leftIdx + rightIdx)/2;

            mergeSortRecurse(A, leftIdx, midIdx);
            mergeSortRecurse(A, midIdx+1, rightIdx);

            merge(A, leftIdx, midIdx, rightIdx);
        }
    }//mergeSortRecurse()

    private static void merge(int[] A, int leftIdx, int midIdx, int rightIdx){
        int tempArrSize;
        tempArrSize = rightIdx-leftIdx +1;
        int[] tempArr = new int [tempArrSize] ;
	    int i = leftIdx, j = midIdx +1, k = 0;

        while ((i <= midIdx) && (j <= rightIdx)){
            if(A[i]<=A[j]){
                tempArr[k] = A[i];
                i++;
            }
            else{
                tempArr[k] = A[j];
                j++;
                }
            k++;
        }

        for( ; i<=midIdx; i++){
            tempArr[k] = A[i];
            k++;
        }

        for( ; j<=rightIdx; j++){
            tempArr[k] = A[j];
            k++;
        }

        for(k = leftIdx; k <=rightIdx; k++){
            A[k] = tempArr[k-leftIdx];
        }
    }//merge()


    // faulty quickSort - front-end for kick-starting the recursive algorithm
    public static void quickSort(int[] A){
        int leftIdx, rightIdx;
        
        rightIdx = A.length - 1;
        leftIdx = 1;                    //should set leftIdx to 1
        
        quickSortRecurse(A, leftIdx, rightIdx);
    }//quickSort()

    private static void quickSortRecurse(int[] A, int leftIdx, int rightIdx){
        int pivotIdx, newPivotIdx;
        if(rightIdx>leftIdx){
            pivotIdx = (leftIdx + rightIdx)/2;
            newPivotIdx = doPartitioning(A, leftIdx, rightIdx, pivotIdx);
            
            quickSortRecurse(A, leftIdx, newPivotIdx-1);
            quickSortRecurse(A, newPivotIdx+1, rightIdx);
        }
    }//quickSortRecurse()

    private static int doPartitioning(int[] A, int leftIdx, int rightIdx, int pivotIdx){
        int newPivotIdx, pivotVal, i, temp, currIdx;
        pivotVal = A[pivotIdx];
        A[pivotIdx] = A[rightIdx];
        A[rightIdx] = pivotVal;
    
        currIdx = leftIdx;
        
        for(i=leftIdx; i<rightIdx; i++){
            if(A[i]<pivotVal){
                temp = A[i];
                A[i] = A[currIdx];
                A[currIdx] = temp;
                currIdx ++;
            }
        }
        
        newPivotIdx = currIdx;
        A[rightIdx] = A[newPivotIdx];
        A[newPivotIdx] = pivotVal;
        
        return newPivotIdx;
    }//doPartitioning()


}//end Sorts class
