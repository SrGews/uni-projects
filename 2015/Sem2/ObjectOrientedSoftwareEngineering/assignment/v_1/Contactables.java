import java.util.*;





//This holds all the objects needed to contact people later
//Used to send messages over mediums
public class Contactables{

	private SMS phone;
	private Social fBSocial;
	private Social tSocial;

	public Contactables(){
		phone = new SMS();
		fBSocial = new FacebookSocial();
		tSocial = new TwitterSocial();

		runSocials();
	}
	public Contactables(NotificationSet inTrendySet, HashMap<String,Policy> policies){
		phone = new SMS();
		fBSocial = new FacebookSocial(inTrendySet, this, policies);
		tSocial = new TwitterSocial(inTrendySet, this, policies);

		runSocials();
	}

	public Contactables(SMS inPhone, Social inFBSocial, Social inTSocial){
		phone = inPhone;
		fBSocial = inFBSocial;
		tSocial = inTSocial;
	}

	public void runSocials(){
		fBSocial.startThisMonitoring();
		tSocial.startThisMonitoring();
	}

	public void setTheKeywords(Set<String> keywords){
		fBSocial.setKeywords(keywords);
		tSocial.setKeywords(keywords);
	}

	public void setPhone(SMS inPhone){
		phone = inPhone;
	}
	public SMS getPhone(){
		return phone;
	}

	public void setFBsocial(Social inSocial){
		fBSocial = inSocial;
	}
	public Social getFBSocial(){
		return fBSocial;
	}
	public void setTSocial(Social inSocial){
		tSocial = inSocial;
	}
	public Social getTSocial(){
		return tSocial;
	}

	public void sendSMS(long mobileNumber,String message){
		phone.sendSMS(mobileNumber, message);
	}

	public void sendFBMessage(String iD, String message){
		fBSocial.sendMessage(iD, message);
	}
	public void sendTwitMessage(String iD, String message){
		tSocial.sendMessage(iD, message);
	}
}