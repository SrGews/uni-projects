import edu.curtin.messaging;
import java.util.*;


//This class is used to monitor and interact with facebook. It also implements an observer
public class FacebookSocial extends FacebookMessage implements Social {
	
	private ListedResults listedResults;
	private TrendyObserver trendyObserver;


	public FacebookSocial(){
		super();
		listedResults = new ListedResults();
		trendyObserver = new TrendyObserver();
	}

	public FacebookSocial( ListedResults inListedResults, Contactables inContactables){
		super();
		listedResults = inListedResults;
		trendyObserver = new TrendyObserver();
		trendyObserver.setContactables(inContactables);
	}

	public void trendyObserver(TrendyObserver inTrendy){
		trendyObserver = inTrendy;
	}

	@Override public void startThisMonitoring(){
		startMonitoring();
	}
	public void setKeyWords( Set<String> keywords ){
		super.setKeywords(keywords);
	}

	//observer pattern implementation
	//Gets new words in and checks if any are trending. if they are, then it will send messages to everyone who like the related policy
	@Override public void keywordsDetected( Map<String,Integer> keywords, long timestamp){
		listedResults.addEntry(keywords, timestamp);

		if (listedResults.isTrending()){
			for (String keyWord: listedResults.getResultSummary().keySet() ) {
				if (listedResults.getResultSummary().get(keyWord) >= 50 ) {
					if(trendyObserver.addTrendyKeyword(keyword)){
						trendyObserver.notifyPeople(keyWord);
					}
				}
			}
		}
	}
}
