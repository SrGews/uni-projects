
//This starts and runs the program
public class StartProgram{
	public static void main(String[] args) {
		Presenter presenter;

		presenter = new Presenter();
		presenter.beginProgram();
	}
}