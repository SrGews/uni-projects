


//This will hold the contact details of a person
public abstract class ContactDetail{
	public abstract void sendMessage(Contactables contactables, String message);
}