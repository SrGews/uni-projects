
//Observer pattern interface
public interface Observable{
	void notifyPeople(String message, Policy policy);
	void notifyPeople(String message);
}