
//this observer notifies for when keys are added to keywords

public class KeyAddObserver implements Observable {

	private NotificationSet notifyThesePeople;
	private Contactables contactables;

	public KeyAddObserver(){
		notifyThesePeople = new NotificationSet();
		contactables = new Contactables();
	}

	public void setNotifies(NotificationSet inSet){
		notifyThesePeople = inSet;
	}

	public void setContactables(Contactables inContactables){
		contactables = inContactables;
	}

	@Override public void notifyPeople(String message, Policy inPolicy){
		for (Person person: notifyThesePeople.getNotifyList(inPolicy)) {
			person.notify(contactables,message + " has just been added");
		}
	}
	@Override public void notifyPeople(String message){
		for (Person person: notifyThesePeople.getNotifyListKeyWords(message)) {
			person.notify(contactables,message + " has just been added");
		}
	}
}