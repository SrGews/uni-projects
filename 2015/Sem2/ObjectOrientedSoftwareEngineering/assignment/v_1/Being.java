import java.util.*;


//Beings encompasses Person and Group.
public abstract class Being{
	protected String name;
	protected LinkedList<Being> beings;

	public Being(){
		name = "unKnown";
		beings = new LinkedList<Being>();
	}

	public Being(String inName){ name = inName; }

	public String getName(){ return name; }

	public void setName(String inName){ name = inName; }

	public LinkedList<Being> getBeings(){return beings;}

	public void setBeings(LinkedList<Being> inBeings){beings = inBeings;}

	public abstract void notify(Contactables contactables, String message);
	public abstract LinkedList<Person> addToList(LinkedList<Person> inList);
}