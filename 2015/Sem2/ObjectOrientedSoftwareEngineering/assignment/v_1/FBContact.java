
//A Contact detail to be owned by a person

public class FBContact extends ContactDetail{
	private String userName;

	public FBContact(){
		userName = "";	
	}
	public FBContact(String inName){
		userName = inName;	
	}

	public String getName(){return userName;}
	public void setName(String inName){userName = inName;}
	
	@Override public void sendMessage(Contactables contactables, String message){
		contactables.sendFBMessage(userName, message);
	}

	public String toString(){
		return getName();
	}

}