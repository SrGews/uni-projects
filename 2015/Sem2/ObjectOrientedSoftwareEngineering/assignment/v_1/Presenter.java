import java.util.*;



//This class, while huge, doesn't do much. It controles the flow of the program, mostly just
//telling the view to display a menu and telling the model what to do with that information
public class Presenter {
	
	private View view;
	private Model model;
	private boolean continueRunning;

	public Presenter(){
		view = new View();
		model = new Model();
		continueRunning = true;
	}

	public Presenter(View inView, Model inModel, boolean inContinueRunning){
		view = inView;
		model = inModel;
		continueRunning = inContinueRunning;
	}

	public void beginProgram(){
		LinkedList<String> menuItems = new LinkedList<String>();
		
		menuItems.add("People");
		menuItems.add("Groups");
		menuItems.add("Policies");
		menuItems.add("Exit");
		menuItems.add("Save");
		menuItems.add("Load");

		while (continueRunning){
			menuOneResult(view.showMenu(menuItems, "Welcome"));
		}
		continueRunning = true;
	}

	private void menuOneResult(int choice){
		switch (choice) {
			case 1:
				peopleMenuResult(peopleMenu());
			break;
			case 2:
				groupMenuResult(groupMenu());
			break;
			case 3:
				policyMenuResult(policyMenu());
			break;
			case 4:
				continueRunning = false;
			case 5:
				model.saveProgram();
			break;
			case 6:
				model.loadProgram();
			break;
			default:
				System.out.println("Enter a valid input.");
			
		}
	}

	public void peopleMenuResult(int choice){
		switch (choice) {
			case 1:
				addPerson();
			break;
			case 2:
				editPerson();
			break;
			case 3:
				viewPeople();
			break;
			case 4:
				viewPerson();
			break;
			case 5:
				addContact();
			break;
			default:
				System.out.println("Enter a valid input.");	
			}
	}

	public void addPerson(){
		Person newPerson = new Person();

		view.showText("Creating new person");
		view.showText("Enter a person's name");

		newPerson.setName(view.readText());

		model.addPerson(newPerson);
		addContact(newPerson);
	}
	public void editPerson(){

		Person person; 

		view.showText("Enter a Person iD:");

		person = model.getPerson(view.readMenuInt());
		view.showText(person.toString());

		switch (showMenuEditPersonOptions()) {
			case 1:
				changePersonName(person);
			break;
			case 2:
				addGroupToPerson(person);
			break;
			case 3:
				removeGroupFromPerson(person);
			break;
			case 4:
				addPolicyToBeing(person);
			break;
			case 5:
				removePolicyFromBeing(person);
			break;
			case 6:
				addContact(person);
			break;
			default:

			break;			
		};
	}
	public void changePersonName(Person person){
		view.showText("Enter a new name for the Person:");
		person.setName(view.readText());
	}
	public void addGroupToPerson(Person person){
		Group group;

		view.showText("Enter a new Group to add to the Person:");
		group = model.getGroup(view.readText());
		person.addGroup(group);
		group.addPerson(person);
	}
	public void removeGroupFromPerson(Person person){
		Group group;

		view.showText("Enter a Group to remove from the Person:");
		group = model.getGroup(view.readText());
		person.removeGroup(group);
		group.removePerson(person);
	}
	public void addPolicyToBeing(Person being){
		Policy policy;

		view.showText("Enter a Policy to add to the Person:");
		policy = model.getPolicy(view.readText());


		switch (showMenuSelectNotificationType()) {
			case 1:
				model.getAddKeyword().addPair(policy, being);
			break;
			case 2:
				model.getTrendyKeyword().addPair(policy, being);
			break;
			case 3:
				model.getAddTalkingPoint().addPair(policy, being);
			break;
			default:
			break;
		}

	}
	public void addPolicyToBeing(Group being){
		Policy policy;

		view.showText("Enter a Policy to add to the Person:");
		policy = model.getPolicy(view.readText());


		if(being.getOnKeyAdd() == true){
			model.getAddKeyword().addPair(policy, being);
		}
		if(being.getOnKeyTrend() == true){
				model.getTrendyKeyword().addPair(policy, being);
		}
		if(being.getOnTalkingPointAdd() == true){
				model.getAddTalkingPoint().addPair(policy, being);
		}

	}
	public void removePolicyFromBeing(Being being){
		Policy policy;
		
		view.showText("Enter a Policy to remove from the Person:");
		policy = model.getPolicy(view.readText());

		
		model.getAddKeyword().removeLink( policy, being );
		model.getAddTalkingPoint().removeLink( policy, being );
		model.getTrendyKeyword().removeLink( policy, being );
	}

	public int policyMenu(){
		LinkedList<String> menuItems = new LinkedList<String>();
		
		menuItems.add("Add Policy");
		menuItems.add("Delete Policy");
		menuItems.add("View Policies");
		menuItems.add("View Policy details");
		menuItems.add("View Policy Keywords");
		menuItems.add("Add Policy Keywords");
		menuItems.add("Delete Policy Keywords");
		menuItems.add("View Policy TalkingPoints");
		menuItems.add("Add Policy TalkingPoints");
		menuItems.add("Delete Policy TalkingPoints");

		return view.showMenu(menuItems, "Policy");
	}

	public int peopleMenu(){
		LinkedList<String> menuItems = new LinkedList<String>();
		
		menuItems.add("Add Person");
		menuItems.add("Edit Person");
		menuItems.add("View People");
		menuItems.add("View Person Details");
		menuItems.add("Add Contact Detail");

		return view.showMenu(menuItems, "Person");
	}
	public int groupMenu(){
		LinkedList<String> menuItems = new LinkedList<String>();
		
		menuItems.add("Add Group");
		menuItems.add("Edit Group");
		menuItems.add("View Groups");
		menuItems.add("View Group Details");

		return view.showMenu(menuItems, "Person");
	}
	public int editPersonEnterIDCommand(){
		LinkedList<String> stringList = new LinkedList<String>();
		
		stringList.add("Edit Person:");
		stringList.add("Enter their iD:");

		view.showList(stringList);
		return view.readMenuInt();
	}
	public int showMenuEditPersonOptions(){
		LinkedList<String> stringList = new LinkedList<String>();
		
		stringList.add("Change their name");
		stringList.add("Add a Group");//keep adding till they dont want to add any more
		stringList.add("Remove a Group");
		stringList.add("Add a Policy area");
		stringList.add("Remove a Policy area");
		stringList.add("Add a Contact detail");

		return view.showMenu(stringList,"Edit Person Options");
	}
	public void viewPerson(){
		view.showText("Enter a person iD:");
		view.showText(model.getPerson(view.readMenuInt()).toString());
	}
	public void viewPeople(){
		for (Person person: model.getPeople().values()) {
			view.showText(person.toString());
		}
	}

	public int showMenuSelectNotificationType(){
		LinkedList<String> stringList = new LinkedList<String>();
		stringList.add("Keyword addition");
		stringList.add("Keyword trending");
		stringList.add("Talking point added");

		return view.showMenu(stringList,"Select notification type");
	}

	public void addContact(){
		Person person;
		LinkedList<String> stringList = new LinkedList<String>();

		view.showText("Enter a person's name");
		person = model.getPerson(view.readMenuInt());
		
		addContact(person);
	}

	public void addContact(Person inPerson){
		ContactDetail contact;
		LinkedList<String> stringList = new LinkedList<String>();

		stringList.add("Phone number");
		stringList.add("Facebook Username");
		stringList.add("Twitter Username");

		switch(view.showMenu(stringList, "Select Contact type")){
			case 1:
				view.showText("Enter a Phone number");
				contact = new PhoneContact(view.readMenuInt());
			break;
			case 2:
				view.showText("Enter a Username");
				contact = new FBContact(view.readText());
			break;
			case 3:
				view.showText("Enter a Username");
				contact = new TwitContact(view.readText());
			break;
			default:
				view.showText("Defaulted to Phone number");
				view.showText("Enter a Phone number");

				contact = new PhoneContact(view.readMenuInt());
		}
		inPerson.addContactDetail(contact);

	}
	public void addGroup(){
		
		Group newGroup;
		boolean inKAdd, inKTrend, inTAdd;

		view.showText("Creating new Group");

		inKAdd = trueOrFalseMenu("Group alerts on added keywords");
		inKTrend = trueOrFalseMenu("Group alerts on trending keywords");
		inTAdd = trueOrFalseMenu("Group alerts on added talking points");

		newGroup = new Group(inKAdd, inKTrend, inTAdd);
		
		view.showText("Enter the new Group's name");
		newGroup.setName(view.readText());
		model.addGroup(newGroup);
	}

	public boolean trueOrFalseMenu(String varName){
		boolean retVal = false;
		LinkedList<String> stringList = new LinkedList<String>();
		
		stringList.add(varName + " false");
		stringList.add(varName + " true");

		if (view.showMenu(stringList,"Select whether the variables are true or false") == 2){
			retVal = true;
		}

		return retVal;
	}

	public void editGroup(){

		Group group; 

		view.showText("Enter a Group name:");

		group = model.getGroup(view.readText());
		view.showText(group.toString());

		switch (showMenuEditGroupOptions()) {
			case 1:
				addPersonToGroup(group);
			break;
			case 2:
				removePersonFromGroup(group);
			break;
			case 3:
				addPolicyToBeing(group);
			break;
			case 4:
				removePolicyFromBeing(group);
			break;
			default:

			break;			
		};
	}
	public int showMenuEditGroupOptions(){
		LinkedList<String> stringList = new LinkedList<String>();
		
		stringList.add("Add Person");
		stringList.add("Remove Person");//keep adding till they dont want to add any more
		stringList.add("Add a Policy area");
		stringList.add("Remove a Policy area");

		return view.showMenu(stringList,"Edit Group Options");
	}
	public void addPersonToGroup(Group group){
		Person person;
		view.showText("Enter a new Person's iD to add to the Group:");

		person = model.getPerson(view.readMenuInt());
		group.addPerson(person);
		person.addGroup(group);

	}
	public void removePersonFromGroup(Group group){
		Person person;
		view.showText("Enter a Person's iD to remove from the Group:");

		person = model.getPerson(view.readMenuInt());
		group.removePerson(person);
		person.removeGroup(group);
	}

	public void viewGroups(){
		for (Group group: model.getGroups().values()) {
			view.showText(group.toString());
		}
	}

	public void viewGroup(){
		view.showText("Enter a Group's name:");
		view.showText(model.getGroups().get(view.readText()).toString());

	}

	public void groupMenuResult(int choice){
		switch (choice) {
			case 1:
				addGroup();
			break;
			case 2:
				editGroup();
			break;
			case 3:
				viewGroups();
			break;
			case 4:
				viewGroup();
			break;
			default:
				System.out.println("Enter a valid input.");	
			}
	}	
	public void policyMenuResult(int choice){
		switch (choice) {
			case 1:
				addPolicy();
			break;
			case 2:
				deletePolicy();
			break;
			case 3:
				viewPolicies();
			break;
			case 4:
				viewPolicy();
			break;
			case 5:
				viewPolicyKeywords();
			break;
			case 6:
				addPolicyKeyword();
			break;
			case 7:
				deletePolicyKeyword();
			break;
			case 8:
				viewPolicyTalkingPoints();
			break;
			case 9:
				addPolicyTalkingPoint();
			break;
			case 10:
				deletePolicyTalkingPoint();
			break;
			
			default:
				System.out.println("Enter a valid input.");	
			}
	}

	public void addPolicy(){
		view.showText("Enter a name for the new Policy");
		model.addPolicy(view.readText());
	}


	public void deletePolicy(){
		Policy policy;
		LinkedList<String> stringList = new LinkedList<String>();
		view.showText("Enter the name of the policy you wish to delete:");

		policy = model.getPolicy(view.readText());

		view.showText("The following keywords are related to the Policy and will also be deleted:");
		for (String keyword: policy.getKeywords()) {
			view.showText(keyword);
		}
		view.showText("The following talkingPoints are related to the Policy and will also be deleted:");
		for (String talkingPoint: policy.getTalkingPoints()) {
			view.showText(talkingPoint);
		}
	
		if(trueOrFalseMenu("Still delete Policy: " + policy.getName()) == true){
			model.removePolicy(policy);
		}
	}

	public void viewPolicies(){
		for (Policy policy: model.getPolicies().values()) {
			view.showText(policy.toString());
		}
	}

	public void viewPolicy(){
		view.showText("Enter a policy name:");
		view.showText(model.getPolicy(view.readText()).toString());
	}

	public void viewPolicyKeywords(){
		String policyName;

		view.showText("Enter a policy name:");
		policyName = view.readText();

		view.showText("Keywords:");
		for (String keyword: model.getPolicy(policyName).getKeywords()) {
			view.showText(keyword);
		}
	}

	public void addPolicyKeyword(){
		String keyword, policyName;

		view.showText("Enter a policy name:");
		policyName = view.readText();
		view.showText("Enter a new keyword:");
		keyword= view.readText();

		model.getPolicy(policyName).addKeyword(keyword);

	}

	public void deletePolicyKeyword(){
		String keyword, policyName;

		view.showText("Enter a policy name:");
		policyName = view.readText();
		view.showText("Enter a keyword to remove:");
		keyword= view.readText();

		model.getPolicy(policyName).removeKeyword(keyword);
	}

	public void viewPolicyTalkingPoints(){
		String policyName;

		view.showText("Enter a policy name:");
		policyName = view.readText();
		
		view.showText("TalkingPoints:");
		for (String talkingPoint: model.getPolicy(policyName).getTalkingPoints()) {
			view.showText(talkingPoint + ", ");
		}
	}

	public void addPolicyTalkingPoint(){
		String talkingPoint, policyName;

		view.showText("Enter a policy name:");
		policyName = view.readText();
		view.showText("Enter a new talkingPoint:");
		talkingPoint= view.readText();

		model.getPolicy(policyName).addTalkingPoint(talkingPoint);
	}

	public void deletePolicyTalkingPoint(){
		String talkingPoint, policyName;

		view.showText("Enter a policy name:");
		policyName = view.readText();
		view.showText("Enter a talkingPoint to remove:");
		talkingPoint= view.readText();

		model.getPolicy(policyName).removeTalkingPoint(talkingPoint);
	}

}
