import java.util.*;



//This will contain a group of people who will be associated with this goup's likes
public class Group extends Being{

	private boolean onKeyAdd;
	private boolean onKeyTrend;
	private boolean onTalkingAdd;

	public Group(){
		super();
		name = "unKnownGroup";
		onKeyAdd = false;
		onKeyTrend = false;
		onTalkingAdd = false;
	}
	public Group(boolean inKAdd, boolean inKTrend, boolean inTAdd){
		super();
		name = "unKnownGroup";
		onKeyAdd = inKAdd;
		onKeyTrend = inKTrend;
		onTalkingAdd = inTAdd;
	}

	public boolean getOnKeyAdd(){return onKeyAdd;}
	public boolean getOnKeyTrend(){return onKeyTrend;}
	public boolean getOnTalkingPointAdd(){return onTalkingAdd;}

	public LinkedList<Being> getPeople(){ return beings; }
	public void addPerson(Person inPerson){ beings.add(inPerson); }	

	@Override public void notify(Contactables contactables, String message){
		for(Being person:beings){
			person.notify(contactables, message);
		}
	}
	@Override public LinkedList<Person> addToList(LinkedList<Person> inList){
	
		for (Being person : beings) {
			inList = person.addToList(inList);
		}

		return inList;
	}

	public String toString(){
		String retString;

		retString = "Name is:" + super.getName() + " KeyAdd:"+ onKeyAdd +" KeyTrend:" + onKeyTrend + " TalkingPointAdd:" + onTalkingAdd + " People are:" ;

		for (Being group: beings) {
			retString += group.getName() + ", ";
		}

		return retString;
	}

	public void removePerson(Person inPerson){
		if(beings.contains(inPerson)){
			beings.remove(inPerson);
		}
	}
}