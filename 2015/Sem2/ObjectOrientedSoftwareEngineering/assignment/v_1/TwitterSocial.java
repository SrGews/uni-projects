import edu.curtin.messaging;
import java.util.*;


//This class is used to monitor and interact with facebook. It also implements an observer

public class TwitterSocial /*extends TwitterMessage*/ implements Social {

	private ListedResults listedResults;
	private Observable trendyObserver;

	public TwitterSocial(){
		super();
		listedResults = new ListedResults();
		trendyObserver = new TrendyObserver();
	}

	public TwitterSocial( ListedResults inListedResults){
		super();
		listedResults = inListedResults;
		trendyObserver = new TrendyObserver();
	}

	public void trendyObserver(TrendyObserver inTrendy){
		trendyObserver = inTrendy;
	}

	@Override public void startThisMonitoring(){
		startMonitoring();
	}

	//observer pattern implementation
	//Gets new words in and checks if any are trending. if they are, then it will send messages to everyone who like the related policy
	@Override public void keywordsDetected( Map<String,Integer> keywords, long timestamp){
		listedResults.addEntry(keywords, timestamp);

		if (listedResults.isTrending()){
			for (String keyWord: listedResults.getResultSummary().keySet() ) {
				if (listedResults.getResultSummary().get(keyWord) >= 50 ) {
					if(trendyObserver.addTrendyKeyword(keyword)){
						trendyObserver.notifyPeople(keyWord);
					}
				}
			}
		}
	}
}
