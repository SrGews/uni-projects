
//A Contact detail to be owned by a person

public class PhoneContact extends ContactDetail{
	private long phoneNumber;

	public PhoneContact(){
		phoneNumber = 0;	
	}
	public PhoneContact(long inNumber){
		phoneNumber = inNumber;	
	}

	public long getPhoneNumber(){return phoneNumber;}
	public void setPhoneNumber(long inPhoneNumber){phoneNumber = inPhoneNumber;}

	@Override public void sendMessage(Contactables contactables, String message){
		contactables.sendSMS(phoneNumber, message);
	}

	public String toString(){
		return String.valueOf(phoneNumber);
	}
}