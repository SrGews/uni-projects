import java.util.*;



//Used to handle all user interaction 
public class View {
	
	public View(){
	}

	public int readMenuInt(){
		Scanner in = new Scanner(System.in);
		int choice;

		try {
			choice = in.nextInt();	
		}
		catch(Exception e){
			choice = 500;
			showText("The input was invalid: must be a whole number:"+e.getMessage());
		}
		return choice;
	}

	public void showText(String inText){
		try{
			System.out.println(inText);
		}
		catch(Exception e){

		}
	}

	public String readText(){
        Scanner scanner = new Scanner(System.in);
        String retString;

        try{
        	retString = scanner.nextLine();
        }
        catch(Exception e){
        	showText("Cannot read in the entered line:"+e.getMessage());
        	retString = "";
        }

        return retString;
	}

	public int showMenu(List<String> inMenus, String title){
		
		int i = 1, choice = 0;

		try{
			showText(title + ":");
			for (String menu: inMenus) {
				showText(i + ":	" + menu);
				i++;
			}
			choice = readMenuInt();
		}
		catch(Exception e){
			showText("Unable to display menu:"+e.getMessage());
		}
		return choice;
	}

	public void showList(List<String> inList){

		try{
			for (String item: inList) {
				showText(item);
			}
		}
		catch(Exception e){
			showText("Unable to display the list:"+e.getMessage());
		}
	}
}