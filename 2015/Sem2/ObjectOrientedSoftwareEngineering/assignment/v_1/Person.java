import java.util.*;


//This will contain a group of groups who this person will be associated with
public class Person extends Being{

	private static int nextID = 0;
	private int iDNumber;
	private LinkedList<ContactDetail> contactDetails;

	public Person(){
		super();
		name = "unKnownPerson";
		iDNumber = nextID;
		nextID ++;
		contactDetails = new LinkedList<ContactDetail>();
	}
	public Person(Person inPerson){
		super();
		name = "unKnownPerson";
		iDNumber = nextID;
		nextID ++;
		contactDetails = new LinkedList<ContactDetail>();

		super.setName(inPerson.getName());
		super.setBeings(inPerson.getBeings());
		iDNumber = inPerson.getIDNumber();
		contactDetails = new LinkedList<ContactDetail>(inPerson.getContacts());
	}

	public Person(String inName){
		name = inName;
		iDNumber = nextID;
		nextID ++;
		contactDetails = new LinkedList<ContactDetail>();
	}

	public Person(String inName, LinkedList<ContactDetail> inContacts){
		name = inName;
		iDNumber = nextID;
		nextID ++;
		contactDetails = inContacts;
	}

	public LinkedList<Being> getGroups(){ return beings; }
	public void addGroup(Group inGroup){ beings.add(inGroup); }
	public int getIDNumber(){ return iDNumber; }
	public void setiDNumber(int inId){ iDNumber = inId; }
	public LinkedList<ContactDetail> getContacts(){ return contactDetails; }
	public void setContacts(LinkedList<ContactDetail> inContacts){ contactDetails = inContacts; }

	public void addContactDetail(ContactDetail inDetail){ contactDetails.add(inDetail); };
	

	@Override public void notify(Contactables contactables, String message){
		for(ContactDetail contactDetail:contactDetails){
			contactDetail.sendMessage(contactables, message);
		}
	}

	@Override public LinkedList<Person> addToList(LinkedList<Person> inList){
		if (inList.contains(this) != true){
			inList.add(this);
		}
		return inList;
	}

	public String toString(){
		String retString;

		retString = "iD is: " + iDNumber+ " Name is:" + super.getName() + " Groups are:" ;

		for (Being group: beings) {
			retString += group.getName() + ", ";
		}

		retString += " Contact details: ";

		for (ContactDetail contact: contactDetails) {
			retString += contact.toString() + ", ";
		}

		return retString;
	}

	public void removeGroup(Group inGroup){
		if(beings.contains(inGroup)){
			beings.remove(inGroup);
		}
	}
}