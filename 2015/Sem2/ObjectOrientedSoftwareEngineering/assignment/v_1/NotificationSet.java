import java.util.*;




/*policyContactList keeps track of what Beings (groups and people) are monitoring that policy and what policies are being monitored by a being.
One reason this was implemented was to allow O(1) lookup of any Being or Policy. This is particularly useful when removing a Policy or Being.
Another reason this was chosen is because it prevents a person being notified twice for the same trigger.
*/
public class NotificationSet{

	private HashMap<Policy,HashSet<Being>> policyContactList;
	private HashMap<Being,HashSet<Policy>> beingPolicyList;

	public NotificationSet(){
		policyContactList = new HashMap<Policy,HashSet<Being>>();
		beingPolicyList = new HashMap<Being,HashSet<Policy>>();
	}
	public NotificationSet(HashMap<Policy,HashSet<Being>> inPolicyList, HashMap<Being,HashSet<Policy>> inBeingList){
		policyContactList = inPolicyList;
		beingPolicyList = inBeingList;
	}

	private void addToPolicyContactList(Policy inPolicy, Being inBeing){

		if (policyContactList.containsKey(inPolicy) != true) {
			policyContactList.put(inPolicy, new HashSet<Being>());
		}

		if (policyContactList.get(inPolicy).contains(inBeing) != true) {
			policyContactList.get(inPolicy).add(inBeing);	
		}
	}
	private void addToBeingPolicyList(Policy inPolicy, Being inBeing){
		if (beingPolicyList.containsKey(inBeing) != true) {
			beingPolicyList.put(inBeing, new HashSet<Policy>());
		}

		if (beingPolicyList.get(inBeing).contains(inPolicy) != true) {
			beingPolicyList.get(inBeing).add(inPolicy);	
		}
	}

	public void addPair(Policy inPolicy, Being inBeing){
		addToBeingPolicyList(inPolicy, inBeing);
		addToPolicyContactList(inPolicy, inBeing);
	}

	public void remove(Being inBeing){
		for (Policy policy: beingPolicyList.get(inBeing)) {
			policyContactList.get(policy).remove(inBeing);
		}
		beingPolicyList.remove(inBeing);
	}
	public void remove(Policy inPolicy){
		for (Being being: policyContactList.get(inPolicy)) {
			policyContactList.get(being).remove(inPolicy);
		}
		policyContactList.remove(inPolicy);
	}
	public void removeLink(Policy inPolicy, Being inBeing){
		if (policyContactList.get(inPolicy).contains(inBeing) == true) {
			policyContactList.get(inPolicy).remove(inBeing);
		}

		if (beingPolicyList.get(inBeing).contains(inPolicy) == true) {
			beingPolicyList.get(inBeing).remove(inPolicy);
		}
	}

	public void update(Being oldBeing, Being newBeing){
		HashSet<Policy> policySet;
		policySet = beingPolicyList.get(oldBeing);
		remove(oldBeing);

		for (Policy policy: policySet) {
			addPair(policy, newBeing);
		}
	}

	public void update(Policy oldPolicy, Policy newPolicy){
		HashSet<Being> beingSet;
		beingSet = policyContactList.get(oldPolicy);
		remove(oldPolicy);

		for (Being being: beingSet) {
			addPair(newPolicy, being);
		}
	}

	public LinkedList<Person> getNotifyList(Policy inPolicy){
		LinkedList<Person> personList = new LinkedList<Person>();
		for (Being being: policyContactList.get(inPolicy)) {
			being.addToList(personList);
		}

		return personList;
	}
	public LinkedList<Person> getNotifyListKeyWords(String inWord){
		Policy policy;
		LinkedList<Person> personList = new LinkedList<Person>();

		for (Policy thisPolicy: policyContactList.keySet()) {
			
			if (thisPolicy.getKeywords().contains(inWord)) {
				for (Being being: policyContactList.get(thisPolicy)) {
					being.addToList(personList);
				}	
			}		
		}
		return personList;
	}

	public LinkedList<Person> getNotifyListTalkingPoints(String inWord){
		Policy policy;
		LinkedList<Person> personList = new LinkedList<Person>();

		for (Policy thisPolicy: policyContactList.keySet()) {
			
			if (thisPolicy.getTalkingPoints().contains(inWord)) {
				for (Being being: policyContactList.get(thisPolicy)) {
					being.addToList(personList);
				}	
			}		
		}
		return personList;
	}
}