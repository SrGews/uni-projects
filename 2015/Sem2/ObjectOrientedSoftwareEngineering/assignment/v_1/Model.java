import java.util.*;

//This holds all the data and backed stuff which is needed by the system

public class Model {

	private HashMap<Integer,Person> people;
	private HashMap<String,Group> groups;
	private HashMap<String,Policy> policies;
	private NotificationSet addKeyword;
	private NotificationSet addTalkingPoint;
	private NotificationSet trendyKeyword;
	private Contactables contactables;

	public Model(){
		people = new HashMap<Integer,Person>();
		groups = new HashMap<String,Group>();
		policies = new HashMap<String,Policy>();
		addKeyword = new NotificationSet();
		addTalkingPoint = new NotificationSet();
		trendyKeyword = new NotificationSet();
		contactables = new Contactables(trendyKeyword, policies);
	}

	public NotificationSet getAddKeyword(){return addKeyword;}
	public NotificationSet getAddTalkingPoint(){return addTalkingPoint;}
	public NotificationSet getTrendyKeyword(){return trendyKeyword;}

	public Policy getPolicy(String inName){
		return policies.get(inName);
	}
	public void addPolicy(String inName){
		Policy policy = new Policy();
		KeyAddObserver kAO = new KeyAddObserver();
		kAO.setNotifies(addKeyword);
		kAO.setContactables(contactables);

		TalkingPointAddObserver tPAO = new TalkingPointAddObserver();
		tPAO.setNotifies(addTalkingPoint);
		tPAO.setContactables(contactables);

		policy.setName(inName);
		policy.setKeyObv(kAO);
		policy.setTalkingObv(tPAO);

		policies.put(inName, policy);
	}
	public void removePolicy(Policy inPolicy){
		policies.remove(inPolicy.getName());
		notificationsRemovePolicy(inPolicy);
	}
	public HashMap<String,Policy> getPolicies(){
		return policies;
	}

	private void notificationsRemovePolicy(Policy inPolicy){
		addKeyword.remove(inPolicy);
		addTalkingPoint.remove(inPolicy);
		trendyKeyword.remove(inPolicy);
	}

	public void addPerson(Person inPerson){
		Integer iD = new Integer(inPerson.getIDNumber());

		people.put(iD,inPerson);
	}
	public HashMap<String,Group> getGroups(){
		return groups;
	}
	public void addGroup(Group inGroup){
		String name = inGroup.getName();

		groups.put(name,inGroup);
	}
	public Group getGroup(String name){
		Group group;

		if (groups.containsKey(name)) {
			group = groups.get(name);
		}
		else{
			group = new Group();
		}

		return group;
	}

	public Person getPerson(int iDNumber){
		Integer iD = new Integer(iDNumber);

		Person person = new Person();

		if (people.containsKey(iD)) {
			person = people.get(iD);
		}

		return person;
	}
	public HashMap<Integer,Person> getPeople(){
		return people;
	}

	public void saveProgram(){}
	public void loadProgram(){}

}