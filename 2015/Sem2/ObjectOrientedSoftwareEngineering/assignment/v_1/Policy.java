import java.util.*;

//Is a policy of a political campaign. When keywords are added it notifies the 
//keyAddObserver to deal with it, or the TalkingPointAddObser if a talking point is being added there
public class Policy {

	private String policyName;
	private HashSet<String> keywords;
	private HashSet<String> talkingPoints;
	private KeyAddObserver keyAddObv;
	private TalkingPointAddObserver talkAddObv;

	public Policy(){
		policyName = "UnknownPolicy";
		keywords = new HashSet<String>();
		talkingPoints = new HashSet<String>();
	}
	public Policy(String inName, HashSet<String> inKeywords, HashSet<String> inTalkingPoints){
		policyName = inName;
		keywords = inKeywords;
		talkingPoints = inTalkingPoints;
	}

	public void setKeyObv(KeyAddObserver inKeyAddObv){
		keyAddObv = inKeyAddObv;
	}
	public void setTalkingObv(TalkingPointAddObserver inTalkAddObv){
		talkAddObv = inTalkAddObv;
	}
	public String getName(){
		return policyName;
	}

	public void setName(String inName){
		policyName = inName;
	}

	public HashSet<String> getKeywords(){
		return keywords;
	}

	public void setKeywords(HashSet<String> inKeywords){
		keywords = inKeywords;
		for (String keyWord: inKeywords) {
			keyAddObv.notifyPeople(keyWord, this);
		}
	}

	public HashSet<String> getTalkingPoints(){
		return talkingPoints;
	}

	public void setTalkingPoints(HashSet<String> inTalkingPoints){
		talkingPoints = inTalkingPoints;
		for (String talkingPoint: inTalkingPoints) {
			talkAddObv.notifyPeople(talkingPoint, this);
		}
	}

	public void addKeyword(String inKeyword){
		if (keywords.contains(inKeyword) != true) {
			keywords.add(inKeyword);
			keyAddObv.notifyPeople(inKeyword, this);
		}
	}
	public void removeKeyword(String inKeyword){
		if (keywords.contains(inKeyword) == true) {
			keywords.remove(inKeyword);
		}	
	}

	public void addTalkingPoint(String inTalkingPoint){
		if (talkingPoints.contains(inTalkingPoint) != true) {
			talkingPoints.add(inTalkingPoint);
			talkAddObv.notifyPeople(inTalkingPoint, this);
		}
	}
	public void removeTalkingPoint(String inTalkingPoint){
		if (talkingPoints.contains(inTalkingPoint) == true) {
			talkingPoints.remove(inTalkingPoint);
		}	
	}

	public String toString(){
		String retString = "Policy Name: " + policyName;

		retString += " KeyWords:";
		for (String keyword: keywords) {
			retString+=keyword+", ";
		}
		retString += " talkingPoints:";
		for (String talkingPoint: talkingPoints) {
			retString+=talkingPoint+", ";
		}

		return retString;
	}
	
	
}