import java.util.*;


//This class keeps track of the previous results of the twitter and facebook messengers
public class ListedResults{

	private LinkedList<SearchResult> resultList;
	private HashMap<String,Integer> resultSummary;
	private HashMap<String,Long> trendyHistory;
	private boolean isTrendy;

	private class SearchResult{

		private Map<String,Integer> result;
		private long timestamp;

		public SearchResult(Map<String,Integer> inResult, long inTimestamp){
			result = inResult;
			timestamp = inTimestamp;
		}

		public Map<String,Integer> getResult(){
			return result;
		}
		public long getTimestamp(){
			return timestamp;
		}
	}

	public boolean isTrending(){return isTrendy;}

	public Map<String,Integer> getResultSummary(){
		return resultSummary;
	}

	public ListedResults(){
		resultList = new LinkedList<SearchResult>();
		isTrendy = false;
		resultSummary = new HashMap<String,Integer>();
		trendyHistory = new HashMap<String,Long>();
	}

	public void addEntry( Map<String,Integer> inResult, long inTimestamp ){
		SearchResult searchResult = new SearchResult(inResult, inTimestamp);
		resultList.add(searchResult);

		addToSummary(inResult);

		while ( (resultList.getFirst().getTimestamp() + 3600) < inTimestamp) {
			searchResult = resultList.removeFirst();
			removeFromSummary(searchResult.getResult());
		}
	}

	private void removeFromSummary(Map<String,Integer> inResult){
		
		Set stringList;
		int numOccurred;
		isTrendy = false;

		stringList = inResult.keySet();
		for (Object keyWord: stringList){
			if (resultSummary.containsKey(keyWord)) {
				numOccurred = resultSummary.get(keyWord).intValue();
				resultSummary.remove(keyWord);
			}
			else{
				numOccurred = 0;
			}

			numOccurred -= inResult.get(keyWord).intValue();

			resultSummary.put((String)keyWord,new Integer(numOccurred));

			if (numOccurred >= 50) {
				isTrendy = true;
			}
		}
	}

	public boolean addTrendyKeyword(String inKeyWord){
		boolean retVal = true;
		Long timeStamp;

		if (trendyHistory.containsKey(inKeyWord) ) {
			
			if ( trendyHistory.get(inKeyWord) >= (resultList.getLast().getTimestamp() + 3600*24) ) {
				retVal = false;	
			}
			else{
				trendyHistory.remove(inKeyWord);
			}
			
		}	
		if (retVal == true) {
			timeStamp = new Long(resultList.getLast().getTimestamp());
			trendyHistory.put(inKeyWord, timeStamp);
		}
		return retVal;
	}

	private void addToSummary(Map<String,Integer> inResult){
		
		Set stringList;
		int numOccurred;

		stringList = inResult.keySet();
		for (Object keyWord: stringList){
			if (resultSummary.containsKey(keyWord)) {
				numOccurred = resultSummary.get((String)keyWord).intValue();
				resultSummary.remove(keyWord);
			}
			else{
				numOccurred = 0;
			}

			numOccurred += inResult.get(keyWord).intValue();

			resultSummary.put((String)keyWord,new Integer(numOccurred));

			if (numOccurred >= 50) {
				isTrendy = true;
			}
		}
	}
}