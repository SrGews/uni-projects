
//This is part of the observer pattern. this will trigger when a talking piont gets trendy

public class TrendyObserver implements Observable {

	private NotificationSet notifyThesePeople;
	private Contactables contactables;

	public TrendyObserver(){
		notifyThesePeople = new NotificationSet();
		contactables = new Contactables();
	}

	public void setNotifies(NotificationSet inSet){
		notifyThesePeople = inSet;
	}

	public void setContactables(Contactables inContactables){
		contactables = inContactables;
	}

	@Override public void notifyPeople(String message){
		for (Person person: notifyThesePeople.getNotifyListKeyWords(message)) {
			person.notify(contactables,message + " is Trending");
		}
	}
	@Override public void notifyPeople(String message, Policy inPolicy){
		for (Person person: notifyThesePeople.getNotifyList(inPolicy)) {
			person.notify(contactables,message + " is Trending");
		}
	}
}