//This is part of the observer pattern. this will trigger twitContact is added


public class TwitContact extends ContactDetail{
	private String userName;

	public TwitContact(){
		userName = "";	
	}
	public TwitContact(String inName){
		userName = inName;	
	}

	public String getName(){return userName;}
	public void setName(String inName){userName = inName;}
	
	@Override public void sendMessage(Contactables contactables, String message){
		contactables.sendTwitMessage(userName, message);
	}

	public String toString(){
		return getName();
	}
}