#include "colors.inc"   
#include "shapes.inc"
#include "textures.inc"                                                                


      
              
background{
    color DarkSlateGrey
}   

light_source {<400,2000,1000> color White}
                                 
#declare fur = pigment{
    wood                        //other textures are in the textures.ini file
    color_map{
        [0.0 color DarkTan]     //wood will blend from dark tan to dark brown 0 - 90% of the texture, then blend to VeryDarkBrown over the last 10%
        [0.9 color Grey]
        [1.0 color White]
    }
    turbulence 0.075            //blends it to look more like wood
    scale <0.2,0.3,1>           //scales the texture from covering a default radius = 1 sphere. If the scaling is uniform can read "scale 4" instead
}                                 

                                                         

#declare eye = 
difference{
    sphere{
        <0,0,0>,1                               //<x,y,z> coords followed by radius of sphere
        texture{
            pigment{
                color White                //Instead of yellow can use "color red 0.5 green 0.5 blue 0.05" or "color rgb <0.5, 0.5, 0.05>"
            }        
            finish{
                phong 1                     //adds a highlight of the light source on the surface. can be between 0 and 1
            }
        }
    }
    sphere{
        <0,0,-1>,0.5                               //<x,y,z> coords followed by radius of sphere
        texture{
            pigment{
                color Black                //Instead of yellow can use "color red 0.5 green 0.5 blue 0.05" or "color rgb <0.5, 0.5, 0.05>"
            }        
            finish{
                phong 1                     //adds a highlight of the light source on the surface. can be between 0 and 1
            }
        }
    }
}       
               


#declare nose =
union{
    cone{
        <0,0,-1>, 0                            //center and radius of one end
        <0,0,0>, 0.5                            //center and radius of other end
        open                                    //removes end caps
        pigment{                                
            color Brown
        }
    }
    
    cone{
        <0,0.75,0>, 0                            //center and radius of one end
        <0.25,-0.25,0>, 0.5                            //center and radius of other end
        open                                    //removes end caps
        pigment{                                
            color Brown
        }
    }
    cone{
        <0,0.75,0>, 0                            //center and radius of one end
        <-0.25,-0.25,0>, 0.5                            //center and radius of other end
        open                                    //removes end caps
        pigment{                                
            color Brown
        }
    }
}



#declare head =
union{
    sphere{
        <0,0,0>,5
        texture{
            pigment{
                fur
            }    
        }
    }
    object{ eye translate <2,2.5,-4>}
    object{ eye translate <-2,2.5,-4>}
    object{ nose translate <0,0,-3.25> scale 1.5}
}
        

#declare torso =
box{
    <0,0,0>                               //lower left corner
    <4,2,1>                               //upper right corner
    pigment{
        fur
    }    
}

#declare gut =
box{
    <0,0,0>                               //lower left corner
    <5,7.5,1.5>                               //upper right corner
    pigment{
        fur
    }    
}    

    
        



#declare arm =
union{
    cylinder{
        <0,0,0>,                            //Center of one end
        <2,0,0>,                          //center of other end
        0.2                                     //radius
        open
        pigment{
            fur
        }
    }           
    
    box{
        <2,-0.2,-0.2>,
        <2.4,0.2,0.2>
    
    }   
}

#declare leg =
union{
    cylinder{
        <0,0,0>,                            //Center of one end
        <0,-3,0>,                          //center of other end
        0.2                                     //radius
        open
        pigment{
            fur
        }
    }           
    
    box{
        <0.25,-2.8,-0.2>,
        <-0.25,-3.2,0.5>
    
    }   
}    

#declare tail =
union{
    cylinder{
        <0,0,0>,
        <1,-0.1,0>,
        0.2
        
        pigment{fur}
    }
    cylinder{
        <1,-0.1,0>,
        <1.3,-1,0>,
        0.2
        
        pigment{fur}
    }
    cylinder{
        <1.3,-1,0>,
        <2.3,-1,0.3>,
        0.2
        
        pigment{fur}
    }
    cylinder{
        <2.3,-1,0>,
        <2.8,-0.3,-0.1>,
        0.2
        
        pigment{fur}
    }
}        
    

#declare body =
union{
    object{head scale 0.4 rotate y*180 translate <2,4,0.5>}
    object{torso}
    object{gut translate <-0.5,-7.5,-0.25>}
    object{arm scale 2 rotate <0,0,40> rotate x*clock*360 translate <4,1,0.5> }                
    object{arm scale -2 rotate <0,0,-40> rotate x*clock*360 translate <0,1,0.5> }
    object{leg scale 2 translate <3,-7.5, 0.15>}
    object{leg scale -2 rotate x*180 translate<1,-7.5,0.15>}
    object{tail scale 2 rotate y*90 translate <2.5,-5,0>}                
    
    rotate y*180
    translate x*2                
}
object{body rotate <0,0,0>}

camera{ location  <0,5,-40+clock*40>                  //initial location of camera
                                            
        up<0,1,0>                           //up/right = aspect ratio of hieght to width of the image
        right<1,0,0>
        look_at<0,0,0>                      //a point the camera will point to
        
        
      }    