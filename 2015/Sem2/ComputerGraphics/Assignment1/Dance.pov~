#include "colors.inc"   
#include "shapes.inc"
#include "textures.inc"                                                                 
#include "shapesq.inc"
#include "stones2.inc"
#include "shapes2.inc"





                                 
#declare fur = pigment{
    wood                        //other textures are in the textures.ini file
    color_map{
        [0.0 color DarkTan]     //wood will blend from dark tan to dark brown 0 - 90% of the texture, then blend to VeryDarkBrown over the last 10%
        [0.9 color Grey]
        [1.0 color White]
    }
    turbulence 0.075            //blends it to look more like wood
    scale <0.2,0.3,1>           //scales the texture from covering a default radius = 1 sphere. If the scaling is uniform can read "scale 4" instead
}                                                                                         

#declare eye = 
difference{
    sphere{
        <0,0,0>,1                               //<x,y,z> coords followed by radius of sphere
        texture{
            pigment{
                color White                //Instead of yellow can use "color red 0.5 green 0.5 blue 0.05" or "color rgb <0.5, 0.5, 0.05>"
            }        
            finish{
                phong 1                     //adds a highlight of the light source on the surface. can be between 0 and 1
            }
        }
    }
    sphere{
        <0,0,-1>,0.5                               //<x,y,z> coords followed by radius of sphere
        texture{
            pigment{
                color Black                //Instead of yellow can use "color red 0.5 green 0.5 blue 0.05" or "color rgb <0.5, 0.5, 0.05>"
            }        
            finish{
                phong 1                     //adds a highlight of the light source on the surface. can be between 0 and 1
            }
        }
    }
}                      

#declare nose =
union{
    cone{
        <0,0,-1>, 0                            //center and radius of one end
        <0,0,0>, 0.5                            //center and radius of other end
        open                                    //removes end caps
        pigment{                                
            color Brown
        }
    }
    
    cone{
        <0,0.75,0>, 0                            //center and radius of one end
        <0.25,-0.25,0>, 0.5                            //center and radius of other end
        open                                    //removes end caps
        pigment{                                
            color Brown
        }
    }
    cone{
        <0,0.75,0>, 0                            //center and radius of one end
        <-0.25,-0.25,0>, 0.5                            //center and radius of other end
        open                                    //removes end caps
        pigment{                                
            color Brown
        }
    }
}

#declare head =
union{
    sphere{
        <0,0,0>,5
        texture{
            pigment{
                fur
            }    
        }
    }
    object{ eye translate <2,2.5,-4>}
    object{ eye translate <-2,2.5,-4>}
    object{ nose translate <0,0,-3.25> scale 1.5}
}
        
#declare torso =
box{
    <0,0,0>                               //lower left corner
    <4,2,1>                               //upper right corner
    pigment{
        fur
    }    
}

#declare gut =
box{
    <0,0,0>                               //lower left corner
    <5,7.5,1.5>                               //upper right corner
    pigment{
        fur
    }    
}      

#declare arm =
union{
    cylinder{
        <0,0,0>,                            //Center of one end
        <2,0,0>,                          //center of other end
        0.2                                     //radius
        open
        pigment{
            fur
        }
    }           
    
    box{
        <2,-0.2,-0.2>,
        <2.4,0.2,0.2>
    
    }   
}

#declare leg =
union{
    cylinder{
        <0,0,0>,                            //Center of one end
        <0,-3,0>,                          //center of other end
        0.2                                     //radius
        open
        pigment{
            fur
        }
    }           
    
    box{
        <0.25,-2.8,-0.2>,
        <-0.25,-3.2,0.5>
    
    }   
}

#declare tail =
union{
    cylinder{
        <0,0,0>,
        <1,-0.1,0>,
        0.2
        
        pigment{fur}
    }
    cylinder{
        <1,-0.1,0>,
        <1.3,-1,0>,
        0.2
        
        pigment{fur}
    }
    cylinder{
        <1.3,-1,0>,
        <2.3,-1,0.3>,
        0.2
        
        pigment{fur}
    }
    cylinder{
        <2.3,-1,0>,
        <2.8,-0.3,-0.1>,
        0.2
        
        pigment{fur}
    }
}    

#declare body =
union{
    object{head scale 0.4 rotate y*180 translate <2,4,0.5>}
    object{torso}
    object{gut translate <-0.5,-7.5,-0.25>}
    object{arm scale 2 rotate <0,0,70> rotate x*clock*360 translate <4,1,0.5> }                
    object{arm scale -2 rotate <0,0,-70> rotate x*clock*360 translate <0,1,0.5> }
    object{leg scale 2 translate <3,-7.5, 0.15>}
    object{leg scale -2 rotate x*180 translate<1,-7.5,0.15>}                
    object{tail scale 2 rotate y*90 translate <2.5,-5,0>}                
    pigment{fur}
    rotate y*180
    translate x*2                
}





#declare waterBlob =
blob {
   threshold 0.6
   component 1.0, 1.0, <0.75, 1.5, 0>
   component 1.0, 1.0, <-0.375, 2, 0>
   component 1.0, 1.0, <-0.375, 1, 0>

   texture{
    Water
    scale 1
   }
   scale 5
   rotate <360*clock,720*clock,1080*clock>
   translate <0,30,0>
}




#declare pyramid = intersection {                       //Based on Pyramid2 in shapes2.inc
   plane { < 1, 0,  0>, 1  rotate <  0, 0,  30>}
   plane { <-1, 0,  0>, 1  rotate <  0, 0, -30>}
   plane { < 0, 0,  1>, 1  rotate <-30, 0,   0>}
   plane { < 0, 0, -1>, 1  rotate < 30, 0,   0>}
   plane { <0, -1, 0>, 0 }
   translate <0 ,-1, 0>
   
   bounded_by {box {<-1,0,-1>, <1,1,1>}}
   
   texture{
        Yellow
   }
   
   
   scale 40
   rotate y*-30
   translate <-50,0,70>
   
   
}

#declare skyBox =
object{ Hexagon 
    
    texture{
        pigment{
            MediumForestGreen
        }
        normal{
             bumps 0.4
             scale 0.2
        }    
    }
   
    scale 10
    translate <-20,20,50>
    rotate y*90
}        










camera{ location  <0,100,-100>                  //initial location of camera
                                            
        up<0,1,0>                           //up/right = aspect ratio of hieght to width of the image
        right<1,0,0>
        look_at<0,0,0>                      //a point the camera will point to
      }      
              
background{
    color DarkSlateGrey
}   

light_source {<400,2000,1000> color White}
light_source {<0,30,0> color Blue}
light_source {<10,20,-45> color rgb <(clock-clock*clock)*3,(clock-clock*clock)*0.5,(clock-clock*clock)>}
                                                          

plane{                                      
    <0,1,0>,0                               //instead of <0,1,0> could use "y" because it is the normal to the y plane
    pigment{
        color rgb <0.8,0.45,0.25>
    }        
}





object{body translate <40,14,0> rotate <0,clock*360,0>}
object{body translate <40,14,0> rotate <0,clock*360+45,0>}
object{body translate <40,14,0> rotate <0,clock*360+90,0>}
object{body translate <40,14,0> rotate <0,clock*360+135,0>}
object{body translate <40,14,0> rotate <0,clock*360+180,0>}
object{body translate <40,14,0> rotate <0,clock*360+225,0>}
object{body translate <40,14,0> rotate <0,clock*360+270,0>}
object{body translate <40,14,0> rotate <0,clock*360+315,0>}

object{waterBlob}
object{pyramid}
object{skyBox translate <30-clock*180,20,-40>}
//object{Bicorn texture{Glass3} scale 10 translate <15+clock*-15,10,5>}
object{Bicorn texture{Glass3} scale 10 translate <15,10,5>}
                                                                          
