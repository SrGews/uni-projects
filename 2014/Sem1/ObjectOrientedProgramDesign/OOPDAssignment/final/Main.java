/*********************************************************************************/
/*Program Assignment: 	OOPD Assignment						*/
/*Name:			Angus Campbell						*/
/*Date:			03/06/2014						*/
/*Description:		Runs a game of single player battleships using a 	*/
/*			textfile map						*/
/********************************************************************************/

import io.*;

public class Main
	{

	public static void main(String [] args)
		{
		TextFile newFile;
		BattleShips seaMap;

		String title;

		int mapSize;
	
		boolean fileState;

/********************************************************************************/
/*The following section asks for a map file location and checks it is valid. 	*/
/*If not it will repeat itself until a valid location is entered		*/
/*seaMap is then initialised and populated					*/
/********************************************************************************/
		do{
			title = ConsoleInput.readWord("What is the file to be read in?");
		
			newFile = new TextFile(title, "r");
	
			}while(newFile.openFile() == false);
	
		mapSize = newFile.readInt();
	
		seaMap = new BattleShips(mapSize, newFile);
	
		seaMap.fillMap();
/********************************************************************************/
/*The following loop is a turn for the game which when done will aske if you 	*/
/*want to see the full map or just the hits and shows the appropriate map, then */
/*checks if all ships have been sunk. If not then it repeats.			*/
/*										*/
/*Once the game is finished it outputs a funny reference and tells the user they*/
/*won. The TextFile is then closed						*/
/********************************************************************************/
		do
			{
			seaMap.requestPosition();
			seaMap.checkHit();
			if (ConsoleInput.readChar("View ships? y/n ") == 'y')
				{
				seaMap.showFullMap();
				} 
			else
				{
				seaMap.showHitMap();
				}
			}while(seaMap.getHitsLeft() > 0);
	
		System.out.println("You sunk my battleship!");
		System.out.println("Congratulations, you win!");
		newFile.closeFile();
		
		
		
		}
	
	}
