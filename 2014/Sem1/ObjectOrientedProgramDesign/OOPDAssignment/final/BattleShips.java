/*********************************************************************************/
/*Program Assignment: 	OOPD Assignment						*/
/*Name:			Angus Campbell						*/
/*Date:			03/06/2014						*/
/*Description:		This class holds a map which can be filled with entries */
/*			from a TextFile object. All functions to compare map 	*/
/*			positions with inputs by the user are contained.	*/
/********************************************************************************/

import io.*;

public class BattleShips extends Map
	{
	int hitsLeft, mapSize;
	Position position;
	TextFile file;

/********************************************************************************/
/*The default constructor sets mapsize to the super's default map size. This 	*/
/*should not be used by Main.java. The alternate constructor should be called.	*/
/********************************************************************************/
	public BattleShips()
		{
		super();
		position = new Position();
		mapSize = 10;
		}

	public BattleShips(int inMapSize, TextFile inFile)
		{
		super(inMapSize);
		position = new Position();
		mapSize = inMapSize;
		file = inFile;
		}



/********************************************************************************/
/*This imperative goes through the TextFile object and transfers the characters */
/*to the MAP object. i represents the map's rows, and j the columns. It also 	*/
/*finds the total number of ship 'parts' that must be hit to end the game.	*/
/********************************************************************************/
	public void fillMap()
		{
		char element;
		hitsLeft = 0;
		for(int i=0; i<mapSize; i++)
			{
			file.readChar();
			for(int j=0; j<mapSize; j++)
				{
				element = file.readChar();
				super.setMapElement(i,j,element);
				if (element == '#')
					{
					hitsLeft += 1;
					}
				}
			}
		
		}
	
	
	public int getHitsLeft()
		{
		return hitsLeft;
		}
	

	public void requestPosition()
		{
		int temp;
		
		temp = ConsoleInput.readInt("Where will you fire?");
		position.setRow(temp);
		
		temp = ConsoleInput.readInt();
		position.setColumn(temp);
		}

/********************************************************************************/
/*This imperative checks if the position the Position object represents in super*/
/* is a ship. If so super is updated to X, and hitsLeft is updated.		*/
/********************************************************************************/
	public void checkHit()
		{
		char element;
		element = super.getMapElement(position.getColumn(), position.getRow());
		
		if (element == '#')
			{
			System.out.println("HIT!");
			super.setMapElement(position.getColumn(), position.getRow(), 'X');
			hitsLeft -= 1;
			}
		else
			{
			System.out.println("Miss");
			}
		}
		

/********************************************************************************/
/*The following imperatives both print the map to screen. The first imperative 	*/
/*will print the full map but will replace all SHIP characters (#) With SEA 	*/
/*characters (-). The second will print the full map showing hits, ship 	*/
/*locations and hits.								*/
/********************************************************************************/
	public void showHitMap()
		{
		char element;
		for(int i=0; i<mapSize; i++)
			{
			for(int j=0; j<mapSize; j++)
				{
				element = super.getMapElement(i,j);
				if (element == '#')
					{
					element = '-';
					}
				System.out.print(element);
				}
			System.out.print('\n');
			}
		
		}

	public void showFullMap()
		{
		for(int i=0; i<mapSize; i++)
			{
			for(int j=0; j<mapSize; j++)
				{
				System.out.print(super.getMapElement(i,j));
				}
			System.out.print('\n');
			}
		}
	
	}














