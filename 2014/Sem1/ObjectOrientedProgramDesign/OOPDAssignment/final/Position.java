/********************************************************************************/
/*Program Assignment: 	OOPD Assignment						*/
/*Name:			Angus Campbell						*/
/*Date:			03/06/2014						*/
/*Description:		This class holds a position in a 2d array.		*/
/*			The class contains constructors, mutators, accessors and*/
/* 			an imperative						*/
/********************************************************************************/


public class Position
	{

	private int row, column;

	public Position()
		{
		row = 0;
		column = 0;
		}

	public Position(int inRow, int inColumn)
		{
		if (validate(inRow))
			{
			row = inRow-1;
			}
		else
			{
			row = 0;
			}

		if (validate(inColumn))
			{
			column = inColumn-1;
			}
		else
			{
			column = 0;
			}
		}

	public void setRow(int inRow)
		{
		if (validate(inRow))
			{
			row = inRow-1;
			}
		}

	public void setColumn(int inColumn)
		{
		if (validate(inColumn))
			{
			column = inColumn-1;
			}
		
		}

	public void setBoth(int inRow, int inColumn)
		{
		setColumn(inColumn);
		setRow(inRow);
		}

	public int getRow()
		{
		return row;
		}

	public int getColumn()
		{
		return column;
		}

	private boolean validate(int inCoordinate)
		{
		boolean result;
		
		result = false;
		if (inCoordinate > 0)
			{
			result = true;
			}
		
		return result;
		}
	}


