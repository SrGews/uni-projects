import io.*;

public class Main
{

public static void main(String [] args)
	{
	TextFile mapFile;
	BattleShips shipMap;
	String fileTitle;
	int theMapSize;
	boolean shipsLeft;
	BattleShips theMap;


	fileTitle = ConsoleInput.readWord("What is the file called?");
	mapFile = new TextFile(fileTitle, "r");
	mapFile.openFile();

	theMapSize = mapFile.readInt();
	mapFile.closeFile();

	shipMap = new BattleShips(fileTitle, theMapSize);

	shipMap.readMapFromFile();

	shipsLeft = (shipMap.getHitsLeft() > 0);
	do
		{
		shipMap.takeATurn();
		if (ConsoleInput.readChar("Do you wish to see the ships? (y/n)") == 'y')
			{
			shipMap.displayMapFull();
			}
		else
			{
			shipMap.displayMapHits();
			}
		shipsLeft = (shipMap.getHitsLeft() < 0);

		System.out.println(shipMap.getHitsLeft());
		}while(shipsLeft);

	System.out.println("You sunk my battleship! You win!");

	shipMap= new BattleShips(fileTitle,theMapSize);
	
	char element;
	for(int i=0; i<theMapSize; i++)
	{
		for(int j=0; j<theMapSize;j++)
		{
			element = mapFile.readChar();
			shipMap.setMapElement(i,j,element);
			System.out.print(element);	
		}

	}
}

}
