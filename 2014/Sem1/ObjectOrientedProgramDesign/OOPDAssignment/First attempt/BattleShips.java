import io.*;

public class BattleShips extends Map
{
private TextFile mapFile;

private int hitsLeft;
private int mapSize;
private String fileName;

//DEFAULT Constructor
public BattleShips()
	{
	super();
	hitsLeft = 1;
	fileName = "final/game1.dat";
	mapFile = new TextFile(fileName,"r");
	mapSize = mapFile.readInt();
	}

public BattleShips(String inFileName, int inMapSize)
	{
	super(inMapSize);
	hitsLeft = 1;
	fileName = inFileName;
	mapFile = new TextFile(fileName,"r");
	mapSize = inMapSize;
	System.out.println(mapSize);
	}

//Accessors
public String getFileName()
	{
	return fileName;
	}

public char getMapElement(int inRow, int inColumn)
	{
	char result;
	
	result = super.getMapElement(inRow, inColumn);
	
	return result;
	}

public int getHitsLeft()
	{
	return hitsLeft;
	}

//Mutators
public void setFileName(String inFileName)
	{
	fileName = inFileName;	
	}

public void askFileName()
	{
	fileName = ConsoleInput.readWord("What is the File called?");
	}

public void setHitsLeft(int inHitsLeft)
	{
	hitsLeft = inHitsLeft;
	}

public void setMapElement(int inRow, int inColumn, char inElement)
	{
	super.setMapElement(inRow, inColumn, inElement);
	}

public void setTextFile()
	{
	mapFile = new TextFile(fileName,"r");
	}

//Functional methods
public void readMapFromFile()
	{
	//char element;
	hitsLeft = 0;
	for(int loopRow = 0; loopRow<20/*mapSize*/; loopRow++)
	{
		for(int loopColumn = 0; loopColumn<20/*mapSize*/; loopColumn++)
		{
			//element = mapFile.readChar();
			//System.out.printf("%c IAMELEMENTAL", element);
			/*if (element == '#');
				{
				hitsLeft += 1;
				}*/
			super.setMapElement(loopRow, loopColumn, mapFile.readChar());
		}
	}
	System.out.println("hitsLeft  "+hitsLeft);
	}


public void takeATurn()
	{
	int row, column;

	row = inputInt("row");
	column = inputInt("column");
	
	if (super.getMapElement(row, column) == '#')
		{
		super.setMapElement(row, column, 'X');
		hitsLeft --; 
		}
	
	}
	
public void displayMapHits()
	{
	char element;
	for(int loopRow = 0; loopRow<mapSize; loopRow ++)
		{
		for(int loopColumn = 0; loopColumn<mapSize; loopColumn ++)
			{
			element = super.getMapElement(loopRow, loopColumn);
			if (element == '#')
				{
				System.out.print('-');
				}
			else
				{
				System.out.print(element);
				}
			}
		System.out.print('\n');
		}
	}

public void displayMapFull()
	{
	char element;
	for(int loopRow = 0; loopRow<mapSize; loopRow ++)
		{
		System.out.println("");
		for(int loopColumn = 0; loopColumn<mapSize; loopColumn ++)
			{
			element = super.getMapElement(loopRow, loopColumn);
			System.out.print(""+element);
			}
		//System.out.print('\n');
		}
	}


	
	
public void closeTheFile()
	{
	mapFile.closeFile();
	}	

private int inputInt(String value)
	{
	int result;
	do
		{
		result = ConsoleInput.readInt("Enter the "+value+" you wish to attack.");
		}while (result<0);
	return result;
	}
	
}














