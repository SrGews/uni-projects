/********************************************************************************/
/*Program Assignment: 	3							*/
/*Name:			Angus Campbell						*/
/*Date:			22/05/2014						*/
/*Description:		This program reads in up to 2 files input by the user	*/
/*and calculates relative size ranges for very small, small, medium, large and 	*/
/*very large ranges using standard deviation. 					*/
/********************************************************************************/

/********************************************************************************/
/*Listing Contents:								*/
/*	Reuse instructions							*/
/********************************************************************************/

/********************************************************************************/
/*math.h is needed for the sqrtf() and sqrt() functions and stdlib.h is needed 	*/
/*to use the atoi() function							*/
/********************************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/********************************************************************************/
/*The structure dataValuePair is used later to make a list of varX and varY of 	*/
/*equal length.									*/
/*varX is used to hold the data of the first file read in which must contain the*/
/*sizes measured while varY holds the number of items that varX represents. If 	*/
/*only one file is read in varY is automatically set to 1			*/
/********************************************************************************/

typedef struct
	{
	double varX,varY;
	}dataValuePair;

int main (void)
{
/********************************************************************************/
/*sumOfLogX is the sum or log(varX) for all varX				*/
/*avgXPerY is the average value of sumOfLogX for each varX			*/
/*vSmall,small,medium,large and vLarge are the size measurements		*/
/*sumLnTakeAvg is the sum of (log(varX)-avgXPerY)^2 for all varX		*/
/*variance and stdDev are the variance and standard deviation of the data	*/
/********************************************************************************/
double sumOfLogX, avgXPerY, vSmall, small, medium, large, vLarge, sumLnTakeAvg, variance, stdDev;


/********************************************************************************/
/*dataTitleX is the title for the data varX					*/
/*dataTitleY is the title for the data varY					*/
/*dataTitleX and dataTitleY are both read in from the first lines of the files	*/
/*resultLineX is a temporary variable for the result of all fgets() and sscanf	*/
/*statements which relate to VarX						*/
/*resultLineY is a temporary variable for the result of all fgets() and sscanf	*/
/*statements which relate to VarY						*/
/*filenameX is input by the user. It is the address of the file which is to be	*/
/*read in for varX								*/
/*filenameY is input by the user. It is the address of the file which is to be	*/
/*read in for varY								*/
/********************************************************************************/
char dataTitleX[50], dataTitleY[50], resultLineX[50],resultLineY[50], fileNameX[50], fileNameY[50];		

/********************************************************************************/
/*i is used for loops								*/
/*numberOfLinesX and numberOfLinesY are each the number of lines of data there	*/
/*are in each folder and are used to controle the for loops. They are read in at*/
/* the start of the data files. They should be equal.				*/	
/*numberOfFiles is the number of files to be read in. this is input by the user	*/
/********************************************************************************/
int i, numberOfLinesX, numberOfLinesY, numberOfFiles;

/********************************************************************************/
/*dataFileX and dataFileY are the file pointers for the INPUT data		*/
/*resultFile is the file where the results will be written and saved. 		*/
/*This data is vSmall through to vLarge as well as the data titles		*/
/********************************************************************************/

FILE * dataFileX, *dataFileY, *resultFile;					

/********************************************************************************/
/*This section of code asks the user what files contain the relevant data	*/
/*They should be arranged as:	TitleOfData					*/
/*				NoOfLinesOfData					*/
/*				data1						*/
/*				data2						*/
/*				data3						*/
/*This example only works if NoOfLinesOfData = 3. No line can be greater than 50*/
/*characters or the code will not function correctly.				*/
/*The user is also asked for the number of files to be read in.			*/
/********************************************************************************/

printf("How many files are to be read in?");
scanf("%i", &numberOfFiles);


printf("What is the first file called?\n");
scanf("%s", &fileNameX);


if(numberOfFiles == 2)
	{
	printf("What is the second file called?\n");
	scanf("%s", &fileNameY);
	}

/********************************************************************************/
/*The following section of code opens the files which contain the sets of data 	*/
/*and the location the data will be written.					*/
/*The following code then reads the title of the data from the first line of the*/
/*data file and the next line which is the number of lines of data in the files.*/
/*It does this for both data files if there are two files to be read in		*/
/********************************************************************************/

dataFileX = fopen(fileNameX, "r");

if(numberOfFiles == 2)
	{
	dataFileY = fopen(fileNameY, "r");
	}

resultFile = fopen("result.txt", "w+");				

fgets(dataTitleX, 50, dataFileX);
fgets(resultLineX, 50, dataFileX);
sscanf(resultLineX,"%s",&resultLineX);
numberOfLinesX = atoi(resultLineX);				 


if(numberOfFiles == 2)
	{
	fgets(dataTitleY, 50, dataFileY);
	fgets(resultLineY, 50, dataFileY);
	sscanf(resultLineY,"%s",&resultLineY);
	numberOfLinesY = atoi(resultLineY);
	}


/********************************************************************************/
/*Here variables are initialised so they behave correctly			*/
/*dataFull is initialised as a list to hold all varX and varY read in from the 	*/
/*files										*/ 
/********************************************************************************/


dataValuePair dataFull[numberOfLinesX];

sumLnTakeAvg = 0;

/********************************************************************************/
/*These loops read the data in from the files and store them in the data list.	*/
/*They also perform the calculations to find sumOfLogX				*/
/********************************************************************************/


for (i = 0; i<numberOfLinesX; i++)
	{
	fscanf(dataFileX,"%lf",&dataFull[i].varX);
	}


if(numberOfFiles == 2)
	{								
	for (i = 0; i<numberOfLinesY; i++)
		{
		fscanf(dataFileY,"%lf",&dataFull[i].varY);	
	
		sumOfLogX = sumOfLogX + log(dataFull[i].varX/dataFull[i].varY);
		}
	
	}
else
	{
	for (i = 0; i<numberOfLinesX; i++)
		{	
		sumOfLogX = sumOfLogX + log(dataFull[i].varX);
		dataFull[i].varY = 1;
		}
	}

/********************************************************************************/
/*This section performs the majority of the calculations			*/
/*Here avgXPerY, sumLnTakeAvg, variance, stdDev and vsmall through to vLarge are*/
/*calculated									*/
/********************************************************************************/


avgXPerY = sumOfLogX/numberOfLinesX;

for (i = 0; i<numberOfLinesX; i++)
	{

	sumLnTakeAvg = sumLnTakeAvg + (log(dataFull[i].varX/dataFull[i].varY) - avgXPerY)*(log(dataFull[i].varX/dataFull[i].varY) - avgXPerY);

	}


variance = sumLnTakeAvg / (numberOfLinesX - 1);

stdDev = sqrt(variance);



vSmall 	= exp(avgXPerY - ((double)2)*stdDev);
small 	= exp(avgXPerY - stdDev);
medium 	= exp(avgXPerY);
large 	= exp(avgXPerY + stdDev);
vLarge  = exp(avgXPerY + ((double)2)*stdDev);

/********************************************************************************/
/*This section outputs the results to resultFile (result.txt).			*/
/*The output will state the titles of the data used as well as vSmall, small, 	*/
/*medium, large and vLarge as well as some flavour text to remind the user which*/
/*is which									*/
/*The program then closes all open files which are in use by the program	*/
/********************************************************************************/

fprintf(resultFile, "The files read in contain:\n%s%sVery Small: %lf\nSmallL:     %lf\nMedium:     %lf\nLarge:      %lf\nVery Large: %lf\n",dataTitleX,dataTitleY,vSmall,small,medium,large,vLarge);
							
fclose(dataFileX);

if(numberOfFiles == 2)
	{
	fclose(dataFileY);
	}
fclose(resultFile);
return 0;
}
