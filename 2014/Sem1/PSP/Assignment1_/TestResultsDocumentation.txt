The test cases are in the directories test 1 and test 2 respectively.

Each test directory contains files labelled result.txt and an input file. While running the code in the same directory as the input file the user must input the input file's name. 

So for test one from the console (while in the directory containing meanStdDevCalc.c) typed "gcc -lm meanStdDevCalc.c" into the console then input "./a.out" into the console.
The program requests which file I want to retrieve the data from, so for test1 I typed "data32.txt" and for test 2 I typed "data.txt".
The program then output the results in the same directory in a file called "result.txt".

Both test cases output a file with correct results.

The total run time excluding user input was less than a second.


All the data in failed is from my first attempt at the assignment. I was on the wrong track so I started again.
