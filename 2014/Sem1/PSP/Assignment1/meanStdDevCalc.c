#include <stdio.h>	/* math.h is needed for the sqrtf() function and stdlib.h is needed to use the atoi() function*/
#include <math.h>	
#include <stdlib.h>

int main (void)
{
float stdDeviation, mean, sumNumbers, sumMeanDifferences; 	/* stdDeviation and mean are the end results for the standard deviation and mean of the data set. sumNumbers is
 								the sum of all the numbers in the data set. sumMeanDifferences is for the sum of (each data set-mean)^2*/
char dataTitle[50], resultLine[50], fileName[50];		/*dataTitle is the title of the data which will be read in from the first line of the file. resultLine is a
								temporary variable for the result of all fgets() statements. fileName is the user input of the name of the file to
								be read, the user will input this when asked by the program.*/
int i, numberOfLines;						/*i is for loops later. numberOfLines is a number read from the file which states the number of data values which 									follow. */
FILE * dataFile, *resultFile;					/* dataFile is the pointer for the INPUT data. resultFile is the pointer to where the EXPORT data will be written
								which is the name of the data, the std deviation of the data, and the mean of the data.*/

printf("What is the file called?\n");
scanf("%s", fileName);						/*This asks the user what file it is to get the data from ie the INPUT data. This should be of
								type:	TitleOfData
									NoOfLinesOfData
									data1
									data2
									data3
								*/    /*the example is if NoOfLinesOfData = 3. No line can be greater than 50 characters long or the program will 									not work correctly*/

dataFile = fopen(fileName, "r");
resultFile = fopen("result.txt", "w+");				/*This opens the dataFile and the location the data is to be written*/

fgets(dataTitle, 50, dataFile);					/*This line is to read the first line of the dataFile which MUST BE the name of the data with no spaces.*/

fgets(resultLine, 50, dataFile);
sscanf(resultLine,"%s",&resultLine);
numberOfLines = atoi(resultLine);				/*These lines retrieve the second line of the dataFile and converts it from a string to an int. The second line of 									the dataFile MUST BE the number of data points.*/

float data[numberOfLines];					/*Declares the array that will hold the entirety of the data from dataFile excluding the TitleOfData and
								noOfLines. Each data point after the noOfLines MUST BE on it's own line with NO spaces.*/

sumNumbers = 0;

for (i = 0; i<numberOfLines; i++)
	{
	fscanf(dataFile,"%f",&data[i]);	
	sumNumbers = sumNumbers+data[i];
	}							/*This reads the data points from dataFile and assigns them to the array of variables, data. It then adds each
								value to sumNumbers to generate the sum of all the data points.*/


mean = sumNumbers/numberOfLines;				/*This calculates the mean of the set of data*/

for (i = 0; i<numberOfLines; i++)
	{
	sumMeanDifferences = sumMeanDifferences + (mean - data[i])*(mean - data[i]);
	}							/*This loop calculates the sum of the mean minus each data squared or the sum of ((mean-data)^2) for all data
								points. This is used to calculate the standard deviation of the data in the next step.*/

stdDeviation = sqrtf(sumMeanDifferences/(numberOfLines - 1));	/*This calculates the std deviation by dividing sumMeanDifferences by (n-1) and finding the square root of the
								result*/

fprintf(resultFile, "For the data %s%f is the mean of the data.\n%f is the std deviation of the data.\n",dataTitle, mean, stdDeviation);
								/*This outputs the results to the resultFile. The output will first state the data's title then the mean then the
								std Dev. Each has some flavour text to remind the user which is which.*/

fclose(dataFile);
return 0;
}
