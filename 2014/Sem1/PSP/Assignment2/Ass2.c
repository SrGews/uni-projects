/********************************************************************************/
/*Program Assignment: 	2							*/
/*Name:			Angus Campbell						*/
/*Date:			29/04/2014						*/
/*Description:		This program reads in 2 files and an estimate input by 	*/
/*the user, and calculates the linear regression parameters and correlation 	*/
/*coefficients as well as an improved prediction for x given the past data.	*/
/********************************************************************************/

/********************************************************************************/
/*Listing Contents:								*/
/*	Reuse instructions							*/
/********************************************************************************/

/********************************************************************************/
/*math.h is needed for the sqrtf() and sqrt() functions and stdlib.h is needed 	*/
/*to use the atoi() function							*/
/********************************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/********************************************************************************/
/*The structure dataValuePair is used later to make a list of varX and varY of 	*/
/*equal length.									*/
/********************************************************************************/

typedef struct
	{
	float varX,varY;
	}dataValuePair;

int main (void)
{
/********************************************************************************/
/*sumOfVarXTimesVarY is the sum of data[n].varX*data[n].varY for all valid n	*/
/*sumOfVarX and sumOfVarY are the sum of all data in data.varX and data.varY	*/
/*sumOfVarXSquared is the sum of data[n].varX*data[n].varX for all valid n	*/
/*sumOfVarYSquared is the sum of data[n].varY*data[n].varY for all valid n	*/
/*avgVarX and avgVarY are the averages for all data.varX and data.varY		*/
/*betaOne and betaZero are the regression parameters for the data sets		*/
/*rXY and rSquared are the correlation coefficients for the data sets		*/
/*varY is the improved prediction						*/
/*varX is the initial prediction						*/
/********************************************************************************/
float sumOfVarXTimesVarY, sumOfVarX, sumOfVarY, sumOfVarXSquared, sumOfVarYSquared, avgVarX, avgVarY, betaOne, betaZero,rXY,rSquared, varY, varX;


/********************************************************************************/
/*dataTitleX is the title for the data varX					*/
/*dataTitleY is the title for the data varY					*/
/*dataTitleX and dataTitleY are both read in from the first lines of the files	*/
/*resultLineX is a temporary variable for the result of all fgets() and sscanf	*/
/*statements which relate to VarX						*/
/*resultLineX is a temporary variable for the result of all fgets() and sscanf	*/
/*statements which relate to VarY						*/
/*filenameX is input by the user. It is the address of the file which is to be	*/
/*read in for varX								*/
/*filenameY is input by the user. It is the address of the file which is to be	*/
/*read in for varY								*/
/********************************************************************************/
char dataTitleX[50], dataTitleY[50], resultLineX[50],resultLineY[50], fileNameX[50], fileNameY[50];		

/********************************************************************************/
/*i is used for loops								*/
/*numberOfLinesX and numberOfLinesY are each the number of lines of data there	*/
/*are in each folder and are used to controle the for loops. They are read in at*/
/* the start of the data files. They should be equal				*/	
/********************************************************************************/
int i, numberOfLinesX, numberOfLinesY;	
/********************************************************************************/
/*dataFileX and dataFileY are the file pointers for the INPUT data		*/
/*resultFile is the file where the results will be written and saved. This data is betaOne, betaZero, rXY, rSquared, varY and varX						*/
/********************************************************************************/

FILE * dataFileX, *dataFileY, *resultFile;					

/********************************************************************************/
/*This section of code asks the user what files contain the relevant data	*/
/*They should be arranged as:	TitleOfData					*/
/*				NoOfLinesOfData					*/
/*				data1						*/
/*				data2						*/
/*				data3						*/
/*This example only works if NoOfLinesOfData = 3. No line can be greater than 50*/
/*characters or the code will not function correctly				*/
/*The user is also asked for their initial estimate				*/
/********************************************************************************/

printf("What is the first file called?\n");
scanf("%s", fileNameX);
printf("What is the second file called?\n");
scanf("%s", fileNameY);
printf("What is the estimated number of lines of code?\n");
scanf("%f",&varX);


/********************************************************************************/
/*The following section of code opens the files which contain the sets of data 	*/
/*and the location the data will be written.					*/
/*The following code then reads the title of the data from the first line of the*/
/*data file and the next line which is the number of lines of data in the files.*/
/*It does this for both data files						*/
/********************************************************************************/

dataFileX = fopen(fileNameX, "r");
dataFileY = fopen(fileNameY, "r");
resultFile = fopen("result.txt", "w+");				

fgets(dataTitleX, 50, dataFileX);
fgets(resultLineX, 50, dataFileX);
sscanf(resultLineX,"%s",&resultLineX);
numberOfLinesX = atoi(resultLineX);				 

fgets(dataTitleY, 50, dataFileY);
fgets(resultLineY, 50, dataFileY);
sscanf(resultLineY,"%s",&resultLineY);
numberOfLinesY = atoi(resultLineY);


/********************************************************************************/
/*Here variables are initialised so they behave correctly			*/
/*data is initialised as a list to hold all varX and varY read in from the files*/ 
/********************************************************************************/

dataValuePair data[numberOfLinesX];
sumOfVarXTimesVarY = 0;
sumOfVarX = 0;
sumOfVarY = 0;
sumOfVarXSquared = 0;
sumOfVarYSquared = 0;


/********************************************************************************/
/*These loops read the data in from the files and store them in the data list.	*/
/*They also perform the calculations to find sumOfVarX, sumOfVarXSquared, 	*/
/*sumOfVarY, sumOfVarYSquared and sumOfVarXTimesVarY				*/
/********************************************************************************/

for (i = 0; i<numberOfLinesX; i++)
	{
	fscanf(dataFileX,"%f",&data[i].varX);
	
	sumOfVarX = sumOfVarX + data[i].varX;
	sumOfVarXSquared = sumOfVarXSquared + (data[i].varX)*(data[i].varX);
	}
								
for (i = 0; i<numberOfLinesY; i++)
	{
	fscanf(dataFileY,"%f",&data[i].varY);	

	sumOfVarXTimesVarY = sumOfVarXTimesVarY + data[i].varX*data[i].varY;
	sumOfVarY = sumOfVarY + data[i].varY;
	sumOfVarYSquared = sumOfVarYSquared + (data[i].varY)*(data[i].varY);
	}


/********************************************************************************/
/*This section is where all the big calculations are done.			*/
/*avgVarX and avgVarY are first calculated					*/
/*The linear regression parameters, betaOne and the constant betaZero are 	*/
/*calculated									*/
/*The correlation coefficients, rXY and rSquared, are calculated		*/
/*The improved prediction, varY, is then calculated				*/
/********************************************************************************/

avgVarX = sumOfVarX/numberOfLinesX;
avgVarY = sumOfVarY/numberOfLinesY;

betaOne = (sumOfVarXTimesVarY - numberOfLinesX*avgVarX*avgVarY)/(sumOfVarXSquared - (numberOfLinesX*avgVarX*avgVarX));

betaZero = avgVarY - betaOne*avgVarX;

rXY = (numberOfLinesX*sumOfVarXTimesVarY - sumOfVarX*sumOfVarY)/(sqrt((numberOfLinesX*sumOfVarXSquared-(sumOfVarX*sumOfVarX))*(numberOfLinesX*sumOfVarYSquared-sumOfVarY*sumOfVarY)));

rSquared = rXY * rXY;

varY = betaZero + varX*betaOne;


/********************************************************************************/
/*This section outputs the results to resultFile (result.txt).			*/
/*The output will state the titles of the data used as well as betaOne,		*/
/*betaZero, rXY, rSquared, varY and the initial estimate varX with flavour text */
/*to remind the user which is which						*/
/*The program then closes all open files used					*/
/********************************************************************************/

fprintf(resultFile, "For the data values:\n%s%s%f is betaOne.\n%f is betaZero.\n%f is the correlation coefficient rxy.\n%f is the correlation coefficient r^2.\n%f is the expected number of lines of code for the extimated %f lines of code.\n",dataTitleX, dataTitleY,betaOne, betaZero,rXY,rSquared, varY, varX);
							
fclose(dataFileX);
fclose(dataFileY);
fclose(resultFile);
return 0;
}
