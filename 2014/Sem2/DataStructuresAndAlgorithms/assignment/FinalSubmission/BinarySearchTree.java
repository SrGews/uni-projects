/**
*FILE:          BinarySearchTree.java
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          DSA120
*LAST MOD:      03/11/2014
**/




import io.*;
import java.io.*;
import java.util.*;
import java.text.Collator;

public class BinarySearchTree
{



	/*Class field is the root of the binary tree*/

	private TreeNode m_root;



	/*This is the TreeNode class which is used in the binary tree*/
	
	private class TreeNode
	{
		private String m_key;
		private StudentObject m_value;
		private TreeNode m_leftChild;
		private TreeNode m_rightChild;



		/*Alternate constructor*/

		public TreeNode ( String inKey, StudentObject inVal )
		{
			if ( inKey == null )
			{
				throw new IllegalArgumentException("Key cannot be null");
			}

			m_key = inKey;
			m_value = inVal;
			m_leftChild = null;
			m_rightChild = null;
		}



		/*Accessors for the TreeNode class*/

		public String getKey()
		{
			return m_key;
		}

		public StudentObject getValue()
		{
			return m_value;
		}

		public TreeNode getLeft()
		{
			return m_leftChild;
		}

		public TreeNode getRight()
		{
			return m_rightChild;
		}




		/*Mutators for the TreeNode class*/

		public void setLeft( TreeNode newLeft )
		{
			m_leftChild = newLeft;
		}

		public void setRight( TreeNode newRight )
		{
			m_rightChild = newRight;
		}
	}



	/*Default constructor of the binary tree*/

	public BinarySearchTree(  )
	{
		m_root = null;
	}



	/*Calls a recursive function to search for a key*/

	public int findName( String nameToFind, StudentObject[] A )
	{
		return findTheNameRec(nameToFind, A, findRecursive( nameToFind, m_root ), 0) + 1;
	}

	private int findTheNameRec( String nameToFind, StudentObject[] A, TreeNode currNode, int count)
	{
		int temp;

		if( currNode != null )
		{
			count = findTheNameRec( nameToFind, A, currNode.getLeft(  ), count );

			count = findTheNameRec( nameToFind, A, currNode.getRight(  ), count );

			if(currNode.getKey().startsWith(nameToFind))
			{
				A[count] = currNode.getValue(  );
				System.out.println(A[count].getNumber() + "\t\t\t" +
					A[count].getName());
				count ++;
			}
		}

		return count;
	}



	/*Calls a recursive function to insert an StudentObject into the tree*/

	public void insert( String key, StudentObject value )
	{
		m_root = insertRec( key, value, m_root );
	}



	/*Calls a recursive algorithm to delete an StudentObject from the tree while maintaining the tree's 
	integrity*/

	public void delete( String key )
	{
		deleteRecursive( key, m_root );
	}



	/*Calls a recursive algorithm to find the height of the tree*/

	public int height(  )
	{
		return heightRecursive( m_root );
	}



	/*Calls a recursive algorithm to fin the minimum height of the tree, ie shortest length from the
	root to a null entry*/

	public int minHeight(  )
	{
		return minHeightRecursive( m_root );
	}



	/*Finds a key in the tree recursively*/

	private TreeNode findRecursive( String key, TreeNode currNode )
	{
		TreeNode val = null;
		Collator myCollator = Collator.getInstance();

		if( currNode == null )
		{
			throw new IllegalArgumentException( "Key " + key + " not found" );
		}
		else if( currNode.getKey(  ).startsWith( key ) )
		{
			val = currNode;
		}
		else if( myCollator.compare(key, currNode.getKey(  ) ) < 0 )
		{
			val = findRecursive( key, currNode.getLeft(  ) );
		}
		else
		{
			val = findRecursive( key, currNode.getRight(  ) );
		}
		return val;
	}




	/*Inserts a node into the tree as a leaf using recursion to find a free leaf*/

	private TreeNode insertRec( String key, StudentObject data, TreeNode currNode )
	{
		Collator myCollator = Collator.getInstance();
		if( currNode == null )
		{
			currNode = new TreeNode( key, data);
		}
		else if( key.equals( currNode.getKey(  ) ) )
		{
			throw new IllegalArgumentException("Key already exists");
		}
		else if( myCollator.compare(key, currNode.getKey(  ) ) < 0 )
		{
			currNode.setLeft( insertRec( key, data, currNode.getLeft(  ) ) );
		}
		else
		{
			currNode.setRight( insertRec( key, data, currNode.getRight(  ) ) );
		}

		return currNode;
	}



	/*Deletes a treenode from the binary tree while maintaining the tree's integrity by using 
	recursion*/

	private TreeNode deleteRecursive( String key, TreeNode currNode )
	{
		TreeNode upDateNode;
		upDateNode = null;

		if( currNode == null)
		{

		}
		else if( key.equals( currNode.getKey(  ) ) )
		{
			upDateNode = deleteNode( key, currNode );
		}
		else if( key.compareTo( currNode.getKey(  ) ) < 0 )
		{
			currNode.setLeft( deleteRecursive( key, currNode.getLeft(  ) ) );
		}
		else
		{
			currNode.setRight( deleteRecursive( key, currNode.getRight(  ) ) );
		}

		return upDateNode;
	}



	/*Deletes a treenode from the binary tree while maintaining the tree's integrity by using 
	recursion*/

	private TreeNode deleteNode( String key, TreeNode delNode)
	{
		TreeNode upDateNode;
		upDateNode = null;

		if( ( delNode.getLeft(  ) == null ) && ( delNode.getRight(  ) == null ) )
		{
			upDateNode = null;
		}
		else if( ( delNode.getLeft(  ) !=null ) && ( delNode.getRight(  ) == null ) )
		{
			upDateNode = delNode.getLeft(  );
		}
		else if( ( delNode.getLeft(  ) ==null ) && ( delNode.getRight(  ) != null ) )
		{
			upDateNode = delNode.getRight(  );
		}
		else
		{
			upDateNode = promoteSuccessor( delNode.getRight( ) );
			if ( upDateNode != delNode.getRight(  ) )
			{
				upDateNode.setRight( delNode.getRight(  ) );
			}
			upDateNode.setLeft( delNode.getLeft(  ) );
		}

		return upDateNode;
	}




	/*Deletes a treenode from the binary tree while maintaining the tree's integrity by using 
	recursion*/

	private TreeNode promoteSuccessor( TreeNode currNode )
	{
		TreeNode successor;
		successor = currNode;

		if(currNode.getLeft(  ) == null)
		{
			successor = currNode;
		}
		else if( currNode.getLeft(  ) != null)
		{
			successor = promoteSuccessor( currNode.getLeft(  ) );
			if( successor == currNode.getLeft(  ) )
			{
				currNode.setLeft( successor.getRight(  ) );
			}
		}

		return successor;
	}




	/*Finds the height of the tree using recursion*/

	private int heightRecursive( TreeNode currNode )
	{
		int heightSoFar, iLeftHeight, iRightHeight;

		heightSoFar = 0;

		if( currNode  == null )
		{
			heightSoFar = -1;
		}
		else
		{
			iLeftHeight = heightRecursive( currNode.getLeft(  ) );
			iRightHeight = heightRecursive( currNode.getRight(  ) );

			if( iLeftHeight > iRightHeight )
			{
				heightSoFar = iLeftHeight + 1;
			}
			else
			{
				heightSoFar = iRightHeight + 1;
			}
		}

		return heightSoFar;
	}




	/*Calls a recursive algorithm to fin the minimum height of the tree, ie shortest length from the
	root to a null entry*/
	
	private int minHeightRecursive( TreeNode startNode )
	{
		int heightSoFar, iLeftHeight, iRightHeight;

		heightSoFar = 0;

		if( startNode == null )
		{
			heightSoFar = -1;
		}
		else
		{
			iLeftHeight = heightRecursive( startNode.getLeft(  ) );
			iRightHeight = heightRecursive( startNode.getRight(  ) );

			if( iLeftHeight < iRightHeight )
			{
				heightSoFar = iLeftHeight + 1;
			}
			else
			{
				heightSoFar = iRightHeight + 1;
			}
		}

		return heightSoFar;
	}
}