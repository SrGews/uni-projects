/**
*FILE:          StudentObject.java
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          DSA120
*LAST MOD:      03/11/2014
**/


import io.*;


/*Used to retain a student's student number and name.*/
public class StudentObject
{
	private String studentName;
	private int studentNumber;



	/*Constructors*/
	public StudentObject(  )
	{
		studentName = null;
		studentNumber = -1;
	}

	public StudentObject( String inName, int inNumber)
	{
		studentName =  inName;
		studentNumber = inNumber;
	}



	/*Accessors*/
	public String getName()
	{
		return studentName;
	}

	public int getNumber()
	{
		return studentNumber;
	}



	/*Mutators*/
	public void setName(String inName)
	{
		studentName = inName;
	}

	public void setNumber(int inNumber)
	{
		studentNumber = inNumber;
	}




	/*toString methods. toCSVString is used for convenience when saving to a file*/
	public String toString()
	{
		String retVal;

		retVal = studentName + " " + studentNumber;

		return retVal;
	}

public String toCSVString()
	{
		String retVal;

		retVal = studentName + "," + studentNumber;

		return retVal;
	}
}