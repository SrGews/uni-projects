/**
*FILE:          StudentSearchInterface.java
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          DSA120
*LAST MOD:      03/11/2014
**/



import io.*;
import java.io.*;
import java.util.*;


/*This class processes the menu options from StudentSearch and performs all
**file I/O needed. It calls BinarySearchTree and StudentMergeSort to order
**data and find student info as requested by the user.*/


/*numberStudents is the maximum number of students the program can handle. 
**Increasing the number may lead to unexpected results. studentArray is an
**array which is sorted by mergesort of the data. searchTree is a binary
**search tree whis is used for finding names of students. sIDMatch and 
sNameMatch hold the results for the last searches done. numMatches is the 
**number of matches from the last name search. arraySize is the number of 
**students read in from a file, and is used to resize the array*/
public class StudentSearchInterface
{
	private static Scanner input = new Scanner(System.in);

	private int numberStudents = 10000;

	private BinarySearchTree searchTree;
	private StudentObject[] studentArray;
	private StudentObject[] sNameMatch;
	private int sIDMatch;
	private int numMatches, arraySize;

	public StudentSearchInterface()
	{
		sIDMatch = -1;
		numMatches = -1;
		searchTree = new BinarySearchTree();
		arraySize = -1;
	}

	


	/*Handles the menu options from StudentSearch*/
	public int doOption( int optionNo )
	{
		int error = 0;

		switch (optionNo)
		{
			case 1: error = loadStudents();
				break;
			case 2: error = sIDSearch();
				break;
			case 3: error = sNameSearch();
				break;
			case 4: error = saveMatches();
				break;
			case 5: error = 0;
				break;
			default:  System.out.println("You did not enter a valid option");
		}

		return error;
	}




	/*The first menu option:
	**This reads in the student file and stores each entry into a binary tree
	**and a mergesorted array for quick location of the students.*/
	private int loadStudents()
	{
		String fileName;
		int error = 1;

		FileInputStream fileStrm = null;
		InputStreamReader rdr;
		BufferedReader bufRdr;
		int lineNum;
		String line;
		StudentMergeSort studentMergeSort;

		fileName = ConsoleInput.readLine("Enter a file to be read in");

		studentArray = new StudentObject[numberStudents];

		lineNum = 0;
		arraySize = 0;
		try
		{
			fileStrm = new FileInputStream( fileName );
			rdr = new InputStreamReader( fileStrm );
			bufRdr = new BufferedReader( rdr );

			
			line = bufRdr.readLine();

			while ( line != null )
			{
				lineNum ++;
				error = processLine( line );

				line = bufRdr.readLine(  );
				error = 0;
			}

			resizeStudentArray();
			studentMergeSort = new StudentMergeSort();
			studentMergeSort.mergeSort(studentArray, arraySize);

			makeSearchTree();

			fileStrm.close();

			System.out.println("The file " + fileName + " was opened and read in");

		}

		catch ( IOException e )
		{
			if ( fileStrm != null )
			{
				try
				{
					fileStrm.close();
				}
				catch ( IOException ex2 )
				{

				}

				System.out.println("Error in file processing: " + 
					e.getMessage());
			}

		}



		if (error == 0)
		{
			for(int i = 0; i < (arraySize - 1) ; i++)
			{
				if(studentArray[i].getNumber() > studentArray[i+1].getNumber() )
				{
					error = 1;
					System.out.println("ERROR: The following two students have"
						+ " the same student number:" + studentArray[i].toString()
						+ " and " + studentArray[i+1].toString());
				}
			}
		}

		return error;
	}





	/*This handles the second menu option:
	**Calls a recursive algorithm to find the ID of a student from the merge
	**sorted array*/
	private int sIDSearch()
	{
		int sIDToFind, error = 1;
		StudentMergeSort studentMergeSort;
		studentMergeSort = new StudentMergeSort();

		sIDMatch = -1;

		System.out.println("Enter a student ID you wish to find");
		sIDToFind = Integer.parseInt(input.nextLine());

		sIDMatch = studentMergeSort.findTheID( studentArray , sIDToFind);

		if (sIDMatch != -1)
		{
			System.out.println(studentArray[sIDMatch].getNumber() + "\t" +
					studentArray[sIDMatch].getName());
			error = 0;
			numMatches = -1;
		}
		else
		{
			System.out.println("The student ID " + sIDToFind +
			 " is not in the list");
		}

		return error;
	}




	/*Handles the third menu option:
	**Calls a recursive algorith to find students whose names begin with a 
	**string entered by the user. The list of students is then stored in an
	**array until they are used*/
	private int sNameSearch()
	{
		String nameToFind;
		int error;

		error = 0;

		sNameMatch = new StudentObject[arraySize];

		nameToFind = ConsoleInput.readLine("Enter the start of a name to find.");

		numMatches = searchTree.findName(nameToFind, sNameMatch)-1;

		if (numMatches <= 0)
		{
			error = -1;
			System.out.println("No students match your search");
		}
		else
		{
			System.out.println(numMatches + " students match your search");
			sIDMatch = -1;
		}

		return error;
	}



	/*This handles the fourth menu option:
	**Saves the most recent search results to a file designated by the user.*/
	private int saveMatches()
	{
		String fileName;
		FileOutputStream fileStrm;
		PrintWriter pw;
		fileStrm = null;


		if((sIDMatch == -1) && (numMatches == -1))
		{
			System.out.println("There are no successful searches to save");
		}
		else
		{
			fileName = ConsoleInput.readWord("Enter an output file");

			try
			{
				fileStrm = new FileOutputStream(fileName);
				pw = new PrintWriter(fileStrm);

				if(sIDMatch != -1)
				{
					pw.println(studentArray[sIDMatch].getNumber() + "," + 
						studentArray[sIDMatch].getName());
				}
				if (numMatches != -1)
				{
					for(int i = 0; i < numMatches; i++)
					{
					pw.println(sNameMatch[i].toCSVString());

					}
				}
				pw.close();

				fileStrm.close();
			}
			catch(IOException e)
			{
				if(fileStrm != null)
				{
					try
					{
						fileStrm.close();
					}
					catch(IOException ex2)
					{

					}
					System.out.println("Error in file processing: "+e.getMessage());
				}
			}
		}
		return 0;
	}



	/*Used to convert each line in the input file into the student name and
	**student number*/
	private int processLine( String csvRow )
	{
		int error = 0;
		int sNum;
		String sName, student;
		StringTokenizer strTok;

		StudentObject thisStudent;

		strTok = new StringTokenizer( csvRow, "," );
		try
		{
			sNum = Integer.parseInt( strTok.nextToken(  ) );
			sName = strTok.nextToken(  );

			thisStudent =  new StudentObject( sName, sNum );

			addStudentArray(thisStudent);

		}
		catch ( Exception e )
		{
			error = 1;
			throw new IllegalStateException("CSV row had invalid format" + e);
		}

		return error;
	}




	/*Adds a student to the end of the student array*/
	private void addStudentArray( StudentObject thisStudent)
	{
		StudentObject temp = new StudentObject( thisStudent.getName(), 
			thisStudent.getNumber() );
		studentArray[arraySize] = temp;	
		arraySize ++;	
	}





	/*Used to resize the array once all entries from the input file have been
	**read in*/
	private void resizeStudentArray()
	{
		StudentObject[] tempArr;
		tempArr = new StudentObject[arraySize];

		for( int i = 0; i<arraySize; i++)
		{
			tempArr[i] = studentArray[i];
		}

		studentArray = new StudentObject[arraySize];

		for( int i = 0; i<arraySize; i++)
		{
			studentArray[i] = tempArr[i];
		}
	}


	/*Used to make and populate the binary search tree*/
	private void makeSearchTree()
	{
		searchTree = new BinarySearchTree();

		for( int i = 0; i < arraySize; i++ )
		{
			searchTree.insert(studentArray[i].getName(), studentArray[i]);
		}
	}


}