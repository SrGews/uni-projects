/**
*FILE:          StudentMergeSort.java
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          DSA120
*LAST MOD:      03/11/2014
**/




public class StudentMergeSort
{
	public StudentMergeSort() { }
	// mergeSort - front-end for kick-starting the recursive algorithm
    public static void mergeSort(StudentObject[] A, int rightIdx)
    {
        int leftIdx;

        rightIdx = rightIdx - 1;
		leftIdx = 0;

        mergeSortRecurse(A,leftIdx, rightIdx);
    }//mergeSort()


    private static void mergeSortRecurse(StudentObject[] A, int leftIdx, int rightIdx)
    {
        int midIdx;
        if(leftIdx < rightIdx)
        {
            midIdx = (leftIdx + rightIdx)/2;

            mergeSortRecurse(A, leftIdx, midIdx);
            mergeSortRecurse(A, midIdx+1, rightIdx);

            merge(A, leftIdx, midIdx, rightIdx);
        }
    }//mergeSortRecurse()

    private static void merge(StudentObject[] A, int leftIdx, int midIdx, int rightIdx)
    {
        int tempArrSize;
        tempArrSize = rightIdx-leftIdx + 1;
        StudentObject[] tempArr = new StudentObject[tempArrSize] ;
		int i = leftIdx, j = midIdx +1, k = 0;

        while ((i <= midIdx) && (j <= rightIdx))
        {
            if( ( A[i].getNumber(  ) ) < ( A[j].getNumber(  ) ) )
            {
                tempArr[k] = A[i];
                i++;
            }
            else if( ( A[i].getNumber(  ) ) == ( A[j].getNumber(  ) ))
            {
                System.out.println("At least 2 students with same sNumber " + A[i].getNumber(  ) );
                tempArr[k] = A[i];
                i ++;
                k ++;
                tempArr[k] = A[j];
                j ++;
            }
            else
            {
                tempArr[k] = A[j];
                j++;
            }
            k++;
        }

        for( ; i<=midIdx; i++){
            tempArr[k] = A[i];
            k++;
        }

        for( ; j<=rightIdx; j++){
            tempArr[k] = A[j];
            k++;
        }

        for(k = leftIdx; k <= rightIdx; k++)
       	{
            A[k] = tempArr[k-leftIdx];
        }
    }//merge()




    /*Finds the student ID in the given array*/
	public int findTheID(StudentObject[] A, int sIDToFind)
		{
			int retIndex, leftIdx, rightIdx;

			leftIdx = 0;
			rightIdx = A.length;
			retIndex = findTheIDRecursive(leftIdx, rightIdx, A, sIDToFind);


			return retIndex;
		}



    /*Finds the matching ID in the function*/
	private int findTheIDRecursive(int leftIdx, int rightIdx, StudentObject[] A, int sIDToFind)
	{
		int midIdx, retIndex;

		midIdx = (leftIdx+rightIdx)/2;

		if((A[midIdx].getNumber() == sIDToFind))
		{
			retIndex = midIdx;
		}
		else
		{
			if((leftIdx - rightIdx)>0)
			{
				retIndex = -1;
			}
			else if( (A[midIdx].getNumber()) > sIDToFind)
			{
				retIndex = findTheIDRecursive(leftIdx, midIdx-1 , A, sIDToFind);
			}
			else
			{
				retIndex = findTheIDRecursive(midIdx+1, rightIdx, A, sIDToFind);
			}
		}
		return retIndex;
	}
}