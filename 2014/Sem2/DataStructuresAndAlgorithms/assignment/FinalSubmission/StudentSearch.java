/**
*FILE:          StudentSearch.java
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          DSA120
*LAST MOD:      03/11/2014
**/



import io.*;
import java.io.*;
import java.util.*;

public class StudentSearch
{
	private static Scanner input = new Scanner(System.in);



	
	/*This program asks for a user to input a filename which contains student 
	**numbers and student names separated by a comma. It then reads the file
	**in, organises the data into an array and a binary tree and awaits 
	**furthur input. The user can then chose to search the data for a single
	**student by their number or by entering the first part of someone's 
	**name. The latter query will return a list of resluts that match.
	**
	**The user can perform these actions as many times as they like but only
	**the most recent searches will be stored and saved when the user selects
	**menu option 4. When option 4 is selected the user is prompted to 
	**enter a destination file to save the search results to.
	**
	**Example input file:
	** 			14552681,John Johnson
	**			13884523,Nickola Hard
	**			32315548,Mark ToughGuy
	**
	**End Example.
	**
	**Note that the program can only handle files up to 10,000 entries in 
	**length
	***/
	public static void main( String[] args )
	{
		
		int menuOption, error;
		StudentSearchInterface searchInterface;

		searchInterface = new StudentSearchInterface();

		menuOption = 0;
		error = 0;

		do
		{
			System.out.println("(1)\t- Load Students");
			System.out.println("(2)\t- Search for Single Student");
			System.out.println("(3)\t- Search for List of Students");
			System.out.println("(4)\t- Save Matches");
			System.out.println("(5)\t- Quit");

			System.out.println("Choice:>");
			menuOption = Integer.parseInt(input.nextLine());

			error = searchInterface.doOption(menuOption);

			while( (menuOption == 1) && (error !=0) )
			{
				System.out.println("File reading failed. Enter 1 to re-enter a filename otherwise enter another character");
				menuOption = Integer.parseInt(input.nextLine());

				error = searchInterface.doOption(menuOption);
				
				if (menuOption == 5)
				{
					menuOption = 2;
				}
			}

			if ( error != 0 )
			{
				System.out.println("An error has occurred");
			}
			
		}while( menuOption != 5 );
		
	}
}