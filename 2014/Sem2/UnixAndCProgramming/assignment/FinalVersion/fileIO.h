/**
*FILE:          fileIO.h
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/

#include "linkedList.h"
#include <string.h>



/*Used to easily manage the input and output files
*/

typedef struct
{
    FILE *inputFile;
    FILE *outputFile;
}Files;


int openFiles( Files *files, char **argv );
    GraphDimensions readGraphDimensions( Files *files );
void readExpressions( LinkedList *exprList, Files *files );
void saveExpressions( LinkedList *exprList, Files *files );
void saveValue( Files *files,double result );
void saveNewLine( Files *files );
void saveComma( Files *files );
void closeFiles( Files *files );
void saveTxtNAN( Files *files );
void saveTxtInvalid( Files *files );
