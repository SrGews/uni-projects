/**
*FILE:          fileGrapher.c
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/

#include "fileGrapher.h"




/*Takes two filenames as inputs, an input and output file respectively, reads 
** the first in and plots the functions contained within in accor to how the 
** file has been laid out.
**The first line of the input file should contain four real numbers which
** are the graph dimensions set out as 'xmin ymin xmax ymax' the following
** lines must each contain three integer numbers from 0 to 255 representing
** the red green and blue values of a colour followed by an expression 
** containing x, numerical operators and unary operators limited to sin, cos, 
** tan, exp, log, floor and ceiling.
**
**Example file:
**
**              -10 -5 10 5
**              255 50 160 floor(4*sin(x))
**              50 160 255 ceiling(4*sin(x))
**              160 255 50 4*sin(x)
**
**End of example file.
**
**After checking for errors in opening the files, it will assign the data to a
** linked list, save the functions to the output, and plot the data to a 
** graph. While doing so the y-values for each function will be saved below 
** their corresponding function. The first column saved will always be of an
** 'x' function. The program will do this for every column of pixels in the 
** graph. If the function is invalid or undefined it will save invalid or nan 
** instead of the y-value respectively. If the expression is invalid the 
** program will inform the user once.
**
**Example output file where width == 8 for input file's example:
**              x,floor(4*sin(x)),ceiling(4*sin(x)),4*sin(x)
**              -10.00000,2.00000,3.00000,2.17608
**              -7.50000,-4.00000,-3.00000,-3.75200
**              -5.00000,3.00000,4.00000,3.83570
**              -2.50000,-3.00000,-2.00000,-2.39389
**              0.00000,0.00000,0.00000,0.00000
**              2.50000,2.00000,3.00000,2.39389
**              5.00000,-4.00000,-3.00000,-3.83570
**              7.50000,3.00000,4.00000,3.75200
**End example.
**
**The program performs error checks to ensure the files opened successfully,
** and that all operations are being successfully completed.
**
**Known errors:
**		When an asymptote occurs between pixels the program will plot
**		a solid line from one to the other, eg tan(x) where the 
**		asymptote occurs at x = PI/2 
*/

int main( int argc, char **argv )
{
    int error = 0;

    Files *files;
    GraphDimensions graphDim;
    LinkedList *exprList;
    OperatorEvaluator ptr;

    if( argc!=3 )
    {
        perror( "ERROR: Wrong number of inputs\n" );
    }
    else
    {
        files = ( Files* )malloc( sizeof( Files ) );

        error = openFiles( files, argv );

        if ( error == -1 )
        {
            perror( "ERROR: Files didn't open successfully\n" );
        }
        else
        {
            graphDim = readGraphDimensions( files );
            
            exprList = ( LinkedList* )malloc( sizeof( LinkedList ) );
            exprList->head = NULL;
            exprList->tail = NULL;

            readExpressions( exprList, files );

            ptr = &opEvaluator;

            saveExpressions( exprList, files );

            error = createGraphSaveValues( graphDim, exprList, files, ptr );
            if ( error == -1 )
            {
                perror( "ERROR: createGraphSaveValues failed\n" );
            }

            freeList( exprList );

            closeFiles( files );
        }
        free( files );
    }
    return 0;
}
