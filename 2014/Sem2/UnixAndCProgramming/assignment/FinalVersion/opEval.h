/**
*FILE:          opEval.h
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/


/*ERR_MARGIN is the margin of error used when comparing real numbers
**PI is the approximate value of pi
*/

#define ERR_MARGIN 0.000001
#define PI 3.14159266

#include "eval.h"
#include <stdio.h>
#include <math.h>
#include <string.h>

EvalErrorType opEvaluator( double *result, char *operatorText );
