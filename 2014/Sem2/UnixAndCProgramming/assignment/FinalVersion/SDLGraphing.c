/**
*FILE:          SDLGraphing.c
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/

#include "SDLGraphing.h"



/*Input a x value and xmin and xmax values via the graphDim struct to get
** the corresponding grid value for SDL.
**In short it converts from cartesian co-ords to SDL co-ords
*/

int getGridLocationX( GraphDimensions graphDim, double xval )
{
    int z;

    z = WIDTH * ( ( xval-( graphDim.xmin ) )/
        ( graphDim.xmax - graphDim.xmin ) );

    return z;

}



/*Input a y value and ymin and ymax values via the graphDim struct to get
** the corresponding grid value for SDL.
**In short it converts from cartesian co-ords to SDL co-ords
*/

int getGridLocationY( GraphDimensions graphDim, double yval )
{
    double z;

    z = 0;
    z = HEIGHT*( ( yval-( graphDim.ymin ) )/( 
        graphDim.ymax-graphDim.ymin ) );

    z = HEIGHT - z;

    return ( int )z;
}




/*Imports an SDL_Surface pointer, x co-ord, y co-ord and a pixel colour
** then sets the surface's corresponding pixel's colour to the imported
** colour
*/

void setPixelColour( SDL_Surface *screen, int x, int y, Uint32 pixel )
{
    if( ( y < HEIGHT ) && ( y >= 0 ) ){
        if( ( x < WIDTH ) && ( x >= 0 ) ){
            ( ( Uint32* )screen->pixels )[WIDTH*y + x] = pixel;
        }
    }
}



/*Draws the grey axis at x = 0 and y = 0 in the SDL_Surface
*/

void drawAxis( SDL_Surface *screen, GraphDimensions graphDim )
{
    int x, y, i;
    Uint32 greyPixel;

    y = getGridLocationY( graphDim, 0 );
    x = getGridLocationX( graphDim, 0 );

    greyPixel = SDL_MapRGB( screen->format, 128, 128, 128 );

    for ( i = 0; i < WIDTH; i++ )
    {
        setPixelColour( screen, i, y, greyPixel );
    }

    for ( i = 0; i < HEIGHT; i++ )
    {
        setPixelColour( screen,x,i,greyPixel );
    }
}



/*Joins any vertical gaps in the plotted functions.
**Imports SDL_Surface pointer, a pixel colour (pixel), a Node pointer which 
** holds the previous value of y, and the current values of x and y where x 
** and y are SDL graph co-ords.
*/

void solidifyLine( SDL_Surface *screen, Uint32 pixel, 
    Node *currNode, int x, int y ){

    if( x == 0 )
    {
        ( currNode->lineData.prevy ) = y;
    }else
    {
        if( y > HEIGHT )
        {
            y = HEIGHT;
        }
        else if( y<0 )
        {
            y = -1;
        }

        while( y > ( currNode->lineData.prevy ) )
        {
            setPixelColour( screen, x, currNode->lineData.prevy, pixel );
            ( currNode->lineData.prevy ) ++;
        }
        while( y < ( currNode->lineData.prevy ) )
        {
            setPixelColour( screen, x, currNode->lineData.prevy, pixel );
            currNode->lineData.prevy --;
        }
    }
}



/*Keeps SDL open until the user attempts to close it.
*/

void waitToQuit(  )
    {
    SDL_Event ev;

    do
    {
        SDL_WaitEvent( &ev );
    }while( ev.type != SDL_QUIT );

    SDL_Quit(  );  
}




/*Gets the SDL co-ord for the cartesian y-value result, then sets a pixel
** colour and plots it on an open SDL screen. The cartesian value is then
** saved to the output file and solidifyLine is called to fill in any 
** vertical gaps in the plotted line
*/

void plotGraphSaveData( Node *currNode, int x, 
    double result, Files *files, GraphDimensions graphDim, 
    SDL_Surface *screen )
{
    int y;

    Uint32 pixel;
    y = getGridLocationY( graphDim, result );

    pixel = SDL_MapRGB( screen->format, 
        ( Uint8 )currNode->lineData.colour.red, 
        ( Uint8 )currNode->lineData.colour.green, 
        ( Uint8 )currNode->lineData.colour.blue );

    setPixelColour( screen, x, y, pixel );

    saveValue( files, result );

    solidifyLine( screen, pixel, currNode, x, y );
}




/*Handles the errors from eval. If the function result is undefined at that
** point it saves nan to the output file. Otherwise it will save invalid
** to the output file and inform the user once.
*/

void handleErrorType(int x, EvalError exprError, Files *files, Node *currNode )
{
    if ( exprError.type == ER_RESULT_UNDEFINED )
    {
        saveTxtNAN( files );
    }
    else if ( exprError.type>ER_RESULT_UNDEFINED )
    {
        saveTxtInvalid( files );
        if( x == 0 )
        {
            printf( "ERROR: '%s' is not a valid function error %d\n",
                currNode->lineData.expression, exprError.type );
        }
    }
}




/*Opens SDL with error handeling and sets it to the appropriate values.
**If SDL opened then the grey axis will be drawn. For each horizontal value
** in the SDL graph a corresponding xVal will be calculated and input into 
** eval. eval error handeling is then done. If no errors from eval then
** xVal's corresponding y-value will be plotted in SDL. SDL will then wait
** for user input before quiting.
*/

int createGraphSaveValues( GraphDimensions graphDim, LinkedList *exprList, 
    Files *files, OperatorEvaluator ptr )
{
    int error, x, j;
    double xVal, result;

    Node *currNode;
    EvalError exprError;
    SDL_Surface *screen;

    error = SDL_Init( SDL_INIT_VIDEO );
    if ( error == -1 )
    {
        perror( "ERROR: SDL failed to initialise\n" );
    }
    else
    {
        screen = SDL_SetVideoMode( WIDTH, HEIGHT, 32, 0 );

        drawAxis( screen, graphDim );

        for( j=0; j < WIDTH; j++ )
        {
            currNode = exprList->head;
            xVal = j*( ( double )( graphDim.xmax-graphDim.xmin ) / 
                ( double )WIDTH )+graphDim.xmin;
            saveValue( files, xVal );
            saveComma( files );
            x = j;

            while( currNode != NULL )
            {
                exprError = eval( &result, currNode->lineData.expression, 
                    xVal, ptr );
                if ( exprError.type != 0 )
                {
                    handleErrorType( x, exprError, files, currNode);
                }
                else
                {
                    plotGraphSaveData( currNode, x, result, files, 
                    graphDim, screen );            
                }

                currNode = currNode->nextNode;
                if( currNode!=NULL )
                {
                    saveComma( files );
                }
            }
            if ( ( j + 1 ) != WIDTH )
            {
                saveNewLine( files );
            }
        }
        SDL_UpdateRect( screen, 0, 0, 0, 0 );

        waitToQuit(  );
    }
    return error;
}
