/**
*FILE:          SDLGraphing.h
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/


/*HEIGHT and WIDTH are the height and width of the SDL graph that will be 
** created
*/

#define HEIGHT 800
#define WIDTH 800

#include <SDL/SDL.h>
#include "fileIO.h"
#include "opEval.h"

int getGridLocationX( GraphDimensions graphDim, double xval );
int getGridLocationY( GraphDimensions graphDim, double yval );
void setPixelColour( SDL_Surface *screen, int x, int y, Uint32 pixel );
void drawAxis( SDL_Surface *screen, GraphDimensions graphDim );
void solidifyLine( SDL_Surface *screen, Uint32 pixel, Node *currNode, int x, 
    int y );
void waitToQuit(  );
void plotGraphSaveData( Node *currNode, int x, double result, Files *files, 
	GraphDimensions graphDim, SDL_Surface *screen );
int createGraphSaveValues( GraphDimensions graphDim, LinkedList *exprList, 
    Files *files, OperatorEvaluator ptr );
