/**
*FILE:          linkedList.c
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/

#include "linkedList.h"



/*Initialises the first node in the linked list
*/

void firstNode( LinkedList *lList )
{
    Node *newNode;
    newNode = ( Node* )malloc( sizeof( Node ) );
    newNode->nextNode = NULL;

    lList->head = newNode;
    lList->tail = newNode;
}




/*Initialises a new node at the end of the linked list
*/

void newNodeAtEnd( LinkedList *lList )
{
    if( ( lList->tail ) != NULL )
    {
        Node *newNode;
        newNode = ( Node* )malloc( sizeof( Node ) );
        newNode->nextNode = NULL;
        lList->tail->nextNode = newNode;
        lList->tail = newNode;
    }
    else
    {
        firstNode( lList );
    }
}





/*Frees all nodes in the linked list through recursion
*/

void freeNode( Node *node )
{
    if( node->nextNode != NULL )
    {
        freeNode( node->nextNode );
    }
    free( node->lineData.expression );
    free( node );
}




/*This frees all memory allocated to the linked list
*/

void freeList( LinkedList *lList )
    {
    if( lList->head != NULL )
    {
        freeNode( lList->head );
    }

    free( lList );
}




/*Sets the expression to flush any buffers that may effect the expression
*/

void initialiseExpression( LinkedList *lList )
{
    lList->tail->lineData.expression = ( char* )malloc( 
        sizeof( char )*( EXPR_LENGTH ) );
        lList->tail->lineData.expression[0] = '\0';
}




/*If the expression is not large enough to hold the input expression, this 
** will resize the char array
*/

void resizeExpression( LinkedList *lList, int oldSize )
{
    char *tempStr;
    int j;

    tempStr = ( char* )malloc( sizeof( char )*( oldSize ) );

    for( j = 0; j < oldSize; j++ )
    {
        tempStr[j] = lList->tail->lineData.expression[j];
    }

    free( lList->tail->lineData.expression );

    lList->tail->lineData.expression = ( char* )malloc( 
        sizeof( char )*( oldSize + EXPR_LENGTH ) );

    for( j = 0; j < oldSize; j++ )
    {
        lList->tail->lineData.expression[j] = tempStr[j];
    }

    free( tempStr );
}
