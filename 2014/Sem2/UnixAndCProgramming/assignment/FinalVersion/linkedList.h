/**
*FILE:          linkedList.h
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/



/*EXPR_LENGHT is the default length Linedata->expression will be and how much ** it will be increased by if it is not enough
*/

#define EXPR_LENGTH 10

#include <stdio.h>
#include <stdlib.h>




/*Holds the graph dimensions of the graph which are read in from the first
** line of the file
*/

typedef struct
{
    double ymin;
    double ymax;
    double xmin;
    double xmax;
}GraphDimensions;



/*Holds the RGB values of a function
*/

typedef struct
{
    int red;
    int green;
    int blue;
}Colour;



/*Holds all data of an expression. colour has the colour of the line, 
** expression is the line's equation, prevy is the previous y value. prevy is ** used when removing vertical gaps in the line.
*/

typedef struct
{
    Colour colour;
    char *expression;
    int prevy;
}LineData;



/*This is the node for the linked list which contains all data read in from 
** the input file excluding the graphdimensions
*/

typedef struct Node
{
    LineData lineData;
    struct Node *nextNode;
}Node;


/*This is the head of the Linked List
*/
typedef struct
{
    Node *head;
    Node *tail;
}LinkedList;


void firstNode( LinkedList *lList );
void newNodeAtEnd( LinkedList *lList );
void freeNode( Node *node );
void freeList( LinkedList *lList );
void initialiseExpression( LinkedList *lList );
void resizeExpression( LinkedList *lList, int oldSize );