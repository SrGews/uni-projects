/**
*FILE:          fileIO.c
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/

#include "fileIO.h"


/*Opens the input and output files which are contained in the struct files 
** using the names contained in argv.
*/

int openFiles( Files *files, char **argv )
{
    int error = 0;

    files->inputFile = fopen( argv[1], "r" );
    if( files->inputFile == NULL )
    {
        printf( "ERROR: could not open '%s'\n", argv[1] );
        perror( "ERROR: Input file not valid" );
        error = -1;
    }
    else
    {
        files->outputFile = fopen( argv[2], "w" );
        if( files->outputFile == NULL )
        {
            printf( "ERROR: could not open '%s'\n", argv[2] );
            perror( "ERROR: Output file not valid" );
            fclose( files->inputFile );
            error = -1;
        }
    }
    return error;
}




/*Reads in the graph dimensions from an input file contained in files after 
** setting default values for the dimensions.
*/

GraphDimensions readGraphDimensions( Files *files )
{
    GraphDimensions graphDim;

    graphDim.xmin = -5.0;
    graphDim.ymin = -2.0;
    graphDim.xmax = 5.0;
    graphDim.ymax = 2.0;
    
    fscanf( files->inputFile, "%lf %lf %lf %lf\n", &graphDim.xmin, 
        &graphDim.ymin, &graphDim.xmax, &graphDim.ymax );

    if ( graphDim.ymin == graphDim.ymax )
    {
        graphDim.ymin --;
    }

    if ( graphDim.xmin == graphDim.xmax )
    {
        graphDim.xmin --;
    }
    return graphDim;
}



/*Ensures the value of colour is valid and between 0 and 255
*/

int pixelColourCheck( int colour )
{
    if( colour > 255 )
    {
        colour = 255;
    }
    else if( colour<0 )
    {
        colour = 0;
    }
    return colour;
}



/* Sets default colour values for the last line on the list.
*/

void setDefaultColour( LinkedList *exprList )
{
    exprList->tail->lineData.colour.red = 50;
    exprList->tail->lineData.colour.green = 160;
    exprList->tail->lineData.colour.blue = 250;
}




/*Reads in from the input file in files and dynamically allocates space to 
** store the data.
**
**Each line read in must contain 3 integers in the range 0 to 255 representing ** a red, green, and blue value for a colour followed by an expression. All 
** spaces are removed from the expression and stored in a linked list called ** exprList.
*/

void readExpressions( LinkedList *exprList, Files *files )
{
    int i, ch;
    char nextChar = 'a';

    do
    {
        newNodeAtEnd( exprList );

        i = 0;
        setDefaultColour( exprList );
        fscanf( files->inputFile,"%d %d %d", 
            &exprList->tail->lineData.colour.red, 
            &exprList->tail->lineData.colour.green, 
            &exprList->tail->lineData.colour.blue );



        exprList->tail->lineData.colour.red = pixelColourCheck( 
            exprList->tail->lineData.colour.red );
        exprList->tail->lineData.colour.green = pixelColourCheck( 
            exprList->tail->lineData.colour.green );
        exprList->tail->lineData.colour.blue = pixelColourCheck( 
            exprList->tail->lineData.colour.blue );

        initialiseExpression( exprList );
        while( ( nextChar != '\n' ) && ( feof( files->inputFile ) == 0 ) )
        {
            if ( ( i%EXPR_LENGTH ) == ( EXPR_LENGTH-1 ) )
            {
                resizeExpression( exprList, i );
            }

            ch = fgetc( files->inputFile );
            nextChar = ' ';
            if ( ch != -1 )
            {
                nextChar = ( char )ch;
            }
            if( ( nextChar != '\n' )  )
            {
                exprList->tail->lineData.expression[i] = nextChar;
                i++;
            }

        }
        exprList->tail->lineData.expression[i] = '\0';
        ch = fgetc( files->inputFile );
        nextChar = ( char )ch;
    }while( ( int )nextChar!=-1 );

}



/*Saves all the expressions contained in the linked list exprList to an output ** file contained within files. Each expression is separated by a comma.
*/
void saveExpressions( LinkedList *exprList, Files *files )
{
    Node *currNode;

    currNode = exprList->head;
    fprintf( files->outputFile, "x," );
    while( ( currNode != NULL ) )
    {
        if( currNode->lineData.expression != NULL )
        {
            fprintf( files->outputFile,"%s", currNode->lineData.expression );
            }
        else
        {
            saveTxtInvalid( files );
        }
        currNode = currNode->nextNode;
        if( currNode != NULL )
        {
            fprintf( files->outputFile, "," );
        }
    }    
    saveNewLine( files );
}




/*Saves a real number to an output file contained within files
*/

void saveValue( Files *files, double result )
{
    fprintf( files->outputFile, "%.5f", result );
}



/*Saves a new line character to an output file contained within files
*/

void saveNewLine( Files *files )
{
    fprintf( files->outputFile, "\n" );
}



/*Saves a comma to an output file contained within files
*/

void saveComma( Files *files )
{
    fprintf( files->outputFile, "," );
}



/*Closes both files contained within files
*/

void closeFiles( Files *files )
{
    fclose( files->inputFile );
    fclose( files->outputFile );
}



/*Saves 'NaN' to an output file contained within files
*/

void saveTxtNAN( Files *files )
{
    fprintf( files->outputFile, "NaN" );
}



/*Saves 'invalid' to an output file contained within files
*/

void saveTxtInvalid( Files *files )
{
    fprintf( files->outputFile, "invalid" );
}
