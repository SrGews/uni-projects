/**
*FILE:          opEval.c
*AUTHOR:        Angus Campbell
*STUDENT ID:    17163042
*UNIT:          UCP 120
*LAST MOD:      25/10/2014
**/

#include "opEval.h"



/*Takes in a real number and a unary operator. It then performs the operation ** on the number and returns the number (pass by reference), and an 
** EvalErrorType. If no error occured EvalErrorType = ER_NONE, if the result ** is undefined EvalErrorType = ER_RESULT_UNDEFINED, if the unary operator is ** not valid EvalErrorType = ER_INVALID_SYMBOL
*/

EvalErrorType opEvaluator( double *result, char *operatorText )
{
    EvalErrorType retVal;

    retVal = ER_NONE;

    if( retVal == ER_NONE )
    {
        if( 0 == strncmp( operatorText,"sin\0", 8 ) )
        {
            *result = sin( *result );
        }
        else if( 0 == strncmp( operatorText,"cos\0", 8 ) )
        {
            *result = cos( *result );
        }
        else if( 0 == strncmp( operatorText,"tan\0", 8 ) )
        {
            if ( ( *result < ( PI/2+ERR_MARGIN ) ) && 
                ( *result > ( PI/2-ERR_MARGIN ) ) )
            {
                retVal = ER_RESULT_UNDEFINED;
            }
            else
            {
                *result = tan( *result );
            }
        }
        else if( 0 == strncmp( operatorText,"log\0", 8 ) )
        {
            *result = log( *result );
        }
        else if( 0 == strncmp( operatorText,"exp\0", 8 ) )
        {
            *result = exp( *result );
        }
        else if( 0 == strncmp( operatorText,"floor\0", 8 ) )
        {
            *result = floor( *result );
        }
        else if( 0 == strncmp( operatorText,"ceiling\0", 9 ) )
        {
            *result = ceil( *result );
        }
        else{
            retVal = ER_INVALID_SYMBOL;
        }
    }
    return retVal;
}
