#include "fileGrapher.h"
/*Draws a grey axis in SDL. It needs the SDL grid values for x and y axis*/
void drawAxis(SDL_Surface *screen,int x, int y){
    int i;
    Uint32 greyPixel;
    
    greyPixel = SDL_MapRGB(screen->format, 127, 127, 127);
    if((y < HEIGHT) && (y>=0)){
        for (i = 0; i<WIDTH; i++){
            ((Uint32*)screen->pixels)[WIDTH*y+i] = greyPixel;
        }
    }
    if((x<WIDTH) && (y>=0)){
        for (i = 0; i<HEIGHT; i++){
            ((Uint32*)screen->pixels)[x+i*WIDTH] = greyPixel;
        }
    }
}
/*Input actual x values to get the corresponding grid value for SDL*/
int getGridLocationX(double xmax, double xmin, double xval){
    double z;
    z = WIDTH*((xval-(xmin))/(xmax-xmin));
    return (int)z;
    
}

int getGridLocationY(double ymax, double ymin, double yval){
    double z;

            z = HEIGHT*((yval-(ymin))/(ymax-ymin));
            z = HEIGHT - z;

    return (int)z;
}

/*Does stuff*/
int main(void){
/*Things*/
    double result, xVal;
    int fileEnd, ch, i,j,failedStatus,x,y;
    char fileIn[51], fileOut[51], nextChar;
    FILE *inputFile, *outputFile;
    GraphDimensions graphDim;
    LinkedList* exprList;
    Node* currNode;
    EvalError exprError;
    OperatorEvaluator ptr;

/*SDL things*/
    Uint32 pixel;
    SDL_Surface *screen;
    SDL_Event ev;
    
/*Gets file names*/
    printf("Enter the input file and output file\n");
    scanf(" %s", fileIn);
    scanf(" %s", fileOut);

/*Reads files in*/
    inputFile = fopen(fileIn, "r");
    outputFile = fopen(fileOut, "w");
    
/*Checks files were opened successfully. If not the rest of the program doesnt run.*/

/*validate each individually*/
    if(inputFile == NULL){
        printf("Error: could not open '%s'\n", fileIn);
    }
    else if(outputFile == NULL){
        printf("Error: could not open '%s'\n", fileOut);
    }
    else{

/*reads in the first line of the file and places it into graphDim*/
        fscanf(inputFile, "%lf %lf %lf %lf\n", &graphDim.xmin, &graphDim.ymin, &graphDim.xmax, &graphDim.ymax);

/*malloc and initialising variables to make sure things work as intended*/
        exprList = (LinkedList*)malloc(sizeof(LinkedList));
        exprList->head = NULL;
        exprList->tail = NULL;
        nextChar = 'a';
        fileEnd = 0;

/*Mallocs a new node at end of linked list and reads in the colour values into the l.List. Stops when it reaches end of file*/
        do{
            newNodeAtEnd(exprList);
            i = 0;
            fscanf(inputFile,"%d %d %d", &exprList->tail->lineData.colour.red, &exprList->tail->lineData.colour.blue, &exprList->tail->lineData.colour.green);
/*Reads in the expression removing all spaces (I dont think this is necessary). Stops when it reaches new line or end of file*/
            while((nextChar!='\n') && (feof(inputFile) == 0)){
                ch = fgetc(inputFile);
                nextChar = (char)ch;

                if((nextChar!='\n') && (nextChar!=' ')){
                    exprList->tail->lineData.expression[i] = nextChar;
                    i++;
                }
            }
            ch = fgetc(inputFile);
            nextChar = (char)ch;
        }while((int)nextChar!=-1);
    
    
/*Assigns the function pointer for eval*/
    ptr= &opEvaluator;
    
/*Prints all expressions into the output file in for "expr1,expr2,expr3\n"*/
    currNode = exprList->head;
    while((currNode!=NULL)){
        fprintf(outputFile,"%s", currNode->lineData.expression);
        currNode = currNode->nextNode;
        if(currNode!=NULL){
            fprintf(outputFile,",");
        }
    }    
    fprintf(outputFile,"\n");
    /**/
    
/*SDL IS PAINFUL*/
/*REALLY PAINFUL*/

/*               up to here                 */
/*Tries to open SDL. If failedStatus = -1, SDL failed to open*/
        failedStatus = SDL_Init(SDL_INIT_VIDEO);
        if (failedStatus == -1){
            printf("ERROR: SDL failed to open\n");
        }
        else{
/*Initialises screen*/
        screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32, 0);
    
/*Gets X and Y axis locations, ie where x == 0 and y == 0, then draws both axis*/
        y = getGridLocationY(graphDim.ymax, graphDim.ymin, 0);
        x = getGridLocationX(graphDim.xmax, graphDim.xmin, 0);
        drawAxis(screen, x, y);
        
        
        
/*These nested loops draw the functions on the graph by finding what the y-value is for each pixel's x-value then setting that pixel's colour to the appropriate colour.*/

/*The for loop goes through the x-value for each SDL pixel and sets the current node pointer to the head of the list and the graph's xValue for the ith SDL pixel column and SDL's x grid location and writes the graph's y value into the file*/
        for (j=1; j<=WIDTH; j++){

            currNode = exprList->head;
            xVal = ((double)(graphDim.xmax-graphDim.xmin)*(double)j/(double)WIDTH)+graphDim.xmin;
            x = getGridLocationX(graphDim.xmax,graphDim.xmin,xVal);

/*The while loop goes through each list node, finds the y-value for each pixel by using eval and sets the corresponding pixel's colour to the appropriate colour*/
            while(currNode!=NULL){
/*Finds the graph's y-val then the corresponding SDL y value*/
                exprError = eval(&result, currNode->lineData.expression, xVal, ptr);
                y = getGridLocationY(graphDim.ymax,graphDim.ymin,result);
                
                if(y>HEIGHT){
                    y = HEIGHT;
                }
                else if(y<0){
                    y = 0;
                }
                                
/*Gets the appropriate colour value this expression and sets the pixel's colour to it.*/
                pixel = SDL_MapRGB(screen->format, (Uint8)currNode->lineData.colour.red,(Uint8)currNode->lineData.colour.green,(Uint8)currNode->lineData.colour.blue);
                ((Uint32*)screen->pixels)[WIDTH*y+x] = pixel;

                fprintf(outputFile,"%f",result);
                
/*This section fills in the blanks in the functions between y-values. In other words, if the graph would have a coloured pixel at 1,1 then 2,1 it will do nothing, but if it went from 1,1 to 2,3 it will colour in pixels 2,1 and 2,2 to make the line continuous*/
                if(x == 1){
                    (currNode->lineData.prevy) = y;
                }else{
                    while(y > (currNode->lineData.prevy)){
                        ((Uint32*)screen->pixels)[WIDTH*(currNode->lineData.prevy)+x] = pixel;
                        (currNode->lineData.prevy) ++;
                    }
                    while(y < (currNode->lineData.prevy)){
                        ((Uint32*)screen->pixels)[WIDTH*(currNode->lineData.prevy)+x] = pixel;
                        (currNode->lineData.prevy) --;
                    }
                }

                currNode = currNode->nextNode;

/*Checks if there is another expression. If so then it prints ',' to separate the y values*/
                if (currNode!=NULL){
                    fprintf(outputFile,",");
                }
            }
            fprintf(outputFile, "\n");
        }
        SDL_UpdateRect(screen, 0,0,0,0);
        
/*Keeps SDL open until it is exited by the user then closes SDL*/
        do{
            SDL_WaitEvent(&ev);
        }while(ev.type!=SDL_QUIT);
        SDL_Quit();
    }
    
/*Frees the linked list as it is no longer needed*/
    freeList(exprList);

    }
    fclose(inputFile);
    fclose(outputFile);
    return 0;
}
